﻿## 一、檔案介紹：

+ DeliveryPlatform：外送平台操作介面

- BankGUI：銀行系統

- DataBase：資料庫與外送平台邏輯

- Resources：執行程式所需jar檔

- Final\_2048：自選題

## 二、JavaFX下載：

1. 至https://gluonhq.com/products/javafx/ 下載JavaFX

    (我們使用的版本為 JavaFX Windows x64 SDK 18.0.1)

2. 在eclipse中，Window > Preferences > Java Build Path > User Libraries

3. 點New，User library name: JavaFX

4. 點JavaFX，Add External JARs，將javafx-sdk-18.0.1\lib加入，Apply and close

## 三、指定題：

環境設置：

1. 在eclipse中，File > New Project，創建JavaFX Project

2.  Use an execution environment JRE: 選擇JavaSE-17

    (此處我們使用17版的Java，8以前的版本無Classpath和Modulepath)

3. 將src中的application和module-info.java刪除

4. 將DeliveryPlatform\src的所有檔案加入此專案的src中

5. 將resources資料夾加入此專案中

    (mydao.jar為DataBase資料夾的檔案)

6. 專案右鍵  Properties > Libraries > Classpath > Add Library > User Library > JavaFX > Finish

7. Classpath > Add External JARs  加入resources全部的10個Jar檔 

    (若Modulepath有JavaFX SDK，移至Classpath)  Apply and close

8. 專案右鍵  Run As > Run Configurations > Arguments > VM arguments輸入

    `--module-path " javafx-sdk-18.0.1\lib路徑" --add-modules javafx.controls,javafx.fxml`

    (我放在C槽User\Gary裡就是--module-path "C:\Users\Gary\javafx-sdk-18.0.1\lib" --add-modules javafx.controls,javafx.fxml)

9. Apply > Run即可開始使用我們的外送平台 ><

* json檔中的29家餐廳帳號分別是 shop0 ~ shop28 ，密碼都是 0000 

* 地圖點案任意移處即可返回

## 四、自選題：

1. Final\_2048資料夾中，有2048.jar

2. Alt+D  輸入cmd  Enter

3. 輸入java VM arguments -jar 2048.jar

    (VM arguments與上方JavaFX project相同，注意前後皆有空格)

    (例：java --module-path "C:\Users\Gary\javafx-sdk-18.0.1\lib" --add-modules javafx.controls,javafx.fxml -jar 2048.jar)

4. 即可開始遊玩2048

## 銀行系統：

* 與指定題環境設置完全相同，其金鑰系統會與外送平台之線上付款連結，在執行外送平台線上付款功能前須先安裝完畢。
