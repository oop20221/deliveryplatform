package util;

import com.oct.dp.model.CustomerInfo;

public class Container {
	private static CustomerInfo customerInfo;
	
	private static String css;



	public static String getCss() {
		return css;
	}

	public static void setCss(String css) {
		Container.css = css;
	}

	public static CustomerInfo getCustomerInfo() {
		return customerInfo;
	}

	public static void setCustomerInfo(CustomerInfo customerInfo) {
		Container.customerInfo = customerInfo;
	}


	
}
