package util;

import java.util.List;

import com.oct.dp.model.CustomerInfo;
import com.oct.dp.model.TokenInfo;
import com.oct.dp.model.TokenInfo.Token_State;
import com.oct.dp.mydao.CrudDAO;
import com.oct.dp.mydao.DAOException;
import com.oct.dp.mydao.DAOFactory;
import com.oct.dp.mydao.KeyGenerator;
import com.oct.dp.mydao.UserDAO;
import com.oct.dp.mydao.condition.Conditions;
import com.oct.dp.mydao.condition.Fields;


public class Customer {

	private static DAOFactory javabase = DAOFactory.getInstance();
	private static UserDAO<CustomerInfo> customerDAO = javabase.getCustomerDAO();
	private static CrudDAO<TokenInfo> tokenDAO = javabase.getTokenDAO();

	
	public static CustomerInfo login(String account, String password) {
		return customerDAO.find(account, password);
	}
	
	public static CustomerInfo register(String account, String password, String name) throws IllegalArgumentException, DAOException {
		CustomerInfo customerInfo = new CustomerInfo();
		customerInfo.setAccount(account);
		customerInfo.setPassword(password);
		customerInfo.setUserName(name);
		customerInfo = customerDAO.create(customerInfo);
		return customerInfo;
	}
	
	public static CustomerInfo update(CustomerInfo customerInfo) throws IllegalArgumentException, DAOException {
		return customerDAO.update(customerInfo);
	}
	
	public static CustomerInfo findAccount(String account) {
		return customerDAO.find(account, Fields.CUSTOMER_ACCOUNT);
	}
	
	public static CustomerInfo refresh(Long id) {
		return customerDAO.find(id);
	}
	
	// Token --------------------------------------------------------------------------------------------
	
	public static List<TokenInfo> getTokenInfos(Long customerID){
		return tokenDAO.findAll(customerID, Fields.CUSTOMER_ID);
	}

	public static CustomerInfo regenerateToken(CustomerInfo customerInfo) {
		deleteToken(customerInfo);
		return createToken(customerInfo);
	}
	
	public static CustomerInfo createToken(CustomerInfo customerInfo) {
		customerInfo.setToken(KeyGenerator.generateRandomAlphaNumToken(16));
		return customerDAO.update(customerInfo);
		
	}
	
	public static List<TokenInfo> getVerifyingToken(Long customerID){
		return tokenDAO.findAll(new Conditions(customerID, Fields.CUSTOMER_ID)
				.and(Token_State.VERIFYING.name(), Fields.CUSTOMER_TOKEN_STATE));
	}
	
	public static void deleteToken(CustomerInfo customerInfo) {
		List<TokenInfo> tokenInfos = tokenDAO.findAll(customerInfo.getId(), Fields.CUSTOMER_ID);
		for (TokenInfo token : tokenInfos) {
			tokenDAO.delete(token);
		}
	}
	
	public static TokenInfo updateToken(TokenInfo tokenInfo) {
		return tokenDAO.update(tokenInfo);
	}
}
