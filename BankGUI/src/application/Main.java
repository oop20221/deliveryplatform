package application;
	
import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import util.Container;

/**
 * THis is the main class of the project BankGUI,
 * it can start the application
 * @author USER
 *
 */
public class Main extends Application {
	
	@FXML
	private Button logoutButton;
	@FXML
	private AnchorPane menuPane;
	/**
	 * The start method start the application and open the "Main" page.
	 */
	@Override
	public void start(Stage stage) {
		try {
			Parent root = FXMLLoader.load(getClass().getResource("/fxml/Main.fxml"));
			Scene scene = new Scene(root);
			//scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			Container.setCss(this.getClass().getResource("application.css").toExternalForm());
			Image icon = new Image("NTU_LOGO.jpg");
			stage.setResizable(false);
			stage.getIcons().add(icon);
			stage.setTitle("Bank System Program");
			scene.getStylesheets().add(Container.getCss());
			stage.setScene(scene);
			stage.show();
			
			stage.setOnCloseRequest(event ->{
				event.consume();
				try {
					logout(stage);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			});
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * To alert the user when they click on the cross button.
	 * To close the window
	 * @param stage
	 * @throws IOException
	 */
	public void logout(Stage stage) throws IOException {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Exit");
		alert.setHeaderText("You are about to exit");
		alert.setContentText("Are you sure to exit?");
		if(alert.showAndWait().get() == ButtonType.OK) {
			stage.close();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
