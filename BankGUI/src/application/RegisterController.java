package application;

import java.io.IOException;

import com.oct.dp.mydao.DAOException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import util.Container;
import util.Customer;
/**
 * The RegisterController will be used when the "Register" page is shown.
 * It can switch the window back to "Menu",and to register an account.
 * @author USER
 * 
 */
public class RegisterController {
	
	@FXML
	TextField accountTextField;
	@FXML
	TextField passwordTextField;
	@FXML
	TextField nameTextField;
	
	private Stage stage;
	private Scene scene;
	private Parent root;
	/**
	 * When an user click on the button "Back To Menu",
	 * this method will work and change the window to the "Menu" page.
	 * @param event
	 * @throws IOException
	 */
	public void switchToMenu(ActionEvent event) throws IOException {
		root = FXMLLoader.load(getClass().getResource("/fxml/Menu.fxml"));
		stage = (Stage)((Node)event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		scene.getStylesheets().add(Container.getCss());

		stage.setScene(scene);
		stage.show();
	}	
	/**
	 * When an user click on the button "Back To Menu",
	 * this method will work and change the window to the "Main" page.
	 * @param event
	 * @throws IOException
	 */
	public void switchToMain(ActionEvent event) throws IOException {
		root = FXMLLoader.load(getClass().getResource("/fxml/Main.fxml"));
		stage = (Stage)((Node)event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		scene.getStylesheets().add(Container.getCss());

		stage.setScene(scene);
		stage.show();
	}
	/**
	 * When an user click on the button "Register",
	 * this method will work and change the window to the "Main" page
	 * if the operation is successful
	 * @param event
	 * @throws IOException
	 */
	public void register(ActionEvent event) throws IOException {
		
		String account = accountTextField.getText();
		String password = passwordTextField.getText();
		String name = nameTextField.getText();

		boolean invalid = false;
		if(name.equals("")) {
			nameTextField.setText("Invalid Name");
			nameTextField.setStyle("-fx-text-inner-color: red;");
			invalid = true;
		}
		if (account.equals("")) {
			accountTextField.setText("Invalid Account");
			accountTextField.setStyle("-fx-text-inner-color: red;");
			invalid = true;
		}
		if(password.equals("")) {
			passwordTextField.setText("Invalid password");
			passwordTextField.setStyle("-fx-text-inner-color: red;");
			invalid = true;
		}
		if (!invalid) {
			try {
				Container.setCustomerInfo(Customer.register(account, password, name));
				root = FXMLLoader.load(getClass().getResource("/fxml/Main.fxml"));
				stage = (Stage)((Node)event.getSource()).getScene().getWindow();
				scene = new Scene(root);
				scene.getStylesheets().add(Container.getCss());

				stage.setScene(scene);
				stage.show();
			} catch (IllegalArgumentException | DAOException e) {
				accountTextField.setText("Account already exists");
				accountTextField.setStyle("-fx-text-inner-color: red;");
			}

		}
	}
}
