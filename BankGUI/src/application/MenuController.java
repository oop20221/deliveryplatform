package application;

import java.io.IOException;

import com.oct.dp.model.CustomerInfo;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import util.Container;
/**
 * 
 * The MenunController will be used when the "Menu" page is shown.
 * It can switch the window back to "Deposit", "Withdraw", "Transfer", "Balance", "Token", "Confirm".
 * @author USER
 *
 */
public class MenuController {
	private Stage stage;
	private Scene scene;
	private Parent root;
	
	@FXML
	private Button logoutButton;
	@FXML
	private AnchorPane menuPane;
	@FXML
	Label nameLabel;
	
	CustomerInfo customerInfo = Container.getCustomerInfo();
	/**
	 * To display the name when entering the Menu page
	 */
	public void displayName() {
		nameLabel.setText("Hello! "+ customerInfo.getUserName());
	}
	/**
	 * When the user press on the log out button , to log out the system
	 * @param event
	 * @throws IOException
	 */
	public void logout(ActionEvent event) throws IOException {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Log Out");
		alert.setHeaderText("You are about to log out");
		alert.setContentText("Are you sure to log out?");
		if(alert.showAndWait().get() == ButtonType.OK) {
			stage = (Stage)menuPane.getScene().getWindow(); 
			root = FXMLLoader.load(getClass().getResource("/fxml/Main.fxml"));
			scene = new Scene(root);
			scene.getStylesheets().add(Container.getCss());

			stage.setScene(scene);
			stage.show();
		}
	}
	/**
	 * When an user click on the button "Back To Menu",
	 * this method will work and change the window to the "Menu" page.
	 * @param event
	 * @throws IOException
	 */
	
	public void switchToMain(ActionEvent event) throws IOException {
		
		root = FXMLLoader.load(getClass().getResource("/fxml/Main.fxml"));
		stage = (Stage)((Node)event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		scene.getStylesheets().add(Container.getCss());

		stage.setScene(scene);
		
		stage.show();
	}
	/**
	 * When an user click on the button "Deposit",
	 * this method will work and change the window to the "Deposit" page.
	 * @param event
	 * @throws IOException
	 */
	public void switchToDeposit(ActionEvent event) throws IOException {
		root = FXMLLoader.load(getClass().getResource("/fxml/Deposit.fxml"));
		stage = (Stage)((Node)event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		scene.getStylesheets().add(Container.getCss());

		stage.setScene(scene);
		stage.show();	
	}
	/**
	 * When an user click on the button "Withdraw",
	 * this method will work and change the window to the "Withdraw" page.
	 * @param event
	 * @throws IOException
	 */
	public void switchToWithdraw(ActionEvent event) throws IOException {
		root = FXMLLoader.load(getClass().getResource("/fxml/Withdraw.fxml"));
		stage = (Stage)((Node)event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		scene.getStylesheets().add(Container.getCss());

		stage.setScene(scene);
		stage.show();
	}
	/**
	 * When an user click on the button "Transfer",
	 * this method will work and change the window to the "Transfer" page.
	 * @param event
	 * @throws IOException
	 */
	public void switchToTransfer(ActionEvent event) throws IOException {
		root = FXMLLoader.load(getClass().getResource("/fxml/Transfer.fxml"));
		stage = (Stage)((Node)event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		scene.getStylesheets().add(Container.getCss());

		stage.setScene(scene);
		stage.show();
	}
	/**
	 * When an user click on the button "Balance",
	 * this method will work and change the window to the "Balance" page.
	 * @param event
	 * @throws IOException
	 */
	public void switchToBalance(ActionEvent event) throws IOException {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/Balance.fxml"));
		root = loader.load();
		BalanceController balanceController = loader.getController();
		balanceController.displayBalance();
		
		stage = (Stage)((Node)event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		scene.getStylesheets().add(Container.getCss());

		stage.setScene(scene);
		stage.show();
	}
	/**
	 * When an user click on the button "Token",
	 * this method will work and change the window to the "Token" page.
	 * @param event
	 * @throws IOException
	 */
	public void switchToToken(ActionEvent event) throws IOException {
		root = FXMLLoader.load(getClass().getResource("/fxml/Token.fxml"));
		stage = (Stage)((Node)event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		scene.getStylesheets().add(Container.getCss());
		stage.setScene(scene);
		stage.show();
	}
	/**
	 * When an user click on the button "Confirm",
	 * this method will work and change the window to the "Confirm" page.
	 * @param event
	 * @throws IOException
	 */
	public void switchToAccountConfirm(ActionEvent event) throws IOException {
		root = FXMLLoader.load(getClass().getResource("/fxml/AccountConfirm.fxml"));
		stage = (Stage)((Node)event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		scene.getStylesheets().add(Container.getCss());

		stage.setScene(scene);
		stage.show();	
	}
}
