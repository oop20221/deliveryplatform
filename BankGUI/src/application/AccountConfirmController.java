package application;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import com.oct.dp.model.TokenInfo;
import com.oct.dp.model.TokenInfo.Token_State;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import util.Container;
import util.Customer;
/**
 * The AccountConfirmController will be used when the "AccountConfirm" page is shown.
 * It can switch the window back to "Menu"
 * @author USER
 * 
 */
public class AccountConfirmController implements Initializable {
	private Stage stage;
	private Scene scene;
	private Parent root;
	
	
	@FXML
	private ListView<String> accountList;
	
	private static List<TokenInfo> verifyingTokens;
	private static int choosenIndex;
	/**
	 * To initialize
	 */
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		accountList.setOnMouseClicked(new EventHandler<MouseEvent>() {
			public void handle(MouseEvent arg0) {
				choosenIndex = accountList.getSelectionModel().getSelectedIndex();			
			}
		});		
	}
	/**
	 * When an user click on the button "Back To Menu",
	 * this method will work and change the window to the "Menu" page.
	 * @param event
	 * @throws IOException
	 */
	public void switchToMenu(ActionEvent event) throws IOException {
		root = FXMLLoader.load(getClass().getResource("/fxml/Menu.fxml"));
		stage = (Stage)((Node)event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		scene.getStylesheets().add(Container.getCss());

		stage.setScene(scene);
		stage.show();
	}
	
	public void deleteAccount(){	
	}
	/**
	 * When an user click on the button "Confirm",
	 * this method will work and connect hte account with the delivary platform
	 * @param event
	 * @throws IOException
	 */
	public void confirmAccount() {
		TokenInfo choosenToken = verifyingTokens.get(choosenIndex);
		if (choosenToken == null) return;
		choosenToken.setState(Token_State.ACTIVATED);
		Customer.updateToken(choosenToken);
		verifyingTokens.remove(choosenIndex);
	}
	/**
	 * When an user click on the button "Refresh",
	 * this method will work and refresh the window
	 * @param event
	 * @throws IOException
	 */
	public void refreshList() {
		accountList.getItems().clear();
		verifyingTokens = Customer.getVerifyingToken(Container.getCustomerInfo().getId());
		System.out.println(verifyingTokens.isEmpty());
		System.out.println(verifyingTokens.size());
		if (verifyingTokens.isEmpty()) {
			accountList.getItems().add("還沒有與此金鑰連結的帳戶");
			return;
		}
		
		for (TokenInfo tokenInfo : verifyingTokens) {
			accountList.getItems().add(tokenInfo.getAccount_info());
		}
	}
}
