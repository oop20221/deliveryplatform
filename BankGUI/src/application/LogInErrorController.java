package application;
import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import util.Container;
/**
 * The LogInErrorController will be used when the "LogInError" page is shown.
 * It can switch the window back to "Main", or "LogIn".
 * @author USER
 * 
 */
public class LogInErrorController {
	private Stage stage;
	private Scene scene;
	private Parent root;

	/**
	 * When an user click on the button "Try again",
	 * this method will work and change the window to the "LogIn" page.
	 * @param event
	 * @throws IOException
	 */
	public void switchToLogIn(ActionEvent event) throws IOException {
		root = FXMLLoader.load(getClass().getResource("/fxml/LogIn.fxml"));
		stage = (Stage)((Node)event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		scene.getStylesheets().add(Container.getCss());

		stage.setScene(scene);
		stage.show();
	}
	/**
	 * When an user click on the button "Register now",
	 * this method will work and change the window to the "Register" page.
	 * @param event
	 * @throws IOException
	 */
	public void switchToRegister(ActionEvent event) throws IOException {
		root = FXMLLoader.load(getClass().getResource("/fxml/Register.fxml"));
		stage = (Stage)((Node)event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		scene.getStylesheets().add(Container.getCss());

		stage.setScene(scene);
		stage.show();
	}
}
