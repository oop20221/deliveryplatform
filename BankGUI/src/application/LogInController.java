package application;

import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import util.Container;
import util.Customer;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
/**
 * The LogInController will be used when the "LogIn" page is shown.
 * It can switch the window to "Menu", or "LogInError".
 * @author USER
 * 
 */
public class LogInController {
	
	@FXML
	TextField accountTextField;
	@FXML
	TextField passwordTextField;
	//String account;
	
	
	private Stage stage;
	private Scene scene;
	private Parent root;
	/**
	 * When an user click on the button "Go Back",
	 * this method will work and change the window to the "Main" page.
	 * @param event
	 * @throws IOException
	 */
	public void switchToMain(ActionEvent event) throws IOException {
		root = FXMLLoader.load(getClass().getResource("/fxml/Main.fxml"));
		stage = (Stage)((Node)event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		scene.getStylesheets().add(Container.getCss());

		stage.setScene(scene);
		stage.show();
	}
	/**
	 * When an user click on the button "Back To Menu",
	 * this method will work and change the window to the "Menu" page.
	 * @param event
	 * @throws IOException
	 */
	public void switchToMenu(ActionEvent event) throws IOException {
		root = FXMLLoader.load(getClass().getResource("/fxml/Menu.fxml"));
		stage = (Stage)((Node)event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		scene.getStylesheets().add(Container.getCss());

		stage.setScene(scene);
		stage.show();
	}
	/**
	 * When an user click on the button "Log In",
	 * this method will work and change the window to the "Menu" or "LogInError" page
	 * depends on the operation is successful or not
	 * @param event
	 * @throws IOException
	 */
	public void logIn(ActionEvent event) throws IOException {
		String account = accountTextField.getText();
		String password = passwordTextField.getText();
		Container.setCustomerInfo(Customer.login(account, password));
		
		if (Container.getCustomerInfo() == null) {
			root = FXMLLoader.load(getClass().getResource("/fxml/LogInError.fxml"));
			stage = (Stage)((Node)event.getSource()).getScene().getWindow();
			scene = new Scene(root);
			scene.getStylesheets().add(Container.getCss());

			stage.setScene(scene);
			stage.show();
		}
		else {
			
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/Menu.fxml"));
			root = loader.load();
			MenuController menuController = loader.getController();
			menuController.displayName();
			
			
			
			stage = (Stage)((Node)event.getSource()).getScene().getWindow();
			scene = new Scene(root);
			scene.getStylesheets().add(Container.getCss());

			stage.setScene(scene);
			stage.show();
		}
	}

	public void loginByEnter(KeyEvent event) throws IOException {
		if (event.getCode() == KeyCode.ENTER) {
			String account = accountTextField.getText();
			String password = passwordTextField.getText();
			Container.setCustomerInfo(Customer.login(account, password));			
			if (Container.getCustomerInfo() == null) {
				root = FXMLLoader.load(getClass().getResource("/fxml/LogInError.fxml"));
				stage = (Stage)((Node)event.getSource()).getScene().getWindow();
				scene = new Scene(root);
				scene.getStylesheets().add(Container.getCss());
				stage.setScene(scene);
				stage.show();
			}
			else {			
				FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/Menu.fxml"));
				root = loader.load();
				MenuController menuController = loader.getController();
				menuController.displayName();
				stage = (Stage)((Node)event.getSource()).getScene().getWindow();
				scene = new Scene(root);
				scene.getStylesheets().add(Container.getCss());
				stage.setScene(scene);
				stage.show();
			}
		}
	}
}
