package application;

import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import util.Container;
/**
 * The AnnouncementController will be used when the "Announcement" page is shown.
 * It can switch the window to either "Menu" or "Balance".
 * @author USER
 *
 */
public class AnnouncementController {
	private Stage stage;
	private Scene scene;
	private Parent root;
	/**
	 * When an user click on the button "Back To Menu",
	 * this method will work and change the window to the "Menu" page.
	 * @param event
	 * @throws IOException
	 */
	public void switchToMenu(ActionEvent event) throws IOException {
		
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/Menu.fxml"));
		root = loader.load();
		MenuController menuController = loader.getController();
		menuController.displayName();	
		stage = (Stage)((Node)event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		scene.getStylesheets().add(Container.getCss());

		stage.setScene(scene);
		stage.show();
	}
	/**
	 * When an user click on the button "Check Balance",
	 * this method will work and change the window to the "Balance" page.
	 * @param event
	 * @throws IOException
	 */
	public void switchToBalance(ActionEvent event) throws IOException {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/Balance.fxml"));
		root = loader.load();
		BalanceController balanceController = loader.getController();
		balanceController.displayBalance();
		stage = (Stage)((Node)event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		scene.getStylesheets().add(Container.getCss());

		stage.setScene(scene);
		stage.show();
	}

}
