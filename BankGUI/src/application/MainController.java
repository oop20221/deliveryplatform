package application;

import java.io.IOException;


import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene; 
import javafx.scene.control.Button;
import javafx.stage.Stage;
import util.Container;
/**
 * 
 * The MainController will be used when the "Main" page is shown.
 * It can switch the window back to "LogIn", "Register" or "Help".
 * @author USER
 *
 */
public class MainController {

	@FXML
	Button registerButton;
	@FXML
	Button logInButton;
	
	
	private Stage stage;
	private Scene scene;
	private Parent root;
	/**
	 * When an user click on the button "Back To Menu",
	 * this method will work and change the window to the "Menu" page.
	 * @param event
	 * @throws IOException
	 */

	public void switchToMain(ActionEvent event) throws IOException {
		root = FXMLLoader.load(getClass().getResource("/Main.fxml"));
		stage = (Stage)((Node)event.getSource()).getScene().getWindow();
		scene = new Scene(root);

		stage.setScene(scene);
		stage.show();
	}
	/**
	 * When an user click on the button "Log In",
	 * this method will work and change the window to the "Log In" page.
	 * @param event
	 * @throws IOException
	 */
	public void switchToLogIn(ActionEvent event) throws IOException {
		root = FXMLLoader.load(getClass().getResource("/fxml/LogIn.fxml"));
		stage = (Stage)((Node)event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		scene.getStylesheets().add(Container.getCss());
		stage.setScene(scene);
		stage.show();
	}
	/**
	 * When an user click on the button "Register",
	 * this method will work and change the window to the "Register" page.
	 * @param event
	 * @throws IOException
	 */
	public void switchToRegister(ActionEvent event) throws IOException {
		root = FXMLLoader.load(getClass().getResource("/fxml/Register.fxml"));
		stage = (Stage)((Node)event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		scene.getStylesheets().add(Container.getCss());

		stage.setScene(scene);
		stage.show();
	}
	/**
	 * When an user click on the button "Help",
	 * this method will work and change the window to the "Help" page.
	 * @param event
	 * @throws IOException
	 */
	public void switchToHelp(ActionEvent event) throws IOException {
		root = FXMLLoader.load(getClass().getResource("/fxml/Help.fxml"));
		stage = (Stage)((Node)event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		scene.getStylesheets().add(Container.getCss());

		stage.setScene(scene);
		stage.show();
	}
	
	
	
}
