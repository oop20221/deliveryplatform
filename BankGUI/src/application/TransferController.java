package application;

import java.io.IOException;

import com.oct.dp.model.CustomerInfo;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import util.Container;
import util.Customer;
/**
 * The TransferController will be used when the "Transfer" page is shown.
 * It can switch the window back to "Menu","Announcement" or "TransferError".
 * @author USER
 * 
 */
public class TransferController {
	private Stage stage;
	private Scene scene;
	private Parent root;
	@FXML
	TextField accountTextField;
	@FXML
	TextField transferTextField;
	/**
	 * When an user click on the button "Back To Menu",
	 * this method will work and change the window to the "Menu" page.
	 * @param event
	 * @throws IOException
	 */
	public void switchToMenu(ActionEvent event) throws IOException {
		root = FXMLLoader.load(getClass().getResource("/fxml/Menu.fxml"));
		stage = (Stage)((Node)event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		scene.getStylesheets().add(Container.getCss());

		stage.setScene(scene);
		stage.show();
	}
	/**
	 * When an user click on the button "Confirm",
	 * this method will work and change the window to
	 * the "Announcement" page or the "TransferError" page depends on 
	 * the operation is successful or not.
	 * @param event
	 * @throws IOException
	 */
	public void switchToAnnouncement(ActionEvent event) throws IOException {
		
		String account = accountTextField.getText();
		String amount = transferTextField.getText();


		try {
			Double amountindouble = Double.parseDouble(amount);
			if (amountindouble <= 0) {
				root = FXMLLoader.load(getClass().getResource("/fxml/TransferError.fxml"));
				stage = (Stage)((Node)event.getSource()).getScene().getWindow();
				scene = new Scene(root);
				scene.getStylesheets().add(Container.getCss());

				stage.setScene(scene);
				stage.show();
				return;
			}
			
			CustomerInfo customerInfo = Container.getCustomerInfo();
			CustomerInfo customerInfoReceiver = Customer.findAccount(account);

			if (amountindouble > customerInfo.getBalance() ) {
				root = FXMLLoader.load(getClass().getResource("/fxml/TransferError.fxml"));
				stage = (Stage)((Node)event.getSource()).getScene().getWindow();
				scene = new Scene(root);
				scene.getStylesheets().add(Container.getCss());

				stage.setScene(scene);
				stage.show();
			}

			else if( customerInfoReceiver == null) {
				root = FXMLLoader.load(getClass().getResource("/fxml/TransferError.fxml"));
				stage = (Stage)((Node)event.getSource()).getScene().getWindow();
				scene = new Scene(root);
				scene.getStylesheets().add(Container.getCss());

				stage.setScene(scene);
				stage.show();
			}
			else {						
				
				customerInfoReceiver.setBalance(customerInfoReceiver.getBalance() + amountindouble);
				customerInfo.setBalance(customerInfo.getBalance() - amountindouble);

				Customer.update(customerInfo);
				Customer.update(customerInfoReceiver);

				
				Container.setCustomerInfo(customerInfo);
				
				root = FXMLLoader.load(getClass().getResource("/fxml/Announcement.fxml"));
				stage = (Stage)((Node)event.getSource()).getScene().getWindow();
				scene = new Scene(root);
				scene.getStylesheets().add(Container.getCss());

				stage.setScene(scene);
				stage.show();
			}
		} catch (NullPointerException|NumberFormatException e) {
			root = FXMLLoader.load(getClass().getResource("/fxml/TransferError.fxml"));
			stage = (Stage)((Node)event.getSource()).getScene().getWindow();
			scene = new Scene(root);
			scene.getStylesheets().add(Container.getCss());

			stage.setScene(scene);
			stage.show();
			}

	
	}
}
