package application;

import java.io.IOException;

import com.oct.dp.model.CustomerInfo;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import util.Container;
import util.Customer;
/**
 * The WithdrawController will be used when the "Withdraw" page is shown.
 * It can switch the window back to either "Menu", "Announcement" or "WithdrawError".
 * @author USER
 * 
 */
public class WithdrawController {
	private Stage stage;
	private Scene scene;
	private Parent root;
	
	@FXML
	TextField withdrawTextField;

	/**
	 * When an user click on the button "Back To Menu",
	 * this method will work and change the window to the "Menu" page.
	 * @param event
	 * @throws IOException
	 */
	public void switchToMenu(ActionEvent event) throws IOException {
		root = FXMLLoader.load(getClass().getResource("/fxml/Menu.fxml"));
		stage = (Stage)((Node)event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		scene.getStylesheets().add(Container.getCss());

		stage.setScene(scene);
		stage.show();
	}
	/**
	 * When an user click on the button "Confirm",
	 * this method will work and change the window to
	 * the "Announcement" page or the "WithdrawError" page depends on 
	 * the operation is successful or not.
	 * @param event
	 * @throws IOException
	 */
	public void switchToAnnouncement(ActionEvent event) throws IOException {
		
		String amount = withdrawTextField.getText();


		try {
			Double amountindouble = Double.parseDouble(amount);
			if (amountindouble <= 0) {
				root = FXMLLoader.load(getClass().getResource("/fxml/WithdrawError.fxml"));
				stage = (Stage)((Node)event.getSource()).getScene().getWindow();
				scene = new Scene(root);
				scene.getStylesheets().add(Container.getCss());

				stage.setScene(scene);
				stage.show();
				return;
			}
			CustomerInfo customerInfo = Container.getCustomerInfo();

			if (amountindouble > customerInfo.getBalance()) {
				root = FXMLLoader.load(getClass().getResource("/fxml/WithdrawError.fxml"));
				stage = (Stage)((Node)event.getSource()).getScene().getWindow();
				scene = new Scene(root);
				scene.getStylesheets().add(Container.getCss());

				stage.setScene(scene);
				stage.show();
			}
			else {
				
				customerInfo.setBalance(customerInfo.getBalance() - amountindouble);
				Customer.update(customerInfo);
				
				Container.setCustomerInfo(customerInfo);
				
				root = FXMLLoader.load(getClass().getResource("/fxml/Announcement.fxml"));
				stage = (Stage)((Node)event.getSource()).getScene().getWindow();
				scene = new Scene(root);
				scene.getStylesheets().add(Container.getCss());

				stage.setScene(scene);
				stage.show();
			}
		} catch (NullPointerException|NumberFormatException e) {
			root = FXMLLoader.load(getClass().getResource("/fxml/WithdrawError.fxml"));
			stage = (Stage)((Node)event.getSource()).getScene().getWindow();
			scene = new Scene(root);
			scene.getStylesheets().add(Container.getCss());

			stage.setScene(scene);
			stage.show();		}

	
	}
}
