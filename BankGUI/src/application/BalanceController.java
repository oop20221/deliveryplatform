package application;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import util.Container;
import util.Customer;

/**
 * The BalanceController will be used when the "Balance" page is shown.
 * It can switch the window back to "Menu".
 * @author USER
 *
 */
public class BalanceController implements Initializable {
	private Stage stage;
	private Scene scene;
	private Parent root;
	
	@FXML
	Label balanceLabel;
	
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		Container.setCustomerInfo(Customer.refresh(Container.getCustomerInfo().getId()));
		
	}
	
	/**
	 * This method can set the text to show the balance.
	 */
	public void displayBalance() {
		balanceLabel.setText("Your balance: "+ Container.getCustomerInfo().getBalance());
	}
	/**
	 * When an user click on the button "Go Back",
	 * this method will work and change the window to the "Menu" page.
	 * @param event
	 * @throws IOException
	 */
	public void switchToMenu(ActionEvent event) throws IOException {
		root = FXMLLoader.load(getClass().getResource("/fxml/Menu.fxml"));
		stage = (Stage)((Node)event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		scene.getStylesheets().add(Container.getCss());

		stage.setScene(scene);
		stage.show();
	}

}
