package application;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.stage.Stage;
import util.Container;
import util.Customer;
/**
 * The TokenController will be used when the "Token" page is shown.
 * It can switch the window back to "Menu" or get the user's unique token
 * @author USER
 * 
 */
public class TokenController implements Initializable {
	private Stage stage;
	private Scene scene;
	private Parent root;

	
	@FXML
	private TextField tokenField;
	/**
	 * When an user click on the button "Back To Menu",
	 * this method will work and change the window to the "Menu" page.
	 * @param event
	 * @throws IOException
	 */
	public void switchToMenu(ActionEvent event) throws IOException {
		root = FXMLLoader.load(getClass().getResource("/fxml/Menu.fxml"));
		stage = (Stage)((Node)event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		scene.getStylesheets().add(Container.getCss());

		stage.setScene(scene);
		stage.show();
	}
	/**
	 * When an user click on the button "Regenerate",
	 * this method will work and regenerate a token and display it on the screen
	 * @param event
	 * @throws IOException
	 */
	public void regenerateToken(){
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("重新產生金鑰");
		alert.setHeaderText("確定要重新產生金鑰嗎?");
		alert.setContentText("重新產生金鑰後，所有與此金鑰連結的服務都將失效!");
		if(alert.showAndWait().get() == ButtonType.OK) {
			Container.setCustomerInfo(Customer.regenerateToken(Container.getCustomerInfo()));
			tokenField.setText(Container.getCustomerInfo().getToken());
		}	
	}
	/**
	 * To initalize
	 * @param event
	 * @throws IOException
	 */
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		if(Container.getCustomerInfo().getToken() == null) {
			Container.setCustomerInfo(Customer.createToken(Container.getCustomerInfo()));
			tokenField.setText(Container.getCustomerInfo().getToken());
		}
		else {
			tokenField.setText(Container.getCustomerInfo().getToken());
		}
	}
	/**
	 *TO copy the content
	 * @param event
	 * @throws IOException
	 */
	public void copyToClipBoard() {
			final Clipboard clipboard = Clipboard.getSystemClipboard();
			final ClipboardContent content = new ClipboardContent();
			content.putString(tokenField.getText());
			clipboard.setContent(content);
	}

}
