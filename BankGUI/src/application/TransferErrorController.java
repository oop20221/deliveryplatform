package application;

import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import util.Container;

public class TransferErrorController {
	private Stage stage;
	private Scene scene;
	private Parent root;
	
	public void switchToMenu(ActionEvent event) throws IOException {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/Menu.fxml"));
		root = loader.load();
		MenuController menuController = loader.getController();
		menuController.displayName();
		stage = (Stage)((Node)event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		scene.getStylesheets().add(Container.getCss());

		stage.setScene(scene);
		stage.show();
	}

	public void switchToTransfer(ActionEvent event) throws IOException {
		root = FXMLLoader.load(getClass().getResource("/fxml/Transfer.fxml"));
		stage = (Stage)((Node)event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		scene.getStylesheets().add(Container.getCss());

		stage.setScene(scene);
		stage.show();
	}
}
