package application;

import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import util.Container;
/**
 * The HelpController will be used when the "Help" page is shown.
 * It can switch the window back to "Main".
 * @author USER
 * 
 */
public class HelpController {
	private Stage stage;
	private Scene scene;
	private Parent root;
	/**
	 * When an user click on the button "Go back",
	 * this method will work and change the window to the "Main" page.
	 * @param event
	 * @throws IOException
	 */
	public void switchToMain(ActionEvent event) throws IOException {
		root = FXMLLoader.load(getClass().getResource("/fxml/Main.fxml"));
		stage = (Stage)((Node)event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		scene.getStylesheets().add(Container.getCss());

		stage.setScene(scene);
		stage.show();
	}

}
