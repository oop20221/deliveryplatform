package api;

import com.oct.dp.model.CustomerInfo;
import com.oct.dp.model.TokenInfo;
import com.oct.dp.mydao.CrudDAO;
import com.oct.dp.mydao.DAOFactory;
import com.oct.dp.mydao.KeyGenerator;
import com.oct.dp.mydao.UserDAO;
import com.oct.dp.mydao.condition.Conditions;
import com.oct.dp.mydao.condition.Fields;

public class BankAPI {

	private static DAOFactory factory = DAOFactory.getInstance();
	private static CrudDAO<TokenInfo> tokenDAO = factory.getTokenDAO();
	private static UserDAO<CustomerInfo> customerDAO = factory.getCustomerDAO();
	
	
	public static String verifyToken(String token, String account_info) {
		CustomerInfo customerInfo = customerDAO.find(token, Fields.CUSTOMER_TOKEN);
		if (customerInfo != null) {
			TokenInfo newToken = new TokenInfo();
			newToken.setCustomer_id(customerInfo.getId());
			newToken.setToken(token);
			newToken.setAccount_info(account_info);
			String key = KeyGenerator.generateRandomAlphaNumToken(16);
			newToken.setKey(key);
			tokenDAO.create(newToken);
			return key;
		}
		return null;
	}
	
	/**
	 * Pays bills from customer's account.
	 * @param token	- token shown on customer's bank account's token page.
	 * @param key	- auto generated key.
	 * @param bills	- Amount to be paid.
	 * @return	balance remaining after this payment.
	 * @throws TokenExpiredException		If token or key not found. May cause by token expiration.
	 * @throws BalanceNotEnoughException	If balance not enough to pay the bill.
	 * @throws IllegalArgumentException		if bills is negative or zero.
	 */
	public static double payBill(String token, String key, double bills) 
			throws TokenExpiredException, BalanceNotEnoughException, IllegalArgumentException 
	{
		if (bills <= 0) 
			throw new IllegalArgumentException("Illegal bill input. Bill must be greater than 0.");

		CustomerInfo customerInfo = customerDAO.find(token, Fields.CUSTOMER_TOKEN);
		
		if (customerInfo == null) 
			throw new TokenExpiredException("Token not found.");
		
		TokenInfo tokenInfo = tokenDAO.find(new Conditions(customerInfo.getId(), Fields.CUSTOMER_ID)
											.and(key, Fields.CUSTOMER_KEY));
		if (tokenInfo == null) 
			throw new TokenExpiredException("Key not found.");

		double balance = customerInfo.getBalance();
		
		if (balance < bills) 
			throw new BalanceNotEnoughException("Balance not enough.");

		balance -= bills;
		customerInfo.setBalance(balance);		
		customerDAO.update(customerInfo);
		return balance;		
	}
	
	
}
