package api;

public class BalanceNotEnoughException extends Exception {

private static final long serialVersionUID = 1L;
	
	public BalanceNotEnoughException() {
		super();
	}

    /**
     * Constructs a BalanceNotEnoughException with the given detail message.
     * @param message The detail message of the BalanceNotEnoughException.
     */
    public BalanceNotEnoughException(String message) {
        super(message);
    }

    /**
     * Constructs a BalanceNotEnoughException with the given root cause.
     * @param cause The root cause of the BalanceNotEnoughException.
     */
    public BalanceNotEnoughException(Throwable cause) {
        super(cause);
    }

    /**
     * Constructs a BalanceNotEnoughException with the given detail message and root cause.
     * @param message The detail message of the BalanceNotEnoughException.
     * @param cause The root cause of the BalanceNotEnoughException.
     */
    public BalanceNotEnoughException(String message, Throwable cause) {
        super(message, cause);
    }
}
