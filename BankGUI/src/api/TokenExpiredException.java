package api;

public class TokenExpiredException extends Exception {


	private static final long serialVersionUID = 1L;
	
	public TokenExpiredException() {
		super();
	}

    /**
     * Constructs a TokenExpiredException with the given detail message.
     * @param message The detail message of the TokenExpiredException.
     */
    public TokenExpiredException(String message) {
        super(message);
    }

    /**
     * Constructs a TokenExpiredException with the given root cause.
     * @param cause The root cause of the TokenExpiredException.
     */
    public TokenExpiredException(Throwable cause) {
        super(cause);
    }

    /**
     * Constructs a TokenExpiredException with the given detail message and root cause.
     * @param message The detail message of the TokenExpiredException.
     * @param cause The root cause of the TokenExpiredException.
     */
    public TokenExpiredException(String message, Throwable cause) {
        super(message, cause);
    }
}
