package container;

import java.util.ArrayList;
import java.util.List;

import com.oct.dp.model.DeliveryInfo;
import com.oct.dp.model.FoodInfo;
import com.oct.dp.model.MemberInfo;
import com.oct.dp.model.ShopInfo;

public class Container {
	
	private static List<FoodInfo> foodInfos;
	
	private static List<ShopInfo> shopInfos;
	
	private static List<ShopInfo> dscShopInfos;
	
	private static List<ShopInfo> typeShopInfos;
	
	private static List<FoodInfo> deleteFoods;
	
	private static ShopInfo shopInfo;
	
	private static MemberInfo memberInfo;
	
	private static DeliveryInfo deliveryInfo;
	

	public static int getFoodSize() {
		if (foodInfos == null)
			return 0;
		else {
			return foodInfos.size();
		}
	}

	public static List<FoodInfo> getFoodInfos() {
		return foodInfos;
	}


	public static void setFoodInfos(List<FoodInfo> foodInfos) {
		Container.foodInfos = foodInfos;
	}
	
	public static FoodInfo getFoodInfo(int index) {
		if(index < foodInfos.size()) {
			return foodInfos.get(index);
		}
		return null;
	}

	public static ShopInfo getShopInfo() {
		return shopInfo;
	}

	public static void setShopInfo(ShopInfo shopInfo) {
		Container.shopInfo = shopInfo;
	}

	public static MemberInfo getMemberInfo() {
		return memberInfo;
	}

	public static void setMemberInfo(MemberInfo memberInfo) {
		Container.memberInfo = memberInfo;
	}

	public static DeliveryInfo getDeliveryInfo() {
		return deliveryInfo;
	}

	public static void setDeliveryInfo(DeliveryInfo deliveryInfo) {
		Container.deliveryInfo = deliveryInfo;
	}

	public static List<ShopInfo> getShopInfos() {
		return shopInfos;
	}

	public static void setShopInfos(List<ShopInfo> shopInfos) {
		Container.shopInfos = shopInfos;
	}

	public static List<ShopInfo> getDscShopInfos() {
		return dscShopInfos;
	}

	public static void setDscShopInfos(List<ShopInfo> dscShopInfos) {
		Container.dscShopInfos = dscShopInfos;
	}
	
	
	public static List<ShopInfo> getTypeShopInfos() {
		return typeShopInfos;
	}

	public static void setTypeShopInfos(List<ShopInfo> typeShopInfos) {
		Container.typeShopInfos = typeShopInfos;
	}

	public static void addFood(FoodInfo foodInfo) {
		if(foodInfos == null) {
			foodInfos = new ArrayList<>();
		}
		foodInfos.add(foodInfo);		
	}
	
	public static void deleteFood(int i) {
		if(foodInfos == null) {
			foodInfos = new ArrayList<>();
		}
		foodInfos.remove(i);	
	}
	
	public static void addDeleteFoods(FoodInfo foodInfo) {
		if(deleteFoods == null) {
			deleteFoods = new ArrayList<>();
		}
		deleteFoods.add(foodInfo);		
	}

	public static List<FoodInfo> getDeleteFoods() {
		return deleteFoods;
	}

	public static void setDeleteFoods(List<FoodInfo> deleteFoods) {
		Container.deleteFoods = deleteFoods;
	}

	
}
