package container;

import java.util.List;

import com.oct.dp.model.OrderInfo;

public class OrderContainer {
	private static List<OrderInfo> orderInfos;

	public static int getSize() {
		if (orderInfos == null)
			return 0;
		else {
			return orderInfos.size();
		}
	}

	public static List<OrderInfo> getOrderInfos() {
		return orderInfos;
	}

	public static void setOrderInfos(List<OrderInfo> orderInfos) {
		OrderContainer.orderInfos = orderInfos;
	}

}
