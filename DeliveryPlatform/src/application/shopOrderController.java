package application;

import java.net.URL;
import java.util.ResourceBundle;

import com.oct.dp.model.ShopInfo;
import com.oct.dp.shop.ShopOwner;

import container.Container;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.stage.Stage;

/**
 * shop owner can see how many 
 * unchecked orders, checking orders and history orders.
 * 
 * @author Gary
 *
 */
public class shopOrderController implements Initializable {
	@FXML
	private Label shopNameLabel, uncheckLabel, checkingLabel, historyLabel;

	@FXML
	private Button firstPageButton, informationButton, orderButton, menuButton, logoutButton, uncheckButton,
			checkingButton, historyButton, refreshButton;

	private Stage stage;
	private Scene scene;
	private Parent root;

	private String Rname;
	private int uncheckedOrderNum;
	private int checkingOrderNum;
	private int historyOrderNum;

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		ShopInfo shopInfo = Container.getShopInfo();
		this.Rname = shopInfo.getRname();
		shopNameLabel.setText(Rname);
		uncheckedOrderNum = ShopOwner.getWaitOrderInfo(shopInfo).size();
		checkingOrderNum = ShopOwner.getOrderInfo(shopInfo).size();
		historyOrderNum = ShopOwner.history(shopInfo).size();
		uncheckLabel.setText(Integer.toString(uncheckedOrderNum));
		checkingLabel.setText(Integer.toString(checkingOrderNum));
		historyLabel.setText(Integer.toString(historyOrderNum));
	}

	/**
	 * refresh orders number
	 * 
	 * @param event click refresh button
	 */
	public void refresh(ActionEvent event) {
		ShopInfo shopInfo = Container.getShopInfo();
		uncheckedOrderNum = ShopOwner.getWaitOrderInfo(shopInfo).size();
		checkingOrderNum = ShopOwner.getOrderInfo(shopInfo).size();
		historyOrderNum = ShopOwner.history(shopInfo).size();
		uncheckLabel.setText(Integer.toString(uncheckedOrderNum));
		checkingLabel.setText(Integer.toString(checkingOrderNum));
		historyLabel.setText(Integer.toString(historyOrderNum));
	}

	/**
	 * check button of unchecked orders be clicked
	 * change to unchecked order page.
	 * 
	 * @param event click on checking uncheckedOrder button
	 * @throws Exception shoporderunchecked.fxml not found
	 */
	public void uncheckedOrder(ActionEvent event) throws Exception {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("shoporderunchecked.fxml"));
		root = loader.load();
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}

	/**
	 * check button of checking orders be clicked
	 * change to checking order page.
	 * 
	 * @param event click on checking checkingOrder button
	 * @throws Exception shoporderchecking.fxml not found
	 */
	public void checkingOrder(ActionEvent event) throws Exception {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("shoporderchecking.fxml"));
		root = loader.load();
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}

	/**
	 * check button of history orders be clicked
	 * change to history orders page.
	 * 
	 * @param event click on checking historyOrder button
	 * @throws Exception shoporderhistory.fxml not found
	 */
	public void historyOrder(ActionEvent event) throws Exception {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("shoporderhistory.fxml"));
		root = loader.load();
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}

	/**
	 * home page button be clicked
	 * change to home page.
	 * 
	 * @param event click home page button
	 * @throws Exception shoptable.fxml not found
	 */
	public void changeToHP(ActionEvent event) throws Exception {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("shoptable.fxml"));
		root = loader.load();
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}

	/**
	 * menu button be clicked
	 * change to menu page.
	 * 
	 * @param event click menu button
	 * @throws Exception shopmenu.fxml not found
	 */
	public void changeToMenu(ActionEvent event) throws Exception {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("shopmenu.fxml"));
		root = loader.load();
		shopMenuController shopmenucontroller = loader.getController();
		shopmenucontroller.setMenu(event);
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}

	/**
	 * order button be clicked
	 * change to order page.
	 * 
	 * @param event click order button
	 * @throws Exception shoporder.fxml not found
	 */
	public void changeToOrder(ActionEvent event) throws Exception {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("shoporder.fxml"));
		root = loader.load();
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}

	/**
	 * info button be clicked
	 * change to information page
	 * 
	 * @param event
	 * @throws Exception
	 */
	public void changeToInfo(ActionEvent event) throws Exception {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("shopinfomation.fxml"));
		root = loader.load();
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}

	/**
	 * logout button be clicked, notice user. Back to identity page.
	 * 
	 * @param event click logout button
	 * @throws Exception identity.fxml not found
	 */
	public void logout(ActionEvent event) throws Exception {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("logout");
		alert.setHeaderText("將要登出!!");
		alert.setContentText("確定要登出嗎?");
		if (alert.showAndWait().get() == ButtonType.OK) {
			System.out.println("成功登出!!");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("identity.fxml"));
			root = loader.load();
			stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
			scene = new Scene(root);
			stage.setScene(scene);
			stage.show();
		}
	}
}
