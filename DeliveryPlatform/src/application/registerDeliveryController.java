package application;

import java.net.URL;
import java.util.ResourceBundle;

import com.oct.dp.delivery.Deliveryman;
import com.oct.dp.model.DeliveryInfo;
import com.oct.dp.mydao.DAOException;

import container.Container;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

/**
 * register a bran-new account
 * 
 * @author Gary
 *
 */
public class registerDeliveryController implements Initializable{
	@FXML
	private Button backButton, registerButton;
	@FXML
	private TextField nameText, accountText, emailText, phoneText, passwordText, nickNameText;
	@FXML
	private Label myLabel;

	private Stage stage;
	private Scene scene;
	private Parent root;
	private String account, password;
	private DeliveryInfo deliveryInfo;

	/**
	 * register a delivery account.
	 * 
	 * @param event click register button 
	 * @throws Exception register failed.
	 */
	public void register(ActionEvent event) throws Exception {
		if (nameText.getText().isEmpty() || accountText.getText().isEmpty() || passwordText.getText().isEmpty()) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("error");
			alert.setHeaderText("請輸入姓名、帳號及密碼!!");
			alert.showAndWait();
		} else if (Deliveryman.login(accountText.getText(), passwordText.getText()) != null) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("error");
			alert.setHeaderText("帳號已存在!!");
			alert.showAndWait();
			accountText.clear();
			passwordText.clear();
		} else {
			account = accountText.getText();
			password = passwordText.getText();
			try {
				deliveryInfo = Deliveryman.register(account, password);
				deliveryInfo.setEmail(emailText.getText());
				deliveryInfo.setPhone(phoneText.getText());
				deliveryInfo.setUserName(nameText.getText());
				deliveryInfo.setName(nickNameText.getText());
				deliveryInfo = Deliveryman.update(deliveryInfo);
				Container.setDeliveryInfo(deliveryInfo);
				myLabel.setText("註冊成功");
				FXMLLoader loader = new FXMLLoader(getClass().getResource("deliverytable.fxml"));
				root = loader.load();
				stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
				scene = new Scene(root);
				stage.setScene(scene);
				stage.show();
			} catch (IllegalArgumentException | DAOException e) {
				myLabel.setText("註冊失敗");
				accountText.clear();
				passwordText.clear();
			}
		}
	}

	/**
	 * back to latest page
	 * 
	 * @param event click back button
	 * @throws Exception login.fxml cannot be found.
	 */
	public void back(ActionEvent event) throws Exception {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("login.fxml"));
		root = loader.load();
		loginController logoncontroller = loader.getController();
		logoncontroller.setIdentity("delivery");
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		nameText.setOnKeyPressed(new EventHandler<KeyEvent>() {
		    @Override
		    public void handle(KeyEvent ke) {
		        if (ke.getCode().equals(KeyCode.ENTER)) {
		            nickNameText.requestFocus();
		        }
		    }
		});
		nickNameText.setOnKeyPressed(new EventHandler<KeyEvent>() {
		    @Override
		    public void handle(KeyEvent ke) {
		        if (ke.getCode().equals(KeyCode.ENTER)) {
		            accountText.requestFocus();
		        }
		    }
		});
		accountText.setOnKeyPressed(new EventHandler<KeyEvent>() {
		    @Override
		    public void handle(KeyEvent ke) {
		        if (ke.getCode().equals(KeyCode.ENTER)) {
		            passwordText.requestFocus();
		        }
		    }
		});
		
	}
}
