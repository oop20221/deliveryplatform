package application;

import java.net.URL;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;

import com.oct.dp.member.Member;
import com.oct.dp.model.MemberInfo;

import api.BankAPI;
import container.Container;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

/**
 * member could check and update all information except for password.
 * 
 * @author Gary
 *
 */
public class memberInfoController implements Initializable {

	@FXML
	private TextField passwordText, emailText, phoneText, nameText;

	@FXML
	private Label accountText, timeText;

	@FXML
	private CheckBox VIPCheckBox;

	@FXML
	private Button bankButton, firstPageButton, informationButton, orderButton, updateButton, SHButton, logoutButton,
			deleteButton, botButton;

	@FXML
	private ScrollPane myScrollPane;

	private Stage stage;
	private Scene scene;
	private Parent root;

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		myScrollPane.setVvalue(0);
		MemberInfo memberInfo = Container.getMemberInfo();
		accountText.setText(memberInfo.getAccount());
		emailText.setText(memberInfo.getEmail());
		phoneText.setText(memberInfo.getPhone());
		nameText.setText(memberInfo.getUserName());
		VIPCheckBox.setSelected(memberInfo.isVip());

		String deadLine = null;
		if (memberInfo.getDeadline() != null) {
			LocalDateTime localDateTime = memberInfo.getDeadline().toLocalDateTime();
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
			deadLine = localDateTime.format(dtf);
		}
		timeText.setText(deadLine);

		passwordText.setOnMouseClicked(new EventHandler<MouseEvent>() {
			public void handle(MouseEvent arg0) {
				TextInputDialog textInputDialog = new TextInputDialog("請輸入密碼");
				textInputDialog.setTitle("輸入舊密碼");
				textInputDialog.setHeaderText("輸入原先密碼");
				textInputDialog.setContentText("您的舊密碼是？");
				textInputDialog.showAndWait();
				MemberInfo old = Member.login(Container.getMemberInfo().getAccount(), textInputDialog.getResult());
				if (old != null) {
					Container.setMemberInfo(old);
					TextInputDialog textInputDialog2 = new TextInputDialog("請輸入新密碼");
					textInputDialog2.setTitle("輸入新密碼");
					textInputDialog2.setHeaderText("輸入新密碼");
					textInputDialog2.setContentText("您的新密碼是？");
					textInputDialog2.showAndWait();
					old.setPassword(textInputDialog2.getResult());
					Member.changePassword(old);
					Alert alert = new Alert(AlertType.INFORMATION);
					alert.setTitle("success");
					alert.setHeaderText("成功更改密碼!!");
					alert.showAndWait();
				} else {
					Alert alert = new Alert(AlertType.ERROR);
					alert.setTitle("error");
					alert.setHeaderText("密碼錯誤!!");
					alert.setContentText("重新輸入密碼");
					alert.showAndWait();
				}
			}
		});

		botButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
			public void handle(MouseEvent arg0) {
				myScrollPane.setVvalue(1);
			}
		});

		bankButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
			public void handle(MouseEvent arg0) {
				TextInputDialog keyInput = new TextInputDialog("請輸入金鑰");
				keyInput.setTitle("key");
				keyInput.setHeaderText("輸入銀行金鑰");
				keyInput.setContentText("您的金鑰是？");
				keyInput.showAndWait();

				String key = BankAPI.verifyToken(keyInput.getResult(),
						memberInfo.getAccount() + " " + memberInfo.getUserName());
				if (key == null) {
					Alert alert = new Alert(AlertType.ERROR);
					alert.setTitle("error");
					alert.setHeaderText("金鑰不存在!!");
					alert.setContentText("重新輸入金鑰");
					alert.showAndWait();
				} else {
					Alert info2 = new Alert(AlertType.INFORMATION);
					info2.setTitle("success");
					info2.setHeaderText("綁定成功");
					info2.showAndWait();
					memberInfo.setKey(key);
					memberInfo.setToken(keyInput.getResult());
					Container.setMemberInfo(Member.update(memberInfo));
					
				}
			}
		});
	}

	/**
	 * update information. if VIP change, pop out a window to notice user.
	 * 
	 * @param event click update button
	 * @throws Exception update failed.
	 */
	public void update(ActionEvent event) throws Exception {
		MemberInfo memberInfo = Container.getMemberInfo();
//		memberInfo.setPassword(passwordText.getText());
		memberInfo.setEmail(emailText.getText());
		memberInfo.setPhone(phoneText.getText());
		memberInfo.setUserName(nameText.getText());
		memberInfo.setVip(VIPCheckBox.isSelected());
		if (memberInfo.getDeadline() == null && VIPCheckBox.isSelected()) {
			memberInfo.setDeadline(Timestamp.valueOf(LocalDateTime.now().plusMonths(1)));
		} else if (memberInfo.getDeadline() != null && VIPCheckBox.isSelected()) {
			memberInfo.setDeadline(memberInfo.getDeadline());
		} else {
			memberInfo.setDeadline(null);
		}

		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("update");
		alert.setHeaderText("將要更新!!");
		alert.setContentText("確定要更新嗎?");
		if (alert.showAndWait().get() == ButtonType.OK) {
			Alert info = new Alert(AlertType.INFORMATION);
			info.setTitle("update");
			info.setHeaderText("成功更新");
			info.showAndWait();
			Member.update(memberInfo);
			Member.changePassword(memberInfo);
			if (!memberInfo.isVip()) {
				Alert info2 = new Alert(AlertType.INFORMATION);
				info2.setTitle("VIP deadline");
				info2.setHeaderText("您取消VIP");
				info2.showAndWait();
			}
		}
		changeToInfo(event);
	}
	
	/**
	 * become VIP or cancel VIP
	 * @param event click VIPbutton
	 * @throws Exception 
	 */
	public void isVIP(ActionEvent event)throws Exception {
		if(VIPCheckBox.isSelected()) {
			if(Container.getMemberInfo().getKey()==null||Container.getMemberInfo().getToken()==null) {
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("error");
				alert.setHeaderText("請先綁定銀行!!");
				alert.showAndWait();
				VIPCheckBox.setSelected(false);
			}
		}
	}
	/**
	 * home page button be clicked change to home page.
	 * 
	 * @param event click home page button
	 * @throws Exception memberTable.fxml not found
	 */
	public void changeToHP(ActionEvent event) throws Exception {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("memberTable.fxml"));
		root = loader.load();
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}

	/**
	 * SHbutton be clicked change to search page.
	 * 
	 * @param event click search button
	 * @throws Exception memberSearch.fxml not found
	 */
	public void changeToSearch(ActionEvent event) throws Exception {
		Container.setShopInfos(null);
		FXMLLoader loader = new FXMLLoader(getClass().getResource("memberSearch.fxml"));
		root = loader.load();
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}

	/**
	 * order button be clicked change to order page.
	 * 
	 * @param event click order page
	 * @throws Exception memberOrder.fxml not found
	 */
	public void changeToOrder(ActionEvent event) throws Exception {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("memberOrder.fxml"));
		root = loader.load();
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}

	/**
	 * info button be clicked change to information page.
	 * 
	 * @param event click info page button
	 * @throws Exception memberInformation.fxml not found
	 */
	public void changeToInfo(ActionEvent event) throws Exception {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("memberInformation.fxml"));
		root = loader.load();
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}

	/**
	 * logout button be clicked, notice user. Back to identity page.
	 * 
	 * @param event click logout button
	 * @throws Exception identity.fxml not found
	 */
	public void logout(ActionEvent event) throws Exception {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("logout");
		alert.setHeaderText("將要登出!!");
		alert.setContentText("確定要登出嗎?");
		if (alert.showAndWait().get() == ButtonType.OK) {
			System.out.println("成功登出!!");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("identity.fxml"));
			root = loader.load();
			stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
			scene = new Scene(root);
			stage.setScene(scene);
			stage.show();
		}
	}

	/**
	 * member could delete the account.
	 * 
	 * @param event click delete button
	 * @throws Exception identity.fxml not found
	 */
	public void delete(ActionEvent event) throws Exception {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("delete");
		alert.setHeaderText("將要註銷!!");
		alert.setContentText("確定要註銷嗎?");
		if (alert.showAndWait().get() == ButtonType.OK) {
			Member.delete(Container.getMemberInfo());
			System.out.println("成功註銷!!");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("identity.fxml"));
			root = loader.load();
			stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
			scene = new Scene(root);
			stage.setScene(scene);
			stage.show();
		}
	}

}
