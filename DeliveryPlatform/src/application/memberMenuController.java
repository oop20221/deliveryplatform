package application;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import com.oct.dp.map.GoogleMap;
import com.oct.dp.model.OrderInfo;
import com.oct.dp.model.OrderInfo.Detail;
import com.oct.dp.model.ShopInfo;
import com.oct.dp.shop.ShopOwner;

import container.Container;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollPane;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

/**
 * After member select a shop member could order food from the menu of shop.
 * 
 * @author Gary
 *
 */
public class memberMenuController implements Initializable {

	@FXML
	private ListView<Integer> numberListView;

	@FXML
	private ListView<String> nameListView, priceListView, plusListView, minusListView;

	@FXML
	private Label shopNameLabel;

	@FXML
	private Button mapButton, backButton, confirmButton;

	@FXML
	private ScrollPane myScrollPane;

	private Stage stage;
	private Scene scene;
	private Parent root;
	private String Rname;

	private String[] Food;
	private String[] Price;
	private Integer[] number;
	private String[] plus;
	private String[] minus;

	private static String backPage;
	private int listSize;
	private int total;

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		myScrollPane.setVvalue(-0.5);
		ShopInfo shopInfo = Container.getShopInfo();
		listSize = ShopOwner.getFoodInfo(shopInfo).size();
		Rname = shopInfo.getRname();
		shopNameLabel.setText(Rname);
		Food = new String[listSize];
		Price = new String[listSize];
		number = new Integer[listSize];
		plus = new String[listSize];
		minus = new String[listSize];
		for (int i = 0; i < listSize; i++) {
			Food[i] = ShopOwner.getFoodInfo(shopInfo).get(i).getName();
			Price[i] = ShopOwner.getFoodInfo(shopInfo).get(i).getPrice();
			number[i] = 0;
			plus[i] = "+";
			minus[i] = "-";
		}
		nameListView.getItems().addAll(Food);
		priceListView.getItems().addAll(Price);
		numberListView.getItems().addAll(number);
		plusListView.getItems().addAll(plus);
		minusListView.getItems().addAll(minus);

		plusListView.setOnMouseClicked(new EventHandler<MouseEvent>() {
			public void handle(MouseEvent arg0) {
				if (plusListView.getSelectionModel().getSelectedItem() != null) {
					int i = plusListView.getSelectionModel().getSelectedIndex();
					numberListView.getItems().set(i, (numberListView.getItems().get(i) + 1));
				}
			}
		});

		minusListView.setOnMouseClicked(new EventHandler<MouseEvent>() {
			public void handle(MouseEvent arg0) {
				if (plusListView.getSelectionModel().getSelectedItem() != null) {
					int i = minusListView.getSelectionModel().getSelectedIndex();
					if ((numberListView.getItems().get(i)) == 0) {
						numberListView.getItems().set(i, 0);
					} else {
						numberListView.getItems().set(i, (numberListView.getItems().get(i) - 1));
					}
				}
			}
		});
		
		shopNameLabel.setOnMouseClicked(new EventHandler<MouseEvent>() {
			public void handle(MouseEvent event) {
				FXMLLoader loader = new FXMLLoader(getClass().getResource("memberShopInfo.fxml"));
				try {
					root = loader.load();
					stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
					scene = new Scene(root);
					stage.setScene(scene);
					stage.show();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * confirm food and amount set them into orderInfo.
	 * 
	 * @param event click confirm button
	 * @throws Exception memberOrderConfirm.fxml not found
	 */
	public void confirm(ActionEvent event) throws Exception {
		total = 0;
		List<Detail> details = new ArrayList<>();
		for (int i = 0; i < listSize; i++) {
			int amount = numberListView.getItems().get(i);
			if (amount == 0)
				continue;
			else {
				String product_name = nameListView.getItems().get(i);
				int unit_price = Integer.parseInt(priceListView.getItems().get(i));
				int price = amount * unit_price;
				total += price;
				details.add(new Detail(product_name, amount, unit_price, price, null));
			}
		}
		
		if (total == 0) {
			Alert info = new Alert(AlertType.ERROR);
			info.setTitle("error");
			info.setHeaderText("沒有點餐");
			info.setContentText("請先點餐");
			info.showAndWait();
		} else {
		OrderInfo orderInfo = new OrderInfo();
		orderInfo.setRestName(Rname);
		orderInfo.setUserName(Container.getMemberInfo().getUserName());
		orderInfo.setDetails(details);
		orderInfo.setTotal(total);

		FXMLLoader loader = new FXMLLoader(getClass().getResource("memberOrderConfirm.fxml"));
		root = loader.load();
		memberOrderConfirmController memberorderconfirmcontroller = loader.getController();
		memberorderconfirmcontroller.confirmOrder(orderInfo);
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();}
	}

	/**
	 * member could reorder the food.
	 * 
	 * @param orderInfo orderInfo need to be save
	 */
	public void reorder(OrderInfo orderInfo) {
		numberListView.getItems().clear();
		for (int i = 0; i < listSize; i++) {
			for (int j = 0; j < orderInfo.getDetails().size(); j++) {
				if (orderInfo.getDetails().get(j).getProduct_name().equals(Food[i])) {
					number[i] = orderInfo.getDetails().get(j).getAmount();
				}
			}
		}
		numberListView.getItems().addAll(number);
	}

	/**
	 * back to search page
	 * 
	 * @param event click back button
	 * @throws Exception memberSearch.fxml not found
	 */
	public void back(ActionEvent event) throws Exception {

		FXMLLoader loader = new FXMLLoader(getClass().getResource(backPage));
		root = loader.load();
//		if (backPage.equals("memberSearch.fxml")) {
//			memberSearchController msc = loader.getController();
//		} else if (backPage.equals("memberSearch.fxml")) {
//			memberOrderModelController momc = loader.getController();
//			momc.setOrder();
//		}
//		mapController mapcontroller = loader.getController();
//		mapcontroller.setBackPage("memberOrderConfirm.fxml");
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}
	
	/**
	 * set where it go after back button be clicked
	 * 
	 * @param fileName the page users want to go back
	 * @throws FileNotFoundException fileName invalid
	 */
	public void setBackPage(String fileName) throws FileNotFoundException {
		backPage = fileName;
	}

	/**
	 * show the shop location by map
	 * 
	 * @param event click showMap button
	 * @throws Exception map.fxml not found
	 */
	public void showMap(ActionEvent event) throws Exception {
		GoogleMap.getMap(Container.getShopInfo().getAddress(), "src\\image\\map.png");
		FXMLLoader loader = new FXMLLoader(getClass().getResource("map.fxml"));
		root = loader.load();
		mapController mapcontroller = loader.getController();
		mapcontroller.setBackPage("memberMenu.fxml");
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}
}
