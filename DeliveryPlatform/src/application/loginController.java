package application;

import java.net.URL;
import java.util.ResourceBundle;

import com.oct.dp.delivery.Deliveryman;
import com.oct.dp.member.Member;
import com.oct.dp.model.DeliveryInfo;
import com.oct.dp.model.MemberInfo;
import com.oct.dp.model.ShopInfo;
import com.oct.dp.mydao.DAOException;
import com.oct.dp.shop.ShopOwner;

import container.Container;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

/**
 * Controller of login.fxml After choosing identity, User could login or
 * register.
 * 
 * @author Gary
 *
 */
public class loginController implements Initializable{

	@FXML
	private Label myLabel;
	@FXML
	private Button backButton;
	@FXML
	private Button register;
	@FXML
	private Button login;
	@FXML
	private TextField myAccount;
	@FXML
	private PasswordField myPassword;

	private Stage stage;
	private Scene scene;
	private Parent root;

	private String identity;
	private String account, password;
	private ShopInfo shopInfo;
	private MemberInfo memberInfo;
	private DeliveryInfo deliveryInfo;

	/**
	 * catch identity from identityController
	 * 
 	 * @param identity member, delivery or shopOwner
	 */
	public void setIdentity(String identity) {
		this.identity = identity;
		myLabel.setText(identity);
		System.out.println(this.identity);
	}

	/**
	 * login button be clicked switch login page by identity.
	 * 
	 * @param event click login button
	 * @throws Exception login failed
	 */
	public void login(ActionEvent event) throws Exception {
		account = myAccount.getText();
		password = myPassword.getText();

		switch (identity) {
		case "shop": {
			try {
				shopInfo = ShopOwner.login(account, password);
				if (shopInfo != null) {
					Container.setShopInfo(shopInfo);
					myLabel.setText("登入成功");
					FXMLLoader loader = new FXMLLoader(getClass().getResource("shoptable.fxml"));
					root = loader.load();
					stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
					scene = new Scene(root);
					stage.setScene(scene);
					stage.show();
				} else {
					Alert alert = new Alert(AlertType.ERROR);
					alert.setTitle("error");
					alert.setHeaderText("請輸入正確帳號或密碼!!");
					alert.showAndWait();
					myAccount.clear();
					myPassword.clear();
				}
			} catch (DAOException e) {
				myLabel.setText("登入失敗");
				System.out.println("登入失敗: " + e.getMessage());
				myAccount.clear();
				myPassword.clear();
			}
			break;

		}
		case "member": {
			try {
				memberInfo = Member.login(account, password);
				if (memberInfo != null) {
					Container.setMemberInfo(memberInfo);
					myLabel.setText("登入成功");
					FXMLLoader loader = new FXMLLoader(getClass().getResource("membertable.fxml"));
					root = loader.load();
					stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
					scene = new Scene(root);
					stage.setScene(scene);
					stage.show();
				} else {
					Alert alert = new Alert(AlertType.ERROR);
					alert.setTitle("error");
					alert.setHeaderText("請輸入正確帳號或密碼!!");
					alert.showAndWait();
					myAccount.clear();
					myPassword.clear();
				}
			} catch (DAOException e) {
				myLabel.setText("登入失敗");
				System.out.println("登入失敗: " + e.getMessage());
				myAccount.clear();
				myPassword.clear();
			}
			break;
		}
		case "delivery": {
			try {
				deliveryInfo = Deliveryman.login(account, password);
				if (deliveryInfo != null) {
					Container.setDeliveryInfo(deliveryInfo);
					myLabel.setText("登入成功");
					FXMLLoader loader = new FXMLLoader(getClass().getResource("deliverytable.fxml"));
					root = loader.load();
					stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
					scene = new Scene(root);
					stage.setScene(scene);
					stage.show();
				} else {
					Alert alert = new Alert(AlertType.ERROR);
					alert.setTitle("error");
					alert.setHeaderText("請輸入正確帳號或密碼!!");
					alert.showAndWait();
					myAccount.clear();
					myPassword.clear();
				}
			} catch (DAOException e) {
				myLabel.setText("登入失敗");
				System.out.println("登入失敗: " + e.getMessage());
				myAccount.clear();
				myPassword.clear();
			}
			break;
		}
		default:
			break;
		}
	}

	/**
	 * register button be clicked switch register page by identity.
	 * 
	 * @param event click register button 
	 * @throws Exception register failed
	 */
	public void register(ActionEvent event) throws Exception {
		switch (identity) {
		case "shop": {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("registerShop.fxml"));
			root = loader.load();
			stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
			scene = new Scene(root);
			stage.setScene(scene);
			stage.show();
			break;
		}
		case "member": {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("registerMember.fxml"));
			root = loader.load();
			stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
			scene = new Scene(root);
			stage.setScene(scene);
			stage.show();
			break;
		}
		case "delivery": {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("registerDelivery.fxml"));
			root = loader.load();
			stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
			scene = new Scene(root);
			stage.setScene(scene);
			stage.show();
			break;
		}
		default:
			break;
		}
	}

	/**
	 * back to identity page
	 * 
	 * @param event click back button 
	 * @throws Exception  identity.fxml not be found.
	 */
	public void back(ActionEvent event) throws Exception {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("identity.fxml"));
		root = loader.load();
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		myAccount.setOnKeyPressed(new EventHandler<KeyEvent>() {
		    @Override
		    public void handle(KeyEvent ke) {
		        if (ke.getCode().equals(KeyCode.ENTER)) {
		            myPassword.requestFocus();
		        }
		    }
		});
	}

}
