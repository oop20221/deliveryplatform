package application;

import java.net.URL;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

import com.oct.dp.map.GoogleMap;
import com.oct.dp.model.ShopInfo;

import container.Container;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

public class memberShopInfoController implements Initializable{
	
	@FXML
	private Button mapButton, botButton, backButton;

	@FXML
	private TextField rNameText, emailText, phoneText, addressText, storedetailText, orderdetailText, discountText,
			targetText, monstaText, monendText, tuestaText, tueendText, wedstaText, wedendText, thustaText, thuendText,
			fristaText, friendText, satstaText, satendText, sunstaText, sunendText;

	@FXML
	private ScrollPane myScrollPane;

	@FXML
	private CheckBox JP, CH, WT, SF, ND, BD, DS, CK, DR, OT;

	private Stage stage;
	private Scene scene;
	private Parent root;
	private String Rname;
	
	private static final String[] allType = {"日式","中式","西式","小吃","麵食","便當","甜點","餅類","飲料","其他"};
	private static final int allTypeSize = allType.length;
	private Map<String, CheckBox> MapType = new HashMap<>();
	
//	private CheckBox[] CBarr;
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		
		
		myScrollPane.setVvalue(0);
		ShopInfo shopInfo = Container.getShopInfo();
		this.Rname = shopInfo.getRname();
		rNameText.setText(Rname);


		emailText.setText(shopInfo.getEmail());
		phoneText.setText(shopInfo.getPhone());
		addressText.setText(shopInfo.getAddress());
		storedetailText.setText(shopInfo.getStore_description());
		orderdetailText.setText(shopInfo.getOrder_description());
		
		CheckBox[] CBarr = {JP, CH, WT, SF, ND, BD, DS, CK, DR, OT};
		for(int i = 0; i < allTypeSize; i++) {
			MapType.put(allType[i], CBarr[i]);
		}
		if (shopInfo.getType() == null) {
		} else {
			for (String type : shopInfo.getType()) {
				MapType.get(type).setSelected(true);
			}
		}
		
		
		DecimalFormat df = new DecimalFormat("#0.00");
		discountText.setText(df.format(shopInfo.getDiscount()));
		targetText.setText(Integer.toString(shopInfo.getTargetcost()));

		monstaText.setText(shopInfo.getMonstart());
		monendText.setText(shopInfo.getMonend());
		tuestaText.setText(shopInfo.getTuestart());
		tueendText.setText(shopInfo.getTueend());
		wedstaText.setText(shopInfo.getWedstart());
		wedendText.setText(shopInfo.getWedend());
		thustaText.setText(shopInfo.getThustart());
		thuendText.setText(shopInfo.getThuend());
		fristaText.setText(shopInfo.getFristart());
		friendText.setText(shopInfo.getFriend());
		satstaText.setText(shopInfo.getSatstart());
		satendText.setText(shopInfo.getSatend());
		sunstaText.setText(shopInfo.getSunstart());
		sunendText.setText(shopInfo.getSunend());
		
		botButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
			public void handle(MouseEvent arg0) {
				myScrollPane.setVvalue(1);
			}
		});
	}
	
	/**
	 * show the location of shop address in Map
	 * 
	 * @param event click showMap button
	 * @throws Exception map.fxml not found
	 */
	public void showMap(ActionEvent event) throws Exception {
		GoogleMap.getMap(Container.getShopInfo().getAddress(), "src\\image\\map.png");
		FXMLLoader loader = new FXMLLoader(getClass().getResource("map.fxml"));
		root = loader.load();
		mapController mapcontroller = loader.getController();
		mapcontroller.setBackPage("memberShopInfo.fxml");
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}
	
	/**
	 * back to order menu page In order to reorder food.
	 * 
	 * @param event click on back button
	 * @throws Exception memberTable.fxml cannot be found.
	 */
	public void back(ActionEvent event) throws Exception {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("memberMenu.fxml"));
		root = loader.load();
//		memberMenuController membermenucontroller = loader.getController();
//		membermenucontroller.reorder(orderInfo);
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}

}
