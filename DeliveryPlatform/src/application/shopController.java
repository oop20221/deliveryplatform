package application;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import com.oct.dp.shop.ShopOwner;

import container.Container;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.stage.Stage;

/**
 * home page of shop Shop owner can refresh to see unchecked orders.
 * 
 * @author Gary
 *
 */
public class shopController implements Initializable {

	@FXML
	private Label shopNameLabel, orderNoticeLabel;
	@FXML
	private Button checkOrderButton, refreshOrderButton, firstPageButton, informationButton, orderButton, menuButton, logoutButton;

	private Stage stage;
	private Scene scene;
	private Parent root;

	private String Rname;
	private int uncheckedOrderNum;

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		Rname = Container.getShopInfo().getRname();
		shopNameLabel.setText(Rname);
		uncheckedOrderNum = ShopOwner.getWaitOrderInfo(Container.getShopInfo()).size();
		orderNoticeLabel.setText(uncheckedOrderNum + "個未接訂單");

	}

	/**
	 * refresh unconfirmed orders number
	 * 
	 * @param event click refresh button
	 */
	public void refresh(ActionEvent event) {
		uncheckedOrderNum = ShopOwner.getWaitOrderInfo(Container.getShopInfo()).size();
		orderNoticeLabel.setText(uncheckedOrderNum + "個未接訂單");
	}

	/**
	 * check the unchecked orders
	 * 
	 * @param event click refresh button
	 * @throws IOException deliveryorderunchecked.fxml not found
	 */
	public void check(ActionEvent event) throws IOException {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("shoporderunchecked.fxml"));
		root = loader.load();
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}
	/**
	 * home page button be clicked
	 * change to home page.
	 * 
	 * @param event click on home page button
	 * @throws Exception shoptable.fxml not found
	 */
	public void changeToHP(ActionEvent event) throws Exception {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("shoptable.fxml"));
		root = loader.load();
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}

	/**
	 * menu button be clicked
	 * change to menu.
	 * 
	 * @param event click on menu button 
	 * @throws Exception shopmenu.fxml not found
	 */
	public void changeToMenu(ActionEvent event) throws Exception {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("shopmenu.fxml"));
		root = loader.load();
		shopMenuController shopmenucontroller = loader.getController();
		shopmenucontroller.setMenu(event);
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}

	/**
	 * order button be clicked
	 * change to order page.
	 * 
	 * @param event click order button
	 * @throws Exception shoporder.fxml not found
	 */
	public void changeToOrder(ActionEvent event) throws Exception {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("shoporder.fxml"));
		root = loader.load();
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}

	/**
	 * info button be clicked
	 * change to information page.
	 * 
	 * @param event click on information button
	 * @throws Exception shopinfomation.fxml not fond
	 */
	public void changeToInfo(ActionEvent event) throws Exception {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("shopinfomation.fxml"));
		root = loader.load();
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}

	/**
	 * logout button be clicked, notice user. Back to identity page.
	 * 
	 * @param event click on logout button
	 * @throws Exception identity.fxml not found
	 */
	public void logout(ActionEvent event) throws Exception {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("logout");
		alert.setHeaderText("將要登出!!");
		alert.setContentText("確定要登出嗎?");
		if (alert.showAndWait().get() == ButtonType.OK) {
			System.out.println("成功登出!!");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("identity.fxml"));
			root = loader.load();
			stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
			scene = new Scene(root);
			stage.setScene(scene);
			stage.show();
		}
	}
}
