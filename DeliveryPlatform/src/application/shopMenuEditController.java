package application;

import java.net.URL;
import java.util.ResourceBundle;

import com.oct.dp.model.FoodInfo;
import com.oct.dp.model.ShopInfo;
import com.oct.dp.shop.ShopOwner;

import container.Container;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

/**
 * shop owner can edit menu.
 * 
 * @author Gary
 *
 */
public class shopMenuEditController implements Initializable {

	@FXML
	private ListView<String> foodListView, priceListView;

	@FXML
	private TextField TextField;

	@FXML
	private Button botButton, confirmButton, addFood, backButton, deleteButton;

	@FXML
	private ScrollPane myScrollPane;

	private Stage stage;
	private Scene scene;
	private Parent root;
	private String Rname;

	private String[] Food;
	private String[] Price;

	private int listSize;

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		myScrollPane.setVvalue(0);
		ShopInfo shopInfo = Container.getShopInfo();
		Container.setFoodInfos(ShopOwner.getFoodInfo(shopInfo));

		if (Container.getFoodSize() != 0) {
			listSize = Container.getFoodSize();
			Rname = shopInfo.getRname();
			TextField.setText(Rname);
			Food = new String[listSize];
			Price = new String[listSize];
			for (int i = 0; i < listSize; i++) {
				Food[i] = Container.getFoodInfos().get(i).getName();
				Price[i] = Container.getFoodInfos().get(i).getPrice();
			}
			foodListView.getItems().addAll(Food);
			foodListView.setOnMouseClicked(new EventHandler<MouseEvent>() {

				@Override
				public void handle(MouseEvent arg0) {
					String food = foodListView.getSelectionModel().getSelectedItem();
					int i = foodListView.getSelectionModel().getSelectedIndex();
					TextField.setText(food);

					TextField.setOnKeyPressed(new EventHandler<KeyEvent>() {
						@Override
						public void handle(KeyEvent ke) {
							if (ke.getCode().equals(KeyCode.ENTER)) {
								foodListView.getItems().set(i, TextField.getText());
								priceListView.requestFocus();
							}
						}
					});
				}
			});
			priceListView.getItems().addAll(Price);
			priceListView.setOnMouseClicked(new EventHandler<MouseEvent>() {

				@Override
				public void handle(MouseEvent arg0) {
					String price = priceListView.getSelectionModel().getSelectedItem();
					int i = priceListView.getSelectionModel().getSelectedIndex();
					TextField.setText(price);

					TextField.setOnKeyPressed(new EventHandler<KeyEvent>() {
						@Override
						public void handle(KeyEvent ke) {
							if (ke.getCode().equals(KeyCode.ENTER)) {
								priceListView.getItems().set(i, TextField.getText());

							}
						}
					});
				}
			});

		} else {
			Rname = shopInfo.getRname();
			TextField.setText(Rname);
			Food = new String[1];
			Price = new String[1];
			Food[0] = "請輸入";
			Price[0] = "請輸入";
			Container.addFood(null);
			foodListView.getItems().addAll(Food);

			foodListView.setOnMouseClicked(new EventHandler<MouseEvent>() {

				@Override
				public void handle(MouseEvent arg0) {

					String food = foodListView.getSelectionModel().getSelectedItem();
					int i = foodListView.getSelectionModel().getSelectedIndex();
					TextField.setText(food);

					TextField.setOnKeyPressed(new EventHandler<KeyEvent>() {
						@Override
						public void handle(KeyEvent ke) {
							if (ke.getCode().equals(KeyCode.ENTER)) {
								foodListView.getItems().set(i, TextField.getText());
							}
						}
					});
				}
			});
			priceListView.getItems().addAll(Price);
			priceListView.setOnMouseClicked(new EventHandler<MouseEvent>() {

				@Override
				public void handle(MouseEvent arg0) {
					String price = priceListView.getSelectionModel().getSelectedItem();
					int i = priceListView.getSelectionModel().getSelectedIndex();
					TextField.setText(price);

					TextField.setOnKeyPressed(new EventHandler<KeyEvent>() {
						@Override
						public void handle(KeyEvent ke) {
							if (ke.getCode().equals(KeyCode.ENTER)) {
								priceListView.getItems().set(i, TextField.getText());

							}
						}
					});
				}
			});
		}
		botButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
			public void handle(MouseEvent arg0) {
				myScrollPane.setVvalue(1);
			}
		});
	}

	/**
	 * add an row to set new food
	 * 
	 * @param event click add button
	 * @throws Exception add failed
	 */
	public void add(ActionEvent event) throws Exception {

		foodListView.getItems().add("請輸入");
		priceListView.getItems().add("請輸入");
		Container.addFood(null);
		System.out.println("add" + Container.getFoodInfos());
	}

	/**
	 * delete food and price
	 * 
	 * @param event click delete button
	 * @throws Exception delete failed
	 */
	public void delete(ActionEvent event) throws Exception {

		int index = foodListView.getSelectionModel().getSelectedIndex();
		foodListView.getItems().remove(index);
		priceListView.getItems().remove(index);
		Container.addDeleteFoods(Container.getFoodInfos().get(index));
		Container.deleteFood(index);
		System.out.println("delete" + Container.getFoodInfos());
	}

	/**
	 * update menu 
	 * 
	 * @param event click confirm button
	 * @throws Exception shopmenu.fxml not found
	 */
	public void confirm(ActionEvent event) throws Exception {
		listSize = foodListView.getItems().size();
		ShopInfo shopInfo = Container.getShopInfo();
		for (int i = 0; i < listSize; i++) {
			FoodInfo foodInfo = Container.getFoodInfo(i);
			if (foodListView.getItems().get(i).equals("請輸入") || priceListView.getItems().get(i).equals("請輸入")) {
				continue;
			}
			if (foodInfo == null) {
				foodInfo = new FoodInfo();
				foodInfo.setR_id(shopInfo.getId());
				foodInfo.setName(foodListView.getItems().get(i));
				foodInfo.setPrice(priceListView.getItems().get(i));
				ShopOwner.createFood(foodInfo);
			} else {
				foodInfo.setR_id(shopInfo.getId());
				foodInfo.setName(foodListView.getItems().get(i));
				foodInfo.setPrice(priceListView.getItems().get(i));
				ShopOwner.updateFood(foodInfo);
			}
		}
		if (Container.getDeleteFoods() != null) {
			for (int i = 0; i < Container.getDeleteFoods().size(); i++) {
				FoodInfo deleteFood = Container.getDeleteFoods().get(i);
				if (deleteFood != null) {
					ShopOwner.deleteFood(deleteFood);
				}
			}
		}
		Container.setDeleteFoods(null);
		FXMLLoader loader = new FXMLLoader(getClass().getResource("shopmenu.fxml"));
		root = loader.load();
		shopMenuController shopmenucontroller = loader.getController();
		shopmenucontroller.setMenu(event);
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}

	/**
	 * back to menu page
	 * 
	 * @param event click back button 
	 * @throws Exception shopmenu.fxml not found
	 */
	public void back(ActionEvent event) throws Exception {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("shopmenu.fxml"));
		root = loader.load();
		shopMenuController shopmenucontroller = loader.getController();
		shopmenucontroller.setMenu(event);
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}
}