package application;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import com.oct.dp.delivery.Deliveryman;

import container.Container;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.stage.Stage;

/**
 * home page of delivery. Delivery man can check the UNCHECKED order by press
 * refresh button.
 * 
 * @author Gary
 *
 */
public class deliveryController implements Initializable {

	@FXML
	private Label deliveryNameLabel, orderNoticeLabel;
	@FXML
	private Button refreshOrderButton, checkOrderButton, firstPageButton, informationButton, orderButton, logoutButton;

	private Stage stage;
	private Scene scene;
	private Parent root;
	private String name;
	private int uncheckedOrderNum;

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		name = Container.getDeliveryInfo().getName();
		deliveryNameLabel.setText(name);
		uncheckedOrderNum = Deliveryman.getReadyOrderInfo().size();
		orderNoticeLabel.setText(uncheckedOrderNum + "個未接訂單");
	}

	/**
	 * refresh unconfirmed orders number
	 * 
	 * @param event click refresh button
	 */
	public void refresh(ActionEvent event) {
		uncheckedOrderNum = Deliveryman.getReadyOrderInfo().size();
		orderNoticeLabel.setText(uncheckedOrderNum + "個未接訂單");
	}
	
	/**
	 * check the unchecked orders
	 * 
	 * @param event click refresh button
	 * @throws IOException deliveryorderunchecked.fxml not found
	 */
	public void check(ActionEvent event) throws IOException {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("deliveryorderunchecked.fxml"));
		root = loader.load();
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}

	/**
	 * home page button be clicked. delivery could change to home page.
	 * 
	 * @param event click on home page button
	 * @throws Exception deliverytable.fxml cannot be found.
	 */
	public void changeToHP(ActionEvent event) throws Exception {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("deliverytable.fxml"));
		root = loader.load();
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}

	/**
	 * order button be clicked. delivery could change to order checking page.
	 * 
	 * @param event click on order page
	 * @throws Exception deliveryorder.fxml cannot be found
	 */
	public void changeToOrder(ActionEvent event) throws Exception {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("deliveryorder.fxml"));
		root = loader.load();
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}

	/**
	 * info button be clicked. delivery could change to information page.
	 * 
	 * @param event click on info button
	 * @throws Exception deliveryinformation.fxml cannot be found.
	 */
	public void changeToInfo(ActionEvent event) throws Exception {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("deliveryinformation.fxml"));
		root = loader.load();
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}

	/**
	 * logout button be clicked, notice user. Back to identity page.
	 * 
	 * @param event click on logout button
	 * @throws Exception identity.fxml annot be found.
	 */
	public void logout(ActionEvent event) throws Exception {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("logout");
		alert.setHeaderText("將要登出!!");
		alert.setContentText("確定要登出嗎?");
		if (alert.showAndWait().get() == ButtonType.OK) {
			System.out.println("成功登出!!");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("identity.fxml"));
			root = loader.load();
			stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
			scene = new Scene(root);
			stage.setScene(scene);
			stage.show();
		}

	}
}
