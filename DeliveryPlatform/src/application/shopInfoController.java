package application;

import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import com.oct.dp.map.GoogleMap;
import com.oct.dp.model.ShopInfo;
import com.oct.dp.shop.ShopOwner;

import container.Container;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

/**
 * Information of shop shop owner can see all information except for password.
 * 
 * @author Gary
 *
 */
public class shopInfoController implements Initializable {

	@FXML
	private TextField shopNameLabel;

	@FXML
	private Button mapButton, deleteButton, firstPageButton, informationButton, orderButton, menuButton, logoutButton,
			updateButton, botButton;

	@FXML
	private TextField passwordText, emailText, phoneText, addressText, storedetailText, orderdetailText, discountText,
			targetText, monstaText, monendText, tuestaText, tueendText, wedstaText, wedendText, thustaText, thuendText,
			fristaText, friendText, satstaText, satendText, sunstaText, sunendText;

	@FXML
	private Label accountText;

	@FXML
	private ScrollPane myScrollPane;

	@FXML
	private CheckBox JP, CH, WT, SF, ND, BD, DS, CK, DR, OT;

	
	private Stage stage;
	private Scene scene;
	private Parent root;
	private String Rname;
	
	private static final String[] allType = {"日式","中式","西式","小吃","麵食","便當","甜點","餅類","飲料","其他"};
	private static final int allTypeSize = allType.length;
	private Map<String, CheckBox> MapType = new HashMap<>();
	
	private CheckBox[] CBarr;
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		CheckBox[] CBall = {JP, CH, WT, SF, ND, BD, DS, CK, DR, OT};
		CBarr = CBall;
		for(int i = 0; i < allTypeSize; i++) {
			MapType.put(allType[i], CBarr[i]);
		}
		myScrollPane.setVvalue(0);
		ShopInfo shopInfo = Container.getShopInfo();
		this.Rname = shopInfo.getRname();
		shopNameLabel.setText(Rname);

		accountText.setText(shopInfo.getAccount());
		emailText.setText(shopInfo.getEmail());
		phoneText.setText(shopInfo.getPhone());
		addressText.setText(shopInfo.getAddress());
		storedetailText.setText(shopInfo.getStore_description());
		orderdetailText.setText(shopInfo.getOrder_description());

		passwordText.setOnMouseClicked(new EventHandler<MouseEvent>() {
			public void handle(MouseEvent arg0) {
				TextInputDialog textInputDialog = new TextInputDialog("請輸入密碼");
				textInputDialog.setTitle("輸入舊密碼");
				textInputDialog.setHeaderText("輸入原先密碼");
				textInputDialog.setContentText("您的舊密碼是？");
				textInputDialog.showAndWait();
				ShopInfo old = ShopOwner.login(Container.getShopInfo().getAccount(), textInputDialog.getResult());
				if (old != null) {
					Container.setShopInfo(old);
					TextInputDialog textInputDialog2 = new TextInputDialog("請輸入新密碼");
					textInputDialog2.setTitle("輸入新密碼");
					textInputDialog2.setHeaderText("輸入新密碼");
					textInputDialog2.setContentText("您的新密碼是？");
					textInputDialog2.showAndWait();
					old.setPassword(textInputDialog2.getResult());
					ShopOwner.changePassword(old);
					Alert alert = new Alert(AlertType.INFORMATION);
					alert.setTitle("success");
					alert.setHeaderText("成功更改密碼!!");
					alert.showAndWait();
				} else {
					Alert alert = new Alert(AlertType.ERROR);
					alert.setTitle("error");
					alert.setHeaderText("密碼錯誤!!");
					alert.setContentText("重新輸入密碼");
					alert.showAndWait();
				}
			}
		});
		
		if (shopInfo.getType() == null) {
		} else {
			for (String type : shopInfo.getType()) {
				MapType.get(type).setSelected(true);
			}
		}

		DecimalFormat df = new DecimalFormat("#0.00");
		discountText.setText(df.format(shopInfo.getDiscount()));
		targetText.setText(Integer.toString(shopInfo.getTargetcost()));

		monstaText.setText(shopInfo.getMonstart());
		monendText.setText(shopInfo.getMonend());
		tuestaText.setText(shopInfo.getTuestart());
		tueendText.setText(shopInfo.getTueend());
		wedstaText.setText(shopInfo.getWedstart());
		wedendText.setText(shopInfo.getWedend());
		thustaText.setText(shopInfo.getThustart());
		thuendText.setText(shopInfo.getThuend());
		fristaText.setText(shopInfo.getFristart());
		friendText.setText(shopInfo.getFriend());
		satstaText.setText(shopInfo.getSatstart());
		satendText.setText(shopInfo.getSatend());
		sunstaText.setText(shopInfo.getSunstart());
		sunendText.setText(shopInfo.getSunend());
		
		botButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
			public void handle(MouseEvent arg0) {
				myScrollPane.setVvalue(1);
			}
		});

	}

	/**
	 * update data
	 * 
	 * @param event click update button
	 * @throws Exception update failed
	 */
	public void update(ActionEvent event) throws Exception {
		ShopInfo shopInfo = Container.getShopInfo();
		shopInfo.setRname(shopNameLabel.getText());
		shopInfo.setEmail(emailText.getText());

		shopInfo.setPhone(phoneText.getText());
		shopInfo.setAddress(addressText.getText());
		shopInfo.setStore_description(storedetailText.getText());
		shopInfo.setOrder_description(orderdetailText.getText());

		List<String> types = new ArrayList<>();
			for (int i = 0; i < allTypeSize; i++) {
				if(CBarr[i].isSelected()) {
					types.add(allType[i]);
			}
		}
		shopInfo.setType(types);

		shopInfo.setDiscount(Double.parseDouble(discountText.getText()));
		shopInfo.setTargetcost(Integer.parseInt(targetText.getText()));
		shopInfo.setMonstart(monstaText.getText());
		shopInfo.setMonend(monendText.getText());
		shopInfo.setTuestart(tuestaText.getText());
		shopInfo.setTueend(tueendText.getText());
		shopInfo.setWedstart(wedstaText.getText());
		shopInfo.setWedend(wedendText.getText());
		shopInfo.setThustart(thustaText.getText());
		shopInfo.setThuend(thuendText.getText());
		shopInfo.setFristart(fristaText.getText());
		shopInfo.setFriend(friendText.getText());
		shopInfo.setSatstart(satstaText.getText());
		shopInfo.setSatend(satendText.getText());
		shopInfo.setSunstart(sunstaText.getText());
		shopInfo.setSunend(sunendText.getText());

		if (Double.parseDouble(discountText.getText()) >= 1 || Double.parseDouble(discountText.getText()) < 0
				|| Integer.parseInt(targetText.getText()) < 0) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("error");
			alert.setHeaderText("優惠設定錯誤!!");
			alert.setContentText("重新設定優惠");
			alert.showAndWait();
		} else {
			Alert alert2 = new Alert(AlertType.CONFIRMATION);
			alert2.setTitle("update");
			alert2.setHeaderText("將要更新!!");
			alert2.setContentText("確定要更新嗎?");
			if (alert2.showAndWait().get() == ButtonType.OK) {
				ShopOwner.update(shopInfo);
				System.out.println("成功更新!!");
				Container.setShopInfo(shopInfo);
//				changeToInfo(event);
			}
		}
	}

	/**
	 * home page button be clicked change to home page.
	 * 
	 * @param event click home page button
	 * @throws Exception shoptable.fxml not found
	 */
	public void changeToHP(ActionEvent event) throws Exception {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("shoptable.fxml"));
		root = loader.load();
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}

	/**
	 * menu button be clicked change to menu page.
	 * 
	 * @param event click menu button
	 * @throws Exception shopmenu.fxml not found
	 */
	public void changeToMenu(ActionEvent event) throws Exception {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("shopmenu.fxml"));
		root = loader.load();
		shopMenuController shopmenucontroller = loader.getController();
		shopmenucontroller.setMenu(event);
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}

	/**
	 * order button be clicked change to order page.
	 * 
	 * @param event click order button
	 * @throws Exception shoporder.fxml not found
	 */
	public void changeToOrder(ActionEvent event) throws Exception {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("shoporder.fxml"));
		root = loader.load();
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}

	/**
	 * info button be clicked change to information page.
	 * 
	 * @param event click information button
	 * @throws Exception shopinfomation.fxml not found
	 */
	public void changeToInfo(ActionEvent event) throws Exception {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("shopinfomation.fxml"));
		root = loader.load();
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}

	/**
	 * logout button be clicked, notice user. Back to identity page.
	 * 
	 * @param event click logout button
	 * @throws Exception identity.fxml not found
	 */
	public void logout(ActionEvent event) throws Exception {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("logout");
		alert.setHeaderText("將要登出!!");
		alert.setContentText("確定要登出嗎?");
		if (alert.showAndWait().get() == ButtonType.OK) {
			System.out.println("成功登出!!");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("identity.fxml"));
			root = loader.load();
			stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
			scene = new Scene(root);
			stage.setScene(scene);
			stage.show();
		}
	}

	/**
	 * delete the account from database. and enforcing user logout
	 * 
	 * @param event click logout button
	 * @throws Exception identity.fxml not found
	 */
	public void delete(ActionEvent event) throws Exception {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("delete");
		alert.setHeaderText("將要註銷!!");
		alert.setContentText("確定要註銷嗎?");
		if (alert.showAndWait().get() == ButtonType.OK) {
			ShopOwner.delete(Container.getShopInfo());
			System.out.println("成功註銷!!");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("identity.fxml"));
			root = loader.load();
			stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
			scene = new Scene(root);
			stage.setScene(scene);
			stage.show();
		}
	}

	/**
	 * show the location of shop address in Map
	 * 
	 * @param event click showMap button
	 * @throws Exception map.fxml not found
	 */
	public void showMap(ActionEvent event) throws Exception {
		GoogleMap.getMap(addressText.getText(), "src\\image\\map.png");
		FXMLLoader loader = new FXMLLoader(getClass().getResource("map.fxml"));
		root = loader.load();
		mapController mapcontroller = loader.getController();
		mapcontroller.setBackPage("shopinfomation.fxml");
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}
}
