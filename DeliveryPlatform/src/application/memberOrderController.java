package application;

import java.net.URL;
import java.util.ResourceBundle;

import com.oct.dp.member.Member;
import com.oct.dp.model.MemberInfo;

import container.Container;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.stage.Stage;

/**
 * member can see how many history orders and checking orders.
 * 
 * @author Gary
 *
 */
public class memberOrderController implements Initializable {

	@FXML
	private Label checkingLabel, historyLabel;

	@FXML
	private Button firstPageButton, informationButton, orderButton, refreshButton, SHButton, logoutButton,
			checkingButton, historyButton;

	private Stage stage;
	private Scene scene;
	private Parent root;

	private int checkingOrderNum;
	private int historyOrderNum;

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		MemberInfo memberInfo = Container.getMemberInfo();
		checkingOrderNum = Member.getOrderInfo(memberInfo).size();
		historyOrderNum = Member.history(memberInfo).size();
		checkingLabel.setText(Integer.toString(checkingOrderNum));
		historyLabel.setText(Integer.toString(historyOrderNum));
	}

	/**
	 * refresh orders number
	 * 
	 * @param event click on refresh button
	 */
	public void refresh(ActionEvent event) {
		MemberInfo memberInfo = Container.getMemberInfo();
		checkingOrderNum = Member.getOrderInfo(memberInfo).size();
		historyOrderNum = Member.history(memberInfo).size();
		checkingLabel.setText(Integer.toString(checkingOrderNum));
		historyLabel.setText(Integer.toString(historyOrderNum));
	}

	/**
	 * check button of checking orders be clicked change to checking orders list.
	 * 
	 * @param event click on checkingOrder button
	 * @throws Exception memberOrderChecking.fxml not found
	 */
	public void checkingOrder(ActionEvent event) throws Exception {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("memberOrderChecking.fxml"));
		root = loader.load();
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}

	/**
	 * check button of history orders be clicked change to history orders page.
	 * 
	 * @param event click on historyOrder button
	 * @throws Exception memberOrderHistory.fxml not found
	 */
	public void historyOrder(ActionEvent event) throws Exception {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("memberOrderHistory.fxml"));
		root = loader.load();
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}

	/**
	 * home page button be clicked change to home page.
	 * 
	 * @param event click home page button
	 * @throws Exception memberTable.fxml not found
	 */
	public void changeToHP(ActionEvent event) throws Exception {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("memberTable.fxml"));
		root = loader.load();
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}

	/**
	 * SHbutton be clicked change to search page.
	 * 
	 * @param event click search page button
	 * @throws Exception memberSearch.fxml not found
	 */
	public void changeToSearch(ActionEvent event) throws Exception {
		Container.setShopInfos(null);
		FXMLLoader loader = new FXMLLoader(getClass().getResource("memberSearch.fxml"));
		root = loader.load();
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}

	/**
	 * order button be clicked change to order page.
	 * 
	 * @param event click order page button
	 * @throws Exception memberOrder.fxml not found
	 */
	public void changeToOrder(ActionEvent event) throws Exception {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("memberOrder.fxml"));
		root = loader.load();
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}

	/**
	 * info button be clicked change to information page.
	 * 
	 * @param event click info page button
	 * @throws Exception memberInformation.fxml not found
	 */
	public void changeToInfo(ActionEvent event) throws Exception {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("memberInformation.fxml"));
		root = loader.load();
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}

	/**
	 * logout button be clicked, notice user. Back to identity page.
	 * 
	 * @param event click logout button
	 * @throws Exception identity.fxml not found
	 */
	public void logout(ActionEvent event) throws Exception {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("logout");
		alert.setHeaderText("將要登出!!");
		alert.setContentText("確定要登出嗎?");
		if (alert.showAndWait().get() == ButtonType.OK) {
			System.out.println("成功登出!!");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("identity.fxml"));
			root = loader.load();
			stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
			scene = new Scene(root);
			stage.setScene(scene);
			stage.show();
		}
	}

}
