package application;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.image.Image;
import javafx.stage.Stage;

/**
 * open a JavaFX to start this delivery platform.
 * 
 * @author Gary
 *
 */
public class Main extends Application {
	@Override
	public void start(Stage stage) {
		try {
			Parent root = FXMLLoader.load(getClass().getResource("identity.fxml"));
			Scene scene = new Scene(root);

			Image icon = new Image("image\\piclogo.jpg");
			stage.getIcons().add(icon);
			stage.setTitle("OOPFOOD");
			stage.setScene(scene);
			stage.show();

			stage.setOnCloseRequest(event -> {
				event.consume();
				exit(stage);
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * notice if you click X button to close page.
	 * 
	 * @param stage javaFX window
	 */
	public void exit(Stage stage) {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("exit");
		alert.setHeaderText("將要離開!!");
		alert.setContentText("確定要離開嗎?");

		if (alert.showAndWait().get() == ButtonType.OK) {
			System.out.println("成功離開!!");
			stage.close();
		}

	}

	/**
	 * to start the program
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		launch(args);
	}
}
