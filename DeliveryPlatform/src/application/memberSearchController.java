package application;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import com.oct.dp.member.Member;
import com.oct.dp.model.ShopInfo;

import container.Container;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

/**
 * member can search restaurant by type, name and discount.
 * 
 * @author Gary
 *
 */
public class memberSearchController implements Initializable {

	@FXML
	private ListView<String> shopListView;

	@FXML
	private RadioButton JP, CH, WT, SF, ND, BD, DS, CK, DR, OT, ALL;

	@FXML
	private CheckBox discountCheckBox;

	@FXML
	private Button searchButton, firstPageButton, informationButton, SHButton, orderButton, logoutButton;

	@FXML
	private TextField searchTextField;

	@FXML
	private ScrollPane myScrollPane;

	private Stage stage;
	private Scene scene;
	private Parent root;

	private String type;
	private static String name;

	private String[] ShopNames;

	private RadioButton[] RBarr;
	private static final String[] allType = { "日式", "中式", "西式", "小吃", "麵食", "便當", "甜點", "餅類", "飲料", "其他" };
	private static final int allTypeSize = allType.length;
	private Map<RadioButton, String> MapType = new HashMap<>();
	private static List<ShopInfo> tmp;

	/**
	 * search button be pressed or ENTER, and search shop.
	 * 
	 */
	public void search() {
		discountCheckBox.setSelected(false);

		try {
			name = searchTextField.getText();
			Container.setShopInfos(Member.searchByName(name));

			tmp = new ArrayList<>();
			if (ALL.isSelected()) {
				if (Container.getShopInfos().isEmpty()) {
					Alert alert = new Alert(AlertType.INFORMATION);
					alert.setTitle("ERROR");
					alert.setHeaderText("查無結果!!");
					alert.showAndWait();
					searchTextField.clear();
				} else {
					tmp.addAll(Container.getShopInfos());
				}
			} else if (shopListView.getItems() != null) {
				for (int i = 0; i < allTypeSize; i++) {
					if (RBarr[i].isSelected()) {
						type = MapType.get(RBarr[i]);
					}
				}
				for (int i = 0; i < Container.getTypeShopInfos().size(); i++) {
					for (int j = 0; j < Container.getShopInfos().size(); j++) {
						if (Container.getTypeShopInfos().get(i).getId()
								.equals(Container.getShopInfos().get(j).getId())) {
							tmp.add(Container.getTypeShopInfos().get(i));
						}
					}
				}
			}
			if (tmp.isEmpty()) {
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("ERROR");
				alert.setHeaderText("查無結果!!");
				alert.showAndWait();
				searchTextField.clear();
			} else {
			ShopNames = new String[tmp.size()];
			for (int i = 0; i < ShopNames.length; i++) {
				ShopNames[i] = tmp.get(i).getRname();
			}
			shopListView.getItems().clear();
			shopListView.getItems().addAll(ShopNames);
			shopListView.setOnMouseClicked(new EventHandler<MouseEvent>() {
				@Override
				public void handle(MouseEvent event) {
					if (shopListView.getSelectionModel().getSelectedItem() != null) {
						Container.setShopInfo(tmp.get(shopListView.getSelectionModel().getSelectedIndex()));
						FXMLLoader loader = new FXMLLoader(getClass().getResource("memberMenu.fxml"));
						
						try {
							root = loader.load();
							memberMenuController mmc = loader.getController();
							mmc.setBackPage("memberSearch.fxml");
							
						} catch (IOException e) {
							e.printStackTrace();
						}
						stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
						scene = new Scene(root);
						stage.setScene(scene);
						stage.show();
					}
				}
			});
			}} catch (Exception e) {
			tmp.clear();
			Container.setTypeShopInfos(null);
			Container.setDscShopInfos(null);
			Container.setShopInfos(null);
			shopListView.getItems().clear();
			ALL.setSelected(true);
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("ERROR");
			alert.setHeaderText("請輸入!!");
			alert.showAndWait();
		}
	}

	/**
	 * search the shop which have discount.
	 */
	public void isDiscount() {
		Container.setDscShopInfos(Member.searchByDiscount());
		List<ShopInfo> disctmp = new ArrayList<>();
		if (discountCheckBox.isSelected()) {
			if (tmp==null || tmp.isEmpty()) {
				disctmp.addAll(Container.getDscShopInfos());
			} else {
				for (int i = 0; i < tmp.size(); i++) {
					for (int j = 0; j < Container.getDscShopInfos().size(); j++) {
						if (tmp.get(i).getId().equals(Container.getDscShopInfos().get(j).getId())) {
							disctmp.add(tmp.get(i));
						}
					}
				}
			}
			ShopNames = new String[disctmp.size()];
			for (int i = 0; i < ShopNames.length; i++) {
				ShopNames[i] = disctmp.get(i).getRname();
			}
//			tmp = disctmp;
			shopListView.getItems().clear();
			shopListView.getItems().addAll(ShopNames);
			shopListView.setOnMouseClicked(new EventHandler<MouseEvent>() {

				@Override
				public void handle(MouseEvent event) {
					if (shopListView.getSelectionModel().getSelectedItem() != null) {
						Container.setShopInfo(disctmp.get(shopListView.getSelectionModel().getSelectedIndex()));
						FXMLLoader loader = new FXMLLoader(getClass().getResource("memberMenu.fxml"));
						
						try {
							
							root = loader.load();
							memberMenuController mmc = loader.getController();
							mmc.setBackPage("memberSearch.fxml");
						} catch (IOException e) {
							e.printStackTrace();
						}
						stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
						scene = new Scene(root);
						stage.setScene(scene);
						stage.show();
					}
				}
			});

		} else {
			if (tmp != null) {
				ShopNames = new String[tmp.size()];
				for (int i = 0; i < ShopNames.length; i++) {
					ShopNames[i] = tmp.get(i).getRname();
				}
				shopListView.getItems().clear();
				shopListView.getItems().addAll(ShopNames);
				shopListView.setOnMouseClicked(new EventHandler<MouseEvent>() {

					@Override
					public void handle(MouseEvent event) {
						if (shopListView.getSelectionModel().getSelectedItem() != null) {
							Container.setShopInfo(tmp.get(shopListView.getSelectionModel().getSelectedIndex()));
							FXMLLoader loader = new FXMLLoader(getClass().getResource("memberMenu.fxml"));
							
							try {
								
								root = loader.load();
								memberMenuController mmc = loader.getController();
								mmc.setBackPage("memberSearch.fxml");
							} catch (IOException e) {
								e.printStackTrace();
							}
							stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
							scene = new Scene(root);
							stage.setScene(scene);
							stage.show();
						}
					}
				});
			} else {
				shopListView.getItems().clear();
			}
		}
	}

	/**
	 * search the shop by type
	 */
	public void byType() {
		discountCheckBox.setSelected(false);
		for (int i = 0; i < allTypeSize; i++) {
			if (RBarr[i].isSelected()) {
				type = MapType.get(RBarr[i]);
			}
		}
		Container.setTypeShopInfos(Member.searchByType(type));
		tmp = new ArrayList<>();
		if ((Container.getShopInfos() != null) && (!Container.getShopInfos().isEmpty())) {
			for (int i = 0; i < Container.getShopInfos().size(); i++) {
				for (int j = 0; j < Container.getTypeShopInfos().size(); j++) {
					if (Container.getShopInfos().get(i).getId().equals(Container.getTypeShopInfos().get(j).getId())) {
						tmp.add(Container.getShopInfos().get(i));
					}
				}
			}
		} else {
			tmp.addAll(Container.getTypeShopInfos());
		}
		ShopNames = new String[tmp.size()];
		for (int i = 0; i < ShopNames.length; i++) {
			ShopNames[i] = tmp.get(i).getRname();
		}
		shopListView.getItems().clear();
		shopListView.getItems().addAll(ShopNames);
		shopListView.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				if (shopListView.getSelectionModel().getSelectedItem() != null) {
					Container.setShopInfo(tmp.get(shopListView.getSelectionModel().getSelectedIndex()));
					FXMLLoader loader = new FXMLLoader(getClass().getResource("memberMenu.fxml"));
					
					try {
						
						root = loader.load();
						memberMenuController mmc = loader.getController();
						mmc.setBackPage("memberSearch.fxml");
					} catch (IOException e) {
						e.printStackTrace();
					}
					stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
					scene = new Scene(root);
					stage.setScene(scene);
					stage.show();
				}
			}
		});
	}

	/**
	 * get all type
	 */
	public void allType() {
		discountCheckBox.setSelected(false);
		if (searchTextField.getText()==null || searchTextField.getText().isEmpty()) {
			shopListView.getItems().clear();
			tmp.clear();
		} else {
			name = searchTextField.getText();
			Container.setShopInfos(Member.searchByName(name));
			tmp = new ArrayList<>();
			if (Container.getShopInfos().isEmpty()) {
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("ERROR");
				alert.setHeaderText("查無結果!!");
				alert.showAndWait();
			} else {
				tmp.addAll(Container.getShopInfos());
				ShopNames = new String[tmp.size()];
				for (int i = 0; i < ShopNames.length; i++) {
					ShopNames[i] = tmp.get(i).getRname();
				}
				shopListView.getItems().clear();
				shopListView.getItems().addAll(ShopNames);
				shopListView.setOnMouseClicked(new EventHandler<MouseEvent>() {
					@Override
					public void handle(MouseEvent event) {
						if (shopListView.getSelectionModel().getSelectedItem() != null) {
							Container.setShopInfo(tmp.get(shopListView.getSelectionModel().getSelectedIndex()));
							FXMLLoader loader = new FXMLLoader(getClass().getResource("memberMenu.fxml"));
							
							try {
								
								root = loader.load();
								memberMenuController mmc = loader.getController();
								mmc.setBackPage("memberSearch.fxml");
							} catch (IOException e) {
								e.printStackTrace();
							}
							stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
							scene = new Scene(root);
							stage.setScene(scene);
							stage.show();
						}
					}
				});
			}
		}
	}

	/**
	 * home page button be clicked change to home page.
	 * 
	 * @param event click home page button
	 * @throws Exception memberTable.fxml not found
	 */
	public void changeToHP(ActionEvent event) throws Exception {
		if (tmp != null) {
			tmp.clear();
		}
		FXMLLoader loader = new FXMLLoader(getClass().getResource("memberTable.fxml"));
		root = loader.load();
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}

	/**
	 * SHbutton be clicked change to search page.
	 * 
	 * @param event click search page button
	 * @throws Exception memberSearch.fxml not found
	 */
	public void changeToSearch(ActionEvent event) throws Exception {
		if (tmp != null) {
			tmp.clear();
		}
		FXMLLoader loader = new FXMLLoader(getClass().getResource("memberSearch.fxml"));
		root = loader.load();
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}

	/**
	 * order button be clicked change to order page.
	 * 
	 * @param event click order page button
	 * @throws Exception memberOrder.fxml not found
	 */
	public void changeToOrder(ActionEvent event) throws Exception {
		if (tmp != null) {
			tmp.clear();
		}
		FXMLLoader loader = new FXMLLoader(getClass().getResource("memberOrder.fxml"));
		root = loader.load();
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}

	/**
	 * info button be clicked change to information page.
	 * 
	 * @param event click info page button
	 * @throws Exception memberInformation.fxml not found
	 */
	public void changeToInfo(ActionEvent event) throws Exception {
		if (tmp != null) {
			tmp.clear();
		}
		FXMLLoader loader = new FXMLLoader(getClass().getResource("memberInformation.fxml"));
		root = loader.load();
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}

	/**
	 * logout button be clicked, notice user. Back to identity page.
	 * 
	 * @param event click logout button
	 * @throws Exception identity.fxml not found
	 */
	public void logout(ActionEvent event) throws Exception {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("logout");
		alert.setHeaderText("將要登出!!");
		alert.setContentText("確定要登出嗎?");
		if (alert.showAndWait().get() == ButtonType.OK) {
			System.out.println("成功登出!!");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("identity.fxml"));
			root = loader.load();
			stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
			scene = new Scene(root);
			stage.setScene(scene);
			stage.show();
		}

	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		myScrollPane.setVvalue(-0.5);
		RadioButton[] RBall = { JP, CH, WT, SF, ND, BD, DS, CK, DR, OT };
		RBarr = RBall;
		for (int i = 0; i < allTypeSize; i++) {
			MapType.put(RBarr[i], allType[i]);
		}
		searchTextField.setOnKeyPressed(new EventHandler<KeyEvent>() {
			@Override
			public void handle(KeyEvent ke) {
				if (ke.getCode().equals(KeyCode.ENTER)) {
					search();
				}
			}
		});
		if ((tmp != null) && (!tmp.isEmpty())) {
			ShopNames = new String[tmp.size()];
			for (int i = 0; i < ShopNames.length; i++) {
				ShopNames[i] = tmp.get(i).getRname();
			}
			searchTextField.setText(name);
			shopListView.getItems().clear();
			shopListView.getItems().addAll(ShopNames);
			shopListView.setOnMouseClicked(new EventHandler<MouseEvent>() {

				@Override
				public void handle(MouseEvent event) {
					if (shopListView.getSelectionModel().getSelectedItem() != null) {
						Container.setShopInfo(tmp.get(shopListView.getSelectionModel().getSelectedIndex()));
						FXMLLoader loader = new FXMLLoader(getClass().getResource("memberMenu.fxml"));
						
						try {
							
							root = loader.load();
							memberMenuController mmc = loader.getController();
							mmc.setBackPage("memberSearch.fxml");
						} catch (IOException e) {
							e.printStackTrace();
						}
						stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
						scene = new Scene(root);
						stage.setScene(scene);
						stage.show();
					}
				}
			});
		} else {
			ALL.setSelected(true);
		}

	}

}
