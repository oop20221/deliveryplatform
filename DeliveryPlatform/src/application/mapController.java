package application;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

/**
 * the controller of map.fxml.
 * User could back to previous page by clicking anywhere in map.
 * 
 * @author Gary
 *
 */
public class mapController {

	@FXML
	private Button backButton;

	@FXML
	private ImageView mapImage;

	private Stage stage;
	private Scene scene;
	private Parent root;
	private String backPage;

	/**
	 * back to order page
	 * 
	 * @param event click back button 
	 * @throws Exception 
	 */
	public void back(ActionEvent event) throws Exception {
		FXMLLoader loader = new FXMLLoader(getClass().getResource(backPage));
		root = loader.load();
		if (backPage.equals("memberOrderConfirm.fxml")) {
			memberOrderConfirmController mocc = loader.getController();
			mocc.confirmOrder();
		} else if (backPage.equals("memberOrderModel.fxml")) {
			memberOrderModelController momc = loader.getController();
			momc.setOrder();
		} else if (backPage.equals("shopordermodel.fxml")) {
			shopOrderModelController somc = loader.getController();
			somc.setOrder();
		} else if (backPage.equals("deliveryordermodel.fxml")) {
			deliveryOrderModelController dcoc = loader.getController();
			dcoc.setOrderInfo();
		}

		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}
	
	/**
	 * set where it go after back button be clicked
	 * 
	 * @param fileName the page users want to go back
	 * @throws FileNotFoundException fileName invalid
	 */
	public void setBackPage(String fileName) throws FileNotFoundException {
		this.backPage = fileName;
		Image image = new Image(new FileInputStream(new File("src\\image\\map.png")));
		mapImage.setImage(image);
	}
}
