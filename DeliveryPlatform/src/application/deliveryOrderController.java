package application;

import java.net.URL;
import java.util.ResourceBundle;

import com.oct.dp.delivery.Deliveryman;
import com.oct.dp.model.DeliveryInfo;

import container.Container;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.stage.Stage;

/**
 * delivery order page that delivery man could check all orders.
 * 
 * @author Gary
 *
 */
public class deliveryOrderController implements Initializable {
	@FXML
	private Label deliveryNameLabel, uncheckLabel, checkingLabel, historyLabel;

	@FXML
	private Button firstPageButton, informationButton, orderButton, logoutButton, uncheckButton, checkingButton,
			historyButton, refreshButton;

	private Stage stage;
	private Scene scene;
	private Parent root;

	private String name;
	private int uncheckedOrderNum;
	private int checkingOrderNum;
	private int historyOrderNum;

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		DeliveryInfo deliveryInfo = Container.getDeliveryInfo();
		name = deliveryInfo.getName();
		deliveryNameLabel.setText(name);
		uncheckedOrderNum = Deliveryman.getReadyOrderInfo().size();
		checkingOrderNum = Deliveryman.getDeliveryOrderInfo(deliveryInfo).size();
		historyOrderNum = Deliveryman.history(deliveryInfo).size();
		uncheckLabel.setText(Integer.toString(uncheckedOrderNum));
		checkingLabel.setText(Integer.toString(checkingOrderNum));
		historyLabel.setText(Integer.toString(historyOrderNum));
	}

	/**
	 * refresh orders number
	 * 
	 * @param event click on refresh button
	 */
	public void refresh(ActionEvent event) {
		DeliveryInfo deliveryInfo = Container.getDeliveryInfo();
		uncheckedOrderNum = Deliveryman.getReadyOrderInfo().size();
		checkingOrderNum = Deliveryman.getDeliveryOrderInfo(deliveryInfo).size();
		historyOrderNum = Deliveryman.history(deliveryInfo).size();
		uncheckLabel.setText(Integer.toString(uncheckedOrderNum));
		checkingLabel.setText(Integer.toString(checkingOrderNum));
		historyLabel.setText(Integer.toString(historyOrderNum));
	}

	/**
	 * check button of unchecked orders be clicked, change to unchecked orders page.
	 * 
	 * @param event click on check uncheckedOrder button
	 * @throws Exception deliveryorderunchecked.fxml not found
	 */
	public void uncheckedOrder(ActionEvent event) throws Exception {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("deliveryorderunchecked.fxml"));
		root = loader.load();
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}

	/**
	 * check button of checking orders be clicked, change to checking order page.
	 * 
	 * @param event click on check checkingOrder button
	 * @throws Exception deliveryorderchecking.fxml not found
	 */
	public void checkingOrder(ActionEvent event) throws Exception {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("deliveryorderchecking.fxml"));
		root = loader.load();
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}

	/**
	 * check button of history orders be clicked, change to history order page
	 * 
	 * @param event click on check history order page button
	 * @throws Exception deliveryorderhistory.fxml not found
	 */
	public void historyOrder(ActionEvent event) throws Exception {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("deliveryorderhistory.fxml"));
		root = loader.load();
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}

	/**
	 * home page button be clicked, change to home page
	 * 
	 * @param event click on home page button
	 * @throws Exception deliverytable.fxml not found
	 */
	public void changeToHP(ActionEvent event) throws Exception {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("deliverytable.fxml"));
		root = loader.load();
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}

	/**
	 * order button be clicked, change to order page
	 * 
	 * @param event click on order button
	 * @throws Exception deliveryorder.fxml not found
	 */
	public void changeToOrder(ActionEvent event) throws Exception {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("deliveryorder.fxml"));
		root = loader.load();
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}

	/**
	 * info button be clicked, change to information page
	 * 
	 * @param event click on information page button
	 * @throws Exception deliveryinformation.fxml not found
	 */
	public void changeToInfo(ActionEvent event) throws Exception {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("deliveryinformation.fxml"));
		root = loader.load();
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}

	/**
	 * logout button be clicked, notice user. Back to identity page.
	 * 
	 * @param event click on back page
	 * @throws Exception identity.fxml not found
	 */
	public void logout(ActionEvent event) throws Exception {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("logout");
		alert.setHeaderText("將要登出!!");
		alert.setContentText("確定要登出嗎?");
		if (alert.showAndWait().get() == ButtonType.OK) {
			System.out.println("成功登出!!");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("identity.fxml"));
			root = loader.load();
			stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
			scene = new Scene(root);
			stage.setScene(scene);
			stage.show();
		}

	}
}
