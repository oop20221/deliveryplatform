package application;

import com.oct.dp.model.FoodInfo;
import com.oct.dp.model.ShopInfo;
import com.oct.dp.shop.ShopOwner;

import container.Container;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

/**
 * menu of shop
 * 
 * @author Gary
 *
 */
public class shopMenuController{

	@FXML
	private Label shopNameLabel;

	@FXML
	private TableView<FoodInfo> testTableViews;

	@FXML
	private Button firstPageButton, informationButton, orderButton, menuButton, logoutButton, editButton;

	@FXML
	private ScrollPane myScrollPane;
	
	private Stage stage;
	private Scene scene;
	private Parent root;
	private String Rname;

	/**
	 * set menu
	 * 
	 * @param event click on check menu button
	 */
	public void setMenu(ActionEvent event) {
		myScrollPane.setVvalue(0);
		ShopInfo shopInfo = Container.getShopInfo();
		this.Rname = shopInfo.getRname();
		shopNameLabel.setText(Rname);
		
		Container.setFoodInfos(ShopOwner.getFoodInfo(shopInfo));
		
		TableColumn<FoodInfo, String> column1 = new TableColumn<>("品項");
		column1.setCellValueFactory(new PropertyValueFactory<>("Name"));
		TableColumn<FoodInfo, String> column2 = new TableColumn<>("單價");
		column2.setCellValueFactory(new PropertyValueFactory<>("Price"));
		testTableViews.getColumns().set(0,column1);
		testTableViews.getColumns().set(1,column2);
		
		for (int i = 0; i < Container.getFoodSize(); i++) {
			
			testTableViews.getItems().add(Container.getFoodInfos().get(i));
		}
		
		testTableViews.prefHeightProperty().bind(Bindings.max(2, Bindings.size(testTableViews.getItems())).multiply(30)
				.add(20).add(10));
		testTableViews.minHeightProperty().bind(testTableViews.prefHeightProperty());
		testTableViews.maxHeightProperty().bind(testTableViews.prefWidthProperty());
		for (TableColumn<FoodInfo, ?> i : testTableViews.getColumns()) {
			i.prefWidthProperty().bind(new SimpleIntegerProperty(179));
			i.minWidthProperty().bind(i.prefWidthProperty());
			i.maxWidthProperty().bind(i.prefWidthProperty());
		}
	}

	/**
	 * change to edit menu page.
	 * 
	 * @param event click edit button
	 * @throws Exception shopMenuEdit.fxml not found
	 */
	public void edit(ActionEvent event) throws Exception {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("shopMenuEdit.fxml"));
		root = loader.load();
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}
	/**
	 * home page button be clicked
	 * change to home page.
	 * 
	 * @param event click home page button
	 * @throws Exception shoptable.fxml not found
	 */
	public void changeToHP(ActionEvent event) throws Exception {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("shoptable.fxml"));
		root = loader.load();
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}

	/**
	 * menu button be clicked
	 * change to menu page.
	 * 
	 * @param event click menu button
	 * @throws Exception shopmenu.fxml not found
	 */
	public void changeToMenu(ActionEvent event) throws Exception {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("shopmenu.fxml"));
		root = loader.load();
		shopMenuController shopmenucontroller = loader.getController();
		shopmenucontroller.setMenu(event);
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}

	/**
	 * order button be clicked
	 * change to order page.
	 * 
	 * @param event click order button
	 * @throws Exception shoporder.fxml not found
	 */
	public void changeToOrder(ActionEvent event) throws Exception {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("shoporder.fxml"));
		root = loader.load();
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}

	/**
	 * info button be clicked
	 * change to information page.
	 * 
	 * @param event click information button
	 * @throws Exception shopinfomation.fxml not found
	 */
	public void changeToInfo(ActionEvent event) throws Exception {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("shopinfomation.fxml"));
		root = loader.load();
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}

	/**
	 * logout button be clicked, notice user. Back to identity page.
	 * 
	 * @param event click logout button
	 * @throws Exception identity.fxml not found
	 */
	public void logout(ActionEvent event) throws Exception {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("logout");
		alert.setHeaderText("將要登出!!");
		alert.setContentText("確定要登出嗎?");
		if (alert.showAndWait().get() == ButtonType.OK) {
			System.out.println("成功登出!!");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("identity.fxml"));
			root = loader.load();
			stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
			scene = new Scene(root);
			stage.setScene(scene);
			stage.show();
		}

	}
}
