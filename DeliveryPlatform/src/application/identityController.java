package application;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

/**
 * select an identity
 * member, shop owner or delivery 
 * 
 * @author Gary
 *
 */
public class identityController {

	@FXML
	private Button ShopButton;
	@FXML
	private Button MemberButton;
	@FXML
	private Button DeliveryButton;

	private Stage stage;
	private Scene scene;
	private Parent root;

	/**
	 * user select member
	 * 
	 * @param event click member button 
	 * @throws Exception login.fxml cannot be found.
	 */
	public void memberidentity(ActionEvent event) throws Exception {

		String identity = MemberButton.getText();
		FXMLLoader loader = new FXMLLoader(getClass().getResource("login.fxml"));
		root = loader.load();
		loginController logoncontroller = loader.getController();
		logoncontroller.setIdentity(identity);
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}

	/**
	 * user select shop
	 * 
	 * @param event click shop button
	 * @throws Exception login.fxml cannot be found.
	 */
	public void shopidentity(ActionEvent event) throws Exception {

		String identity = ShopButton.getText();
		FXMLLoader loader = new FXMLLoader(getClass().getResource("login.fxml"));
		root = loader.load();
		loginController logoncontroller = loader.getController();
		logoncontroller.setIdentity(identity);
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}

	/**
	 * user select delivery
	 * 
	 * @param event click delivery button
	 * @throws Exception login.fxml cannot be found.
	 */
	public void deliveryidentity(ActionEvent event) throws Exception {

		String identity = DeliveryButton.getText();
		FXMLLoader loader = new FXMLLoader(getClass().getResource("login.fxml"));
		root = loader.load();
		loginController logoncontroller = loader.getController();
		logoncontroller.setIdentity(identity);
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}
}
