package application;

import java.net.URL;
import java.util.ResourceBundle;

import com.oct.dp.delivery.Deliveryman;
import com.oct.dp.model.DeliveryInfo;

import container.Container;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

/**
 * Information of delivery
 * 
 * @author Gary
 *
 */
public class deliveryInfoController implements Initializable {

	@FXML
	private Label accountText;

	@FXML
	private Button firstPageButton, informationButton, orderButton, updateButton, logoutButton, deleteButton;

	@FXML
	private TextField nickNameText, passwordText, emailText, phoneText, nameText;

	private Stage stage;
	private Scene scene;
	private Parent root;

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		DeliveryInfo deliveryInfo = Container.getDeliveryInfo();
		nickNameText.setText(deliveryInfo.getName());
		accountText.setText(deliveryInfo.getAccount());
//		passwordText.setText(deliveryInfo.getPassword());
		emailText.setText(deliveryInfo.getEmail());
		phoneText.setText(deliveryInfo.getPhone());
		nameText.setText(deliveryInfo.getUserName());
		
		passwordText.setOnMouseClicked(new EventHandler<MouseEvent>() {
			public void handle(MouseEvent arg0) {
				TextInputDialog textInputDialog = new TextInputDialog("請輸入密碼");
				textInputDialog.setTitle("輸入舊密碼");
				textInputDialog.setHeaderText("輸入原先密碼");
				textInputDialog.setContentText("您的舊密碼是？");
				textInputDialog.showAndWait();
				DeliveryInfo old = Deliveryman.login(Container.getDeliveryInfo().getAccount(), textInputDialog.getResult());
				if (old != null) {
					Container.setDeliveryInfo(old);
					TextInputDialog textInputDialog2 = new TextInputDialog("請輸入新密碼");
					textInputDialog2.setTitle("輸入新密碼");
					textInputDialog2.setHeaderText("輸入新密碼");
					textInputDialog2.setContentText("您的新密碼是？");
					textInputDialog2.showAndWait();
					old.setPassword(textInputDialog2.getResult());
					Deliveryman.changePassword(old);
					Alert alert = new Alert(AlertType.INFORMATION);
					alert.setTitle("success");
					alert.setHeaderText("成功更改密碼!!");
					alert.showAndWait();
				} else {
					Alert alert = new Alert(AlertType.ERROR);
					alert.setTitle("error");
					alert.setHeaderText("密碼錯誤!!");
					alert.setContentText("重新輸入密碼");
					alert.showAndWait();
				}
			}
		});
	}

	/**
	 * update information into database.
	 * 
	 * @param event click on update button 
	 * @throws Exception update failed.
	 */
	public void update(ActionEvent event) throws Exception {
		DeliveryInfo deliveryInfo = Container.getDeliveryInfo();
		deliveryInfo.setName(nickNameText.getText());
//		deliveryInfo.setPassword(passwordText.getText());
		deliveryInfo.setEmail(emailText.getText());
		deliveryInfo.setPhone(phoneText.getText());
		deliveryInfo.setUserName(nameText.getText());
		Deliveryman.update(deliveryInfo);
		Deliveryman.changePassword(deliveryInfo);

		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("update");
		alert.setHeaderText("將要更新!!");
		alert.setContentText("確定要更新嗎?");
		if (alert.showAndWait().get() == ButtonType.OK) {
			System.out.println("成功更新!!");
			changeToInfo(event);
		}
	}

	/**
	 * home page button be clicked.
	 * Delivery could change to home page.
	 * 
	 * @param event click on home page button
	 * @throws Exception deliverytable.fxml not found
	 */
	public void changeToHP(ActionEvent event) throws Exception {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("deliverytable.fxml"));
		root = loader.load();
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}

	/**
	 * order button be clicked.
	 * Delivery could change to order page.
	 * 
	 * @param event click on order button.
	 * @throws Exception deliveryorder.fxml not found.
	 */
	public void changeToOrder(ActionEvent event) throws Exception {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("deliveryorder.fxml"));
		root = loader.load();
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}

	/**
	 * info button be clicked.
	 * Delivery change to information page.
	 * 
	 * @param event click on info page
	 * @throws Exception deliveryinformation.fxml not found.
	 */
	public void changeToInfo(ActionEvent event) throws Exception {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("deliveryinformation.fxml"));
		root = loader.load();
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}

	/**
	 * logout button be clicked, notice user. Back to identity page.
	 * 
	 * @param event click on logout button.
	 * @throws Exception identity.fxml not found.
	 */
	public void logout(ActionEvent event) throws Exception {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("logout");
		alert.setHeaderText("將要登出!!");
		alert.setContentText("確定要登出嗎?");
		if (alert.showAndWait().get() == ButtonType.OK) {
			System.out.println("成功登出!!");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("identity.fxml"));
			root = loader.load();
			stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
			scene = new Scene(root);
			stage.setScene(scene);
			stage.show();
		}

	}
	
	/**
	 * delivery could delete their own account.
	 * and clear data from database.
	 * 
	 * @param event click on delete button.
	 * @throws Exception delete failed or identity.fxml not found.
	 */
	public void delete(ActionEvent event)throws Exception{
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("delete");
		alert.setHeaderText("將要註銷!!");
		alert.setContentText("確定要註銷嗎?");
		if (alert.showAndWait().get() == ButtonType.OK) {
			Deliveryman.delete(Container.getDeliveryInfo());
			System.out.println("成功註銷!!");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("identity.fxml"));
			root = loader.load();
			stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
			scene = new Scene(root);
			stage.setScene(scene);
			stage.show();
		}
	}

}
