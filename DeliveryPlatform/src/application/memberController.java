package application;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import com.oct.dp.member.Member;

import container.Container;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollPane;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

/**
 * home page of member member can check the restaurant which has discount.
 * 
 * @author Gary
 *
 */
public class memberController implements Initializable {

	@FXML
	private ListView<String> allShopListView;

	@FXML
	private Button firstPageButton, informationButton, orderButton, SHButton, logoutButton;

	@FXML
	private ScrollPane myScrollPane;

	private Stage stage;
	private Scene scene;
	private Parent root;

	private String[] ShopNames;

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		myScrollPane.setVvalue(-0.1);

		Container.setShopInfos(Member.searchByDiscount());
		ShopNames = new String[Container.getShopInfos().size()];
		for (int i = 0; i < ShopNames.length; i++) {
			ShopNames[i] = Container.getShopInfos().get(i).getRname();
		}
		allShopListView.getItems().addAll(ShopNames);
		allShopListView.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				if (allShopListView.getSelectionModel().getSelectedItem() != null) {
					Container.setShopInfo(
							Container.getShopInfos().get(allShopListView.getSelectionModel().getSelectedIndex()));
					FXMLLoader loader = new FXMLLoader(getClass().getResource("memberMenu.fxml"));

					try {
						root = loader.load();
						memberMenuController mmc = loader.getController();
						mmc.setBackPage("memberTable.fxml");

					} catch (IOException e) {
						e.printStackTrace();
					}
					stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
					scene = new Scene(root);
					stage.setScene(scene);
					stage.show();
				}
			}
		});
	}

	/**
	 * home page button be clicked change to home page.
	 * 
	 * @param event click home page button
	 * @throws Exception memberTable.fxml not found
	 */
	public void changeToHP(ActionEvent event) throws Exception {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("memberTable.fxml"));
		root = loader.load();
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}

	/**
	 * SHbutton be clicked change to search page.
	 * 
	 * @param event click search page button
	 * @throws Exception memberSearch.fxml not found
	 */
	public void changeToSearch(ActionEvent event) throws Exception {

		Container.setShopInfos(null);
		FXMLLoader loader = new FXMLLoader(getClass().getResource("memberSearch.fxml"));
		root = loader.load();
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}

	/**
	 * order button be clicked change to order page.
	 * 
	 * @param event click order page button
	 * @throws Exception memberOrder.fxml not found
	 */
	public void changeToOrder(ActionEvent event) throws Exception {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("memberOrder.fxml"));
		root = loader.load();
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}

	/**
	 * info button be clicked change to information page.
	 * 
	 * @param event click info page button
	 * @throws Exception memberInformation.fxml not found
	 */
	public void changeToInfo(ActionEvent event) throws Exception {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("memberInformation.fxml"));
		root = loader.load();
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}

	/**
	 * logout button be clicked, notice user. Back to identity page.
	 * 
	 * @param event click logout button
	 * @throws Exception identity.fxml not found
	 */
	public void logout(ActionEvent event) throws Exception {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("logout");
		alert.setHeaderText("將要登出!!");
		alert.setContentText("確定要登出嗎?");
		if (alert.showAndWait().get() == ButtonType.OK) {
			System.out.println("成功登出!!");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("identity.fxml"));
			root = loader.load();
			stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
			scene = new Scene(root);
			stage.setScene(scene);
			stage.show();
		}

	}
}
