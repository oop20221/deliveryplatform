package application;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import com.oct.dp.model.OrderInfo;
import com.oct.dp.model.ShopInfo;
import com.oct.dp.shop.ShopOwner;

import container.Container;
import container.OrderContainer;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollPane;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

/**
 * a list for checking orders
 * shop owner can see each order's detail by clicked on list.
 * 
 * @author Gary
 *
 */
public class shopOrderCheckingController implements Initializable {
	
	@FXML
	private Label orderTypeLabel;

	@FXML
	private Button backButton;

	@FXML
	private ListView<String> orderListView;
	
	@FXML
	private ScrollPane myScrollPane;

	private Stage stage;
	private Scene scene;
	private Parent root;
	private String[] Orders;
	private String orderType;
	private OrderInfo currentOrderInfo;

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		myScrollPane.setVvalue(0);
		ShopInfo shopInfo = Container.getShopInfo();
		orderType = "追蹤訂單";
		orderTypeLabel.setText(orderType);
		OrderContainer.setOrderInfos(ShopOwner.getOrderInfo(shopInfo));
		Orders = new String[OrderContainer.getSize()];
		for (int i = 0; i < Orders.length; i++) {
			Orders[i] = OrderContainer.getOrderInfos().get(i).getState().toString() + "\t\t"
					+ OrderContainer.getOrderInfos().get(i).getUserName() + "\t\t"
					+ OrderContainer.getOrderInfos().get(i).getDeliveryName();
		}
		orderListView.getItems().addAll(Orders);
		orderListView.setOnMouseClicked(new EventHandler<MouseEvent>() {
			
			@Override
			public void handle(MouseEvent event) {
				if(orderListView.getSelectionModel().getSelectedItem()!=null) {
				FXMLLoader loader = new FXMLLoader(getClass().getResource("shopordermodel.fxml"));
				try {
					root = loader.load();
				} catch (IOException e) {
					e.printStackTrace();
				}
				shopOrderModelController shopordermodelcontroller = loader.getController();
				currentOrderInfo = OrderContainer.getOrderInfos().get(orderListView.getSelectionModel().getSelectedIndex());
				shopordermodelcontroller.setOrder(currentOrderInfo);
				stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
				scene = new Scene(root);
				stage.setScene(scene);
				stage.show();
			}}
		});
	}

	/**
	 * back to shopOrder page
	 * 
	 * @param event click back button
	 * @throws Exception shoporder.fxml not found
	 */
	public void back(ActionEvent event) throws Exception {

		FXMLLoader loader = new FXMLLoader(getClass().getResource("shoporder.fxml"));
		root = loader.load();
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}

}
