package application;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import com.oct.dp.delivery.Deliveryman;
import com.oct.dp.model.DeliveryInfo;
import com.oct.dp.model.OrderInfo;

import container.Container;
import container.OrderContainer;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

/**
 * a list of delivering orders to be check by delivery. 
 * 
 * @author Gary
 *
 */
public class deliveryOrderCheckingController implements Initializable {

	@FXML
	private Label orderTypeLabel;

	@FXML
	private Button backButton;

	@FXML
	private ListView<String> orderListView;

	private Stage stage;
	private Scene scene;
	private Parent root;
	private String[] Orders;
	private String orderType;
	private OrderInfo currentOrderInfo;

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		DeliveryInfo deliveryInfo = Container.getDeliveryInfo();
		orderType = "追蹤訂單";
		orderTypeLabel.setText(orderType);
		OrderContainer.setOrderInfos(Deliveryman.getDeliveryOrderInfo(deliveryInfo));
		Orders = new String[OrderContainer.getSize()];
		for (int i = 0; i < Orders.length; i++) {
			Orders[i] =  OrderContainer.getOrderInfos().get(i).getState().toString() + "\t\t"
					+ OrderContainer.getOrderInfos().get(i).getUserName() + "\t\t"
					+ OrderContainer.getOrderInfos().get(i).getRestName();
		}
		orderListView.getItems().addAll(Orders);
		orderListView.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				if(orderListView.getSelectionModel().getSelectedItem()!=null) {
				FXMLLoader loader = new FXMLLoader(getClass().getResource("deliveryordermodel.fxml"));	
				try {
					root = loader.load();
				} catch (IOException e) {
					e.printStackTrace();
				}
				deliveryOrderModelController deliveryordermodelcontroller = loader.getController();
				currentOrderInfo = OrderContainer.getOrderInfos().get(orderListView.getSelectionModel().getSelectedIndex());
				try {
					deliveryordermodelcontroller.setOrderInfo(currentOrderInfo);
				} catch (IOException e) {
					e.printStackTrace();
				}
				stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
				scene = new Scene(root);
				stage.setScene(scene);
				stage.show();
			}}
		});
	}

	/**
	 * back to delivery Order page
	 * 
	 * @param event click on back button.
	 * @throws Exception deliveryorder.fxml not found.
	 */
	public void back(ActionEvent event) throws Exception {

		FXMLLoader loader = new FXMLLoader(getClass().getResource("deliveryorder.fxml"));
		root = loader.load();
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}

}
