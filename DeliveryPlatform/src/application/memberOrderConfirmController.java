package application;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import com.oct.dp.map.Direction;
import com.oct.dp.map.GoogleMap;
import com.oct.dp.member.Member;
import com.oct.dp.model.MemberInfo;
import com.oct.dp.model.OrderInfo;
import com.oct.dp.model.OrderInfo.Detail;
import com.oct.dp.model.OrderInfo.OrderState;

import api.BalanceNotEnoughException;
import api.BankAPI;
import api.TokenExpiredException;

import com.oct.dp.model.ShopInfo;

import container.Container;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

/**
 * member can check the order detail after picking the food and confirm it or go
 * back to reorder.
 * 
 * @author Gary
 *
 */
public class memberOrderConfirmController {

	@FXML
	private Label stateText, shopText, memberText, arriveTimeText, foodtotalText, discountText, VIPText, feeText,
			orderTimeText, totalText, deliveryText;

	@FXML
	private Button botButton, mapButton, confirmButton, backButton;

	@FXML
	private TableView<Detail> orderListView;

	@FXML
	private TextField addressTextField;

	@FXML
	private ScrollPane myScrollPane;

	@FXML
	private AnchorPane myPane;

	@FXML
	private RadioButton cashRButton, onlineRButton;

	@FXML
	private AnchorPane myAnchorPane;

	private Stage stage;
	private Scene scene;
	private Parent root;
	private static OrderInfo orderInfo;

	private static int fee;
	private static int total;
	private static String duration;
	private static String VIPfee;
	private static boolean isFeeFree;

	private String orderTime;
	private String address;
	private int shopDiscount;

	/**
	 * set the order information
	 * 
	 * @param myOrderInfo set what member ordered at menu page.
	 * @throws IOException
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeyException
	 */
	public void confirmOrder(OrderInfo myOrderInfo) throws InvalidKeyException, NoSuchAlgorithmException, IOException {
		myScrollPane.setVvalue(-0.1);
		MemberInfo memberInfo = Container.getMemberInfo();
		ShopInfo shopInfo = Container.getShopInfo();
		orderInfo = myOrderInfo;
		stateText.setText("未成立");
		shopText.setText(orderInfo.getRestName());
		memberText.setText(orderInfo.getUserName());

		if (shopInfo.getDiscount() != 0) {
			if (orderInfo.getTotal() >= (shopInfo.getTargetcost())) {
				shopDiscount = (int) (orderInfo.getTotal() * (1 - shopInfo.getDiscount()));
				discountText.setText("優惠抵免" + shopDiscount);
			} else {
				discountText.setText("未達折扣標準：" + shopInfo.getTargetcost() + "元");
			}
		} else {
			discountText.setText("店家無優惠");
		}

		TableColumn<Detail, String> column1 = new TableColumn<>("品項");
		column1.setCellValueFactory(new PropertyValueFactory<>("product_name"));
		TableColumn<Detail, String> column2 = new TableColumn<>("單價");
		column2.setCellValueFactory(new PropertyValueFactory<>("unit_price"));
		TableColumn<Detail, String> column3 = new TableColumn<>("數量");
		column3.setCellValueFactory(new PropertyValueFactory<>("amount"));
		TableColumn<Detail, String> column4 = new TableColumn<>("價格");
		column4.setCellValueFactory(new PropertyValueFactory<>("price"));
		orderListView.getColumns().set(0, column1);
		orderListView.getColumns().set(1, column2);
		orderListView.getColumns().set(2, column3);
		orderListView.getColumns().set(3, column4);
		for (int i = 0; i < orderInfo.getDetails().size(); i++) {
			orderListView.getItems().add(orderInfo.getDetails().get(i));
		}
		orderListView.prefHeightProperty()
				.bind(Bindings.max(2, Bindings.size(orderListView.getItems())).multiply(30).add(20));
		orderListView.minHeightProperty().bind(orderListView.prefHeightProperty());
		orderListView.maxHeightProperty().bind(orderListView.prefWidthProperty());
		int[] width = { 175, 60, 60, 60 };
		for (int i = 0; i < orderListView.getColumns().size(); i++) {
			TableColumn<Detail, ?> index = orderListView.getColumns().get(i);
			index.prefWidthProperty().bind(new SimpleIntegerProperty(width[i]));
			index.minWidthProperty().bind(index.prefWidthProperty());
			index.maxWidthProperty().bind(index.prefWidthProperty());
		}

		myPane.setLayoutY(orderListView.getLayoutY() + orderListView.getPrefHeight() + 40);

		myAnchorPane.setPrefHeight(myPane.getLayoutY() + myPane.getPrefHeight() + 40);

		foodtotalText.setText(Integer.toString(orderInfo.getTotal()));

		address = Container.getMemberInfo().getPrefAddress();
		if (address == null) {
			addressTextField.setText(null);
		} else {
			addressTextField.setText(address);
			address = addressTextField.getText();
			Container.getMemberInfo().setPrefAddress(address);
			Member.update(memberInfo);
			Direction direction = Direction.getDirection(shopInfo.getAddress(), address);
			duration = direction.getDuration();
			arriveTimeText.setText(duration);
			String[] duration = arriveTimeText.getText().split(" ");
			if (duration.length == 2) {
				fee = 5 * Integer.parseInt(duration[0]);
			} else {
				fee = 5 * (Integer.parseInt(duration[0]) * 60 + Integer.parseInt(duration[2]));
			}

			if (memberInfo.isVip()) {
				if (orderInfo.getTotal() >= 300) {
					fee = 0;
					VIPfee = "會員免運";
					isFeeFree = true;
					VIPText.setText(VIPfee);
				} else {
					VIPfee = "未達免運標準：300元";
					isFeeFree = false;
					VIPText.setText(VIPfee);
				}
			} else {
				VIPfee = "非會員";
				isFeeFree = false;
				VIPText.setText(VIPfee);
			}

			feeText.setText(String.valueOf(fee));
			total = orderInfo.getTotal() - shopDiscount + fee;
			totalText.setText(String.valueOf(total));
		}

//		if(addressTextField.getText()!=memberInfo.getPrefAddress()) {
		// add member address after press ENTER
		addressTextField.setOnKeyPressed(new EventHandler<KeyEvent>() {
			@Override
			public void handle(KeyEvent ke) {
				if (ke.getCode().equals(KeyCode.ENTER)) {
					address = addressTextField.getText();
					Container.getMemberInfo().setPrefAddress(address);
					Member.update(memberInfo);
					Direction direction;
					try {
						direction = Direction.getDirection(shopInfo.getAddress(), address);
						duration = direction.getDuration();
						arriveTimeText.setText(duration);
					} catch (IOException e) {
						Alert info = new Alert(AlertType.ERROR);
						info.setTitle("error");
						info.setHeaderText("地址無效");
						info.setContentText("請重新輸入地址");
						info.showAndWait();
					}
					String[] duration = arriveTimeText.getText().split(" ");
					if (duration.length == 2) {
						fee = 5 * Integer.parseInt(duration[0]);
					} else {
						fee = 5 * (Integer.parseInt(duration[0]) * 60 + Integer.parseInt(duration[2]));
					}

					if (memberInfo.isVip()) {
						if (orderInfo.getTotal() >= 300) {
							fee = 0;
							VIPfee = "會員免運";
							isFeeFree = true;
							VIPText.setText(VIPfee);
						} else {
							VIPfee = "未達免運標準：300元";
							isFeeFree = false;
							VIPText.setText(VIPfee);
						}
					} else {
						VIPfee = "非會員";
						isFeeFree = false;
						VIPText.setText(VIPfee);
					}

					feeText.setText(String.valueOf(fee));
					total = orderInfo.getTotal() - shopDiscount + fee;
					totalText.setText(String.valueOf(total));
				}
			}
		});
//		}

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		orderTime = dtf.format(LocalDateTime.now());
		orderTimeText.setText(orderTime);

		botButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
			public void handle(MouseEvent arg0) {
				myScrollPane.setVvalue(1);
			}
		});
	}

	/**
	 * overloading confirmOrder method to set data when other page back to this
	 * page.
	 * 
	 * @throws InvalidKeyException
	 * @throws NoSuchAlgorithmException
	 * @throws IOException
	 */
	public void confirmOrder() throws InvalidKeyException, NoSuchAlgorithmException, IOException {
		if (orderInfo != null) {
			confirmOrder(orderInfo);
			addressTextField.setText(address);
//			feeText.setText(String.valueOf(fee));
//			VIPText.setText(VIPfee);
//			totalText.setText(String.valueOf(total));
//			arriveTimeText.setText(duration);
		}
	}

	/**
	 * confirm the order and set state to WAITING for shop selecting.
	 * 
	 * @param event click on confirm Button
	 * @throws Exception memberTable.fxml cannot be found.
	 */
	public void confirm(ActionEvent event) throws Exception {
		orderInfo.setState(OrderState.WAITING);
		orderInfo.setTotal(total);
		orderInfo.setArrivetime(null);
		orderInfo.setOrdertime(Timestamp.valueOf(LocalDateTime.now()));
		orderInfo.setDestination(addressTextField.getText());
		orderInfo.setUserID(Container.getMemberInfo().getId());
		orderInfo.setRestID(Container.getShopInfo().getId());
		orderInfo.setRestName(Container.getShopInfo().getRname());
		orderInfo.setUserName(Container.getMemberInfo().getUserName());
		orderInfo.setDiscount(shopDiscount);
		orderInfo.setFee(fee);
		orderInfo.setFeeFree(isFeeFree);

		if (orderInfo.getTotal() == 0) {
			Alert info = new Alert(AlertType.INFORMATION);
			info.setTitle("order fail");
			info.setHeaderText("請輸入地址");
			info.showAndWait();
		} else {

			if (cashRButton.isSelected()) {
				Alert info = new Alert(AlertType.INFORMATION);
				info.setTitle("order success");
				info.setHeaderText("下單成功");
				info.setContentText("取餐時付款，共" + orderInfo.getTotal() + "元");
				info.showAndWait();
				Member.order(orderInfo);
				FXMLLoader loader = new FXMLLoader(getClass().getResource("memberTable.fxml"));
				root = loader.load();
				stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
				scene = new Scene(root);
				stage.setScene(scene);
				stage.show();
			} else if (onlineRButton.isSelected()) {
				if (Container.getMemberInfo().getToken() != null || Container.getMemberInfo().getToken() != null) {
					try {
						System.out.println(orderInfo.getTotal());
						double ramain = BankAPI.payBill(Container.getMemberInfo().getToken(),
								Container.getMemberInfo().getKey(), orderInfo.getTotal());
						System.out.println(orderInfo.getTotal());
						Alert info = new Alert(AlertType.INFORMATION);
						info.setTitle("order success");
						info.setHeaderText("下單成功");
						info.setContentText("已線上扣款，共" + orderInfo.getTotal() + "元,餘額：" + Double.toString(ramain));
						info.showAndWait();
						Member.order(orderInfo);
						FXMLLoader loader = new FXMLLoader(getClass().getResource("memberTable.fxml"));
						root = loader.load();
						stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
						scene = new Scene(root);
						stage.setScene(scene);
						stage.show();
					} catch (TokenExpiredException e1) {
						Alert info = new Alert(AlertType.ERROR);
						info.setTitle("error");
						info.setHeaderText("token failed");
						info.showAndWait();
					} catch (BalanceNotEnoughException e2) {
						Alert info = new Alert(AlertType.ERROR);
						info.setTitle("error");
						info.setHeaderText("餘額不足");
						info.showAndWait();
					} catch (IllegalArgumentException e3) {
						System.out.println(orderInfo.getTotal());
						Alert info = new Alert(AlertType.ERROR);
						info.setTitle("error");
						info.setHeaderText("錯誤");
						info.showAndWait();
					}
				} else {
					Alert info = new Alert(AlertType.ERROR);
					info.setTitle("error");
					info.setHeaderText("付款失敗");
					info.setContentText("請先連結銀行");
					info.showAndWait();
				}
			} 

			
		}
	}

	/**
	 * back to order menu page In order to reorder food.
	 * 
	 * @param event click on back button
	 * @throws Exception memberTable.fxml cannot be found.
	 */
	public void back(ActionEvent event) throws Exception {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("memberMenu.fxml"));
		root = loader.load();
		memberMenuController membermenucontroller = loader.getController();
		membermenucontroller.reorder(orderInfo);
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}

	/**
	 * calling map to show the location.
	 * 
	 * @param event click on showMap button
	 * @throws Exception member
	 */
	public void showMap(ActionEvent event) throws Exception {
		address = addressTextField.getText();
		GoogleMap.getMap(Container.getShopInfo().getAddress(), address, "src\\image\\map.png");
		FXMLLoader loader = new FXMLLoader(getClass().getResource("map.fxml"));
		root = loader.load();
		mapController mapcontroller = loader.getController();
		mapcontroller.setBackPage("memberOrderConfirm.fxml");
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}

}
