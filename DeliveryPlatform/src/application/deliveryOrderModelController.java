package application;

import java.io.IOException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import com.oct.dp.delivery.Deliveryman;
import com.oct.dp.map.GoogleMap;
import com.oct.dp.model.OrderInfo;
import com.oct.dp.model.OrderInfo.Detail;
import com.oct.dp.model.OrderInfo.OrderState;

import container.Container;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

/**
 * all information of an order to be check by delivery
 * 
 * @author Gary
 *
 */
public class deliveryOrderModelController {
	@FXML
	private Label OrderNumLabel, myLabel, stateText, shopText, memberText, deliveryText, totalText, orderTimeText,
			arrivedTimeText, addressText, feeText;

	@FXML
	private Button botButton, backButton, confirmButton, finishButton, showMapButton;

	@FXML
	private TableView<Detail> orderListView;

	@FXML
	private ScrollPane myScrollPane;
	
	@FXML
	private AnchorPane myPane, myAnchorPane;
	
	private Stage stage;
	private Scene scene;
	private Parent root;
	private long orderNum;
	private static OrderInfo orderInfo;

	/**
	 * set orderInfo in the page
	 * 
	 * @param myOrderInfo the orderInfo which delivery want to check.
	 * @throws IOException src\\image\\map.png not found
	 */
	public void setOrderInfo(OrderInfo myOrderInfo) throws IOException {
		
		myScrollPane.setVvalue(-2);
		orderInfo = myOrderInfo;
		Container.setShopInfo(Deliveryman.searchByID(orderInfo.getRestID()));
		this.orderNum = myOrderInfo.getId();
		OrderNumLabel.setText("#" + orderNum);
		stateText.setText(orderInfo.getState().toString());
		shopText.setText(orderInfo.getRestName());
		memberText.setText(orderInfo.getUserName());
		totalText.setText(Integer.toString(orderInfo.getTotal()));
		
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");

		LocalDateTime orderDate = orderInfo.getOrdertime().toLocalDateTime();
		String orderTime = orderDate.format(dtf);
		orderTimeText.setText(orderTime);
	
		addressText.setText(orderInfo.getDestination());
		
		TableColumn<Detail, String> column1 = new TableColumn<>("品項");
		column1.setCellValueFactory(new PropertyValueFactory<>("product_name"));
		TableColumn<Detail, String> column2 = new TableColumn<>("單價");
		column2.setCellValueFactory(new PropertyValueFactory<>("unit_price"));
		TableColumn<Detail, String> column3 = new TableColumn<>("數量");
		column3.setCellValueFactory(new PropertyValueFactory<>("amount"));
		TableColumn<Detail, String> column4 = new TableColumn<>("價格");
		column4.setCellValueFactory(new PropertyValueFactory<>("price"));
		orderListView.getColumns().set(0,column1);
		orderListView.getColumns().set(1,column2);
		orderListView.getColumns().set(2,column3);
		orderListView.getColumns().set(3,column4);
		for (int i = 0; i < orderInfo.getDetails().size(); i++) {
			orderListView.getItems().add(orderInfo.getDetails().get(i));
		}
		
		orderListView.prefHeightProperty().bind(Bindings.max(2, Bindings.size(orderListView.getItems())).multiply(30)
				.add(20).add(10));
		orderListView.minHeightProperty().bind(orderListView.prefHeightProperty());
		orderListView.maxHeightProperty().bind(orderListView.prefWidthProperty());
		int[] width = {175,60,60,60};
		for (int i = 0; i < orderListView.getColumns().size(); i++) {
			TableColumn<Detail, ?> index = orderListView.getColumns().get(i);
			index.prefWidthProperty().bind(new SimpleIntegerProperty(width[i]));
			index.minWidthProperty().bind(index.prefWidthProperty());
			index.maxWidthProperty().bind(index.prefWidthProperty());
		}
		
		String arrivedTime = null;
		if (orderInfo.getArrivetime() != null) {
			LocalDateTime localDateTime = orderInfo.getArrivetime().toLocalDateTime();
			arrivedTime = localDateTime.format(dtf);
		}
		arrivedTimeText.setText(arrivedTime);
		
		deliveryText.setText(orderInfo.getDeliveryName());
		
		feeText.setText(String.valueOf(orderInfo.getFee()));
		
		// show shop location
//		GoogleMap.getMap(Container.getShopInfo().getAddress(), orderInfo.getDestination(), "src\\image\\map.png");
//		Image image2 = new Image(new FileInputStream(new File("src\\image\\map.png")));
//		myImageView.setImage(image2);
		
		myPane.setLayoutY(orderListView.getLayoutY() + orderListView.getPrefHeight() + 40);
		
		myAnchorPane.setPrefHeight(myPane.getLayoutY() + myPane.getPrefHeight() + 40);
		
		botButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
			public void handle(MouseEvent arg0) {
				myScrollPane.setVvalue(1);
			}
		});
		
		if(orderInfo.getState()!=OrderState.PREPARING) {
			confirmButton.setVisible(false);
		}
		
		if(orderInfo.getState()!=OrderState.DELIVERING) {
			finishButton.setVisible(false);
		}
	}

	/**
	 * confirm preparing order, and if order state is not PREPARING, pop out an error window
	 * 
	 * @param event click confirm button
	 * @throws IOException deliveryorder.fxml not found
	 */
	public void confirm(ActionEvent event) throws IOException {
		if (orderInfo.getState() == OrderState.PREPARING) {
			orderInfo.setDeliveryID(Container.getDeliveryInfo().getId());
			orderInfo.setDeliveryName(Container.getDeliveryInfo().getUserName());
			Deliveryman.confirm(orderInfo);
			myLabel.setText("成功接單");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("deliveryorder.fxml"));
			root = loader.load();
			stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
			scene = new Scene(root);
			stage.setScene(scene);
			stage.show();
		} else {
			myLabel.setText("操作錯誤");
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("error");
			alert.setHeaderText("非未接訂單!!");
			alert.showAndWait();
		}
	}
	
	/**
	 * order arrived, set order state from DELIVERING to ARRIVED
	 * 
	 * @param event click arrive button 
	 * @throws IOException deliveryorder.fxml not found
	 */
	public void arrive(ActionEvent event) throws IOException {
		if (orderInfo.getState() == OrderState.DELIVERING) {
			orderInfo.setArrivetime(Timestamp.valueOf(LocalDateTime.now()));
			Deliveryman.arrive(orderInfo);
			myLabel.setText("成功送達");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("deliveryorder.fxml"));
			root = loader.load();
			stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
			scene = new Scene(root);
			stage.setScene(scene);
			stage.show();
		} else {
			myLabel.setText("操作錯誤");
			myLabel.setText("操作錯誤");
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("error");
			alert.setHeaderText("非運送中訂單!!");
			alert.showAndWait();
		}
	}
	/**
	 * back to delivery order page
	 * 
	 * @param event click back button
	 * @throws Exception deliveryorder.fxml not found
	 */
	public void back(ActionEvent event) throws Exception {

		FXMLLoader loader = new FXMLLoader(getClass().getResource("deliveryorder.fxml"));
		root = loader.load();
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}
	
	/**
	 * show member's location in map
	 * 
	 * @param event click on showMap button
	 * @throws Exception map.fxml not found
	 */
	public void showMap(ActionEvent event)throws Exception{
		GoogleMap.getMap(Container.getShopInfo().getAddress(), orderInfo.getDestination(),"src\\image\\map.png");
		FXMLLoader loader = new FXMLLoader(getClass().getResource("map.fxml"));
		root = loader.load();
		mapController mapcontroller = loader.getController();
		mapcontroller.setBackPage("deliveryordermodel.fxml");
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}
	/**
	 * overloading setOrderInfo in order to set data which already exists,
	 *  if this page back from other page.
	 * @throws IOException src\\image\\map.png not found
	 */
	public void setOrderInfo() throws IOException {
		if(orderInfo!=null) {
			setOrderInfo(orderInfo);
		} 
	}
}
