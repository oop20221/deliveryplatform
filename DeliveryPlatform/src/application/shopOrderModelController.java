package application;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import com.oct.dp.map.GoogleMap;
import com.oct.dp.model.OrderInfo;
import com.oct.dp.model.OrderInfo.Detail;
import com.oct.dp.model.OrderInfo.OrderState;
import com.oct.dp.shop.ShopOwner;

import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

/**
 * Controller of shopordermodel.fxml order could be selected and deleted by
 * ShopOwner
 * 
 * @author Gary
 *
 */
public class shopOrderModelController {
	@FXML
	private Label OrderNumLabel, myLabel, stateText, shopText, memberText, deliveryText, totalText, orderTimeText,
			arriveTimeText, discountText, VIPText, feeText;

	@FXML
	private TextField addressTextField;

	@FXML
	private Button botButton, mapButton, backButton, confirmButton, disposeButton;

	@FXML
	private TableView<Detail> orderListView;

	@FXML
	private ScrollPane myScrollPane;

	@FXML
	private AnchorPane myPane, myAnchorPane;

	private Stage stage;
	private Scene scene;
	private Parent root;
	private long orderNum;
	private static OrderInfo orderInfo;

	/**
	 * set data for order
	 * 
	 * @param myOrderInfo
	 */
	public void setOrder(OrderInfo myOrderInfo) {
		myScrollPane.setVvalue(-0.1);
		orderInfo = myOrderInfo;
		this.orderNum = myOrderInfo.getId();
		OrderNumLabel.setText("#" + orderNum);
		stateText.setText(orderInfo.getState().toString());
		shopText.setText(orderInfo.getRestName());
		memberText.setText(orderInfo.getUserName());
		addressTextField.setText(orderInfo.getDestination());

		TableColumn<Detail, String> column1 = new TableColumn<>("品項");
		column1.setCellValueFactory(new PropertyValueFactory<>("product_name"));
		TableColumn<Detail, String> column2 = new TableColumn<>("單價");
		column2.setCellValueFactory(new PropertyValueFactory<>("unit_price"));
		TableColumn<Detail, String> column3 = new TableColumn<>("數量");
		column3.setCellValueFactory(new PropertyValueFactory<>("amount"));
		TableColumn<Detail, String> column4 = new TableColumn<>("價格");
		column4.setCellValueFactory(new PropertyValueFactory<>("price"));
		orderListView.getColumns().set(0, column1);
		orderListView.getColumns().set(1, column2);
		orderListView.getColumns().set(2, column3);
		orderListView.getColumns().set(3, column4);
		for (int i = 0; i < orderInfo.getDetails().size(); i++) {
			orderListView.getItems().add(orderInfo.getDetails().get(i));
		}
		orderListView.prefHeightProperty()
				.bind(Bindings.max(2, Bindings.size(orderListView.getItems())).multiply(30).add(20).add(10));
		orderListView.minHeightProperty().bind(orderListView.prefHeightProperty());
		orderListView.maxHeightProperty().bind(orderListView.prefWidthProperty());
		int[] width = { 175, 60, 60, 60 };
		for (int i = 0; i < orderListView.getColumns().size(); i++) {
			TableColumn<Detail, ?> index = orderListView.getColumns().get(i);
			index.prefWidthProperty().bind(new SimpleIntegerProperty(width[i]));
			index.minWidthProperty().bind(index.prefWidthProperty());
			index.maxWidthProperty().bind(index.prefWidthProperty());
		}

		myPane.setLayoutY(orderListView.getLayoutY() + orderListView.getPrefHeight() + 40);

		myAnchorPane.setPrefHeight(myPane.getLayoutY() + myPane.getPrefHeight() + 40);

		discountText.setText("商家優惠：" + orderInfo.getDiscount() + "元");
		if (orderInfo.isFeeFree()) {
			VIPText.setText("會員免運");
		} else {
			VIPText.setText("不滿足免運條件");
		}
		feeText.setText(String.valueOf(orderInfo.getFee()));
		totalText.setText(String.valueOf(orderInfo.getTotal()));

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		LocalDateTime orderDate = orderInfo.getOrdertime().toLocalDateTime();
		String orderTime = dtf.format(orderDate);
		orderTimeText.setText(orderTime);

		String arrivedTime = null;
		if (orderInfo.getArrivetime() != null) {
			LocalDateTime localDateTime = orderInfo.getArrivetime().toLocalDateTime();
			arrivedTime = localDateTime.format(dtf);
		}
		arriveTimeText.setText(arrivedTime);

		deliveryText.setText(orderInfo.getDeliveryName());

		botButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
			public void handle(MouseEvent arg0) {
				myScrollPane.setVvalue(1);
			}
		});

		if (orderInfo.getState() != OrderState.WAITING) {
			confirmButton.setVisible(false);
			disposeButton.setVisible(false);
		}
	}

	/**
	 * confirm unchecked order
	 * 
	 * @param event click confirm button
	 * @throws IOException shoporder.fxml noy found
	 */
	public void confirm(ActionEvent event) throws IOException {
		if (orderInfo.getState() == OrderState.WAITING) {
			ShopOwner.confirm(orderInfo);
		} else {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("error");
			alert.setHeaderText("非未接訂單!!");
			alert.showAndWait();
		}
		FXMLLoader loader = new FXMLLoader(getClass().getResource("shoporder.fxml"));
		root = loader.load();
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}

	/**
	 * discard order
	 * 
	 * @param event click discard button
	 * @throws Exception shoporder.fxml not found
	 */
	public void discard(ActionEvent event) throws Exception {
		if (orderInfo.getState() == OrderState.DISCARD) {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("error");
			alert.setHeaderText("已刪除!!");
			alert.showAndWait();
		} else if (orderInfo.getState() == OrderState.WAITING) {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("success");
			alert.setHeaderText("成功刪除!!");
			alert.showAndWait();
			ShopOwner.discard(orderInfo);
			FXMLLoader loader = new FXMLLoader(getClass().getResource("shoporder.fxml"));
			root = loader.load();
			stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
			scene = new Scene(root);
			stage.setScene(scene);
			stage.show();
		} else {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("error");
			alert.setHeaderText("無法刪除!!訂單狀態:" + orderInfo.getState());
			alert.showAndWait();
		}
	}

	/**
	 * back to shop order page
	 * 
	 * @param event click back button
	 * @throws Exception shoporder.fxml not found
	 */
	public void back(ActionEvent event) throws Exception {

		FXMLLoader loader = new FXMLLoader(getClass().getResource("shoporder.fxml"));
		root = loader.load();
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}

	/**
	 * show location of member in the map
	 * 
	 * @param event click show Map button
	 * @throws Exception map.fxml not found
	 */
	public void showMap(ActionEvent event) throws Exception {
		GoogleMap.getMap(addressTextField.getText(), "src\\image\\map.png");
		FXMLLoader loader = new FXMLLoader(getClass().getResource("map.fxml"));
		root = loader.load();
		mapController mapcontroller = loader.getController();
		mapcontroller.setBackPage("shopordermodel.fxml");
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}

	/**
	 * overloading method that set data which already exists
	 */
	public void setOrder() {
		if (orderInfo != null) {
			setOrder(orderInfo);
		}
	}
}
