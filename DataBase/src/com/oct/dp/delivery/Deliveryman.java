package com.oct.dp.delivery;

import java.util.List;

import com.oct.dp.model.DeliveryInfo;
import com.oct.dp.model.OrderInfo;
import com.oct.dp.model.ShopInfo;
import com.oct.dp.model.OrderInfo.OrderState;
import com.oct.dp.mydao.CrudDAO;
import com.oct.dp.mydao.DAOException;
import com.oct.dp.mydao.DAOFactory;
import com.oct.dp.mydao.UserDAO;
import com.oct.dp.mydao.condition.Conditions;
import com.oct.dp.mydao.condition.Fields;

/**
 * all method that delivery would use. connect delivery and database to prevent
 * data breach
 * 
 * @author Gary
 *
 */
public class Deliveryman {

	private static DAOFactory javabase = DAOFactory.getInstance();
	private static UserDAO<DeliveryInfo> deliveryDAO = javabase.getDeliveryDAO();
	private static UserDAO<ShopInfo> shopDAO = javabase.getShopDAO();
	private static CrudDAO<OrderInfo> orderDAO = javabase.getOrderDAO();

	/**
	 * register a delivery account.
	 * 
	 * @param account  delivery account
	 * @param password delivery password
	 * @return deliveryInfo
	 * @throws IllegalArgumentException delivery account create failed.
	 * @throws DAOException             delivery account create failed.
	 */
	public static DeliveryInfo register(String account, String password) throws IllegalArgumentException, DAOException {
		DeliveryInfo deliveryInfo = new DeliveryInfo();
		deliveryInfo.setAccount(account);
		deliveryInfo.setPassword(password);
		deliveryInfo = deliveryDAO.create(deliveryInfo);
		return deliveryInfo;
	}

	/**
	 * delivery account login.
	 * 
	 * @param account  delivery account
	 * @param password delivery password
	 * @return deliveryInfo
	 * @throws DAOException login failed.
	 */
	public static DeliveryInfo login(String account, String password) throws DAOException {
		return deliveryDAO.find(account, password);
	}

	/**
	 * update data of deliveryInfo to database
	 * 
	 * @param deliveryInfo a deliveryInfo want to be update
	 * @return an updated deliveryInfo
	 * @throws IllegalArgumentException update failed
	 * @throws DAOException             update failed
	 */
	public static DeliveryInfo update(DeliveryInfo deliveryInfo) throws IllegalArgumentException, DAOException {
		return deliveryDAO.update(deliveryInfo);
	}

	/**
	 * change password and update it into database.
	 * 
	 * @param deliveryInfo a deliveryInfo need to be change password.
	 * @return a new password deliveryInfo
	 * @throws IllegalArgumentException change password failed.
	 * @throws DAOException             change password failed.
	 */
	public static DeliveryInfo changePassword(DeliveryInfo deliveryInfo) throws IllegalArgumentException, DAOException {
		return deliveryDAO.changePassword(deliveryInfo);
	}

	/**
	 * delivery confirm an order and set its state to DELIVERING
	 * 
	 * @param orderInfo an PREPARING order that already prepared by shop
	 * @throws IllegalArgumentException update failed
	 * @throws DAOException             update failed
	 */
	public static void confirm(OrderInfo orderInfo) throws IllegalArgumentException, DAOException {
		orderInfo.setState(OrderState.DELIVERING);
		orderDAO.update(orderInfo);

	}

	/**
	 * find shop by id
	 * 
	 * @param ID shop id
	 * @return a shopInfo
	 * @throws DAOException find failed
	 */
	public static ShopInfo searchByID(Long ID) throws DAOException {
		return shopDAO.find(ID);
	}
	
	/**
	 * delivery check history orders.
	 * 
	 * @param deliveryInfo a deliveryInfo
	 * @return a list of all orderInfo
	 * @throws IllegalArgumentException findAll failed
	 * @throws DAOException             findAll failed
	 */
	public static List<OrderInfo> history(DeliveryInfo deliveryInfo) throws IllegalArgumentException, DAOException {
		return orderDAO.findAll(new Conditions(deliveryInfo.getId(), Fields.DELIV_ID)
				.and(new Conditions(OrderState.FINISHED.name(), Fields.ORDER_STATE)
				.or(OrderState.ARRIVED.name(), Fields.ORDER_STATE)));
	}

	/**
	 * get orders list which could be delivered
	 * 
	 * @return a list of orderInfo that already prepared.
	 * @throws IllegalArgumentException findAll failed
	 * @throws DAOException             findAll failed
	 */
	public static List<OrderInfo> getReadyOrderInfo() throws IllegalArgumentException, DAOException {

		return orderDAO.findAll(OrderInfo.OrderState.PREPARING.name(), Fields.ORDER_STATE);

	}

	/**
	 * get order list which is delivering
	 * 
	 * @param deliveryInfo a deliveryInfo
	 * @return a order list which is delivering
	 * @throws IllegalArgumentException findAll failed
	 * @throws DAOException             findAll failed
	 */
	public static List<OrderInfo> getDeliveryOrderInfo(DeliveryInfo deliveryInfo)
			throws IllegalArgumentException, DAOException {

		return orderDAO.findAll(new Conditions(deliveryInfo.getId(), Fields.DELIV_ID).and(OrderState.DELIVERING.name(),
				Fields.ORDER_STATE));

	}

	/**
	 * when delivery man arrived member's address, set order state to ARRIVED.
	 * 
	 * @param orderInfo the delivering order
	 * @throws IllegalArgumentException update failed
	 * @throws DAOException             update failed
	 */
	public static void arrive(OrderInfo orderInfo) throws IllegalArgumentException, DAOException {
		orderInfo.setState(OrderState.ARRIVED);
		orderDAO.update(orderInfo);
	}

	/**
	 * delete deliveryInfo from database.
	 * 
	 * @param deliveryInfo deliveryInfo which would be deleted.
	 * @throws DAOException delete failed.
	 */
	public static void delete(DeliveryInfo deliveryInfo) throws DAOException {
		deliveryDAO.delete(deliveryInfo);
	}
}
