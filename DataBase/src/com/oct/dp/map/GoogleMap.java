package com.oct.dp.map;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.imageio.ImageIO;

import com.oct.dp.mydao.Pair;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * 
 * @author Rick
 *
 */
public class GoogleMap {

	// Note: Generally, you should store your private key someplace safe
	// and read them into your code

	private static String SIGNATURE;
	private static String APIKEY;
	private static final int ZOOM = 16;
	private static final int SIZEX = 600;
	private static final int SIZEY = 400;
	
	// initialize
	static {
        PropertiesLoader pl;
		try {
			pl = new PropertiesLoader("api.properties", "map");
			SIGNATURE = pl.getProperty("signature", true);
			APIKEY = pl.getProperty("key", false);
		} catch (FileNotFoundException e) {
			SIGNATURE = null;
			APIKEY = null;
			e.printStackTrace();
		} 
    }
	
	/**
	 * Saves a google map png centered at given location to given path.
	 * @param location	- the location you want the map to center at. 
	 * 					Can be either an address or a set of latitude and longitude.
	 * @param imgPath	- the path the generated png is saved to.
	 * @throws IOException	if given location is not valid.
	 */
	public static void getMap(String location, String imgPath) throws IOException {
		File file = new File(imgPath);
		ImageIO.write(getMap(location, ZOOM, SIZEX, SIZEY), "PNG", file);
	}

	/**
	 * Saves a google map png centered at given location to given path.
	 * @param location	- the location you want the map to center at. 
	 * 					Can be either an address or a set of latitude and longitude.
	 * @param imgPath	- the path the generated png is saved to.
	 * @param zoom		- the resolution of the current view.
	 * @param size 		- the rectangular dimensions of the map image.
	 * @throws IOException	if given location is not valid.
	 */
	public static void getMap(String location, String imgPath, int zoom, int x, int y) throws IOException {
		File file = new File(imgPath);
		ImageIO.write(getMap(location, zoom, x, y), "PNG", file);
	}
	
	/**
	 * Saves a google map png containing the two given locations to given path.
	 * the view is autoscaled.
	 * @param location	- the location you want the map to show
	 * 					Can be either an address or a set of latitude and longitude.
	 * @param imgPath	- the path the generated png is saved to.
	 * @throws IOException	if given location is not valid.
	 */
	public static void getMap(String shopLocation, String memberLocation, String imgPath) throws IOException
	{
		File file = new File(imgPath);
		ImageIO.write(getMap(shopLocation, memberLocation, SIZEX, SIZEY), "PNG", file);
	}
	
	/**
	 * Saves a google map png containing the two given locations to given path.
	 * the view is autoscaled.
	 * @param location	- the pair of location you want the map to show. 
	 * 					Can be either an address or a set of latitude and longitude.
	 * @param imgPath	- the path the generated png is saved to.
	 * @param color 	- pair of color in hex code format. For example "353935", "FF00FF".
	 * @param label		- pair of label to show on the markers.
	 * 					A single uppercase alphanumeric character from the set {A-Z, 0-9}.
	 * @param pathColor	- color of the path. A hex code formated string, like "353935", "FF00FF".
	 * @param pathTransparency	- transparency of the path. 
	 * 							Two characters specify the 8-bit alpha transparency value. 
	 * 							This value varies between 00 (completely transparent) and FF (completely opaque).
	 * @throws IOException	if given location is not valid.
	 */
	public static void getMap(Pair<String, String> location, String imgPath, Pair<String, String> color, Pair<String, String> label,
			String pathColor, String pathTransparency, String pathWidth, int x, int y) throws IOException {
		File file = new File(imgPath);
		ImageIO.write(getMap(location, color, label, pathColor, pathTransparency, pathWidth, x, y), "PNG", file);
	}
	
	// Helpers -------------------------------------------------------------------------------------------------

	private static BufferedImage getMap(String location, int zoom, int x, int y) 
	{
		Request request = new Request.Builder().url(
				"https://maps.googleapis.com/maps/api/staticmap?"
				+ "&zoom=" + zoom
				+ "&size=" + x + "x" + y
				+ "&markers=" + location
				+ "&key="  + APIKEY)
				.build();

		return request(request);
	}
	
	private static BufferedImage getMap(Pair<String, String> location, Pair<String, String> color, Pair<String, String> label,
			String pathColor, String pathTransparency, String pathWidth, int x, int y) 
	{
		try {				
			Request request = new Request.Builder().url(
					"https://maps.googleapis.com/maps/api/staticmap?"
					+ "size=" + x + "x" + y
					+ "&markers=color:0x" + color.getFirst() + "%7Clabel:" + label.getFirst() + "%7C" + location.getFirst()
					+ "&markers=color:0x" + color.getSecond() + "%7Clabel:" + label.getSecond() + "%7C" + location.getSecond()
					+ "&path=color:0x" + pathColor + pathTransparency + "%7Cweight:" + pathWidth + "%7C" 
					+ String.join("%7C", Direction.getDirection(location.getFirst(), location.getSecond()).getPath())
					+ "&key="  + APIKEY)
					.build();
			
			return request(request);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}

	}
	
	private static BufferedImage getMap(String shop, String member, int x, int y)
	{
		try {
			Request request = new Request.Builder().url(
					"https://maps.googleapis.com/maps/api/staticmap?"
					+ "size=" + x + "x" + y
					+ "&markers=label:S%7C" + shop
					+ "&markers=label:Y%7C" + member
					+ "&path=color:0x353935F0%7Cweight:4%7C" 
					+ String.join("%7C", Direction.getDirection(shop, member).getPath())
					+ "&key="  + APIKEY)
					.build();
			
			return request(request);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		} 
		
		
	}
	
	private static BufferedImage request(Request request) {
		try {
			URL url = new URL(request.url().toString());
			
			
			OkHttpClient client = new OkHttpClient().newBuilder().build();
			request = new Request.Builder().url(encode(url, SIGNATURE)).build();
			
			try ( Response response = client.newCall(request).execute();
			){	
				return ImageIO.read(response.body().byteStream());
			} catch (IOException e) {
				e.printStackTrace();
				return null;
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
			return null;
		}

	}
	
	private static String encode(URL url, String signature) {
	    
		try {
			String resource = url.getPath() + "?" + url.getQuery();
			
			// Convert the key from 'web safe' base 64 to binary
			signature = signature.replace('-', '+').replace('_', '/');

		    // Base64 is JDK 1.8 only - older versions may need to use Apache Commons or similar.
		    byte[] key = Base64.getDecoder().decode(signature);
			
		    // Get an HMAC-SHA1 signing key from the raw key bytes
		    SecretKeySpec sha1Key = new SecretKeySpec(key, "HmacSHA1");

		    
		    // Get an HMAC-SHA1 Mac instance and initialize it with the HMAC-SHA1 key
		    Mac mac = Mac.getInstance("HmacSHA1");
		    mac.init(sha1Key);

		    // compute the binary signature for the request
		    byte[] sigBytes = mac.doFinal(resource.getBytes());

		    // base 64 encode the binary signature
		    // Base64 is JDK 1.8 only - older versions may need to use Apache Commons or similar.
		    signature = Base64.getEncoder().encodeToString(sigBytes);
		    
		    // convert the signature to 'web safe' base 64
		    signature = signature.replace('+', '-').replace('/', '_');

		    return url.getProtocol() + "://" + url.getHost() + resource + "&signature=" + signature;
		} catch (NoSuchAlgorithmException | InvalidKeyException e) {
			e.printStackTrace();
			return null;
		}
		
		
	}
	  
}