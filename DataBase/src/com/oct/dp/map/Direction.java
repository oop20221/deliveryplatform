package com.oct.dp.map;




import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


/**
 * 
 * @author Rick
 *
 */
public class Direction {
	
	private static String key;
	
	// initialize
	static {
        PropertiesLoader pl;
		try {
			pl = new PropertiesLoader("api.properties", "map");
			key = pl.getProperty("key", true);
		} catch (FileNotFoundException e) {
			key = null;
		} 
    }
	
	
	private double northeast_lat;
	private double northeast_lng;
	private double southwest_lat;
	private double southwest_lng;
	private String distance;
	private long distance_in_meter;
	private String duration;
	private long duration_in_second;
	private String start_address;
	private String end_address;
	private double start_lat;
	private double start_lng;
	private double end_lat;
	private double end_lng;
	private List<String> path = new ArrayList<>();
	
	public static JSONObject request(String origin, String destinaion) throws IOException {
		OkHttpClient client = new OkHttpClient().newBuilder().build();
		Request request = new Request.Builder().url(
				"https://maps.googleapis.com/maps/api/directions/json?"
				+ "origin=" + origin
				+ "&destination=" + destinaion
				+ "&avoid=tolls%7Chighways%7Cferries"
				+ "&key=" + key)
				.build();
		
		try ( Response response = client.newCall(request).execute();
		){		
			return (JSONObject)(new JSONParser().parse(response.body().string()));
		} catch (IOException e) {
			throw new IOException(e);
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		} 
	}
	
	/**
	 * Returns the information of the path between the two given location.
	 * @param addressA	- start location
	 * @param addressB	- end location
	 * @return the information of the path between the two given location.
	 * @throws IOException
	 */
	public static Direction getDirection(String addressA, String addressB) throws IOException {
		JSONObject dir = request(addressA, addressB);
		JSONObject route = ((JSONObject)((JSONArray) dir.get("routes")).get(0));
		dir = null;
		JSONObject bound = (JSONObject) route.get("bounds");
		JSONObject leg = (JSONObject) ((JSONArray)route.get("legs")).get(0);
		route = null;
		
		Direction direction = new Direction();
		direction.setNortheast_lat((double)((JSONObject)bound.get("northeast")).get("lat"));
		direction.setNortheast_lng((double)((JSONObject)bound.get("northeast")).get("lng"));
		direction.setSouthwest_lat((double)((JSONObject)bound.get("southwest")).get("lat"));
		direction.setSouthwest_lat((double)((JSONObject)bound.get("southwest")).get("lng"));
		direction.setDistance((String)((JSONObject)leg.get("distance")).get("text"));
		direction.setDistance_in_meter((long)((JSONObject)leg.get("distance")).get("value"));
		direction.setDuration((String)((JSONObject)leg.get("duration")).get("text"));
		direction.setDuration_in_second((long)((JSONObject)leg.get("duration")).get("value"));
		direction.setStart_address((String)leg.get("start_address"));
		direction.setEnd_address((String)leg.get("end_address"));
		direction.setStart_lat((double)((JSONObject)leg.get("start_location")).get("lat"));
		direction.setStart_lng((double)((JSONObject)leg.get("start_location")).get("lng"));
		direction.setEnd_lat((double)((JSONObject)leg.get("end_location")).get("lat"));
		direction.setEnd_lng((double)((JSONObject)leg.get("end_location")).get("lng"));
		List<String> path = new ArrayList<>();
		path.add(direction.getStart_lat() + "," + direction.getStart_lng());
		JSONArray steps = (JSONArray)leg.get("steps");
		leg = null;
		for (int i = 0; i < steps.size(); i++) {
			JSONObject pos = ((JSONObject)((JSONObject)steps.get(i)).get("end_location"));
			path.add(Double.toString((Double)pos.get("lat")) +","+ Double.toString((Double)pos.get("lng")));
		}
		direction.setPath(path);
		return direction;
	}
	
	
	// Getters and Setters -----------------------------------------------------------------------------------------------

	public double getNortheast_lat() {
		return northeast_lat;
	}

	public double getNortheast_lng() {
		return northeast_lng;
	}

	public double getSouthwest_lat() {
		return southwest_lat;
	}

	public double getSouthwest_lng() {
		return southwest_lng;
	}

	public String getDistance() {
		return distance;
	}

	public long getDistance_in_meter() {
		return distance_in_meter;
	}

	public String getDuration() {
		return duration;
	}

	public long getDuration_in_second() {
		return duration_in_second;
	}

	public String getStart_address() {
		return start_address;
	}

	public String getEnd_address() {
		return end_address;
	}

	public double getStart_lat() {
		return start_lat;
	}

	public double getStart_lng() {
		return start_lng;
	}

	public double getEnd_lat() {
		return end_lat;
	}

	public double getEnd_lng() {
		return end_lng;
	}

	public void setNortheast_lat(double northeast_lat) {
		this.northeast_lat = northeast_lat;
	}

	public void setNortheast_lng(double northeast_lng) {
		this.northeast_lng = northeast_lng;
	}

	public void setSouthwest_lat(double southwest_lat) {
		this.southwest_lat = southwest_lat;
	}

	public void setSouthwest_lng(double southwest_lng) {
		this.southwest_lng = southwest_lng;
	}

	public void setDistance(String distance) {
		this.distance = distance;
	}

	public void setDistance_in_meter(long distance_in_meter) {
		this.distance_in_meter = distance_in_meter;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public void setDuration_in_second(long duration_in_second) {
		this.duration_in_second = duration_in_second;
	}

	public void setStart_address(String start_address) {
		this.start_address = start_address;
	}

	public void setEnd_address(String end_address) {
		this.end_address = end_address;
	}

	public void setStart_lat(double start_lat) {
		this.start_lat = start_lat;
	}

	public void setStart_lng(double start_lng) {
		this.start_lng = start_lng;
	}

	public void setEnd_lat(double end_lat) {
		this.end_lat = end_lat;
	}

	public void setEnd_lng(double end_lng) {
		this.end_lng = end_lng;
	}

	public List<String> getPath() {
		return path;
	}

	public void setPath(List<String> path) {
		this.path = path;
	}
	
	
}


