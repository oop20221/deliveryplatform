package com.oct.dp.model;

import java.sql.Timestamp;
import java.util.List;

/**
 * This class contains the infomation of an order.
 * @author Rick
 *
 */
public class OrderInfo extends DataInfo {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long rest_id;
	private Long user_id;
	private Long deliv_id;
	private String rest_name;	// max 20
	private String user_name;	// max 20
	private String deliv_name;	// max 20
	private List<Detail> details;
	private int total;
	private int fee;
	private int discount;
	private boolean isFeeFree;
	private OrderState state;
	private Timestamp ordertime;
	private Timestamp arrivetime;
	private String departure;	// max 30
	private String destination;	// max 30
	private String order_note;	// max 80

	public enum OrderState{
		WAITING,
		PREPARING,
		DELIVERING,
		ARRIVED,
		FINISHED,
		DISCARD
	}
	
	public static class Detail{
		private String product_name;	// max 30
		private int amount;
		private int unit_price;
		private int price;
		private String note;			// max 20
		
		public Detail() {}
		public Detail(String product_name, int amount, int unit_price, int price, String note) {
			this.product_name = product_name;
			this.amount = amount;
			this.unit_price = unit_price;
			this.price = price;
			this.note = note;
		}
		public String getProduct_name() {return product_name;}
		public int getAmount() {return amount;}
		public int getUnit_price() {return unit_price;}
		public int getPrice() {return price;}
		public String getNote() {return note;}
		public void setProduct_name(String product_name) {this.product_name = product_name;}
		public void setAmount(int amount) {this.amount = amount;}
		public void setUnit_price(int unit_price) {this.unit_price = unit_price;}
		public void setPrice(int price) {this.price = price;}
		public void setNote(String note) {this.note = note;}		
	}
	
	public OrderInfo() {
		this.state = OrderState.WAITING;
	}	
	
	public Long getRestID() {return rest_id;}
	public void setRestID(Long rest_id) {this.rest_id = rest_id;}
	public Long getUserID() {return user_id;}
	public void setUserID(Long user_id) {this.user_id = user_id;}
	public Long getDeliveryID() {return deliv_id;}
	public void setDeliveryID(Long deliv_id) {this.deliv_id = deliv_id;}
	public String getRestName() {return rest_name;}
	public void setRestName(String rest_name) {this.rest_name = rest_name;}
	public List<Detail> getDetails() {return details;}
	public void setDetails(List<Detail> details) {this.details = details;}
	public OrderState getState() {return state;}
	public void setState(OrderState state) {this.state = state;}
	public int getTotal() {return total;}
	public void setTotal(int total) {this.total = total;}
	public int getFee() {return fee;}
	public void setFee(int fee) {this.fee = fee;}
	public int getDiscount() {return discount;}
	public void setDiscount(int discount) {this.discount = discount;}
	public boolean isFeeFree() {return isFeeFree;}
	public void setFeeFree(boolean isFeeFree) {this.isFeeFree = isFeeFree;}
	public Timestamp getOrdertime() {return ordertime;}
	public void setOrdertime(Timestamp ordertime) {this.ordertime = ordertime;}
	public Timestamp getArrivetime() {return arrivetime;}
	public void setArrivetime(Timestamp arrivetime) {this.arrivetime = arrivetime;}
	public String getDeparture() {return departure;}
	public void setDeparture(String departure) {this.departure = departure;}
	public String getDestination() {return destination;}
	public void setDestination(String destination) {this.destination = destination;}
	public String getUserName() {return user_name;}
	public void setUserName(String user_name) {this.user_name = user_name;}
	public String getDeliveryName() {return deliv_name;}
	public void setDeliveryName(String deliv_name) {this.deliv_name = deliv_name;}
	public String getOrder_note() {return order_note;}
	public void setOrder_note(String order_note) {this.order_note = order_note;}

	@Override
	public String toString() {
		return String.format(
				"OrderInfo:\n" + "id:%d\n" + "username:%s\nrestaurant:%s\n" 
						+ "total:%d\n" + "ordertime:%s\narrivetime:%s\n" + "destination:%s\n" + "deliveryname:%s\n",
				id, user_name, rest_name, state, total, ordertime, arrivetime, destination,
				deliv_name);
	}

}
