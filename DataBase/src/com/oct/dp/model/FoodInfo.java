package com.oct.dp.model;

/**
 * This class contains the infomation of food.
 * @author Rick
 *
 */
public class FoodInfo extends DataInfo {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long r_id;
	private String name;
	private String price;

	public FoodInfo() {
	}

	public FoodInfo(Long r_id, String name, String price) {
		this.r_id = r_id;
		this.name = name;
		this.price = price;
	}
	
	public Long getR_id() {
		return r_id;
	}

	public void setR_id(Long r_id) {
		this.r_id = r_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPrice() {
		return price;
	}
	
	public void setPrice(String price) {
		this.price = price;
	}
	
	@Override
	public String toString() {
		return String.format(
				"FoodInfo:\n" + "name:%s\nprice:%s\n",
				name, price);
		}

	
}
