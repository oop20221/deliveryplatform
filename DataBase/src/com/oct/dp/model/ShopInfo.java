package com.oct.dp.model;

import java.util.List;

/**
 * This class contains the infomation of member.
 * @author Rick
 *
 */
public class ShopInfo extends UserInfo {

	private static final long serialVersionUID = 1L;
	
	private double discount;
	private int targetcost;
	private String email;
	private String Rname;
	
	private String address;
	private double latitude;
	private double longitude;
	private String phone;
	private String store_description;
	private String order_description;
	private List<String> type;

	//business_time
	private String Monstart;
	private String Monend;
	private String Tuestart;
	private String Tueend;
	private String Wedstart;
	private String Wedend;
	private String Thustart;
	private String Thuend;
	private String Fristart;
	private String Friend;
	private String Satstart;
	private String Satend;
	private String Sunstart;
	private String Sunend;

	public double getDiscount() {
		return discount;
	}

	public ShopInfo setDiscount(double discount) {
		this.discount = discount;
		return this;
	}

	public int getTargetcost() {
		return targetcost;
	}

	public ShopInfo setTargetcost(int targetcost) {
		this.targetcost = targetcost;
		return this;
	}

	public String getEmail() {
		return email;
	}

	public ShopInfo setEmail(String email) {
		this.email = email;
		return this;
	}

	public String getRname() {
		return Rname;
	}

	public ShopInfo setRname(String rname) {
		Rname = rname;
		return this;
	}

	public String getAddress() {
		return address;
	}

	public ShopInfo setAddress(String address) {
		this.address = address;
		return this;
	}

	public double getLatitude() {
		return latitude;
	}

	public ShopInfo setLatitude(double latitude) {
		this.latitude = latitude;
		return this;
	}

	public double getLongitude() {
		return longitude;
	}

	public ShopInfo setLongitude(double longitude) {
		this.longitude = longitude;
		return this;
	}

	public String getPhone() {
		return phone;
	}

	public ShopInfo setPhone(String phone) {
		this.phone = phone;
		return this;
	}

	public String getStore_description() {
		return store_description;
	}

	public ShopInfo setStore_description(String store_description) {
		this.store_description = store_description;
		return this;
	}

	public String getOrder_description() {
		return order_description;
	}

	public ShopInfo setOrder_description(String order_description) {
		this.order_description = order_description;
		return this;
	}

	public List<String> getType() {
		return type;
	}

	public ShopInfo setType(List<String> type) {
		this.type = type;
		return this;
	}
	
	public String getMonstart() {
		return Monstart;
	}

	public ShopInfo setMonstart(String monstart) {
		Monstart = monstart;
		return this;
	}

	public String getMonend() {
		return Monend;
	}

	public ShopInfo setMonend(String monend) {
		Monend = monend;
		return this;
	}

	public String getTuestart() {
		return Tuestart;
	}

	public ShopInfo setTuestart(String tuestart) {
		Tuestart = tuestart;
		return this;
	}

	public String getTueend() {
		return Tueend;
	}

	public ShopInfo setTueend(String tueend) {
		Tueend = tueend;
		return this;
	}

	public String getWedstart() {
		return Wedstart;
	}

	public ShopInfo setWedstart(String wedstart) {
		Wedstart = wedstart;
		return this;
	}

	public String getWedend() {
		return Wedend;
	}

	public ShopInfo setWedend(String wedend) {
		Wedend = wedend;
		return this;
	}

	public String getThustart() {
		return Thustart;
	}

	public ShopInfo setThustart(String thustart) {
		Thustart = thustart;
		return this;
	}

	public String getThuend() {
		return Thuend;
	}

	public ShopInfo setThuend(String thuend) {
		Thuend = thuend;
		return this;
	}

	public String getFristart() {
		return Fristart;
	}

	public ShopInfo setFristart(String fristart) {
		Fristart = fristart;
		return this;
	}

	public String getFriend() {
		return Friend;
	}

	public ShopInfo setFriend(String friend) {
		Friend = friend;
		return this;
	}

	public String getSatstart() {
		return Satstart;
	}

	public ShopInfo setSatstart(String satstart) {
		Satstart = satstart;
		return this;
	}

	public String getSatend() {
		return Satend;
	}

	public ShopInfo setSatend(String satend) {
		Satend = satend;
		return this;
	}

	public String getSunstart() {
		return Sunstart;
	}

	public ShopInfo setSunstart(String sunstart) {
		Sunstart = sunstart;
		return this;
	}

	public String getSunend() {
		return Sunend;
	}

	public ShopInfo setSunend(String sunend) {
		Sunend = sunend;
		return this;
	}

	@Override
	public String toString() {
		return String.format(
				"ShopInfo:\n" + "id:%d\naccount:%s\npassword:%s\n" + "restaurant:%s\nphone:%s\n"
						+ "address:%s\nlatitude:%.3f\nlongitude:%.3f\n" + "store_description:%s\norder_description:%s\n"
						+ "type:%s\n" + "mon:%s~%s\n" + "tue:%s~%s\n" + "wed:%s~%s\n" + "thu:%s~%s\n" + "fri:%s~%s\n"
						+ "sat:%s~%s\n" + "sun:%s~%s\n" + "targetcost:%d\ndiscount:%.2f\n" + "email:%s",
				id, account, password, Rname, phone, address, latitude, longitude, store_description, order_description,
				type, Monstart, Monend, Tuestart, Tueend, Wedstart, Wedend, Thustart, Thuend, Fristart, Friend,
				Satstart, Satend, Sunstart, Sunend, targetcost, discount, email);
	}

}