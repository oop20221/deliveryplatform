package com.oct.dp.model;

import java.io.Serializable;

/**
 * All info should extend the class.
 * @author Rick
 *
 */
public class DataInfo implements Serializable {
	
	/**
	 * a version number which is used during deserialization to 
	 * verify that the sender and receiver of a serialized object 
	 * have loaded classes for that object that are compatible 
	 * with respect to serialization.
	 */
	private static final long serialVersionUID = 1L;
	
	protected Long id;    
   
    // Constructors -------------------------------------------------------------------------
    
    public DataInfo() {}
    
    public DataInfo(Long id) {
    	this.id = id;
    }
    

    
    // Getters and Setters --------------------------------------------------------------------
    public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;		
	}
	
	// methods -----------------------------------------------------------------------------
	
    public void invalid() {
    	id = null;
    }
	
	/**
	 * Every Data's id is unique. 
	 * Datas with the same id are regarded as equal.
	 */
	@Override
    public boolean equals(Object other) {
    	return (other instanceof DataInfo) && (id != null)
                ? id.equals(((DataInfo) other).id)
                : (other == this);
    }
	
	/**
	 * Every Data's id is unique. 
	 * Data with the same id should return the same hashcode.
	 */
	@Override
	public int hashCode() {
		return (id != null) 
	             ? (this.getClass().hashCode() + id.hashCode()) 
	             : super.hashCode();
	}
    
	/**
	 * Returns a string representation of the object.
	 */
	@Override
	public String toString() {
		return String.format("Data[id=%d]", id);
	}


}
