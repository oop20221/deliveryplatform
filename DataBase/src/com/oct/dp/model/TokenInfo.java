package com.oct.dp.model;

import com.oct.dp.mydao.implementation.TokenDAOJDBC;

public class TokenInfo extends DataInfo {


	private static final long serialVersionUID = 1L;
	
	private Long customer_id;
	private String token;
	private String key;
	private Token_State state = Token_State.VERIFYING;
	private String account_info;
	
	public enum Token_State{
		ACTIVATED, VERIFYING, REJECTED
	}
	
	public static final Class<?> getDAO(){
		return TokenDAOJDBC.class;
	}

	public Long getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(Long customer_id) {
		this.customer_id = customer_id;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public Token_State getState() {
		return state;
	}

	public void setState(Token_State state) {
		this.state = state;
	}

	public String getAccount_info() {
		return account_info;
	}

	public void setAccount_info(String account_info) {
		this.account_info = account_info;
	}
	
	
}
