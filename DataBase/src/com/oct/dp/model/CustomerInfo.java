package com.oct.dp.model;

/**
 * This class contains the infomation of a customer.
 * @author Rick
 *
 */
public class CustomerInfo extends UserInfo  {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Double balance;
	private String token;

	public CustomerInfo() {
		
	}
	public CustomerInfo(String account, String password, String userName, Double balance) {
		 super(account, password, userName);
		 this.balance = balance;
	 }
	public Double getBalance() {
		return balance;
	}

	public void setBalance(Double balance) {
		this.balance = balance;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}		
	
}
