package com.oct.dp.model;

/**
 * This class contains the infomation of deliveryman.
 * @author Rick
 *
 */
public class DeliveryInfo extends UserInfo {

	private static final long serialVersionUID = 1L;
	
	private String name;	// max 20
	private String email;	// max 30
	private String phone;	// max 30
	//private boolean busy=false;
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}
	
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	public String toString() {
		return String.format(
				"DeliveryInfo:\n" + "id:%d\naccount:%s\npassword:%s\n" + "phone:%s\nemail:%s\n",
				id, account, password, phone, email);
	}
	
}
