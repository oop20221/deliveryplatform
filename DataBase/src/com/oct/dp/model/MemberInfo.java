package com.oct.dp.model;

import java.sql.Timestamp;

/**
 * This class contains the infomation of member.
 * @author Rick
 *
 */
public class MemberInfo extends UserInfo {

	private static final long serialVersionUID = 1L;
	
	private String member_name;	//nickname
	private String email;
	private String phone;
	private boolean vip;
	private Timestamp deadline;
	private String prefAddress;
	

	public String getMemberName() {
		return member_name;
	}
	public void setMemberName(String member_name) {
		this.member_name = member_name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public boolean isVip() {
		return vip;
	}
	public void setVip(boolean vip) {
		this.vip = vip;
	}
	public Timestamp getDeadline() {
		return deadline;
	}
	public void setDeadline(Timestamp deadline) {
		this.deadline = deadline;
	}
	public String getPrefAddress() {
		return prefAddress;
	}
	public void setPrefAddress(String prefAddress) {
		this.prefAddress = prefAddress;
	}
	
}
