package com.oct.dp.model;


/**
 * This class represents a prototype of all kinds of users.
 * No matter it is a member, store or delivery person, they all share
 * some basic properties and methods.
 * 
 * @author Rick
 *
 */
public class UserInfo extends DataInfo {

	/**
	 * a version number which is used during deserialization to 
	 * verify that the sender and receiver of a serialized object 
	 * have loaded classes for that object that are compatible 
	 * with respect to serialization.
	 */
	private static final long serialVersionUID = 1L;
	
	
	protected String account;
	protected String password;
	protected String userName;
	
	private String token;
	private String key;
    private String authentication;
    
   
    // Constructors -------------------------------------------------------------------------
    
    public UserInfo() {
    	super();
    }
    
    public UserInfo(Long id, String authentication) {
    	super(id);
    	this.authentication = authentication;
    }
    
    public UserInfo(String account, String password) {
    	this.account = account;
    	this.password = password;
    }
    
    public UserInfo(String account, String password, String userName) {
    	this.account = account;
    	this.password = password;
    	this.userName = userName;
    }

    
    // Getters and Setters --------------------------------------------------------------------
    
    public String getAuthentication() {
		return authentication;
	}
    
    public UserInfo setAuthentication(String authentication) {
		this.authentication = authentication;
		return this;
	}
	
	public String getAccount() {
		return account;
	}

	public UserInfo setAccount(String account) {
		this.account = account;
		return this;
	}

	public String getPassword() {
		return password;
	}

	public UserInfo setPassword(String password) {
		this.password = password;
		return this;
	}
	
	public String getUserName() {
		return userName;
	}

	public UserInfo setUserName(String userName) {
		this.userName = userName;
		return this;
	}	
	
	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	// methods -----------------------------------------------------------------------------

	
	@Override
	public void invalid() {
		super.invalid();
		authentication = null;
	}
    
	/**
	 * Returns a string representation of the object.
	 */
	@Override
	public String toString() {
		return String.format("User[id=%d,account=%s]", getId(), account);
	}
}


