package com.oct.dp.model;

import java.sql.Timestamp;

/**
 * @author tree
 * for Rick to build method for database
 */
public class PlayerInfo extends UserInfo{

	private static final long serialVersionUID = 1L;
	
	private int bestScore;
	private Timestamp bestScoreDate;

	public int getBestScore() {
		return bestScore;
	}
	public void setBestScore(int bestScore) {
		this.bestScore = bestScore;
	}
	public Timestamp getBestScoreDate() {
		return bestScoreDate;
	}
	public void setBestScoreDate(Timestamp bestScoreDate) {
		this.bestScoreDate = bestScoreDate;
	}
	
}
