package com.oct.dp.member;

import java.util.List;

import com.oct.dp.model.MemberInfo;
import com.oct.dp.model.OrderInfo;
import com.oct.dp.model.OrderInfo.OrderState;
import com.oct.dp.model.ShopInfo;
import com.oct.dp.mydao.CrudDAO;
import com.oct.dp.mydao.DAOException;
import com.oct.dp.mydao.DAOFactory;
import com.oct.dp.mydao.UserDAO;
import com.oct.dp.mydao.condition.Comparators;
import com.oct.dp.mydao.condition.Conditions;
import com.oct.dp.mydao.condition.Fields;
import com.oct.dp.mydao.condition.Query;

/**
 * all method that member would use. connect member and database to prevent data
 * breach
 * 
 * @author Gary
 *
 */
public class Member {

	private static DAOFactory javabase = DAOFactory.getInstance();
	private static UserDAO<MemberInfo> memberDAO = javabase.getMemberDAO();
	private static UserDAO<ShopInfo> shopDAO = javabase.getShopDAO();
	private static CrudDAO<OrderInfo> orderDAO = javabase.getOrderDAO();

	/**
	 * register a new member account
	 * 
	 * @param account  member account
	 * @param password member password
	 * @return memberInfo
	 * @throws IllegalArgumentException member account create failed
	 * @throws DAOException             member account create failed
	 */
	public static MemberInfo register(String account, String password) throws IllegalArgumentException, DAOException {
		MemberInfo memberInfo = new MemberInfo();
		memberInfo.setAccount(account);
		memberInfo.setPassword(password);
		memberInfo = memberDAO.create(memberInfo);
		return memberInfo;
	}

	/**
	 * login for member and it would generate an id
	 * 
	 * @param account  member account
	 * @param password member password
	 * @return memberInfo
	 * @throws DAOException find failed
	 */
	public static MemberInfo login(String account, String password) throws DAOException {
		return memberDAO.find(account, password);
	}

	/**
	 * update data of memberInfo to database
	 * 
	 * @param memberInfo a memberInfo want to be update
	 * @return a updated memberInfo
	 * @throws IllegalArgumentException update failed
	 * @throws DAOException             update failed
	 */
	public static MemberInfo update(MemberInfo memberInfo) throws IllegalArgumentException, DAOException {
		return memberDAO.update(memberInfo);
	}

	/**
	 * change password and update it into database.
	 * 
	 * @param memberInfo a memberInfo need to be change password.
	 * @return a new password memberInfos
	 * @throws IllegalArgumentException change password failed
	 * @throws DAOException             change password failed
	 */
	public static MemberInfo changePassword(MemberInfo memberInfo) throws IllegalArgumentException, DAOException {
		return memberDAO.changePassword(memberInfo);
	}

	/**
	 * search shop by name, and it would not need to input full name.
	 * 
	 * @param name a fragment of shop name
	 * @return a list of shop
	 * @throws DAOException findAll failed
	 */
	public static List<ShopInfo> searchByName(String name) throws DAOException {

		return shopDAO.findAll(new Query(new Conditions(name, Fields.REST_NAME, Comparators.START_WITH))
				.union(new Query(new Conditions(name, Fields.REST_NAME, Comparators.CONTAIN)))
				.union(new Query(new Conditions(name, Fields.REST_NAME, Comparators.INTERMITTENT)))
				.union(new Query(new Conditions(name, Fields.REST_NAME, Comparators.SCATTER))));
	}

	/**
	 * find shop by id
	 * 
	 * @param ID shop id
	 * @return a shopInfo
	 * @throws DAOException find failed
	 */
	public static ShopInfo searchByID(Long ID) throws DAOException {
		return shopDAO.find(ID);
	}

	/**
	 * search shop by type
	 * 
	 * @param type shop type
	 * @return a list of shop
	 * @throws DAOException findAll failed
	 */
	public static List<ShopInfo> searchByType(String type) throws DAOException {
		return shopDAO.findAll(type, Fields.REST_TYPE);
	}

	/**
	 * search shop by whether shop has discount or not
	 * 
	 * @return a list of shop
	 * @throws DAOException findAll failed
	 */
	public static List<ShopInfo> searchByDiscount() throws DAOException {
		return shopDAO.findAll(0, Fields.REST_DISCOUNT, Comparators.NOT_EQUAL);
	}

	/**
	 * create a new orderInfo to order
	 * 
	 * @param orderInfo a orderInfo member want to order
	 * @return a ordered order
	 * @throws IllegalArgumentException create failed
	 * @throws DAOException             create failed
	 */
	public static OrderInfo order(OrderInfo orderInfo) throws IllegalArgumentException, DAOException {
		return orderDAO.create(orderInfo);
	}

	/**
	 * update order information
	 * 
	 * @param orderInfo a orderInfo member want to update
	 * @return a updated order
	 * @throws IllegalArgumentException update failed
	 * @throws DAOException             update failed
	 */
	public static OrderInfo updateOrder(OrderInfo orderInfo) throws IllegalArgumentException, DAOException {
		return orderDAO.update(orderInfo);
	}

	/**
	 * finish an arrived order
	 * 
	 * @param orderInfo an arrived order
	 * @throws IllegalArgumentException update failed
	 * @throws DAOException             update failed
	 */
	public static void finish(OrderInfo orderInfo) throws IllegalArgumentException, DAOException {
		orderInfo.setState(OrderState.FINISHED);
		orderDAO.update(orderInfo);
	}

	/**
	 * find history orders
	 * 
	 * @param memberInfo a memberInfo
	 * @return a list of history order
	 * @throws DAOException findAll failed
	 */
	public static List<OrderInfo> history(MemberInfo memberInfo) throws DAOException {
		return orderDAO.findAll(new Conditions(memberInfo.getId(), Fields.MEMBER_ID)
				.and(OrderState.FINISHED.name(), Fields.ORDER_STATE));
				
	}

	/**
	 * find orders which is not finished and discard.
	 * 
	 * @param memberInfo a memberInfo
	 * @return a list of checking orders
	 * @throws DAOException findAll failed
	 */
	public static List<OrderInfo> getOrderInfo(MemberInfo memberInfo) throws DAOException {

		return orderDAO.findAll(new Conditions(memberInfo.getId(), Fields.MEMBER_ID)
				.and(OrderState.FINISHED.name(), Fields.ORDER_STATE, Comparators.NOT_EQUAL)
				.and(OrderState.DISCARD.name(), Fields.ORDER_STATE, Comparators.NOT_EQUAL));
	}

	/**
	 * delete account from database
	 * 
	 * @param memberInfo a memberInfo
	 * @throws DAOException delete failed
	 */
	public static void delete(MemberInfo memberInfo) throws DAOException {
		memberDAO.delete(memberInfo);
	}

}
