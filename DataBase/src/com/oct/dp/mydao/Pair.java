package com.oct.dp.mydao;

/**
 * This class store a pair of any type.
 * @author Rick
 *
 * @param <T>	any type
 * @param <V>	any type
 */
public class Pair<T, V> {
	private T t;
	private V v;
	
	public Pair(T t , V v) {
		this.t = t;
		this.v = v;
	}

	/**
	 * Returns the first object of the pair.
	 * @return the first object of the pair.
	 */
	public T getFirst() {
		return t;
	}

	/**
	 * Sets the first object of the pair.
	 * @param t	- Object to be set.
	 */
	public void setFirst(T t) {
		this.t = t;
	}

	/**
	 * Returns the second object of the pair.
	 * @returnthe second object of the pair.
	 */
	public V getSecond() {
		return v;
	}

	/**
	 * Sets the first object of the pair.
	 * @param v - Object to be set.
	 */
	public void setSecond(V v) {
		this.v = v;
	}
	
}
