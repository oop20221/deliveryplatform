package com.oct.dp.mydao;

import com.oct.dp.model.UserInfo;

/**
 * This interface represents a contract for a DAO for all class extending the
 * {@link UserInfo} model.
 * Note that all methods which returns the Info from the DB, will not
 * fill the model with the password, due to security reasons.
 * 
 * @author Rick
 */
public interface UserDAO<T extends UserInfo> extends CrudDAO<T> {

    // Actions ------------------------------------------------------------------------------------

    /**
     * Returns the user from the database matching the given account and password, otherwise null.
     * @param account The account of the user to be returned.
     * @param password The password of the user to be returned.
     * @return The user from the database matching the given account and password, otherwise null.
     * @throws DAOException If something fails at database level.
     */
    public T find(String account, String password) throws DAOException;

    /**
     * Returns true if the given account address exist in the database.
     * @param account The account address which is to be checked in the database.
     * @return True if the given account address exist in the database.
     * @throws DAOException If something fails at database level.
     */
    public boolean existAccount(String account) throws DAOException;

    /**
     * Change the password of the given user. The user ID must not be null, otherwise it will throw
     * IllegalArgumentException.
     * @param user The user to change the password for.
     * @throws IllegalArgumentException If the user ID is null.
     * @throws DAOException If something fails at database level.
     */
    public T changePassword(T user) throws DAOException;

}
