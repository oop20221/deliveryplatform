package com.oct.dp.mydao;

import java.util.List;

import com.oct.dp.model.DataInfo;
import com.oct.dp.mydao.condition.Comparators;
import com.oct.dp.mydao.condition.Conditions;
import com.oct.dp.mydao.condition.Fields;
import com.oct.dp.mydao.condition.OrderBy;
import com.oct.dp.mydao.condition.Query;
import com.oct.dp.mydao.condition.Union;


/**
 * This interface represents a contract for a DAO for all class extending the
 * {@link DataInfo} model.
 * 
 * @author Rick
 */
public interface CrudDAO<T extends DataInfo> {
	
	/**
     * Returns the info from the database matching the given ID, otherwise null.
     * @param id The ID of the info to be returned.
     * @return The info from the database matching the given ID, otherwise null.
     * @throws DAOException If something fails at database level.
     */
    public T find(Long id) throws DAOException;
    
    /**
     * Returns the first occurance from the database matching the given field, otherwise null.
     * @param target	- the data to be searched for.
     * @param field		- the field to be matched with.
     * @return 			- The first occurance from the database matching the given field, otherwise null.
     * @throws DAOException If something fails at database level.
     */
    public T find(Object target, Fields field) throws IllegalArgumentException, DAOException;
    
    /**
     * Returs the first occurance from the database matching the given condition, otherwise null.
     * @param target	- the data to be searched for.
     * @param field		- the field to be matched with.
     * @param cmp		- the comparator to be used. If not specified, 
     * 					  {@link Comparators#EQUAL EQUAL} will be used by default.
     * @return 			- The first occurance from the database matching the given field, otherwise null.
     * @throws DAOException If something fails at database level.
     */
    public T find(Object target, Fields field, Comparators cmp) throws IllegalArgumentException, DAOException;
    
    /**
     * Returs the first occurance from the database matching the given condition, otherwise null.
     * @param conditions	- {@link Conditions} that contains the condition to be matched.
     * @return 	- the first occurance from the database matching the given condition, otherwise null.
     * @throws DAOException If something fails at database level.
     */
    public T find(Conditions conditions) throws IllegalArgumentException, DAOException;
    
    /**
     * Returs the first data from the database after sorted in the given way, otherwise null.
     * @param orderBy	- OrderBy containing the field to sort with and whether it should be descending or not.
     * @return 	- the first occurance from the database matching the given condition, otherwise null.
     * @throws DAOException If something fails at database level.
     */
    public T find(OrderBy orderBy) throws IllegalArgumentException, DAOException;
    
    /**
     * Returs the first data from the database matching the given condition after sorted in the given way.
     * , otherwise null.
     * @param conditions- Conditions that contains the condition to be matched.
     * @param orderBy	- OrderBy containing the field to sort with and whether it should be descending or not.
     * @return 	- the first occurance from the database matching the given condition, otherwise null.
     * @throws DAOException If something fails at database level.
     */
    public T find(Conditions conditions, OrderBy orderBy) throws IllegalArgumentException, DAOException;
    
    /**
     * Returs the first data from the database matching the given query, otherwise null.
     * The list is never null and is empty when the database does not contain any info.
     * @param query - Query that contains the condition to be matched and how the data 
     * 				should be sorted.
     * @return 	- the first occurance from the database matching the given query, otherwise null.
     * @throws DAOException If something fails at database level.
     */
    public T find(Query query) throws IllegalArgumentException, DAOException;
    
    /**
     * Returs the first data from the database matching the given Union, otherwise null.
     * The list is never null and is empty when the database does not contain any info.
     * @param union - Union that can contain multiple queries.
     * @return 	- the first occurance from the database matching the given union, otherwise null.
     * @throws DAOException If something fails at database level.
     */
    public T find(Union union) throws IllegalArgumentException, DAOException;
    
    // findAll ------------------------------------------------------------------------------------------------------------
    
    /**
     * Returs all infos from the database matching the given field.
     * The list is never null and is empty when the database does not contain any info.
     * @param target	- the data to be searched for.
     * @param field		- the field to be matched with.
     * @return 			- A list of all infos from the database matching the given field.
     * @throws DAOException If something fails at database level.
     */
    public List<T> findAll(Object target, Fields field) throws IllegalArgumentException, DAOException;
    
    /**
     * Returs all infos from the database matching the given condition.
     * The list is never null and is empty when the database does not contain any info.
     * @param target	- the data to be searched for.
     * @param field		- the field to be matched with.
     * @param cmp		- the comparator to be used. If not specified, 
     * 					  {@link Comparators#EQUAL EQUAL} will be used by default.
     * @return 			- A list of all infos from the database matching the given condition.
     * @throws DAOException If something fails at database level.
     */
    public List<T> findAll(Object target, Fields field, Comparators cmp) throws IllegalArgumentException, DAOException;
    
    /**
     * Returs all infos from the database matching all given conditions.
     * The list is never null and is empty when the database does not contain any info.
     * <p> example
     * <pre>{@code 
     * public static List<OrderInfo> history(ShopInfo shopInfo) throws DAOException {
     *     return orderDAO.findAll(
     *                 new Conditions(shopInfo.getId(), Fields.REST_ID),
     *                 new Conditions(OrderState.WAITING, Fields.ORDER_STATE, Comparators.NOT_EQUAL)
     *            );
     * } </pre>
     * @param conditions	- Array of {@link Conditions} that contains all conditions to be matched.
     * @return 	- A list of all infos from the database matching the given conditions.
     * @throws DAOException If something fails at database level.
     */
    public List<T> findAll(Conditions condition) throws IllegalArgumentException, DAOException;
    
    /**
     * Returs all infos from the database.
     * The list is never null and is empty when the database does not contain any info.
     * @return 	- A list of all infos from the database.
     * @throws DAOException If something fails at database level.
     */
    public List<T> findAll() throws IllegalArgumentException, DAOException;
    
    /**
     * Returs all infos from the database matching the given condition.
     * The list is never null and is empty when the database does not contain any info.
     * @param conditions	- {@link Conditions} that contains the condition to be matched.
     * @param orderBy	- how the data should be sorted.
     * @return 	- A list of all infos from the database matching the given condition.
     * @throws DAOException If something fails at database level.
     */
    public List<T> findAll(Conditions conditions, OrderBy orderBy) throws IllegalArgumentException, DAOException;
    
    /**
     * Returs all infos from the database matching the given condition.
     * The list is never null and is empty when the database does not contain any info.
     * @param conditions	- {@link Conditions} that contains the condition to be matched.
     * @param limit		- limit the number of returned data.
     * @return 	- A list of all infos from the database matching the given condition.
     * @throws DAOException If something fails at database level.
     */
    public List<T> findAll(Conditions conditions, Integer limit) throws IllegalArgumentException, DAOException;
    
    /**
     * Returs all infos from the database matching the given condition.
     * The list is never null and is empty when the database does not contain any info.
     * @param conditions	- {@link Conditions} that contains the condition to be matched.
     * @param orderBy	- how the data should be sorted.
	 * @param limit		- limit the number of returned data.
     * @return 	- A list of all infos from the database matching the given condition.
     * @throws DAOException If something fails at database level.
     */
    public List<T> findAll(Conditions conditions, OrderBy orderBy, Integer limit) throws IllegalArgumentException, DAOException;
    
    /**
     * Returs all infos from the database matching the given condition.
     * The list is never null and is empty when the database does not contain any info.
     * @param orderBy	- how the data should be sorted.
     * @return 	- A list of all infos from the database matching the given condition.
     * @throws DAOException If something fails at database level.
     */
    public List<T> findAll(OrderBy orderBy) throws IllegalArgumentException, DAOException;
    
    /**
     * Returs all infos from the database matching the given condition.
     * The list is never null and is empty when the database does not contain any info.
     * @param limit		- limit the number of returned data.
     * @return 	- A list of all infos from the database matching the given condition.
     * @throws DAOException If something fails at database level.
     */
    public List<T> findAll(Integer limit) throws IllegalArgumentException, DAOException;
    
    /**
     * Returs all infos from the database matching the given condition.
     * The list is never null and is empty when the database does not contain any info.
     * @param orderBy	- how the data should be sorted.
     * @param limit		- limit the number of returned data.
     * @return 	- A list of all infos from the database matching the given condition.
     * @throws DAOException If something fails at database level.
     */
    public List<T> findAll(OrderBy orderBy, Integer limit) throws IllegalArgumentException, DAOException;
    
    /**
     * Returs all infos from the database matching the given condition.
     * The list is never null and is empty when the database does not contain any info.
     * @param query	- Query that contains the condition to be matched.
     * @return 	- A list of all infos from the database matching the given condition.
     * @throws DAOException If something fails at database level.
     */
    public List<T> findAll(Query query) throws IllegalArgumentException, DAOException;
    
    /**
     * Returs all infos from the database matching the given condition.
     * The list is never null and is empty when the database does not contain any info.
     * @param union	- Union that contains the condition to be matched.
     * @return 	- A list of all infos from the database matching the given condition.
     * @throws DAOException If something fails at database level.
     */
    public List<T> findAll(Union union) throws IllegalArgumentException, DAOException;


    /**
     * Create the given info in the database. The info ID must be null, otherwise it will throw
     * IllegalArgumentException. After creating, the DAO will set the obtained ID in the given info.
     * @param info The info to be created in the database.
     * @return The info after created.
     * @throws IllegalArgumentException If the info ID is not null.
     * @throws DAOException If something fails at database level.
     */
    public T create(T info) throws IllegalArgumentException, DAOException;

    /**
     * Update the given info in the database. The info ID must not be null, otherwise it will throw
     * IllegalArgumentException. Note: the password will NOT be updated. Use changePassword() instead.
     * @param info The info to be updated in the database.
     * @return The info after updated.
     * @throws IllegalArgumentException If the info ID is null.
     * @throws DAOException If something fails at database level.
     */
    public T update(T info) throws IllegalArgumentException, DAOException;

    /**
     * Delete the given info from the database. After deleting, the DAO will set the ID of the given
     * info to null.
     * @param info The info to be deleted from the database.
     * @return The info after deleted.
     * @throws DAOException If something fails at database level.
     */
    public T delete(T info) throws DAOException;
}
