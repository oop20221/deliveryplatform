package com.oct.dp.mydao.condition;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.function.Consumer;

/**
 * 
 * @author Rick
 *
 */
public class OrderBy {
	
	private List<By> Bys;
	
	private class By{
		private Fields field;
		private boolean Desc;
		
		private By(Fields field, boolean Desc) {
			this.field = field;
			this.Desc = Desc;
		}
		
		private By(Fields field) {
			this.field = field;
			this.Desc = false;
		}
		
		private String toSql() {
			if (Desc)
				return field.toString() + " " + "DESC";
			return field.toString();
		}
	}
	
	public OrderBy() {
		Bys = new ArrayList<>();
	}
	
	public OrderBy(Fields field, boolean Desc) {
		this();
		Bys.add(new By(field, Desc));
	}
	
	public OrderBy(Fields field) {
		this(field, false);
	}
	
	public OrderBy(boolean Desc, Fields... fields) {
		for (Fields field : fields) {
			Bys.add(new By(field, Desc));
		}
	}
	
	public OrderBy(Fields... fields) {
		this(false, fields);
	}
	
	public OrderBy add(Fields field, boolean Desc) {
		Bys.add(new By(field, Desc));
		return this;
	}
	
	public OrderBy add(Fields field) {
		return add(field, false);
	}
	
	public boolean isValidField(EnumSet<Fields> fields) {
		for (By by: Bys) {
			if (!fields.contains(by.field)) 
				return false;
		}
		return true;
	}
	
	public String toSql() {
		StringBuilder orderBy = new StringBuilder(" ORDER BY ");
		boolean start = true;
		By by;
    	for (int i = 0; i < Bys.size(); i++) 
    	{
    		by = Bys.get(i);
    		if (Bys.get(i) == null) continue;
			
			// add order statement to stringBuilder
			if (start) {
				orderBy.append(by.toSql());
				start = false;
			}
    		else
    			orderBy.append(", ").append(by.toSql());
		}
    	return orderBy.toString();
	}
	
	public String toSql(Consumer<Fields> check) {
		StringBuilder orderBy = new StringBuilder(" ORDER BY ");
		boolean start = true;
		By by;
    	for (int i = 0; i < Bys.size(); i++) 
    	{
    		by = Bys.get(i);
    		if (Bys.get(i) == null) continue;
    		
    		// check if given field is valid
    		check.accept(by.field);
			
			// add condition statement to stringBuilder
			if (start) {
				orderBy.append(by.toSql());
				start = false;
			}
    		else
    			orderBy.append(", ").append(by.toSql());
		}
    	return orderBy.toString();
	}
}

