package com.oct.dp.mydao.condition;

import java.util.EnumSet;

/**
 * A enum containing some common ways of comparison.
 * @author Rick
 */
public enum Comparators{
	
	// Enum ---------------------------------------------------------------------------------------------------
	/**literal "=" */	EQUAL, 
	/**literal ">" */	GREATER, 
	/**literal "<" */	LESS, 
	/**literal ">=" */	GREATER_EQUAL, 
	/**literal "<=" */	LESS_EQUAL, 
	/**literal "<>" */	NOT_EQUAL,
	
	/**Matches all result containing the given string. Can only be used on a String field.*/				
	CONTAIN,
	/**Matches all result starting with the given string. Can only be used on a String field.*/
	START_WITH,
	/**Matches all result ending with the given string. Can only be used on a String field.*/
	END_WITH,
	/**Matches all result containing the given string, 
	 * but not necessarily contiguous. Can only be used on a String field.*/
	INTERMITTENT,
	/**Matches all result containing the given string, 
	 * but not necessarily contiguous or in the same order.
	 *  Can only be used on a String field.*/
	SCATTER;
	
	// Static fields ------------------------------------------------------------------------------------------
	
	private static final EnumSet<Comparators> like = EnumSet.of(
			CONTAIN, START_WITH, END_WITH, INTERMITTENT, SCATTER);		
	
	// Methods ------------------------------------------------------------------------------------------------
	
	/**
	 * Returns whether the given comparator's use is to do similar search.
	 * @param cmp	- comparator to check.
	 * @return whether the given comparator's use is to do similar search.
	 */
	public boolean isLike() {
		return like.contains(this);
	}
	
	public String toString() {
		switch (this) {
		// exact search
		case EQUAL:			return "=";
		case GREATER:		return ">";	
		case LESS:			return "<";
		case GREATER_EQUAL:	return ">=";
		case LESS_EQUAL:	return "<=";
		case NOT_EQUAL:		return "<>";
		
		//similar search
		case CONTAIN:	
		case START_WITH:	
		case END_WITH:	
		case INTERMITTENT:	return "LIKE";
		case SCATTER: 		return "RLIKE";
		
		default:
			return null;
		}
	}
}