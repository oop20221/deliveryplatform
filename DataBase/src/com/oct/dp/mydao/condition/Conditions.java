package com.oct.dp.mydao.condition;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Function;


/**
 * 
 * @author Rick
 *
 */
public class Conditions {
	
	private static final String AND = "AND";
	private static final String OR = "OR";
	
	private List<Condition> conditions;
	private List<Conditions> nestedConditions;
	private List<Object> values;
	
	private boolean isNull = true;
	
	// Inner Class --------------------------------------------------------------------------------------------------
	
	private class Condition{
		private String bool;
		private Fields field;
		private Comparators cmp;
		private boolean isNested = false;
		private int index;
		
		// Constructors ------------------------------------------------------------------
		public Condition(String bool, int index) {
			this.bool = bool;
			isNested = true;
			this.index = index;
		}
		
		public Condition(String bool, Fields field, Comparators cmp) {
			this.bool = bool;
			this.field = field;
			this.cmp = cmp;
		}
		
		// Methods --------------------------------------------------------------------------
		public String toSqlWithoutPrefix() {
			return field.toString() +" "+ cmp.toString() +" ?";
		}	
		
		public String toSqlWithoutPrefix(Function<Fields, String> checkFieldAndSubquery) {
			if (checkFieldAndSubquery == null) return toSqlWithoutPrefix();
			
			String query = checkFieldAndSubquery.apply(field);
			if (query == null)
				return toSqlWithoutPrefix();
			else
				return query +" "+ cmp.toString() +" ?)";
		}	
		
		public String toSql() {
			return bool +" "+ field.toString() +" "+ cmp.toString() +" ?";
		}
		
		public String toSql(Function<Fields, String> checkFieldAndSubquery) {
			if (checkFieldAndSubquery == null) return toSql();
			
			String query = checkFieldAndSubquery.apply(field);
			if (query == null)
				return toSql();
			else
				return bool +" "+ query +" "+ cmp.toString() +" ?)";
		}	
		
	}
	
	
	
	// Constructors -------------------------------------------------------------------------------------------
	
	public Conditions() {}

	public Conditions(Object data, Fields field, Comparators cmp) {
		checkNotString(field, cmp);
		checkValueIsEnum(data);
		checkValidCondition(field, cmp);
		init();
		conditions.add(new Condition(AND, field, cmp));
		values.add(data);
	}

	public Conditions(Object data, Fields field) {
		this(data, field, Comparators.EQUAL);
	}
	
	public Conditions(String data, Fields field, Comparators cmp) {
		checkIsString(field);
		checkValidCondition(field, cmp);
		init();
		conditions.add(new Condition(AND, field, cmp));
		values.add(toWildCard(data, cmp));
	}

	public Conditions(String data, Fields field) {
		this(data, field, Comparators.EQUAL);
	}
	
	// Methods -----------------------------------------------------------------------------------------------
	
	/**
	 * Adds a condition with "AND" boolean operator.
	 * @param data	- data to be matched with.
	 * @param field	- field to match with data.
	 * @param cmp	- the comparator to be used. If not specified, 
     * 				  {@link Comparators#EQUAL EQUAL} will be used by default.
	 * @return	this
	 */
	public Conditions and(Object data, Fields field, Comparators cmp) {
		checkNotString(field, cmp);
		checkValueIsEnum(data);
		checkValidCondition(field, cmp);
		init();
		conditions.add(new Condition(AND, field, cmp));
		values.add(data);
		return this;
	}
	
	/**
	 * Adds a condition with "AND" boolean operator.
	 * @param data	- data to be matched with.
	 * @param field	- field to match with data.
	 * @return	this
	 */
	public Conditions and(Object data, Fields field) {
		return and(data, field, Comparators.EQUAL);
	}
	
	/**
	 * Adds a condition with "AND" boolean operator.
	 * @param data	- data to be matched with.
	 * @param field	- field to match with data.
	 * @param cmp	- the comparator to be used. If not specified, 
     * 				  {@link Comparators#EQUAL EQUAL} will be used by default.
	 * @return	this
	 */
	public Conditions and(String data, Fields field, Comparators cmp) {
		checkIsString(field);
		checkValidCondition(field, cmp);
		init();
		conditions.add(new Condition(AND, field, cmp));
		values.add(toWildCard(data, cmp));
		return this;
	}
	
	/**
	 * Adds a condition with "AND" boolean operator.
	 * @param data	- data to be matched with.
	 * @param field	- field to match with data.
	 * @return	this
	 */
	public Conditions and(String data, Fields field) {
		return and(data, field, Comparators.EQUAL);
	}
	
	public Conditions and(Conditions nestedCondition) {
		if (nestedCondition == null || nestedCondition.isNull()) return this;
		init();
		if (nestedConditions == null) nestedConditions = new ArrayList<>();
		conditions.add(new Condition(AND, nestedConditions.size()));
		nestedConditions.add(nestedCondition);
		values.addAll(nestedCondition.getValuesList());
		nestedCondition.resetValues();
		return this;
	}
	
	/**
	 * Adds a condition with "OR" boolean operator.
	 * @param data	- data to be matched with.
	 * @param field	- field to match with data.
	 * @param cmp	- the comparator to be used. If not specified, 
     * 				  {@link Comparators#EQUAL EQUAL} will be used by default.
	 * @return	this
	 */
	public Conditions or(Object data, Fields field, Comparators cmp) {
		checkNotString(field, cmp);
		checkValueIsEnum(data);
		checkValidCondition(field, cmp);
		init();
		conditions.add(new Condition(OR, field, cmp));
		values.add(data);
		return this;
	}
	
	/**
	 * Adds a condition with "OR" boolean operator.
	 * @param data	- data to be matched with.
	 * @param field	- field to match with data.
	 * @return	this
	 */
	public Conditions or(Object data, Fields field) {
		return or(data, field, Comparators.EQUAL);
	}
	
	/**
	 * Adds a condition with "OR" boolean operator.
	 * @param data	- data to be matched with.
	 * @param field	- field to match with data.
	 * @param cmp	- the comparator to be used. If not specified, 
     * 				  {@link Comparators#EQUAL EQUAL} will be used by default.
	 * @return	this
	 */
	public Conditions or(String data, Fields field, Comparators cmp) {
		checkIsString(field);
		checkValidCondition(field, cmp);
		init();
		conditions.add(new Condition(OR, field, cmp));
		values.add(toWildCard(data, cmp));
		return this;
	}
	
	/**
	 * Adds a condition with "OR" boolean operator.
	 * @param data	- data to be matched with.
	 * @param field	- field to match with data.
	 * @return	this
	 */
	public Conditions or(String data, Fields field) {
		return or(data, field, Comparators.EQUAL);
	}
	
	public Conditions or(Conditions nestedCondition) {
		if (nestedCondition == null || nestedCondition.isNull()) return this;
		init();
		if (nestedConditions == null) nestedConditions = new ArrayList<>();
		conditions.add(new Condition(OR, nestedConditions.size()));
		nestedConditions.add(nestedCondition);
		values.addAll(nestedCondition.getValuesList());
		nestedCondition.resetValues();
		return this;
	}

	public Object[] getValues() {
		if (values == null || values.size() == 0) return null;
		return values.toArray();
	}
	
	public List<Object> getValuesList() {
		if (values == null || values.size() == 0) return null;
		return values;
	}
	
	public void resetValues() {
		values = null;
	}
	
	/**
	 * Returns the sql condition statement to put into a sql query without checking.
	 * @return the sql condition statement to put into a sql query without checking.
	 */
	public String toSql() {
		return toSql(null);
	}
	
	/**
	 * Returns the sql condition statement to put into a sql query.
	 * @return the sql condition statement to put into a sql query.
	 */
	public String toSql(Function<Fields, String> checkFieldAndSubquery) {
		if (isNull) return null;
		if (conditions.size() == 0) return null;
		if (nestedConditions != null) return toNestedSql(checkFieldAndSubquery);
		
		// treat the first element specially
		StringBuilder condBuilder = new StringBuilder(conditions.get(0).toSqlWithoutPrefix(checkFieldAndSubquery));
		
    	for (int i = 1; i < conditions.size(); i++) {
    		condBuilder.append(" ").append(conditions.get(i).toSql(checkFieldAndSubquery));
    	}
    	return condBuilder.toString();
	}
	
	public boolean isNull() { return isNull; }		
	
	public int getSize() { return conditions == null? 0: conditions.size(); }
	
	// Helpers ----------------------------------------------------------------------------------------------------------
	
	/**
	 * Returns the sql condition statement to put into a sql query.
	 * @return the sql condition statement to put into a sql query.
	 */
	private String toNestedSql(Function<Fields, String> checkFieldAndSubquery) {	
		StringBuilder condBuilder = new StringBuilder();
		Condition current = conditions.get(0);

		// treat the first element specially
		if (current.isNested) {
			if (nestedConditions.get(0).getSize() > 1) {
				condBuilder.append("(")
						   .append(nestedConditions.get(0).toSql(checkFieldAndSubquery))
						   .append(")");
			}else {
				condBuilder.append(nestedConditions.get(0).toSql(checkFieldAndSubquery));
			}
		}else {
			condBuilder.append(current.toSqlWithoutPrefix(checkFieldAndSubquery));
		}
		
    	for (int i = 1; i < conditions.size(); i++) {
    		current = conditions.get(i); 	
    		condBuilder.append(" ");
    		if (current.isNested) {
    			if (nestedConditions.get(0).getSize() > 1) {
	    			condBuilder.append(current.bool)
	    					   .append(" (")
	    					   .append(nestedConditions.get(current.index).toSql(checkFieldAndSubquery))
	    					   .append(")");
    			}else {
    				condBuilder.append(nestedConditions.get(current.index).toSql(checkFieldAndSubquery));
    			}
    		}else {
    			condBuilder.append(current.toSql(checkFieldAndSubquery));
    		}
    	}
    	return condBuilder.toString();
	}
	
	private void init() {
		if (conditions == null) conditions = new ArrayList<>();
		if (values == null) values = new ArrayList<>();
		isNull = false;
	}
	
	private void checkValueIsEnum(Object value) throws IllegalArgumentException{
		if (value.getClass().isEnum())
			throw new IllegalArgumentException(
					"Enum can't be passed to this method. Please use enum.name() to convert it to String.");
	}
	
	private String toWildCard(String data, Comparators cmp) {
		switch (cmp) {
		case CONTAIN:
			return "%" + data + "%";
		case START_WITH:
			return  data + "%";
		case END_WITH:
			return "%" + data;
		case INTERMITTENT:
			return "%" + String.join("%", data.split("")) + "%";
		case SCATTER:	// "(?=.*a)(?=.*b)(...
			Set<Character> charSet = new LinkedHashSet<Character>();
			for (int i = 0; i < data.length(); i++)
			    charSet.add(data.charAt(i)); 
			
			StringBuilder sb = new StringBuilder(charSet.size()*6);
			for (Character c : charSet) {
				sb.append("(?=.*").append(c).append(")");
			}
			return sb.toString();

		default:
			return data;
		}
	}
	
	/**
	 * Checks if the 
	 * @param field
	 * @param cmp
	 * @throws IllegalArgumentException
	 */
	private void checkValidCondition(Fields field, Comparators cmp) throws IllegalArgumentException{
		if (cmp.isLike() && !field.isStringField())
			throw new IllegalArgumentException(String.format(
					"Comparators %s is not available to Fields %s,"
					+ "because %s is not a String fields", cmp.name(), field.name(), field.name()));
	}
	
	private void checkNotString(Fields field, Comparators cmp) throws IllegalArgumentException{
		if (cmp.isLike())
			throw new IllegalArgumentException(String.format(
					"Comparators %s is not available to this data,"
					+ "because it is not a String", cmp.name()));
		if (field.isStringField()) 
			throw new IllegalArgumentException(String.format(
					"Data can't match with Fields %s,"
					+ "because data is not a String", field.name()));
	}
	
	private void checkIsString(Fields field) throws IllegalArgumentException{
		if (!field.isStringField())
			throw new IllegalArgumentException(String.format(
					"Data can't match with Fields %s,"
					+ "because %s is not a String", field.name(), field.name()));
	}
	
}


