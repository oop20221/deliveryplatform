package com.oct.dp.mydao.condition;

import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;


/**
 * 
 * @author Rick
 *
 */
public class Query {

	private Query query;
	private Conditions conditions;
	private OrderBy orderBy;
	private Integer limit;
	
	// Constructors -------------------------------------------------------------------------------------------
	
	public Query() {}
	
	public Query(Conditions conditions) {
		this.conditions = conditions;
	}
	
	public Query(Conditions conditions, OrderBy orderBy) {
		this.conditions = conditions;
		this.orderBy = orderBy;
	}
	
	public Query(Conditions conditions, Integer limit) {
		this.conditions = conditions;
		this.limit = limit;
	}
	
	public Query(Conditions conditions, OrderBy orderBy, Integer limit) {
		this(conditions, orderBy);
		this.limit = limit;
	}
	
	public Query(OrderBy orderBy) {
		this.orderBy = orderBy;
	}
	
	public Query(Integer limit) {
		this.limit = limit;
	}
	
	public Query(OrderBy orderBy, Integer limit) {
		this.orderBy = orderBy;
		this.limit = limit;
	}
	
	public Query(Query query, OrderBy orderBy) {
		this.query = query;
		this.orderBy = orderBy;
	}
	
	public Query(Query query, Integer limit) {
		this.query = query;
		this.limit = limit;
	}
	
	public Query(Query query, OrderBy orderBy, Integer limit) {
		this(query, orderBy);
		this.limit = limit;
	}
	
	// Methods -----------------------------------------------------------------------------------------------------------
	
	public Union union(Query query) {
		return new Union(this, query);
	}
	
	public Query setLimitIfNotExist(int limit) {
		if (this.limit == null)
			this.limit = limit;
		return this;
	}
	
	public Object[] getValues() {
		if (query != null)
			return query.getValues();
		return (conditions == null || conditions.isNull())? new Object[0]: conditions.getValues();
	}
	
	public List<Object> getValuesList(){
		if (query != null)
			return query.getValuesList();
		return (conditions == null || conditions.isNull())? Collections.emptyList(): conditions.getValuesList();
	}
	
	public String toSql(String sqlQuery) {
		if (query != null) 
			return "(" + query.toSql(sqlQuery) + ")"
				+ ((orderBy==null)? "": orderBy.toSql())
				+ ((limit==null || limit < 1)? "": " LIMIT " + limit);

		return sqlQuery 
				+ ((conditions==null)? "": " WHERE " + conditions.toSql())
				+ ((orderBy==null)? "": orderBy.toSql())
				+ ((limit==null || limit < 1)? "": " LIMIT " + limit);
	}
	
	public String toSql(String sqlQuery, Function<Fields, String> checkFieldAndSubquery, Consumer<Fields> checkOrderingField) {
		if (query != null) 
			return "(" + query.toSql(sqlQuery) + ")"
				+ ((orderBy==null)? "": orderBy.toSql(checkOrderingField))
				+ ((limit==null || limit < 1)? "": " LIMIT " + limit);

		return sqlQuery 
				+ ((conditions==null)? "": " WHERE " + conditions.toSql(checkFieldAndSubquery))
				+ ((orderBy==null)? "": orderBy.toSql(checkOrderingField))
				+ ((limit==null || limit < 1)? "": " LIMIT " + limit);
	}
}
