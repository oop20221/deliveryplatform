package com.oct.dp.mydao.condition;

import java.util.EnumSet;

/**
 * Defines all available fields for every data infos.
 * These fields are used to find data in the database.
 * @author Rick
 */
public enum Fields {
	
	// platform
	/**{@link String} field. <p>user's name.*/			USER_NAME,	
	/**{@link String} field. <p>restaurant's name.*/	REST_NAME,
	/**{@link String} field. <p>restaurant's type.*/	REST_TYPE,	
	/**{@link Long} field. <p>restaurant's id.*/		REST_ID,
	/**double field. <p>restaurant's discount.*/REST_DISCOUNT,
	/**{@link Long} field. <p>deliveryman's id.*/		DELIV_ID,
	/**{@link Long} field. <p>member's id*/				MEMBER_ID,
	/**{@link com.oct.dp.model.OrderInfo.OrderState} 
	 * field. <p>an order's state.*/
	ORDER_STATE,
	
	// 2048
	/**int field. <p>player's highest score.*/PLAYER_BEST_SCORE,
	/**Date field. <p>The date the player achieved the highest score.*/PLAYER_BEST_SCORE_DATE,
	
	// bank
	CUSTOMER_ACCOUNT,
	CUSTOMER_ID,
	CUSTOMER_TOKEN,
	CUSTOMER_KEY,
	CUSTOMER_TOKEN_STATE;
	
	private static EnumSet<Fields> stringFields = EnumSet.of(
			USER_NAME, REST_NAME, REST_TYPE, ORDER_STATE, 
			CUSTOMER_ACCOUNT, CUSTOMER_TOKEN, CUSTOMER_KEY, CUSTOMER_TOKEN_STATE);
	
	/**
	 * Returns if the given field is a String field.
	 * @param field	- field to check.
	 * @return	whether the given field is a String field.
	 */
	public boolean isStringField() {
		return stringFields.contains(this);
	}
	
	public String toString() {
		switch (this) {
		case USER_NAME:
			return "user_name";
		case REST_NAME:
			return "rest_name";
		case REST_TYPE:
			return "rest_type";
		case REST_ID:
			return "rest_id";
		case ORDER_STATE:
			return "order_state";
		case REST_DISCOUNT:
			return "discount";
		case DELIV_ID:
			return "deliv_id";
		case MEMBER_ID:
			return "member_id";
		case PLAYER_BEST_SCORE:
			return "best_score";
		case PLAYER_BEST_SCORE_DATE:
			return "best_score_date";
		case CUSTOMER_ACCOUNT:
			return "account";
		case CUSTOMER_ID:
			return "customer_id";
		case CUSTOMER_TOKEN:
			return "customer_token";
		case CUSTOMER_KEY:
			return "customer_key";
		case CUSTOMER_TOKEN_STATE:
			return "customer_token_state";

		default:
			return null;
		}
	}
}
