package com.oct.dp.mydao.condition;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;


/**
 * 
 * @author Rick
 *
 */
public class Union {
	
	private List<Query> queries;
	private List<Object> values;
	private Integer limit;
	
	public Union(Query a, Query b) {
		queries = new ArrayList<>();
		values = new ArrayList<>();
		if (a != null) {
			queries.add(a);
			values.addAll(a.getValuesList());
		}
		if (b != null) {
			queries.add(b);
			values.addAll(b.getValuesList());
		}		
	}
	
	public Union union(Query query) {
		if (query != null) {
			queries.add(query);
			values.addAll(query.getValuesList());
		}		
		return this;
	}
	
	// Methods ----------------------------------------------------------------------------------------------
	
	public Object[] getValues() {
		return values.toArray();
	}
	
	public String toSql(String sqlQuery) {
		if (queries.size() == 0) return null;
		if (queries.size() == 1) return queries.get(0).toSql(sqlQuery);
		
		StringBuilder sb = new StringBuilder();
		sb.append("(").append(queries.get(0).toSql(sqlQuery)).append(")");
		
		for (int i = 1; i < queries.size(); i++) {
			sb.append(" UNION (").append(queries.get(i).toSql(sqlQuery)).append(")");
		}
		if (limit > 0) sb.append(" limit ").append(limit);
		return sb.toString();
	}
	
	public String toSql(String sqlQuery, Function<Fields, String> checkFieldAndSubquery, Consumer<Fields> checkOrderingField) {
		if (queries.size() == 0) return null;
		if (queries.size() == 1) return queries.get(0).toSql(sqlQuery, checkFieldAndSubquery, checkOrderingField);
		
		StringBuilder sb = new StringBuilder();
		sb.append("(").append(queries.get(0).toSql(sqlQuery, checkFieldAndSubquery, checkOrderingField)).append(")");
		
		for (int i = 1; i < queries.size(); i++) {
			sb.append(" UNION (").append(queries.get(i).toSql(sqlQuery, checkFieldAndSubquery, checkOrderingField)).append(")");
		}
		if (limit > 0) sb.append(" limit ").append(limit);
		return sb.toString();
	}
	
	public Union setLimitIfNotExist(int limit) {
		if (this.limit == null)
			this.limit = limit;
		return this;
	}
}
