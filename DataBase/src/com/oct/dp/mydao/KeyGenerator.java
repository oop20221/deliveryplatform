package com.oct.dp.mydao;

import java.math.BigInteger;
import java.security.SecureRandom;

public class KeyGenerator {

	public static String generateRandomHexToken(int byteLength) {
	    SecureRandom secureRandom = new SecureRandom();
	    byte[] token = new byte[byteLength];
	    secureRandom.nextBytes(token);
	    return new BigInteger(1, token).toString(16); // Hexadecimal encoding
	}
	
	
	public static String generateRandomAlphaNumToken(int byteLength) {
	    SecureRandom secureRandom = new SecureRandom();
	    byte[] token = new byte[byteLength];
	    secureRandom.nextBytes(token);
	    return new BigInteger(1, token).toString(36); 
	}

}
