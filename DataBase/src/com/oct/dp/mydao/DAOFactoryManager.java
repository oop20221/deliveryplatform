package com.oct.dp.mydao;

import java.util.HashMap;
import java.util.Map;

/**
 * This class is reponse for managing all DAOFactories.
 * @author Rick
 *
 */
public class DAOFactoryManager {
	
	private static Map<String, DAOFactory> factories;

    private static DAOFactoryManager instance;
    
    // private constructor makes sure one can only gets a instance of this class by getInstance().
    private DAOFactoryManager() {
    	factories = new HashMap<>();
    }
    
	public static DAOFactoryManager getInstance() {
		if (instance == null) 
			// use synchronization to achieve thread-safe singleton
			synchronized (DAOFactoryManager.class) {
				if (instance == null) instance = new DAOFactoryManager();
			}			
    	return instance;
	}
	
	/**
	 * Returns a DAOFactory for the given database name.
	 * If a instance with the same name already exist,
	 * return that instance so that a connection pool can
	 * be use to manage all connections connecting to the database.
	 * @param database	- name of database
	 * @return Returns a DAOFactory for the given database name, if exist.
	 * 		   Else return null.
	 */
	public DAOFactory getFactory(String database) {
		return factories.get(database);
	}
	
	/**
	 * Register a DAOFactory with the given name.
	 * @param database	- name of database
	 * @param factory	- a DAOFactory
	 * @return
	 */
	public DAOFactory addFactory(String database, DAOFactory factory) {
		return factories.put(database, factory);
	}
}
