package com.oct.dp.mydao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import com.oct.dp.model.CustomerInfo;
import com.oct.dp.model.DeliveryInfo;
import com.oct.dp.model.FoodInfo;
import com.oct.dp.model.MemberInfo;
import com.oct.dp.model.OrderInfo;
import com.oct.dp.model.PlayerInfo;
import com.oct.dp.model.ShopInfo;
import com.oct.dp.model.TokenInfo;
import com.oct.dp.mydao.implementation.CustomerDAOJDBC;
import com.oct.dp.mydao.implementation.DeliveryDAOJDBC;
import com.oct.dp.mydao.implementation.FoodDAOJDBC;
import com.oct.dp.mydao.implementation.MemberDAOJDBC;
import com.oct.dp.mydao.implementation.OrderDAOJDBC;
import com.oct.dp.mydao.implementation.PlayerDAOJDBC;
import com.oct.dp.mydao.implementation.ShopDAOJDBC;
import com.oct.dp.mydao.implementation.TokenDAOJDBC;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

/**
 * This class represents a DAO factory for a SQL database. You can use {@link #getInstance(String)}
 * to obtain an instance for the given database name. If an instance with the same database name 
 * already exist in {@link DAOFactoryManager}, the existed instance will be returned so that 
 * connections can be reused with the same connection pool. The specific instance returned depends on
 * the properties file configuration. You can obtain DAO's for the DAO factory instance using the 
 * DAO getters.
 * <p>
 * This class requires a properties file named 'dao.properties' in the classpath with among others
 * the following properties:
 * <pre>
 * name.url *
 * name.driver
 * name.username
 * name.password
 * </pre>
 * Those marked with * are required, others are optional and can be left away or empty. Only the
 * username is required when any password is specified.
 * <ul>
 * <li>The 'name' must represent the database name in {@link #getInstance(String)}.</li>
 * <li>The 'name.url' must represent either the JDBC URL or JNDI name of the database.</li>
 * <li>The 'name.driver' must represent the full qualified class name of the JDBC driver.</li>
 * <li>The 'name.username' must represent the username of the database login.</li>
 * <li>The 'name.password' must represent the password of the database login.</li>
 * </ul>
 * If you specify the driver property, then the url property will be assumed as JDBC URL. If you
 * omit the driver property, then the url property will be assumed as JNDI name. When using JNDI
 * with username/password preconfigured, you can omit the username and password properties as well.
 * <p>
 * Here are basic examples of valid properties for a database with the name 'javabase':
 * <pre>
 * javabase.jdbc.url = jdbc:mysql://localhost:3306/javabase
 * javabase.jdbc.driver = com.mysql.jdbc.Driver
 * javabase.jdbc.username = java
 * javabase.jdbc.password = d$7hF_r!9Y
 * </pre>
 * <pre>
 * javabase.jndi.url = jdbc/javabase
 * </pre>
 * Here is a basic use example:
 * <pre>
 * DAOFactory javabase = DAOFactory.getInstance("javabase.jdbc");
 * UserDAO userDAO = javabase.getUserDAO();
 * </pre>
 *
 * @author BalusC, 
 * @author Rick
 * @link http://balusc.blogspot.com/2008/07/dao-tutorial-data-layer.html
 */
public abstract class DAOFactory {

    // Constants ----------------------------------------------------------------------------------

	private static final String DEFAULT_DATABASE = "cloud.dp.jdbc";
    private static final String PROPERTY_URL = "url";
    private static final String PROPERTY_DRIVER = "driver";
    private static final String PROPERTY_USERNAME = "username";
    private static final String PROPERTY_PASSWORD = "password";
    
    // Members ------------------------------------------------------------------------------------
    
    protected HikariDataSource connectionPool;
    
    // Singleton ----------------------------------------------------------------------------------
    

    // protected constructor makes sure one can only gets a instance of this class by getInstance().
    protected DAOFactory() {
	}

    // Actions ------------------------------------------------------------------------------------
    
    /**
     * Return a DAOFactory instance if there is one.
     * If not, create a new one for the default database name.
     * @return - existed DAOFactory instance
     * @throws DAOConfigurationException	If the database name is null, or if the properties file is
     * missing in the classpath or cannot be loaded, or if a required property is missing in the
     * properties file, or if either the driver cannot be loaded or the datasource cannot be found.
     */
    public static DAOFactory getInstance() throws DAOConfigurationException {
//		if (instance == null) 
//			// use synchronization to achieve thread-safe singleton
//			synchronized (DAOFactory.class) {
//				if (instance == null) getInstance(DEFAULT_DATABASE);
//			}			
//    	return instance;
    	return getInstance(DEFAULT_DATABASE);
    }
    
    /**
     * Returns a DAOFactory instance for the given database name.
     * @param name The database name to return a new DAOFactory instance for.
     * @return A new DAOFactory instance for the given database name.
     * @throws DAOConfigurationException If the database name is null, or if the properties file is
     * missing in the classpath or cannot be loaded, or if a required property is missing in the
     * properties file, or if either the driver cannot be loaded or the datasource cannot be found.
     */
    public static DAOFactory getInstance(String name) throws DAOConfigurationException {
        if (name == null) 
            throw new DAOConfigurationException("Database name is null.");
            
        DAOFactory instance = DAOFactoryManager.getInstance().getFactory(name);
        if (instance != null) return instance;

        DAOProperties properties = new DAOProperties(name);
        String url = properties.getProperty(PROPERTY_URL, true);
        String driverClassName = properties.getProperty(PROPERTY_DRIVER, false);
        String password = properties.getProperty(PROPERTY_PASSWORD, false);
        String username = properties.getProperty(PROPERTY_USERNAME, password != null);

        // If driver is specified, then load it to let it register itself with DriverManager.
        if (driverClassName != null) {
            try {
                Class.forName(driverClassName);
            } catch (ClassNotFoundException e) {
                throw new DAOConfigurationException(
                    "Driver class '" + driverClassName + "' is missing in classpath.", e);
            }
            instance = new DriverManagerDAOFactory(url, username, password);
        }

        // Else assume URL as DataSource URL and lookup it in the JNDI.
        else {
            DataSource dataSource;
            try {
                dataSource = (DataSource) new InitialContext().lookup(url);
            } catch (NamingException e) {
                throw new DAOConfigurationException(
                    "DataSource '" + url + "' is missing in JNDI.", e);
            }
            if (username != null) {
                instance = new DataSourceWithLoginDAOFactory(dataSource, username, password);
            } else {
                instance = new DataSourceDAOFactory(dataSource);
            }
        }
        
        DAOFactoryManager.getInstance().addFactory(name, instance);
        return instance;
    }

    /**
     * Returns a connection to the database. Package private so that it can be used inside the DAO
     * package only.
     * @return A connection to the database.
     * @throws SQLException If acquiring the connection fails.
     */
    public abstract Connection getConnection() throws SQLException;

    // DAO implementation getters -----------------------------------------------------------------
    
    public UserDAO<MemberInfo> getMemberDAO() {
    	return new MemberDAOJDBC(this);
    }

    public UserDAO<ShopInfo> getShopDAO() {
		return new ShopDAOJDBC(this);
	}
    
    public UserDAO<DeliveryInfo> getDeliveryDAO() {
    	return new DeliveryDAOJDBC(this);
    }
    
    public CrudDAO<FoodInfo> getFoodDAO(){
    	return new FoodDAOJDBC(this);
    }
    
    public CrudDAO<OrderInfo> getOrderDAO(){
    	return new OrderDAOJDBC(this);
    }
    
    public UserDAO<PlayerInfo> getPlayerDAO(){
    	return new PlayerDAOJDBC(this);
    }
    
    public UserDAO<CustomerInfo> getCustomerDAO(){
    	return new CustomerDAOJDBC(this);
    }
    
    public CrudDAO<TokenInfo> getTokenDAO(){
    	return new TokenDAOJDBC(this);
    }
    
//    public <T extends TokenInfo> CrudDAO<T> getTokenDAO(T info) throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException{
//    	return (CrudDAO<T>) info.getDAO().getConstructor(DAOFactory.class).newInstance(this);
//    }
}



// Default DAOFactory implementations -------------------------------------------------------------

/**
 * The DriverManager based DAOFactory.
 */
class DriverManagerDAOFactory extends DAOFactory {

    DriverManagerDAOFactory(String url, String username, String password) {      
        Properties connProps = new Properties();
        connProps.setProperty("user", username);
        connProps.setProperty("password", password);
        
        // Initialize connection pool
        HikariConfig config = new HikariConfig();
        config.setJdbcUrl(url);
        config.setDataSourceProperties(connProps);
        config.setConnectionTimeout(10000); // 10s
        
        connectionPool = new HikariDataSource(config);
    }

    @Override
	public
    Connection getConnection() throws SQLException {
        return connectionPool.getConnection();
    }
}

/**
 * The DataSource based DAOFactory.
 */
class DataSourceDAOFactory extends DAOFactory {
    private DataSource dataSource;

    DataSourceDAOFactory(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
	public
    Connection getConnection() throws SQLException {
        return dataSource.getConnection();
    }
}

/**
 * The DataSource-with-Login based DAOFactory.
 */
class DataSourceWithLoginDAOFactory extends DAOFactory {
    private DataSource dataSource;
    private String username;
    private String password;

    DataSourceWithLoginDAOFactory(DataSource dataSource, String username, String password) {
        this.dataSource = dataSource;
        this.username = username;
        this.password = password;
    }

    @Override
	public
    Connection getConnection() throws SQLException {
        return dataSource.getConnection(username, password);
    }
}
