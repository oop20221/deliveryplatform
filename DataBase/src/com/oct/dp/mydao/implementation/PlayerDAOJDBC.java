package com.oct.dp.mydao.implementation;

import static com.oct.dp.mydao.DAOUtil.repeat;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.EnumSet;

import com.oct.dp.model.PlayerInfo;
import com.oct.dp.mydao.DAOFactory;
import com.oct.dp.mydao.UserDAO;
import com.oct.dp.mydao.condition.Fields;

/**
 * This class represents a concrete JDBC implementation of the {@link UserDAO} interface.
 * @author Rick
 */
public class PlayerDAOJDBC extends UserDAOJDBC<PlayerInfo> {

    // Constants ----------------------------------------------------------------------------------
	
	private static final String TABLE_NAME = "`two048`";
	private static final String FIELD_NAMES = 
			"user_name, best_score, best_score_date";
	
	private static final int FIELD_COUNT = FIELD_NAMES.split(",").length;
	
	
	private static final String SQL_FIND = String.format(
			"SELECT id, account, %s FROM %s", FIELD_NAMES, TABLE_NAME);
    private static final String SQL_FIND_BY_ID = String.format(
    		"SELECT id, account, %s FROM %s WHERE id = ?", FIELD_NAMES, TABLE_NAME);
    private static final String SQL_FIND_BY_ACCOUNT_AND_PASSWORD = String.format(
    		"SELECT id, account, %s FROM %s WHERE account = ? AND password = MD5(?)", FIELD_NAMES, TABLE_NAME);   
    private static final String SQL_INSERT = String.format(
    		"INSERT INTO %s (account, password, %s) VALUES (?, MD5(?)%s)", TABLE_NAME, FIELD_NAMES, repeat(", ?", FIELD_COUNT));
    private static final String SQL_UPDATE = String.format(
    		"UPDATE %s SET account = ?, %s = ? WHERE id = ?", 
    		TABLE_NAME, String.join(" = ?, ", FIELD_NAMES.split(", ")));
    private static final String SQL_DELETE = String.format(
    		"DELETE FROM %s WHERE id = ?", TABLE_NAME);
    
    
	// Available Fields
	private static final EnumSet<Fields> availableMatchingFields = EnumSet.of(
			Fields.PLAYER_BEST_SCORE, Fields.PLAYER_BEST_SCORE_DATE);
	
	private static final EnumSet<Fields> availableOrderingFields = EnumSet.of(
			Fields.PLAYER_BEST_SCORE, Fields.PLAYER_BEST_SCORE_DATE);

    // Constructors -------------------------------------------------------------------------------

    /**
     * Construct an User DAO for the given DAOFactory. Package private so that it can be constructed
     * inside the DAO package only.
     * @param daoFactory The DAOFactory to construct this User DAO for.
     */
    public PlayerDAOJDBC(DAOFactory daoFactory) {
        super(daoFactory);
    }

    // Methods --------------------------------------------------------------------------------------------
    
	@Override protected boolean isAvailableMatchingField(Fields field) { return availableMatchingFields.contains(field); }
	@Override protected boolean isAvailableOrderingField(Fields field) { return availableOrderingFields.contains(field); }
	@Override protected String getSqlFind() { return SQL_FIND; }	
    @Override protected String getSqlFindById() { return SQL_FIND_BY_ID; }
    @Override protected String getSqlFindByaccountAndPassword() { return SQL_FIND_BY_ACCOUNT_AND_PASSWORD; }
	@Override protected String getSqlInsert() { return SQL_INSERT; }
	@Override protected String getSqlUpdate() { return SQL_UPDATE; }
	@Override protected String getSqlDelete() { return SQL_DELETE; }
	@Override protected String getTableName() { return TABLE_NAME; }
	
    
	@Override
	protected Object[] getValueInsert(PlayerInfo info) {
        return new Object[] {
                info.getAccount(),
                info.getPassword(),
                info.getUserName(),
                info.getBestScore(),
                info.getBestScoreDate()
            };
	}

	@Override
	protected Object[] getValueUpdate(PlayerInfo info) {
        return new Object[] {
                info.getAccount(),
                info.getUserName(),
                info.getBestScore(),
                info.getBestScoreDate(),
                info.getId()
            };
	}

	@Override
	protected Object[] getValueDelete(PlayerInfo data) {
        return new Object[] {
                data.getId()
            };
	}
	
    @Override
    protected PlayerInfo map(ResultSet resultSet) throws SQLException {
    	PlayerInfo user = new PlayerInfo();
        user.setId(resultSet.getLong("id"));
        user.setAccount(resultSet.getString("account"));
        user.setUserName(resultSet.getString("user_name"));
        user.setBestScore(resultSet.getInt("best_score"));
        user.setBestScoreDate(resultSet.getTimestamp("best_score_date"));
        return user;
    }
}
