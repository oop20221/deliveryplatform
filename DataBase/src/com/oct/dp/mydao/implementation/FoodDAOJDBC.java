package com.oct.dp.mydao.implementation;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.EnumSet;

import com.oct.dp.model.FoodInfo;
import com.oct.dp.mydao.CrudDAO;
import com.oct.dp.mydao.DAOFactory;
import com.oct.dp.mydao.condition.Fields;

/**
 * This class represents a concrete JDBC implementation of the {@link CrudDAO} interface.
 * @author Rick
 */
public class FoodDAOJDBC extends CrudDAOJDBC<FoodInfo> {

	 // Constants ----------------------------------------------------------------------------------
	
		private static final String TABLE_NAME = "`food`";
		private static final String FIELD_NAMES = 
				"rest_id, food_name, price";
		
		
		private static final String SQL_FIND = String.format(
	    		"SELECT * FROM %s", TABLE_NAME); 
	    private static final String SQL_FIND_BY_ID = String.format(
	    		"SELECT * FROM %s WHERE id = ?", TABLE_NAME); 
	    private static final String SQL_INSERT = String.format(
	    		"INSERT INTO %s (%s) VALUES (?, ?, ?)", TABLE_NAME, FIELD_NAMES);
	    private static final String SQL_UPDATE = String.format(
	    		"UPDATE %s SET %s = ? WHERE id = ?", 
	    		TABLE_NAME, String.join(" = ?, ", FIELD_NAMES.split(", ")));
	    private static final String SQL_DELETE = String.format(
	    		"DELETE FROM %s WHERE id = ?", TABLE_NAME);
	    
		
		// Available Fields
		private static final EnumSet<Fields> availableMatchingFields = EnumSet.of(
				Fields.REST_ID);
		
		private static final EnumSet<Fields> availableOrderingFields = EnumSet.noneOf(Fields.class);
	    // Constructors ---------------------------------------------------------------------------------------------------
		
		public FoodDAOJDBC(DAOFactory daoFactory) {
			super(daoFactory);
		}
		
		// Methods -------------------------------------------------------------------------------------------------------
		
		@Override protected boolean isAvailableMatchingField(Fields field) { return availableMatchingFields.contains(field); }
		@Override protected boolean isAvailableOrderingField(Fields field) { return availableOrderingFields.contains(field); }
		@Override protected String getSqlFind() { return SQL_FIND; }	
	    @Override protected String getSqlFindById() { return SQL_FIND_BY_ID; }
		@Override protected String getSqlInsert() { return SQL_INSERT; }
		@Override protected String getSqlUpdate() { return SQL_UPDATE; }
		@Override protected String getSqlDelete() { return SQL_DELETE; }
	    
		@Override
		protected Object[] getValueInsert(FoodInfo info) {
	        return new Object[] {
	    	        info.getR_id(),
	    	        info.getName(),
	    	        info.getPrice(),
	            };
		}

		@Override
		protected Object[] getValueUpdate(FoodInfo info) {
	        return new Object[] {
	    	        info.getR_id(),
	    	        info.getName(),
	    	        info.getPrice(),
	    	        info.getId()
	            };
		}

		@Override
		protected Object[] getValueDelete(FoodInfo info) {
	        return new Object[] {
	                info.getId()
	            };
		}

		@Override
		protected FoodInfo map(ResultSet resultSet) throws SQLException {
	        FoodInfo info = new FoodInfo();
	        info.setId(resultSet.getLong("id"));
	        info.setR_id(resultSet.getLong("rest_id"));
	        info.setName(resultSet.getString("food_name"));
	        info.setPrice(resultSet.getString("price"));
	        return info;
		}
	
	
}
