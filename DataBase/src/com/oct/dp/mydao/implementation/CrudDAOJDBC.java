package com.oct.dp.mydao.implementation;

import static com.oct.dp.mydao.DAOUtil.prepareStatement;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

import com.oct.dp.model.DataInfo;
import com.oct.dp.mydao.CrudDAO;
import com.oct.dp.mydao.DAOException;
import com.oct.dp.mydao.DAOFactory;
import com.oct.dp.mydao.condition.Comparators;
import com.oct.dp.mydao.condition.Conditions;
import com.oct.dp.mydao.condition.Fields;
import com.oct.dp.mydao.condition.OrderBy;
import com.oct.dp.mydao.condition.Query;
import com.oct.dp.mydao.condition.Union;

/**
 * This class represents a concrete JDBC implementation of the {@link CrudDAO} interface.
 *
 * @author Rick
 * @param <T> class extending DataInfo
 */
public abstract class CrudDAOJDBC<T extends DataInfo> implements CrudDAO<T> {
// Constant -------------------------------------------------------------------------------------------------
	private static final int MAX_RESULT = 100;

// Vars ------------------------------------------------------------------------------------------------------

    protected DAOFactory daoFactory;
    protected static final EnumSet<Fields> availableFields = EnumSet.noneOf(Fields.class);

// Constructors ----------------------------------------------------------------------------------------------

    /**
     * Construct an data DAO for the given DAOFactory. Package private so that it can be constructed
     * inside the DAO package only.
     * @param daoFactory The DAOFactory to construct this data DAO for.
     */
    public CrudDAOJDBC(DAOFactory daoFactory) {
        this.daoFactory = daoFactory;
    }

// Getters and Setters ---------------------------------------------------------------------------------------

	public DAOFactory getDaoFactory() {
		return daoFactory;
	}
	
// abstract methods -----------------------------------------------------------------------------------------
    
    protected abstract boolean isAvailableMatchingField(Fields field);
    protected abstract boolean isAvailableOrderingField(Fields field);
    
    // SQL
    protected abstract String getSqlFind();
    protected abstract String getSqlFindById();
	protected abstract String getSqlInsert();
	protected abstract String getSqlUpdate();
	protected abstract String getSqlDelete();
	
	// Value
	protected abstract Object[] getValueInsert(T data);
	protected abstract Object[] getValueUpdate(T data);
	protected abstract Object[] getValueDelete(T data);
    
// Methods ----------------------------------------------------------------------------------------------------
	// Find ---------------------------------------------------------------------------------------------------
	
	    public T find(Long id) throws DAOException {
	        return find(getSqlFindById(), id);
	    }
	    
	    public T find(Object target, Fields field) throws IllegalArgumentException, DAOException{
	    	if (target instanceof String) {
	    		return find(new Conditions((String)target, field));
	    	}
	    	return find(new Conditions(target, field));
	    }
	    
	    public T find(Object target, Fields field, Comparators cmp) throws DAOException{
	    	if (target instanceof String) {
	    		return find(new Conditions((String)target, field, cmp));
	    	}
	    	return find(new Conditions(target, field, cmp));
	    }
	    
	    public T find(Conditions conditions) throws IllegalArgumentException, DAOException{
	    	return find(new Query(conditions));
	    }
	    
	    public T find(OrderBy orderBy) throws IllegalArgumentException, DAOException{
	    	return find(new Query(orderBy));
	    }
	    
	    public T find(Conditions conditions, OrderBy orderBy) throws IllegalArgumentException, DAOException{
	    	return find(new Query(conditions, orderBy));
	    }
	    
	    public T find(Query query) throws IllegalArgumentException, DAOException{
	    	return find(query.setLimitIfNotExist(1).toSql(getSqlFind(), 
		    		(f) -> {
		    			checkMatchingField(f);
		    			return getSqlSubquery(f);}, 
		    		(f) -> {
		    			checkOrderingField(f);  		
	    		}), query.getValues());
	    }
	    
	    public T find(Union union) throws IllegalArgumentException, DAOException{
	    	return find(union.setLimitIfNotExist(1).toSql(getSqlFind(), 
		    		(f) -> {
		    			checkMatchingField(f);
		    			return getSqlSubquery(f);}, 
		    		(f) -> {
		    			checkOrderingField(f);  		
		    		}), union.getValues());
	    }
	    
	    /**
	     * Use {@link CrudDAO#find(Conditions)} instead.
	     */
	    @Deprecated
	    public T find(Conditions... conditions) throws IllegalArgumentException, DAOException {
	    	return null;
//	    	ConditionStatement cs = getSqlAndValues(conditions); 	
//	        return find(cs.sql.append(" limit 1").toString(), cs.values);
	    }   
	    
    // FindAll ------------------------------------------------------------------------------------------------------------
	    
	    public List<T> findAll() throws IllegalArgumentException, DAOException{
	    	return findAll(new Query());
	    }
	    
	    public List<T> findAll(Object target, Fields field) throws IllegalArgumentException, DAOException {
	    	if (target instanceof String) {
	    		return findAll(new Conditions((String)target, field));
	    	}
	    	return findAll(new Conditions(target, field));
	    }
	    
	    public List<T> findAll(Object target, Fields field, Comparators cmp) throws IllegalArgumentException, DAOException {
	    	if (target instanceof String) {
	    		return findAll(new Conditions((String)target, field, cmp));
	    	}
	    	return findAll(new Conditions(target, field, cmp));
	    }
	    
	    public List<T> findAll(Conditions conditions) throws IllegalArgumentException, DAOException {	
	    	return findAll(new Query(conditions));
	    }
	        
	    /**
	     * Use {@link CrudDAO#findAll(Conditions)} instead.
	     */
	    @Deprecated
	    public List<T> findAll(Conditions... conditions) throws IllegalArgumentException, DAOException {
	    	return null;
//	    	ConditionStatement cs = getSqlAndValues(conditions);   	
//	    	return findAll(cs.sql.toString(), cs.values);
	    }
	    
	    public List<T> findAll(OrderBy orderBy) throws IllegalArgumentException, DAOException{
	    	return findAll(new Query(orderBy));
	    }
	    
	    public List<T> findAll(Integer limit) throws IllegalArgumentException, DAOException{
	    	return findAll(new Query(limit));
	    }
	    
	    public List<T> findAll(OrderBy orderBy, Integer limit) throws IllegalArgumentException, DAOException{
	    	return findAll(new Query(orderBy, limit));
	    }
	    
	    public List<T> findAll(Conditions conditions, OrderBy orderBy) throws IllegalArgumentException, DAOException {
	    	return findAll(new Query(conditions, orderBy));
	    }
	    
	    public List<T> findAll(Conditions conditions, Integer limit) throws IllegalArgumentException, DAOException {
	    	return findAll(new Query(conditions, limit));
	    }
	    
	    public List<T> findAll(Conditions conditions, OrderBy orderBy, Integer limit) throws IllegalArgumentException, DAOException {
	    	return findAll(new Query(conditions, orderBy, limit));
	    }
	    
	    public List<T> findAll(Query query) throws IllegalArgumentException, DAOException {
	    	return findAll(query.setLimitIfNotExist(MAX_RESULT).toSql(getSqlFind(), 
		    		(f) -> {
		    			checkMatchingField(f);
		    			return getSqlSubquery(f);}, 
		    		(f) -> {
		    			checkOrderingField(f);  		
	    		}), query.getValues());
	    }
	    
	    public List<T> findAll(Union union) throws IllegalArgumentException, DAOException {
	    	return findAll(union.setLimitIfNotExist(MAX_RESULT).toSql(getSqlFind(), 
		    		(f) -> {
		    			checkMatchingField(f);
		    			return getSqlSubquery(f);}, 
		    		(f) -> {
		    			checkOrderingField(f);  		
		    		}), union.getValues());
	    }
	
    // Create -------------------------------------------------------------------------------------------------------------
	    public T create(T data) throws IllegalArgumentException, DAOException {
	        
	        verifyCreation(data);
	
	        try (  		
	            Connection connection = daoFactory.getConnection();
	            PreparedStatement statement = prepareStatement(connection, getSqlInsert(), true,  getValueInsert(data));
	        ) {
	            int affectedRows = statement.executeUpdate();
	            if (affectedRows == 0) {
	                throw new DAOException("Creating data failed, no rows affected.");
	            }
	            
	            try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
	                if (generatedKeys.next()) {
	                    data.setId(generatedKeys.getLong(1));
	                    return data;
	                } else {
	                    throw new DAOException("Creating data failed, no generated key obtained.");
	                }
	            }
	        } catch (SQLException e) {
	            throw new DAOException(e);
	        }
	    }
	
    // Update -------------------------------------------------------------------------------------------------------------
	    public T update(T data) throws IllegalArgumentException, DAOException {
	        
	        verifyUpdate(data);
	
	        try (
	            Connection connection = daoFactory.getConnection();
	            PreparedStatement statement = prepareStatement(connection, getSqlUpdate(), false, getValueUpdate(data));
	        ) {
	            int affectedRows = statement.executeUpdate();
	            if (affectedRows == 0) {
	                throw new DAOException("Updating data failed, no rows affected.");
	            }
	            return data;
	        } catch (SQLException e) {
	            throw new DAOException(e);
	        }
	    }
	
    // Delete -------------------------------------------------------------------------------------------------------------
	    public T delete(T data) throws IllegalArgumentException, DAOException {
	    	
	    	verifyDeletion(data);
	    	
	        try (
	            Connection connection = daoFactory.getConnection();
	            PreparedStatement statement = prepareStatement(connection, getSqlDelete(), false,  getValueDelete(data));
	        ) {
	            int affectedRows = statement.executeUpdate();
	            if (affectedRows == 0) {
	                throw new DAOException("Deleting data failed, no rows affected.");
	            } else {
	                data.invalid();
	                return data;
	            }
	        } catch (SQLException e) {
	            throw new DAOException(e);
	        }
	    }


// Helpers ------------------------------------------------------------------------------------

    /**
     * Map the current row of the given ResultSet to an data.
     * @param resultSet The ResultSet of which the current row is to be mapped to an data.
     * @return The mapped data from the current row of the given ResultSet.
     * @throws SQLException If something fails at database level.
     */
    protected abstract T map(ResultSet resultSet) throws SQLException;  
    
    protected void verifyCreation(T data) throws IllegalArgumentException, DAOException {};
    protected void verifyUpdate(T data) throws IllegalArgumentException, DAOException {};
    protected void verifyDeletion(T data) throws IllegalArgumentException, DAOException {};
    
    /**
     * Returns the data from the database matching the given SQL query with the given values.
     * @param sql The SQL query to be executed in the database.
     * @param values The PreparedStatement values to be set.
     * @return The data from the database matching the given SQL query with the given values.
     * @throws DAOException If something fails at database level.
     */
    protected T find(String sql, Object... values) throws DAOException {
        T data = null;
        
        try (
            Connection connection = daoFactory.getConnection();
            PreparedStatement statement = prepareStatement(connection, sql, false, values);
            ResultSet resultSet = statement.executeQuery();
        ) {
            if (resultSet.next()) {
                data = map(resultSet);
            }      
        } catch (SQLException e) {
            throw new DAOException(e);
        }  
        return data;
    }
    
    /**
     * Returns all datas from the database matching the given SQL query with the given values.
     * @param sql The SQL query to be executed in the database.
     * @param values The PreparedStatement values to be set.
     * @return All datas from the database matching the given SQL query with the given values.
     * @throws DAOException If something fails at database level.
     */
    protected List<T> findAll(String sql, Object... values) throws DAOException {
    	List<T> datas = new ArrayList<>();

        try (
            Connection connection = daoFactory.getConnection();
            PreparedStatement statement = prepareStatement(connection, sql, false, values);
            ResultSet resultSet = statement.executeQuery();
        ) {
        	int count = 0;
        	while (resultSet.next() && count++ < MAX_RESULT) 
                datas.add(map(resultSet));  
        } catch (SQLException e) {
            throw new DAOException(e);
        }       
        return datas;
    }
    
    /**
     * Checks if the given field is valid for this DAO.
     * @param field	- Fields to be checked.
     * @return	true if the given field is valid.
     * @throws IllegalArgumentException	if the given field is not available to this class.
     */
    protected boolean checkMatchingField(Fields field) throws IllegalArgumentException {
    	if (!isAvailableMatchingField(field))
			throw new IllegalArgumentException(
    			String.format("Matching by field `%s` is not available to this class.", field.name()));
    	return true;
    }
    
    /**
     * Checks if the given field is valid for this DAO.
     * @param field	- Fields to be checked.
     * @return	true if the given field is valid.
     * @throws IllegalArgumentException	if the given field is not available to this class.
     */
    protected boolean checkOrderingField(Fields field) throws IllegalArgumentException {
    	if (!isAvailableOrderingField(field))
			throw new IllegalArgumentException(
    			String.format("Ording by field `%s` is not available to this class.", field.name()));
    	return true;
    }   
    
    protected String getSqlSubquery(Fields field) {
    	return null;
    }
    
}


