package com.oct.dp.mydao.implementation;

import static com.oct.dp.mydao.DAOUtil.repeat;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.EnumSet;

import com.oct.dp.model.DeliveryInfo;
import com.oct.dp.mydao.DAOFactory;
import com.oct.dp.mydao.UserDAO;
import com.oct.dp.mydao.condition.Fields;

/**
 * This class represents a concrete JDBC implementation of the {@link UserDAO} interface.
 * @author Rick
 */
public class DeliveryDAOJDBC extends UserDAOJDBC<DeliveryInfo> {

    // Constants ----------------------------------------------------------------------------------
	
	private static final String TABLE_NAME = "`delivery`";
	private static final String FIELD_NAMES = 
			"user_name, deliv_name, email, phone";
	
	private static final int FIELD_COUNT = FIELD_NAMES.split(",").length;
	
	// find
	private static final String SQL_FIND = String.format(
			"SELECT id, account, %s FROM %s", FIELD_NAMES, TABLE_NAME);
    private static final String SQL_FIND_BY_ID = String.format(
    		"SELECT id, account, %s FROM %s WHERE id = ?", FIELD_NAMES, TABLE_NAME);
    private static final String SQL_FIND_BY_ACCOUNT_AND_PASSWORD = String.format(
    		"SELECT id, account, %s FROM %s WHERE account = ? AND password = MD5(?)", FIELD_NAMES, TABLE_NAME);   
    //create
    private static final String SQL_INSERT = String.format(
    		"INSERT INTO %s (account, password, %s) VALUES (?, MD5(?)%s)", TABLE_NAME, FIELD_NAMES, repeat(", ?", FIELD_COUNT));
    //update
    private static final String SQL_UPDATE = String.format(
    		"UPDATE %s SET account = ?, %s = ? WHERE id = ?", TABLE_NAME, String.join(" = ?, ", FIELD_NAMES.split(", ")));
    //delete
    private static final String SQL_DELETE = String.format(
    		"DELETE FROM %s WHERE id = ?", TABLE_NAME);
    
	
	// Available Fields
	private static final EnumSet<Fields> availableMatchingFields = EnumSet.noneOf(Fields.class);
	
	private static final EnumSet<Fields> availableOrderingFields = EnumSet.noneOf(Fields.class);
	
	// Constructors ---------------------------------------------------------------------------------------------------
	
	public DeliveryDAOJDBC(DAOFactory daoFactory) {
		super(daoFactory);
	}
		
	// Methods -------------------------------------------------------------------------------------------------------

	@Override protected boolean isAvailableMatchingField(Fields field) { return availableMatchingFields.contains(field); }
	@Override protected boolean isAvailableOrderingField(Fields field) { return availableOrderingFields.contains(field); }
	@Override protected String getSqlFind() { return SQL_FIND; }	
    @Override protected String getSqlFindById() { return SQL_FIND_BY_ID; }
    @Override protected String getSqlFindByaccountAndPassword() { return SQL_FIND_BY_ACCOUNT_AND_PASSWORD; }
	@Override protected String getSqlInsert() { return SQL_INSERT; }
	@Override protected String getSqlUpdate() { return SQL_UPDATE; }
	@Override protected String getSqlDelete() { return SQL_DELETE; }
	@Override protected String getTableName() { return TABLE_NAME; }
    

	@Override
	protected Object[] getValueInsert(DeliveryInfo info) {
        return new Object[] {
                info.getAccount(),
                info.getPassword(),
                info.getUserName(),
                info.getName(),
                info.getEmail(),
                info.getPhone()
            };
	}


	@Override
	protected Object[] getValueUpdate(DeliveryInfo info) {
        return new Object[] {
                info.getAccount(),
                info.getUserName(),
                info.getName(),
                info.getEmail(),
                info.getPhone(),
                info.getId()
            };
	}


	@Override
	protected Object[] getValueDelete(DeliveryInfo info) {
        return new Object[] {
                info.getId()
            };
	}


	@Override
	protected DeliveryInfo map(ResultSet resultSet) throws SQLException {
        DeliveryInfo info = new DeliveryInfo();
        info.setId(resultSet.getLong("id"));
        info.setAccount(resultSet.getString("account"));
        info.setUserName(resultSet.getString("user_name"));
        info.setName(resultSet.getString("deliv_name"));
        info.setEmail(resultSet.getString("email"));
        info.setPhone(resultSet.getString("phone"));
        return info;
	}


}
