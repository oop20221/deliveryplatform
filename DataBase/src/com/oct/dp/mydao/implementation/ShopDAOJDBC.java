package com.oct.dp.mydao.implementation;


import static com.oct.dp.mydao.DAOUtil.prepareStatement;
import static com.oct.dp.mydao.DAOUtil.repeat;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

import com.oct.dp.model.ShopInfo;
import com.oct.dp.mydao.DAOException;
import com.oct.dp.mydao.DAOFactory;
import com.oct.dp.mydao.UserDAO;
import com.oct.dp.mydao.condition.Fields;;

/**
 * This class represents a concrete JDBC implementation of the {@link UserDAO} interface.
 * @author Rick
 */
public class ShopDAOJDBC extends UserDAOJDBC<ShopInfo> {

    // Constants ----------------------------------------------------------------------------------
	
	private static final String TABLE_NAME = "`shop`";
	private static final String TYPE_TABLE_NAME = "`rest_type`";
	private static final String FIELD_NAMES = 
			"user_name, discount, target_cost, email, rest_name, "
			+ "address, latitude, longitude, phone, store_description, order_description, "
			+ "mon_start, mon_end, tue_start, tue_end, wed_start, wed_end, thu_start, thu_end, "
			+ "fri_start, fri_end, sat_start, sat_end, sun_start, sun_end";
	
	private static final int FIELD_COUNT = FIELD_NAMES.split(",").length;
		
	// find
		private static final String SQL_FIND = String.format(
				"SELECT id, account, %s FROM %s", FIELD_NAMES, TABLE_NAME);
	    private static final String SQL_FIND_BY_ID = String.format(
	    		"SELECT id, account, %s FROM %s WHERE id = ?", FIELD_NAMES, TABLE_NAME);
	    private static final String SQL_FIND_BY_ACCOUNT_AND_PASSWORD = String.format(
	    		"SELECT id, account, %s FROM %s WHERE account = ? AND password = MD5(?)", FIELD_NAMES, TABLE_NAME);   
		private static final String SQL_FIND_BY_TYPE = String.format(
				"id IN (SELECT rest_id FROM %s WHERE rest_type ", TYPE_TABLE_NAME);
	//    private static final String SQL_FIND_BY_NAME = String.format(
	//    		"SELECT id, account, %s FROM %s WHERE rest_name = ?", FIELD_NAMES, TABLE_NAME);  
	//    private static final String SQL_FINDALL_BY_TYPE = String.format(
	//    		"SELECT id, account, %s FROM %s WHERE id = ("
	//    		+ "SELECT rest_id FROM %s WHERE rest_type = ?)", FIELD_NAMES, TABLE_NAME, TYPE_TABLE_NAME); 
	    private static final String SQL_FINDALL_TYPE_BY_REST_ID = String.format(
	    		"SELECT rest_type FROM %s WHERE rest_id = ?", TYPE_TABLE_NAME); 
//	    private static final String SQL_LIST_ORDER_BY_ID = String.format(
//	    		"SELECT id, account, %s FROM %s ORDER BY id", FIELD_NAMES, TABLE_NAME);    
    //create
	    private static final String SQL_INSERT = String.format(
	    		"INSERT INTO %s (account, password, %s) VALUES (?, MD5(?)%s)", TABLE_NAME, FIELD_NAMES, repeat(", ?", FIELD_COUNT));
	    private static final String SQL_INSERT_TYPE = String.format(
	    		"INSERT INTO %s (rest_id, rest_type) VALUES (?, ?)", TYPE_TABLE_NAME);
    //update
	    private static final String SQL_UPDATE = String.format(
	    		"UPDATE %s SET account = ?, %s = ? WHERE id = ?", TABLE_NAME, String.join(" = ?, ", FIELD_NAMES.split(", ")));
    //delete
	    private static final String SQL_DELETE = String.format(
	    		"DELETE FROM %s WHERE id = ?", TABLE_NAME);
	    private static final String SQL_DELETE_ALL_TYPE = String.format(
	    		"DELETE FROM %s WHERE rest_id = ?", TYPE_TABLE_NAME);
    
//    private static final String SQL_EXIST_ACCOUNT = String.format(
//    		"SELECT id FROM %s WHERE account = ?", TABLE_NAME);
//    private static final String SQL_CHANGE_PASSWORD = String.format(
//    		"UPDATE %s SET password = MD5(?) WHERE id = ?", TABLE_NAME);
    
	// Available Fields
		private static final EnumSet<Fields> availableMatchingFields = EnumSet.of(
				Fields.USER_NAME, Fields.REST_NAME, Fields.REST_TYPE, Fields.REST_DISCOUNT);
		
		private static final EnumSet<Fields> availableOrderingFields = EnumSet.of(
				Fields.REST_DISCOUNT);
    
    // Constructors ---------------------------------------------------------------------------------------------------
	
	public ShopDAOJDBC(DAOFactory daoFactory) {
		super(daoFactory);
	}
	
	// Methods -------------------------------------------------------------------------------------------------------

	@Override
	protected ShopInfo find(String sql, Object... values) throws DAOException {
		ShopInfo shopInfo = super.find(sql, values);
		return fillInType(shopInfo);
	}
	
	@Override
	protected List<ShopInfo> findAll(String sql, Object... values) throws DAOException {
        List<ShopInfo> shopInfos = super.findAll(sql, values);
        shopInfos.forEach(s -> s = fillInType(s));
        return shopInfos;
	}
	
	/**
	 * Completes shopInfo by filling the type.
	 * @param shopInfo	- An unfulfilled ShopInfo.
	 * @return			- Completed shopInfo.
	 * @throws DAOException If something fails at database level.
	 */
	protected ShopInfo fillInType(ShopInfo shopInfo) throws DAOException{   
		if (shopInfo == null) return shopInfo;
		
        try (
            Connection connection = daoFactory.getConnection();
            PreparedStatement statement = prepareStatement(connection, SQL_FINDALL_TYPE_BY_REST_ID, false, shopInfo.getId());
            ResultSet resultSet = statement.executeQuery();
        ) {
        	List<String> types = new ArrayList<>();
            while (resultSet.next()) {
                types.add(resultSet.getString("rest_type"));
            }
            shopInfo.setType(types);     
        } catch (SQLException e) {
            throw new DAOException(e);
        }    
        return shopInfo;
	}
	
	
	@Override
	public ShopInfo create(ShopInfo info) throws IllegalArgumentException, DAOException {
		ShopInfo shopInfo = super.create(info);
		createType(shopInfo);
		return shopInfo;
	}
	
	@Override
	public ShopInfo update(ShopInfo data) throws IllegalArgumentException, DAOException {	
		ShopInfo shopInfo = super.update(data);
		if (shopInfo.getType() == null) return shopInfo;
		// delete all existed type first.
        try (	Connection connection = daoFactory.getConnection();
                PreparedStatement statement = prepareStatement(connection, SQL_DELETE_ALL_TYPE, false,  shopInfo.getId());
            ) {
                statement.executeUpdate();
                
        } catch (SQLException e) {
            throw new DAOException(e);
        }
        
        // recreate them.
        createType(shopInfo);
		
	    return shopInfo;
	}
	
	protected void createType(ShopInfo shopInfo) {
		if (shopInfo.getType() == null) return;
		// get connection once.
		try(Connection connection = daoFactory.getConnection();){
			// insert type into database one by one
			shopInfo.getType().forEach(type -> {			
				try (PreparedStatement statement = prepareStatement(connection, SQL_INSERT_TYPE, true, shopInfo.getId(), type);) 
				{
					int affectedRows = statement.executeUpdate();
					if (affectedRows == 0) {
						throw new DAOException("Creating data failed, no rows affected.");
					}
					
					try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
						if (generatedKeys.next()) {
						} else {
							throw new DAOException("Creating rest_type failed, no generated key obtained.");
						}
					}
				} catch (SQLException e) {
					throw new DAOException(e);
				}
			});
		}catch (SQLException e) {
            throw new DAOException(e);
        }
	}
	
	@Override
	public ShopInfo delete(ShopInfo info) throws IllegalArgumentException, DAOException {
		ShopInfo data = super.delete(info);
        try (	Connection connection = daoFactory.getConnection();
                PreparedStatement statement = prepareStatement(connection, SQL_DELETE_ALL_TYPE, false,  info.getId());
            ) {
                statement.executeUpdate();
                
        } catch (SQLException e) {
            throw new DAOException(e);
        }
		return data;
	}
	
	
	//------------------------------------------------------------------------------------------------------------------
	
	@Override protected boolean isAvailableMatchingField(Fields field) { return availableMatchingFields.contains(field); }
	@Override protected boolean isAvailableOrderingField(Fields field) { return availableOrderingFields.contains(field); }
	@Override protected String getSqlFind() { return SQL_FIND; }	
    @Override protected String getSqlFindById() { return SQL_FIND_BY_ID; }
    @Override protected String getSqlFindByaccountAndPassword() { return SQL_FIND_BY_ACCOUNT_AND_PASSWORD; }
	//@Override protected String getSqlListOrderById() { return SQL_LIST_ORDER_BY_ID; }
	@Override protected String getSqlInsert() { return SQL_INSERT; }
	@Override protected String getSqlUpdate() { return SQL_UPDATE; }
	@Override protected String getSqlDelete() { return SQL_DELETE; }
	@Override protected String getTableName() { return TABLE_NAME; }

	@Override
	protected String getSqlSubquery(Fields field) {
		if (field.equals(Fields.REST_TYPE))
			return SQL_FIND_BY_TYPE;
		return null;
	}
    
	@Override
	protected Object[] getValueInsert(ShopInfo info) {
        return new Object[] {
                info.getAccount(),
                info.getPassword(),
                info.getUserName(),
                info.getDiscount(),
                info.getTargetcost(),
                info.getEmail(),
    			info.getRname(),
    			info.getAddress(),
    			info.getLatitude(),
    			info.getLongitude(),
    			info.getPhone(),
    			info.getStore_description(),
    			info.getOrder_description(),
    			info.getMonstart(),
    			info.getMonend(),
    			info.getTuestart(),
    			info.getTueend(),
    			info.getWedstart(),
    			info.getWedend(),
    			info.getThustart(),
    			info.getThuend(),
    			info.getFristart(),
    			info.getFriend(),
    			info.getSatstart(),
    			info.getSatend(),
    			info.getSunstart(),
    			info.getSunend()
            };
	}

	@Override
	protected Object[] getValueUpdate(ShopInfo info) {
        return new Object[] {
                info.getAccount(),
                info.getUserName(),
                info.getDiscount(),
                info.getTargetcost(),
                info.getEmail(),
    			info.getRname(),
    			info.getAddress(),
    			info.getLatitude(),
    			info.getLongitude(),
    			info.getPhone(),
    			info.getStore_description(),
    			info.getOrder_description(),
    			info.getMonstart(),
    			info.getMonend(),
    			info.getTuestart(),
    			info.getTueend(),
    			info.getWedstart(),
    			info.getWedend(),
    			info.getThustart(),
    			info.getThuend(),
    			info.getFristart(),
    			info.getFriend(),
    			info.getSatstart(),
    			info.getSatend(),
    			info.getSunstart(),
    			info.getSunend(),
    			info.getId()
            };
	}

	@Override
	protected Object[] getValueDelete(ShopInfo info) {
        return new Object[] {
                info.getId()
            };
	}

	@Override
	protected ShopInfo map(ResultSet resultSet) throws SQLException {
        ShopInfo info = new ShopInfo();
        info.setId(resultSet.getLong("id"));
        info.setAccount(resultSet.getString("account"));
        info.setUserName(resultSet.getString("user_name"));
        info.setDiscount(resultSet.getFloat("discount"));
        info.setTargetcost(resultSet.getInt("target_cost"));
        info.setEmail(resultSet.getString("email"));
		info.setRname(resultSet.getString("rest_name"));
		info.setAddress(resultSet.getString("address"));
		info.setLatitude(resultSet.getFloat("latitude"));
		info.setLongitude(resultSet.getFloat("longitude"));
		info.setPhone(resultSet.getString("phone"));
		info.setStore_description(resultSet.getString("store_description"));
		info.setOrder_description(resultSet.getString("order_description"));
		info.setMonstart(resultSet.getString("mon_start"));
		info.setMonend(resultSet.getString("mon_end"));
		info.setTuestart(resultSet.getString("tue_start"));
		info.setTueend(resultSet.getString("tue_end"));
		info.setWedstart(resultSet.getString("wed_start"));
		info.setWedend(resultSet.getString("wed_end"));
		info.setThustart(resultSet.getString("thu_start"));
		info.setThuend(resultSet.getString("thu_end"));
		info.setFristart(resultSet.getString("fri_start"));
		info.setFriend(resultSet.getString("fri_end"));
		info.setSatstart(resultSet.getString("sat_start"));
		info.setSatend(resultSet.getString("sat_end"));
		info.setSunstart(resultSet.getString("sun_start"));
		info.setSunend(resultSet.getString("sun_end"));
        return info;
	}
		
}
