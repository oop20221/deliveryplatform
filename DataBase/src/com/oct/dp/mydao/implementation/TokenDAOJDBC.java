package com.oct.dp.mydao.implementation;

import static com.oct.dp.mydao.DAOUtil.repeat;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.EnumSet;

import com.oct.dp.model.TokenInfo;
import com.oct.dp.model.TokenInfo.Token_State;
import com.oct.dp.mydao.CrudDAO;
import com.oct.dp.mydao.DAOFactory;
import com.oct.dp.mydao.condition.Fields;

/**
 * This class represents a concrete JDBC implementation of the {@link CrudDAO} interface.
 * @author Rick
 */
public class TokenDAOJDBC extends CrudDAOJDBC<TokenInfo> {

	// Constants ----------------------------------------------------------------------------------
	
			private static final String TABLE_NAME = "`customer_token`";
			private static final String FIELD_NAMES = 
					"customer_id, customer_token, customer_key, customer_token_state, account_info";
			private static final int FIELD_COUNT = FIELD_NAMES.split(",").length;
			
			private static final String SQL_FIND = String.format(
		    		"SELECT * FROM %s", TABLE_NAME); 
		    private static final String SQL_FIND_BY_ID = String.format(
		    		"SELECT * FROM %s WHERE id = ?", TABLE_NAME); 
		    private static final String SQL_INSERT = String.format(
		    		"INSERT INTO %s (%s) VALUES (?%s)", TABLE_NAME, FIELD_NAMES, repeat(", ?", FIELD_COUNT-1));
		    
		    private static final String SQL_UPDATE = String.format(
		    		"UPDATE %s SET %s = ? WHERE id = ?", 
		    		TABLE_NAME, String.join(" = ?, ", FIELD_NAMES.split(", ")));
		    private static final String SQL_DELETE = String.format(
		    		"DELETE FROM %s WHERE id = ?", TABLE_NAME);
		    
			
			// Available Fields
			private static final EnumSet<Fields> availableMatchingFields = EnumSet.of(
					Fields.CUSTOMER_ID, Fields.CUSTOMER_TOKEN, Fields.CUSTOMER_KEY, Fields.CUSTOMER_TOKEN_STATE);
			
			private static final EnumSet<Fields> availableOrderingFields = EnumSet.noneOf(Fields.class);
		    // Constructors ---------------------------------------------------------------------------------------------------
			
			public TokenDAOJDBC(DAOFactory daoFactory) {
				super(daoFactory);
			}
			
			// Methods -------------------------------------------------------------------------------------------------------
			
			@Override protected boolean isAvailableMatchingField(Fields field) { return availableMatchingFields.contains(field); }
			@Override protected boolean isAvailableOrderingField(Fields field) { return availableOrderingFields.contains(field); }
			@Override protected String getSqlFind() { return SQL_FIND; }	
		    @Override protected String getSqlFindById() { return SQL_FIND_BY_ID; }
			@Override protected String getSqlInsert() { return SQL_INSERT; }
			@Override protected String getSqlUpdate() { return SQL_UPDATE; }
			@Override protected String getSqlDelete() { return SQL_DELETE; }
		    
			@Override
			protected Object[] getValueInsert(TokenInfo info) {
		        return new Object[] {
		    	        info.getCustomer_id(),
		    	        info.getToken(),
		    	        info.getKey(),
		    	        info.getState().name(),
		    	        info.getAccount_info()
		            };
			}

			@Override
			protected Object[] getValueUpdate(TokenInfo info) {
		        return new Object[] {
		        		info.getCustomer_id(),
		    	        info.getToken(),
		    	        info.getKey(),
		    	        info.getState().name(),
		    	        info.getAccount_info(),
		    	        info.getId()
		            };
			}

			@Override
			protected Object[] getValueDelete(TokenInfo info) {
		        return new Object[] {
		                info.getId()
		            };
			}

			@Override
			protected TokenInfo map(ResultSet resultSet) throws SQLException {
				TokenInfo info = new TokenInfo();
		        info.setId(resultSet.getLong("id"));
		        info.setCustomer_id(resultSet.getLong("customer_id"));
    	        info.setToken(resultSet.getString("customer_token"));
    	        info.setKey(resultSet.getString("customer_key"));
    	        info.setState(Token_State.valueOf(resultSet.getString("customer_token_state")));
    	        info.setAccount_info(resultSet.getString("account_info"));
		        return info;
			}
}
