package com.oct.dp.mydao.implementation;

import static com.oct.dp.mydao.DAOUtil.prepareStatement;
import static com.oct.dp.mydao.DAOUtil.repeat;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

import com.oct.dp.model.OrderInfo;
import com.oct.dp.model.OrderInfo.Detail;
import com.oct.dp.model.OrderInfo.OrderState;
import com.oct.dp.mydao.CrudDAO;
import com.oct.dp.mydao.DAOException;
import com.oct.dp.mydao.DAOFactory;
import com.oct.dp.mydao.condition.Fields;

/**
 * This class represents a concrete JDBC implementation of the {@link CrudDAO} interface.
 * @author Rick
 */
public class OrderDAOJDBC extends CrudDAOJDBC<OrderInfo> {

	// Constants ----------------------------------------------------------------------------------
	
			private static final String TABLE_NAME = "`order`";
			private static final String DETAIL_TABLE_NAME = "`order_detail`";
			private static final String FIELD_NAMES = 	
					"rest_id, member_id, deliv_id, rest_name, member_name, deliv_name, "
					+ "total, fee, rest_discount, is_fee_free, order_state, order_time, "
					+ "arrive_time, departure, destination, order_note";		
			private static final String DETAIL_FIELD_NAMES = 	
					"order_id, product_name, amount, unit_price, price, note";
			
			private static final int FIELD_COUNT = FIELD_NAMES.split(",").length;
			private static final int DETAIL_FIELD_COUNT = DETAIL_FIELD_NAMES.split(",").length;
			
		// Find --------------------------------------------------------------------------------------------------
			private static final String SQL_FIND = String.format(
		    		"SELECT * FROM %s", TABLE_NAME); 
		    private static final String SQL_FIND_BY_ID = String.format(
		    		"SELECT * FROM %s WHERE id = ?", TABLE_NAME); 
		    private static final String SQL_FINDALL_DETAIL = String.format(
		    		"SELECT * FROM %s WHERE order_id = ?", DETAIL_TABLE_NAME); 
		// Create --------------------------------------------------------------------------------------------------
		    private static final String SQL_INSERT = String.format(
		    		"INSERT INTO %s (%s) VALUES (?%s)", TABLE_NAME, FIELD_NAMES, repeat(", ?", FIELD_COUNT-1));
		    private static final String SQL_INSERT_DETAIL = String.format(
		    		"INSERT INTO %s (%s) VALUES (?%s)", DETAIL_TABLE_NAME, DETAIL_FIELD_NAMES, repeat(", ?", DETAIL_FIELD_COUNT-1));
		// Update --------------------------------------------------------------------------------------------------
		    private static final String SQL_UPDATE = String.format(
		    		"UPDATE %s SET %s = ? WHERE id = ?", 
		    		TABLE_NAME, String.join(" = ?, ", FIELD_NAMES.split(", ")));
		// Delete --------------------------------------------------------------------------------------------------
		    private static final String SQL_DELETE = String.format(
		    		"DELETE FROM %s WHERE id = ?", TABLE_NAME);
		    private static final String SQL_DELETE_ALL_DETAIL = String.format(
		    		"DELETE FROM %s WHERE order_id = ?", DETAIL_TABLE_NAME);
		    
			// Available Fields
			private static final EnumSet<Fields> availableMatchingFields = EnumSet.of(
					Fields.REST_ID, Fields.MEMBER_ID, Fields.DELIV_ID, Fields.ORDER_STATE);
			
			private static final EnumSet<Fields> availableOrderingFields = EnumSet.noneOf(Fields.class);
		    
		    
    // Constructors ---------------------------------------------------------------------------------------------------
		
		public OrderDAOJDBC(DAOFactory daoFactory) {
			super(daoFactory);
		}
		
	// Methods -------------------------------------------------------------------------------------------------------
		
		@Override
		protected OrderInfo find(String sql, Object... values) throws DAOException {
			OrderInfo orderInfo = super.find(sql, values);
			return fillInDetail(orderInfo);
		}
		
		@Override
		public List<OrderInfo> findAll(String sql, Object... values) throws DAOException {
	        List<OrderInfo> orderInfos = super.findAll(sql, values);
	        orderInfos.forEach(o -> o = fillInDetail(o));
	        return orderInfos;
		}
		
		/**
		 * Completes orderInfo by filling the type.
		 * @param orderInfo	- An unfulfilled OrderInfo.
		 * @return			- Completed OrderInfo.
		 * @throws DAOException If something fails at database level.
		 */
		protected OrderInfo fillInDetail(OrderInfo orderInfo) throws DAOException{   
			if (orderInfo == null) return orderInfo;
			
	        try (
	            Connection connection = daoFactory.getConnection();
	            PreparedStatement statement = prepareStatement(connection, SQL_FINDALL_DETAIL, false, orderInfo.getId());
	            ResultSet resultSet = statement.executeQuery();
	        ) {
	        	List<Detail> details = new ArrayList<>();
	            while (resultSet.next()) {	            	
	            	details.add(mapDetail(resultSet));
	            }
	            orderInfo.setDetails(details);     
	        } catch (SQLException e) {
	            throw new DAOException(e);
	        }    
	        return orderInfo;
		}
		
		
		@Override
		public OrderInfo create(OrderInfo info) throws IllegalArgumentException, DAOException {
			OrderInfo orderInfo = super.create(info);
			createDetail(orderInfo);
			return orderInfo;
		}
		
		@Override
		public OrderInfo update(OrderInfo data) throws IllegalArgumentException, DAOException {	
			OrderInfo orderInfo = super.update(data);
			if (orderInfo.getDetails() == null) return orderInfo;
			// delete all existed detail first.
	        try (	Connection connection = daoFactory.getConnection();
	                PreparedStatement statement = prepareStatement(connection, SQL_DELETE_ALL_DETAIL, false,  orderInfo.getId());
	            ) {
	                int affectedRows = statement.executeUpdate();
	                if (affectedRows == 0) 
	                    throw new DAOException("Deleting data failed, no rows affected.");
	        } catch (SQLException e) {
	            throw new DAOException(e);
	        }
	        
	        // recreate them.
	        createDetail(orderInfo);
			
		    return orderInfo;
		}
		
		protected void createDetail(OrderInfo orderInfo) {
			if (orderInfo.getDetails() == null) return;
			// get connection once.
			try(Connection connection = daoFactory.getConnection();){
				// insert type into database one by one
				orderInfo.getDetails().forEach(detail -> {			
					try (PreparedStatement statement = prepareStatement(
							connection, SQL_INSERT_DETAIL, true, getValueInsertDetail(orderInfo.getId(), detail));
						) {
							int affectedRows = statement.executeUpdate();
							if (affectedRows == 0) {
								throw new DAOException("Creating data failed, no rows affected.");
							}
							
							try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
								if (generatedKeys.next()) {
								} else {
									throw new DAOException("Creating rest_type failed, no generated key obtained.");
								}
							}
					} catch (SQLException e) {
						throw new DAOException(e);
					}
				});
			}catch (SQLException e) {
	            throw new DAOException(e);
	        }
		}
		
	// -----------------------------------------------------------------------------------------------------------
		
		@Override protected boolean isAvailableMatchingField(Fields field) { return availableMatchingFields.contains(field); }
		@Override protected boolean isAvailableOrderingField(Fields field) { return availableOrderingFields.contains(field); }
		@Override protected String getSqlFind() { return SQL_FIND; }	
	    @Override protected String getSqlFindById() { return SQL_FIND_BY_ID; }
		@Override protected String getSqlInsert() { return SQL_INSERT; }
		@Override protected String getSqlUpdate() { return SQL_UPDATE; }
		@Override protected String getSqlDelete() { return SQL_DELETE; }
	    
		@Override
		protected Object[] getValueInsert(OrderInfo info) {
	        return new Object[] {
	    	        info.getRestID(),
	    	        info.getUserID(),
	    	    	info.getDeliveryID(),	    	    	
	    	    	info.getRestName(),		    	    		    	    	
	    	    	info.getUserName(),		    	    	
	    	    	info.getDeliveryName(),		    	    	
	    	    	info.getTotal(),	
	    	    	info.getFee(),
	    	    	info.getDiscount(),
	    	    	info.isFeeFree(),
	    	    	info.getState().name(),		    	    	
	    	    	info.getOrdertime(),		    	    	
	    	    	info.getArrivetime(),		    	    	
	    	    	info.getDeparture(),		    	    	
	    	    	info.getDestination(),		    	    	
	    	    	info.getOrder_note(),		    	    	
	            };
		}

		@Override
		protected Object[] getValueUpdate(OrderInfo info) {
	        return new Object[] {
	        		info.getRestID(),
	    	        info.getUserID(),
	    	    	info.getDeliveryID(),	    	    	
	    	    	info.getRestName(),		    	    		    	    	
	    	    	info.getUserName(),		    	    	
	    	    	info.getDeliveryName(),		    	    	
	    	    	info.getTotal(),		
	    	    	info.getFee(),
	    	    	info.getDiscount(),
	    	    	info.isFeeFree(),
	    	    	info.getState().name(),		    	    	
	    	    	info.getOrdertime(),		    	    	
	    	    	info.getArrivetime(),		    	    	
	    	    	info.getDeparture(),		    	    	
	    	    	info.getDestination(),		    	    	
	    	    	info.getOrder_note(),	
	    	        info.getId()
	            };
		}

		@Override
		protected Object[] getValueDelete(OrderInfo info) {
	        return new Object[] {
	                info.getId()
	            };
		}

		@Override
		protected OrderInfo map(ResultSet resultSet) throws SQLException {
	        OrderInfo info = new OrderInfo();
	        info.setId(resultSet.getLong("id"));
	        info.setRestID(resultSet.getLong("rest_id"));
	        info.setUserID(resultSet.getLong("member_id"));
	    	info.setDeliveryID(resultSet.getLong("deliv_id"));	    	    	
	    	info.setRestName(resultSet.getString("rest_name"));		    	    		    	    	
	    	info.setUserName(resultSet.getString("member_name"));		    	    	
	    	info.setDeliveryName(resultSet.getString("deliv_name"));		    	    	
	    	info.setTotal(resultSet.getInt("total"));		 
	    	info.setFee(resultSet.getInt("fee"));
	    	info.setDiscount(resultSet.getInt("rest_discount"));
	    	info.setFeeFree(resultSet.getBoolean("is_fee_free"));
	    	info.setState(OrderState.valueOf(resultSet.getString("order_state")));		    	    	
	    	info.setOrdertime(resultSet.getTimestamp("order_time"));		    	    	
	    	info.setArrivetime(resultSet.getTimestamp("arrive_time"));		    	    	
	    	info.setDeparture(resultSet.getString("departure"));		    	    	
	    	info.setDestination(resultSet.getString("destination"));		    	    	
	    	info.setOrder_note(resultSet.getString("order_note"));	
	        return info;
		}
		
		private Detail mapDetail(ResultSet resultSet) throws SQLException{
			Detail detail = new Detail();
        	detail.setProduct_name(resultSet.getString("product_name"));
        	detail.setAmount(resultSet.getInt("amount"));
        	detail.setUnit_price(resultSet.getInt("unit_price"));
        	detail.setPrice(resultSet.getInt("price"));
        	detail.setNote(resultSet.getString("note"));
        	return detail;
		}
		
		private Object[] getValueInsertDetail(Long order_id, Detail detail) {
			return new Object[] {
	                order_id,
	                detail.getProduct_name(),
	                detail.getAmount(),
	                detail.getUnit_price(),
	                detail.getPrice(),
	                detail.getNote()
	            };
		}
}
