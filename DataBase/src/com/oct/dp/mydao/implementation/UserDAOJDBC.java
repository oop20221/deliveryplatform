package com.oct.dp.mydao.implementation;

import static com.oct.dp.mydao.DAOUtil.prepareStatement;
import static com.oct.dp.mydao.KeyGenerator.generateRandomAlphaNumToken;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.oct.dp.model.UserInfo;
import com.oct.dp.mydao.DAOException;
import com.oct.dp.mydao.DAOFactory;
import com.oct.dp.mydao.UserDAO;

/**
 * This class represents a concrete JDBC implementation of the {@link UserDAO} interface.
 *
 * @author Rick
 * @param <T> class extending UserInfo
 */
public abstract class  UserDAOJDBC<T extends UserInfo> extends CrudDAOJDBC<T> implements UserDAO<T>{

	
    // Constructors -------------------------------------------------------------------------------

    /**
     * Construct an User DAO for the given DAOFactory.
     * @param daoFactory The DAOFactory to construct this User DAO for.
     */
     public UserDAOJDBC(DAOFactory daoFactory) {
    	super(daoFactory);
    }
     
    // Methods -----------------------------------------------------------------------------------------------------
     
    protected abstract String getSqlFindByaccountAndPassword();
    protected abstract String getTableName();
    private String getSqlExistAccount() {
    	return String.format("SELECT id FROM %s WHERE account = ?", getTableName());
    }
    private String getSqlChangePassword() {
        return String.format("UPDATE %s SET password = MD5(?) WHERE id = ?", getTableName());
    }
    private String getSqlGetAuthentication() {
    	return String.format("SELECT authentication FROM %s WHERE id = ?", getTableName());
    }
    private String getSqlSetAuthentication() {
    	return String.format("UPDATE %s SET authentication = ? WHERE id = ?", getTableName());
    }
    
    
 	public T find(String account, String password) throws DAOException {
 		return authenticate(find(getSqlFindByaccountAndPassword(), account, password));
 	}
 	
 	@Override
 	public T create(T data) throws IllegalArgumentException, DAOException {
 		return authenticate(super.create(data));
 		
 	}
    
    public boolean existAccount(String account) throws DAOException {   	   	
        try (
            Connection connection = daoFactory.getConnection();
            PreparedStatement statement = prepareStatement(connection, getSqlExistAccount(), false, account);
            ResultSet resultSet = statement.executeQuery();
        ) {
            return resultSet.next();
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    
    public T changePassword(T data) throws IllegalArgumentException, DAOException {
    	if (data == null) throw new IllegalArgumentException("data is null");
    	if (data.getPassword() == null) return data;
    	verify(data);
    	
        Object[] values = {
            data.getPassword(),
            data.getId()
        };

        try (
            Connection connection = daoFactory.getConnection();
            PreparedStatement statement = prepareStatement(connection, getSqlChangePassword(), false, values);
        ) {
            int affectedRows = statement.executeUpdate();
            if (affectedRows == 0) {
                throw new DAOException("Changing password failed, no rows affected.");
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
        return data;
    }
    
    @Override
    protected void verifyCreation(T data) throws IllegalArgumentException, DAOException {
    	
    	if (existAccount(data.getAccount())){
    		throw new IllegalArgumentException(String.format("Account \"%s\" already existed", data.getAccount()));
    	}
    }
    
    @Override
    protected void verifyUpdate(T data) throws IllegalArgumentException, DAOException {
    	verify(data);  	
    }
    
    @Override
    protected void verifyDeletion(T data) throws IllegalArgumentException, DAOException {
    	verify(data);
    	
    }
    
    private void verify(T data) throws IllegalArgumentException, DAOException {
    	
    	if (data == null) throw new IllegalArgumentException("data is null");
    	if (data.getId() == null) throw new IllegalArgumentException("data is not created yet, the data ID is null.");
    	if (data.getAuthentication() == null) throw new IllegalArgumentException("Authentication failed! Please login first.");
    	
        try (
            Connection connection = daoFactory.getConnection();
            PreparedStatement statement = prepareStatement(connection, getSqlGetAuthentication(), false, data.getId());
    		ResultSet resultSet = statement.executeQuery();
        ) {
        	if (resultSet.next()) {
                if (!data.getAuthentication().equals(resultSet.getString(1)))          
                	throw new IllegalArgumentException("Authentication failed! Please login again.");
                return;   
            }  
        	throw new IllegalArgumentException("id is invalid! Please login again.");  		
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }
    
    private T authenticate(T t) {
    	if (t != null) 
 		{
 			String token = generateRandomAlphaNumToken(16);
 			Object[] values = {
	            token,
	            t.getId()
 		    };
 			
 			try(Connection connection = daoFactory.getConnection();
 				PreparedStatement statement = prepareStatement(connection, getSqlSetAuthentication(), false, values);
	        ) {
 	            int affectedRows = statement.executeUpdate();
 	            if (affectedRows == 0) {
 	                throw new DAOException("Authenticating failed, no rows affected.");
 	            }
 	            t.setAuthentication(token);
	        } catch (SQLException e) {
	            throw new DAOException(e);
	        }			
 		}			
 		return t;
    }
    
}
