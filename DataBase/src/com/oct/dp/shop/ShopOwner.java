package com.oct.dp.shop;

import java.util.List;

import com.oct.dp.model.FoodInfo;
import com.oct.dp.model.OrderInfo;
import com.oct.dp.model.OrderInfo.OrderState;
import com.oct.dp.model.ShopInfo;
import com.oct.dp.mydao.CrudDAO;
import com.oct.dp.mydao.DAOException;
import com.oct.dp.mydao.DAOFactory;
import com.oct.dp.mydao.UserDAO;
import com.oct.dp.mydao.condition.Comparators;
import com.oct.dp.mydao.condition.Conditions;
import com.oct.dp.mydao.condition.Fields;

/**
 * all method that shop would use. connect shop and database to prevent data
 * breach
 * 
 * @author Gary
 *
 */
public class ShopOwner {

	private static DAOFactory javabase = DAOFactory.getInstance();
	private static UserDAO<ShopInfo> shopDAO = javabase.getShopDAO();
	private static CrudDAO<FoodInfo> foodDAO = javabase.getFoodDAO();
	private static CrudDAO<OrderInfo> orderDAO = javabase.getOrderDAO();

	/**
	 * register a shop account
	 * 
	 * @param account  shop account
	 * @param password shop password
	 * @return shopInfo
	 * @throws IllegalArgumentException shop account create failed
	 * @throws DAOException             shop account create failed
	 */
	public static ShopInfo register(String account, String password) throws IllegalArgumentException, DAOException {
		ShopInfo shopInfo = new ShopInfo();
		shopInfo.setAccount(account);
		shopInfo.setPassword(password);
		shopInfo = shopDAO.create(shopInfo);
		return shopInfo;
	}

	/**
	 * shop account login
	 * 
	 * @param account  shop account
	 * @param password password account
	 * @return shopInfo
	 * @throws DAOException login failed
	 */
	public static ShopInfo login(String account, String password) throws DAOException {
		return shopDAO.find(account, password);
	}

	/**
	 * update data of shopInfo to database
	 * 
	 * @param shopInfo a shopInfo want to be update
	 * @return an updated shopInfo
	 * @throws IllegalArgumentException update failed
	 * @throws DAOException             update failed
	 */
	public static ShopInfo update(ShopInfo shopInfo) throws IllegalArgumentException, DAOException {
		return shopDAO.update(shopInfo);
	}

	/**
	 * Change password and update it into database.
	 * 
	 * @param shopInfo a shopInfo need to be change password.
	 * @return a new password shopInfo
	 * @throws IllegalArgumentException change password failed
	 * @throws DAOException             change password failed
	 */
	public static ShopInfo changePassword(ShopInfo shopInfo) throws IllegalArgumentException, DAOException {
		return shopDAO.changePassword(shopInfo);
	}

	/**
	 * get a list of foodInfos
	 * 
	 * @param shopInfo a shopInfo
	 * @return a list of all foodInfos of the shop
	 * @throws DAOException findAll failed
	 */
	public static List<FoodInfo> getFoodInfo(ShopInfo shopInfo) throws DAOException {
		return foodDAO.findAll(shopInfo.getId(), Fields.REST_ID);
	}

	/**
	 * shop owner check history orders.
	 * 
	 * @param shopInfo a shopInfo
	 * @return a list of all orderInfo
	 * @throws DAOException findAll failed
	 */
	public static List<OrderInfo> history(ShopInfo shopInfo) throws DAOException {
		return orderDAO.findAll(new Conditions(shopInfo.getId(), Fields.REST_ID)
				.and(OrderState.FINISHED.name(), Fields.ORDER_STATE));
	}

	/**
	 * shop owner confirm an order and set its state from WAITING to PREPARING
	 * 
	 * @param orderInfo a WAITING order
	 * @throws IllegalArgumentException update failed
	 * @throws DAOException             update failed
	 */
	public static void confirm(OrderInfo orderInfo) throws IllegalArgumentException, DAOException {
		orderInfo.setState(OrderState.PREPARING);
		orderDAO.update(orderInfo);
	}

	/**
	 * shop owner discard order
	 * 
	 * @param orderInfo an orderInfo
	 * @throws IllegalArgumentException update failed
	 * @throws DAOException             update failed
	 */
	public static void discard(OrderInfo orderInfo) throws IllegalArgumentException, DAOException {
		orderInfo.setState(OrderState.DISCARD);
		orderDAO.update(orderInfo);
	}

	/**
	 * get unconfirmed orderInfo
	 * 
	 * @param shopInfo a shopInfo
	 * @return a list of WAITING orders
	 * @throws DAOException findAll failed
	 */
	public static List<OrderInfo> getWaitOrderInfo(ShopInfo shopInfo) throws DAOException {
		return orderDAO.findAll(
				new Conditions(shopInfo.getId(), Fields.REST_ID).and(OrderState.WAITING.name(), Fields.ORDER_STATE));
	}

	/**
	 * get current order Info to keep checking order state
	 * 
	 * @param shopInfo a shopInfo
	 * @return a list of order
	 * @throws DAOException findAll failed
	 */
	public static List<OrderInfo> getOrderInfo(ShopInfo shopInfo) throws DAOException {
		return orderDAO.findAll(new Conditions(shopInfo.getId(), Fields.REST_ID)
				.and(OrderState.WAITING.name(), Fields.ORDER_STATE, Comparators.NOT_EQUAL)
				.and(OrderState.FINISHED.name(), Fields.ORDER_STATE, Comparators.NOT_EQUAL)
				.and(OrderState.DISCARD.name(), Fields.ORDER_STATE, Comparators.NOT_EQUAL));
	}

	/**
	 * delete shopInfo from database
	 * 
	 * @param shopInfo shopInfo which would be deleted.
	 * @throws DAOException delete failed
	 */
	public static void delete(ShopInfo shopInfo) throws DAOException {
		shopDAO.delete(shopInfo);
	}

	/**
	 * update foodInfo
	 * 
	 * @param foodInfo foodInfo which would be updated
	 * @throws DAOException update failed
	 */
	public static void updateFood(FoodInfo foodInfo) throws DAOException {
		foodDAO.update(foodInfo);
	}

	/**
	 * delete foodInfo
	 * 
	 * @param foodInfo a foodInfo
	 * @throws DAOException delete failed
	 */
	public static void deleteFood(FoodInfo foodInfo) throws DAOException {
		foodDAO.delete(foodInfo);
	}

	/**
	 * create new foodInfo
	 * 
	 * @param foodInfo a fodInfo
	 * @throws DAOException create failed
	 */
	public static void createFood(FoodInfo foodInfo) throws DAOException {
		foodDAO.create(foodInfo);
	}

}
