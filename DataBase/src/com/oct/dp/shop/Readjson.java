package com.oct.dp.shop;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

import org.json.simple.*;
import org.json.simple.parser.*;

import com.oct.dp.model.FoodInfo;
import com.oct.dp.model.ShopInfo;
import com.oct.dp.mydao.*;

/**
 * read json into database
 * 
 * @author Gary
 *
 */
public class Readjson {

	private static JSONObject restaurant;
	public JSONArray dataarr;
	private String Rname;
	private JSONObject Rposition;
	private String address;
	private double latitude;
	private double longitude;
	private String phone;
	private String store_description;
	private String order_description;
	private JSONArray type;
	private JSONArray menu;;
	private JSONObject business_time;
	private String Monstart;
	private String Monend;
	private String Tuestart;
	private String Tueend;
	private String Wedstart;
	private String Wedend;
	private String Thustart;
	private String Thuend;
	private String Fristart;
	private String Friend;
	private String Satstart;
	private String Satend;
	private String Sunstart;
	private String Sunend;

	public static void main(String[] args) throws Exception {
		Readjson test = new Readjson();
		DAOFactory javabase = DAOFactory.getInstance();
		UserDAO<ShopInfo> shopDAO = javabase.getShopDAO();
		CrudDAO<FoodInfo> foodDAO = javabase.getFoodDAO();

		for (int i = 0; i < test.getDataarr().size(); i++) {
			ShopInfo shopInfo = new ShopInfo();
			restaurant = (JSONObject) test.dataarr.get(i);
			shopInfo.setAccount("shop"+i);
			shopInfo.setPassword("0000");
			shopInfo.setRname(test.getRname());

			shopInfo.setAddress(test.getAddress());
			shopInfo.setLatitude(test.getLatitude());
			shopInfo.setLongitude(test.getLongitude());
			shopInfo.setPhone(test.getPhone());
			shopInfo.setStore_description(test.getStore_description());
			shopInfo.setOrder_description(test.getOrder_description());
			shopInfo.setMonstart(test.getMonstart());
			shopInfo.setMonend(test.getMonend());
			shopInfo.setTuestart(test.getTuestart());
			shopInfo.setTueend(test.getTueend());
			shopInfo.setWedstart(test.getWedstart());
			shopInfo.setWedend(test.getWedend());
			shopInfo.setThustart(test.getThustart());
			shopInfo.setThuend(test.getThuend());
			shopInfo.setFristart(test.getFristart());
			shopInfo.setFriend(test.getFriend());
			shopInfo.setSatstart(test.getSatstart());
			shopInfo.setSatend(test.getSatend());
			shopInfo.setSunstart(test.getSunstart());
			shopInfo.setSunend(test.getSunend());
			
			List<String> type = new ArrayList<String>();
			for(Object t : test.getType()) {
				type.add((String)t);
			}
			shopInfo.setType(type);
			
			shopDAO.create(shopInfo);
			
			shopInfo = shopDAO.find("shop"+i, "0000");
			
			/**
			 * spilt strange price like "辣 70",
			 * put "辣" into food name and keep 70 in price.
			 */
			for (int j = 0; j < test.getMenu().size(); j++) {
				JSONObject food = (JSONObject) test.getMenu().get(j);
				String[] priceSplit = food.get("price").toString().split(", ");
				if (priceSplit.length != 1) {
					for (int k = 0; k < priceSplit.length; k++) {
						String[] cut = priceSplit[k].split(" ");
						FoodInfo foodInfo = new FoodInfo();
						foodInfo.setR_id(shopInfo.getId());
						foodInfo.setName(food.get("name").toString() + " " + cut[0]);
						foodInfo.setPrice(cut[1]);
						foodDAO.create(foodInfo);
					}
				} else {
					String[] spaceSplit = priceSplit[0].split(" ");
					if (spaceSplit.length > 1) {
						FoodInfo foodInfo = new FoodInfo();
						foodInfo.setR_id(shopInfo.getId());
						foodInfo.setName(food.get("name").toString() + " " + spaceSplit[0]);
						foodInfo.setPrice(spaceSplit[1]);
						foodDAO.create(foodInfo);

					} else {
						FoodInfo foodInfo = new FoodInfo();
						foodInfo.setR_id(shopInfo.getId());
						foodInfo.setName(food.get("name").toString());
						foodInfo.setPrice(food.get("price").toString());
						foodDAO.create(foodInfo);
					}

				}
			}
		}
	}

	public static final String JSON_PATH = ("resources\\stores_detail.json");
	private static JSONParser parser = new JSONParser();

	/**
	 * constructor
	 * 
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws ParseException
	 */
	public Readjson() throws FileNotFoundException, IOException, ParseException {
		dataarr = (JSONArray) parser.parse(new FileReader(JSON_PATH));
	}

	public JSONArray getDataarr() {
		return dataarr;
	}

	public String getRname() {
		Rname = (String) restaurant.get("name");
		return Rname;
	}

	public JSONObject getRposition() {
		Rposition = (JSONObject) restaurant.get("position");
		return Rposition;
	}

	public String getAddress() {
		getRposition();
		address = (String) Rposition.get("address");
		return address;
	}

	public double getLatitude() {
		getRposition();
		latitude = (double) Rposition.get("latitude");
		return latitude;
	}

	public double getLongitude() {
		getRposition();
		longitude = (double) Rposition.get("longitude");
		return longitude;
	}

	public String getPhone() {
		phone = (String) restaurant.get("phone");
		return phone;
	}

	public String getStore_description() {
		store_description = (String) restaurant.get("store_description");
		return store_description;
	}

	public String getOrder_description() {
		order_description = (String) restaurant.get("order_description");
		return order_description;
	}

	public JSONArray getType() {
		type = (JSONArray) restaurant.get("type");
		return type;
	}

	public JSONArray getMenu() {
		menu = (JSONArray) restaurant.get("menu");
		return menu;
	}

	public JSONObject getBusiness_time() {
		business_time = (JSONObject) restaurant.get("business_time");
		return business_time;
	}

	public String getMonstart() {
		getBusiness_time();
		JSONObject Mon = (JSONObject) business_time.get("mon");
		Monstart = (String) Mon.get("start");
		return Monstart;
	}

	public String getMonend() {
		getBusiness_time();
		JSONObject Mon = (JSONObject) business_time.get("mon");
		Monend = (String) Mon.get("end");
		return Monend;
	}

	public String getTuestart() {
		getBusiness_time();
		JSONObject Tue = (JSONObject) business_time.get("tue");
		Tuestart = (String) Tue.get("start");
		return Tuestart;
	}

	public String getTueend() {
		getBusiness_time();
		JSONObject Tue = (JSONObject) business_time.get("tue");
		Tueend = (String) Tue.get("end");
		return Tueend;
	}

	public String getWedstart() {
		getBusiness_time();
		JSONObject Wed = (JSONObject) business_time.get("wed");
		Wedstart = (String) Wed.get("start");
		return Wedstart;
	}

	public String getWedend() {
		getBusiness_time();
		JSONObject Wed = (JSONObject) business_time.get("wed");
		Wedend = (String) Wed.get("end");
		return Wedend;
	}

	public String getThustart() {
		getBusiness_time();
		JSONObject Thu = (JSONObject) business_time.get("thu");
		Thustart = (String) Thu.get("start");
		return Thustart;
	}

	public String getThuend() {
		getBusiness_time();
		JSONObject Thu = (JSONObject) business_time.get("thu");
		Thuend = (String) Thu.get("end");
		return Thuend;
	}

	public String getFristart() {
		getBusiness_time();
		JSONObject Fri = (JSONObject) business_time.get("fri");
		Fristart = (String) Fri.get("start");
		return Fristart;
	}

	public String getFriend() {
		getBusiness_time();
		JSONObject Fri = (JSONObject) business_time.get("fri");
		Friend = (String) Fri.get("end");
		return Friend;
	}

	public String getSatstart() {
		getBusiness_time();
		JSONObject Sat = (JSONObject) business_time.get("sat");
		Satstart = (String) Sat.get("start");
		return Satstart;
	}

	public String getSatend() {
		getBusiness_time();
		JSONObject Sat = (JSONObject) business_time.get("sat");
		Satend = (String) Sat.get("end");
		return Satend;
	}

	public String getSunstart() {
		getBusiness_time();
		JSONObject Sun = (JSONObject) business_time.get("sun");
		Sunstart = (String) Sun.get("start");
		return Sunstart;
	}

	public String getSunend() {
		getBusiness_time();
		JSONObject Sun = (JSONObject) business_time.get("sun");
		Sunend = (String) Sun.get("end");
		return Sunend;
	}
}