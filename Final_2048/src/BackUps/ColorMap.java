package BackUps;

import java.util.HashMap;
import java.util.Map;

import javafx.scene.paint.Color;

/**
 * @author tree
 * contains color codes for switching between classic mode and dark mode
 */
public class ColorMap {
	
	public Map<Integer, Color> colorMap = new HashMap<Integer, Color>();
	public Map<Integer, Color> textColorMap = new HashMap<Integer, Color>();
	
	public Map<Integer, Color> darkColorMap = new HashMap<Integer, Color>();
	public Map<Integer, Color> darkTextColorMap = new HashMap<Integer, Color>();
	
	public ColorMap(){
		
		colorMap.put(0, Color.web("#cdc0b4"));
		colorMap.put(2, Color.web("#eee4da"));
		colorMap.put(4, Color.web("#ede0c8"));
		colorMap.put(8, Color.web("#f2b179"));
		colorMap.put(16, Color.web("#f59563"));
		colorMap.put(32, Color.web("#f67c5f"));
		colorMap.put(64, Color.web("#f65e3b"));
		colorMap.put(128, Color.web("#edcf72"));
		colorMap.put(256, Color.web("#edcc61"));
		colorMap.put(512, Color.web("#edc850"));
		colorMap.put(1024, Color.web("#edc53f"));
		colorMap.put(2048, Color.web("#edc22e"));
		colorMap.put(4096, Color.web("#3d3d3d"));
		colorMap.put(8192, Color.web("#2e2e2e"));
		colorMap.put(16384, Color.web("#1f1f1f"));
		colorMap.put(32768, Color.web("#101010"));
		colorMap.put(65536, Color.web("#000000"));
		
		
		textColorMap.put(0, Color.web("#776e65"));
		textColorMap.put(2, Color.web("#776e65"));
		textColorMap.put(4, Color.web("#776e65"));
		textColorMap.put(8, Color.web("#f9f6f2"));
		textColorMap.put(16, Color.web("#f9f6f2"));
		textColorMap.put(32, Color.web("#f9f6f2"));
		textColorMap.put(64, Color.web("#f9f6f2"));
		textColorMap.put(128, Color.web("#f9f6f2"));
		textColorMap.put(256, Color.web("#f9f6f2"));
		textColorMap.put(512, Color.web("#f9f6f2"));
		textColorMap.put(1024, Color.web("#f9f6f2"));
		textColorMap.put(2048, Color.web("#f9f6f2"));
		textColorMap.put(4096, Color.web("#f9f6f2"));
		textColorMap.put(8192, Color.web("#f9f6f2"));
		textColorMap.put(16384, Color.web("#ffffff"));
		textColorMap.put(32768, Color.web("#ffffff"));
		textColorMap.put(65536, Color.web("#ffffff"));
	
		darkColorMap.put(0, Color.web("#606060"));
		darkColorMap.put(2, Color.web("#9dc0df"));
		darkColorMap.put(4, Color.web("#428dce"));
		darkColorMap.put(8, Color.web("#0255a0"));
		darkColorMap.put(16, Color.web("#1e4ee9"));
		darkColorMap.put(32, Color.web("#1e2eb8"));
		darkColorMap.put(64, Color.web("#010d85"));
		darkColorMap.put(128, Color.web("#e598e6"));
		darkColorMap.put(256, Color.web("#e372ba"));
		darkColorMap.put(512, Color.web("#f5a5c0"));
		darkColorMap.put(1024, Color.web("#ef7a9f"));
		darkColorMap.put(2048, Color.web("#ed4f82"));
		darkColorMap.put(4096, Color.web("#ce44cb"));
		darkColorMap.put(8192, Color.web("#771db2"));
		darkColorMap.put(16384, Color.web("#b5003b"));
		darkColorMap.put(32768, Color.web("#cc0001"));
		darkColorMap.put(65536, Color.web("#cc0001"));
		
		darkTextColorMap.put(0, Color.web("#303030"));
		darkTextColorMap.put(2, Color.web("#303030"));
		darkTextColorMap.put(4, Color.web("#303030"));
		darkTextColorMap.put(8, Color.web("#ffffff"));
		darkTextColorMap.put(16, Color.web("#ffffff"));
		darkTextColorMap.put(32, Color.web("#ffffff"));
		darkTextColorMap.put(64, Color.web("#ffffff"));
		darkTextColorMap.put(128, Color.web("#ffffff"));
		darkTextColorMap.put(256, Color.web("#ffffff"));
		darkTextColorMap.put(512, Color.web("#ffffff"));
		darkTextColorMap.put(1024, Color.web("#ffffff"));
		darkTextColorMap.put(2048, Color.web("#ffffff"));
		darkTextColorMap.put(4096, Color.web("#ffffff"));
		darkTextColorMap.put(8192, Color.web("#ffffff"));
		darkTextColorMap.put(16384, Color.web("#ffffff"));
		darkTextColorMap.put(32768, Color.web("#ffffff"));
		darkTextColorMap.put(65536, Color.web("#ffffff"));
	}

}
