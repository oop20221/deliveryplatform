package BackUps;

import java.util.List;

import com.oct.dp.model.PlayerInfo;
import com.oct.dp.mydao.DAOException;
import com.oct.dp.mydao.DAOFactory;
import com.oct.dp.mydao.UserDAO;
import com.oct.dp.mydao.condition.Fields;
import com.oct.dp.mydao.condition.OrderBy;

/**
 * @author tree
 * some static methods for operating the database
 */
public class PlayerDAOOperation {

	private static DAOFactory javabase = DAOFactory.getInstance();
	private static UserDAO<PlayerInfo> playerDAO = javabase.getPlayerDAO();
	
	
	/**
	 * @param account
	 * @param password
	 * @return null if account exists, PlayerInfo object if register succeed
	 * @throws IllegalArgumentException
	 * @throws DAOException
	 */
	public static PlayerInfo register(String account, String password) throws IllegalArgumentException, DAOException {
		PlayerInfo playerInfo = new PlayerInfo();
		playerInfo.setAccount(account);
		playerInfo.setPassword(password);
		playerInfo = playerDAO.create(playerInfo);
		return playerInfo;
	}
	
	/**
	 * @param account
	 * @param password
	 * @return null if account exists, PlayerInfo object if login succeed
	 * @throws DAOException
	 */
	public static PlayerInfo login(String account, String password) throws DAOException {
		return playerDAO.find(account, password);
	}
	
	/**
	 * @param playerInfo
	 * @return updated PlayerInfo playerInfo
	 * @throws IllegalArgumentException
	 * @throws DAOException
	 */
	public static PlayerInfo update(PlayerInfo playerInfo) throws IllegalArgumentException, DAOException {
		return playerDAO.update(playerInfo);
	}
	
	/**
	 * @return a List ordered by best score
	 * @throws DAOException
	 */
	public static List<PlayerInfo> getAllRank() throws DAOException {
		return playerDAO.findAll(new OrderBy(Fields.PLAYER_BEST_SCORE, true));
	}
	
}
