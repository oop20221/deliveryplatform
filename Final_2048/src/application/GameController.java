package application;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import BackUps.ColorMap;
import BackUps.PlayerDAOOperation;
import javafx.animation.Interpolator;
import javafx.animation.RotateTransition;
import javafx.animation.TranslateTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.util.Duration;

/**
 * GameController class for controlling the scene.
 * It's a classic mode, meaning there will be bananas dropping and best score ranking.
 * @author tree
 */
public class GameController extends GameNxNController implements Initializable{
	
	/**
	 * When we lose in classic mode, we can no longer undo or get points by tapping bananas.
	 * So we use this variable for mark the status of a game.
	 */
	private boolean end = false;
	
	//------------playing grid-------------
	@FXML
	private Label lb01; @FXML
	private Label lb02; @FXML
	private Label lb03; @FXML
	private Label lb04; @FXML
	private Label lb05; @FXML
	private Label lb06; @FXML
	private Label lb07; @FXML
	private Label lb08; @FXML
	private Label lb09; @FXML
	private Label lb10; @FXML
	private Label lb11; @FXML
	private Label lb12; @FXML
	private Label lb13; @FXML
	private Label lb14; @FXML
	private Label lb15; @FXML
	private Label lb16;
	//------------Pane---------------------
	@FXML
	private AnchorPane acPane0; @FXML
	private AnchorPane acPane1;
	
	@FXML
	private GridPane gdPane;
	//----------other components------------
	@FXML
	private Label lbScore; @FXML
	private Label lbScoreValue;
	@FXML
	private Label lbBest; @FXML
	private Label lbBestValue;
	
	@FXML
	private Label lbPlayer; @FXML
	private Label lbPlayerValue;
	
	@FXML
	private Button btMenu;
	@FXML
	private Button btNewGame;
	@FXML
	private Button btUndo;
	@FXML
	private CheckBox ckBoxDark;

	@FXML
	private ImageView imgBanana; @FXML
	private ImageView imgBanana2;
	//--------------------------------------

	/**
	 * Set up the maps.
	 * @throws IOException 
	 */
	public GameController() throws IOException {
		super();
		this.side = 4;
	}
	
	/**
	 * Besides the super.initialize(), also initialize the end status, player name, and best score.
	 */
	@Override
	public void initialize(){
		super.initialize();
		
		this.end = false;
		this.bestScore = Main.playerInfo.getBestScore();
		
		lbPlayerValue.setText(Main.playerInfo.getAccount());
		lbBestValue.setText("" + Main.playerInfo.getBestScore());
	}
		
	/**
	 * Different from super.goMenu(), here is to go back to the classic mode menu.
	 */
	@Override
	public void goMenu(ActionEvent event) throws IOException {
		// jump alert
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Go Menu Alert");
		alert.setHeaderText("You will lose your work!");
		alert.setContentText("Are you sure you want to go back to menu?");
		
		if (alert.showAndWait().get() == ButtonType.OK) {
			
			FXMLLoader loader = new FXMLLoader(getClass().getResource("SceneMenu.fxml"));
			root = loader.load();
			
			MenuController controller = loader.getController();
			controller.initialize();
			
			stage = (Stage)((Node)event.getSource()).getScene().getWindow();
			scene = new Scene(root);
			stage.setScene(scene);
			stage.centerOnScreen();
			stage.show();
		}
	}
	
	/**
	 * The operation after losing is different from what is in the super.play(String move),
	 * which is that you can't keep on playing or undo in classic mode.
	 */
	@Override
	public void play(String move) {
		int continueMessage = 0;
		if (end == false) {
			continueMessage = playMove(move);
			this.draw();
		}

		if (continueMessage == 2) { // lose
			
			// jump alert
			Alert alert = new Alert(AlertType.CONFIRMATION);
			alert.setTitle("Gameover");
			alert.setHeaderText("Game over, you lose :(");
			alert.setContentText("Try again?");
			
			if (alert.showAndWait().get() == ButtonType.OK) {
				initialize();
				draw();
			}
			else {
				end = true;
			}
		}
		else if (continueMessage == 1) { // win
			// jump alert
			Alert alert = new Alert(AlertType.CONFIRMATION);
			alert.setTitle("Victory");
			alert.setHeaderText("Victory! 2048 reached :)");
			alert.setContentText("Restart?");
						
			if (alert.showAndWait().get() == ButtonType.OK) {
				initialize();
				draw();
			}
			else { // if the player click cancel after winning
				end = true;
				winning_cond = 9999999; // lift the limit of the winning condition
			}
		}
	}
	
	/**
	 * Use the Math.random(), TranslateTransition, and RotateTransition to generate some
	 * random bananas dropping from above.
	 * @see #imgBanana
	 */
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		double startPosition = 0, delay = 0, duration = 0;
		for (int i = 1; i <= 100; i++) {
			startPosition = Math.random()*350;
			delay = Math.random()*10000 + i*7000;
			duration = Math.random()*1000 + 1500;
			
			TranslateTransition translate = new TranslateTransition();
			translate.setNode(imgBanana);
			translate.setDelay(Duration.millis(delay));
			translate.setDuration(Duration.millis(duration));
			translate.setFromX(startPosition);
			translate.setByY(1000);
			translate.play();
			
			RotateTransition rotate = new RotateTransition();
			rotate.setNode(imgBanana);
			rotate.setDelay(Duration.millis(delay));
			rotate.setDuration(Duration.millis(duration));
			rotate.setCycleCount(TranslateTransition.INDEFINITE);
			rotate.setInterpolator(Interpolator.LINEAR);
			rotate.setByAngle(360);
			rotate.play();
		}
		
		for (int i = 1; i <= 100; i++) {
			startPosition = Math.random()*(-350);
			delay = Math.random()*10000 + i*13000;
			duration = Math.random()*1000 + 1500;
			
			TranslateTransition translate = new TranslateTransition();
			translate.setNode(imgBanana2);
			translate.setDelay(Duration.millis(delay));
			translate.setDuration(Duration.millis(duration));
			translate.setFromX(startPosition);
			translate.setByY(1000);
			translate.play();
			
			RotateTransition rotate = new RotateTransition();
			rotate.setNode(imgBanana2);
			rotate.setDelay(Duration.millis(delay));
			rotate.setDuration(Duration.millis(duration));
			rotate.setCycleCount(TranslateTransition.INDEFINITE);
			rotate.setInterpolator(Interpolator.LINEAR);
			rotate.setByAngle(360);
			rotate.play();
		}
	}
	
	/**
	 * If you click the banana, you'll get 500 points.
	 * @throws InterruptedException
	 * @see {@link #updateBestScore()}
	 */
	public void bonus() throws InterruptedException {
		if (!end) {
			this.score += 500;
			lbScoreValue.setText("" + score);
			Thread.sleep(500);
			updateBestScore();
		}
	}
	
	@Override
	public void selectCkBox() throws IOException {
		if (ckBoxDark.isSelected()) {
			ckBoxDark.setSelected(false);
		}
		else {
			ckBoxDark.setSelected(true);
		}
		changeColor();
	}
	
	/**
	 * Change color when the "DARK" checkBox is selected / canceled
	 */
	@Override
	public void changeColor() throws IOException {
		ColorMap colors = new ColorMap();
		
		if (ckBoxDark.isSelected()) {
			this.colorMap = colors.darkColorMap;
			this.textColorMap = colors.darkTextColorMap;

			
			acPane0.setStyle("-fx-background-color: #202020");
			acPane1.setStyle("-fx-background-color: #404040");
			gdPane.setStyle("-fx-background-color: #505050");
			
			lbBest.setStyle("-fx-background-color: #1e50bf");
			lbBestValue.setStyle("-fx-background-color: #1e50bf");
			
			lbScore.setStyle("-fx-background-color: #1e50bf");
			lbScoreValue.setStyle("-fx-background-color: #1e50bf");
			
			lbPlayer.setStyle("-fx-background-color: #1e50bf");
			lbPlayerValue.setStyle("-fx-background-color: #1e50bf");
			
			btNewGame.setStyle("-fx-background-color: #012c8c");
			btUndo.setStyle("-fx-background-color: #012c8c");
			btMenu.setStyle("-fx-background-color: #012c8c");
			ckBoxDark.setStyle("-fx-background-color: #012c8c");
			
			draw();
		}
		else {
			this.colorMap = colors.colorMap;
			this.textColorMap = colors.textColorMap;
			
			acPane0.setStyle("-fx-background-color: #faf8ef");
			acPane1.setStyle("-fx-background-color: #bbada0");
			gdPane.setStyle("-fx-background-color: #bbada0");
			
			lbBest.setStyle("-fx-background-color: #bbada0");
			lbBestValue.setStyle("-fx-background-color: #bbada0");
			
			lbScore.setStyle("-fx-background-color: #bbada0");
			lbScoreValue.setStyle("-fx-background-color: #bbada0");
			
			lbPlayer.setStyle("-fx-background-color: #bbada0");
			lbPlayerValue.setStyle("-fx-background-color: #bbada0");
			
			btNewGame.setStyle("-fx-background-color: #8f7a66");
			btUndo.setStyle("-fx-background-color: #8f7a66");
			btMenu.setStyle("-fx-background-color: #8f7a66");
			ckBoxDark.setStyle("-fx-background-color: #8f7a66");
			draw();
		}
	}
	
	/**
	 * Update the best score on the scene and database if best score is rise.
	 */
	public void updateBestScore() {
		lbScoreValue.setText("" + score);
		if (score >= bestScore) {
			bestScore = score;
			Main.playerInfo.setBestScore(bestScore);
			PlayerDAOOperation.update(Main.playerInfo);
		}
		lbBestValue.setText("" + bestScore);
	}
	
	/**
	 * Draw the status on this.board[][] in parent class "two048" onto the game scene, and also change the colors
	 * It is called when every move is done
	 */
	@Override
	public void draw(){
		
		updateBestScore();
		
		lb01.setBackground(new Background(new BackgroundFill(colorMap.get(board[0][0]), null, null)));
		lb02.setBackground(new Background(new BackgroundFill(colorMap.get(board[0][1]), null, null)));
		lb03.setBackground(new Background(new BackgroundFill(colorMap.get(board[0][2]), null, null)));
		lb04.setBackground(new Background(new BackgroundFill(colorMap.get(board[0][3]), null, null)));
		lb05.setBackground(new Background(new BackgroundFill(colorMap.get(board[1][0]), null, null)));
		lb06.setBackground(new Background(new BackgroundFill(colorMap.get(board[1][1]), null, null)));
		lb07.setBackground(new Background(new BackgroundFill(colorMap.get(board[1][2]), null, null)));
		lb08.setBackground(new Background(new BackgroundFill(colorMap.get(board[1][3]), null, null)));
		lb09.setBackground(new Background(new BackgroundFill(colorMap.get(board[2][0]), null, null)));
		lb10.setBackground(new Background(new BackgroundFill(colorMap.get(board[2][1]), null, null)));
		lb11.setBackground(new Background(new BackgroundFill(colorMap.get(board[2][2]), null, null)));
		lb12.setBackground(new Background(new BackgroundFill(colorMap.get(board[2][3]), null, null)));
		lb13.setBackground(new Background(new BackgroundFill(colorMap.get(board[3][0]), null, null)));
		lb14.setBackground(new Background(new BackgroundFill(colorMap.get(board[3][1]), null, null)));
		lb15.setBackground(new Background(new BackgroundFill(colorMap.get(board[3][2]), null, null)));
		lb16.setBackground(new Background(new BackgroundFill(colorMap.get(board[3][3]), null, null)));
		
		lb01.setTextFill(textColorMap.get(board[0][0]));
		lb02.setTextFill(textColorMap.get(board[0][1]));
		lb03.setTextFill(textColorMap.get(board[0][2]));
		lb04.setTextFill(textColorMap.get(board[0][3]));
		lb05.setTextFill(textColorMap.get(board[1][0]));
		lb06.setTextFill(textColorMap.get(board[1][1]));
		lb07.setTextFill(textColorMap.get(board[1][2]));
		lb08.setTextFill(textColorMap.get(board[1][3]));
		lb09.setTextFill(textColorMap.get(board[2][0]));
		lb10.setTextFill(textColorMap.get(board[2][1]));
		lb11.setTextFill(textColorMap.get(board[2][2]));
		lb12.setTextFill(textColorMap.get(board[2][3]));
		lb13.setTextFill(textColorMap.get(board[3][0]));
		lb14.setTextFill(textColorMap.get(board[3][1]));
		lb15.setTextFill(textColorMap.get(board[3][2]));
		lb16.setTextFill(textColorMap.get(board[3][3]));
		
		lb01.setText(parseInt(board[0][0]));
		lb02.setText(parseInt(board[0][1]));
		lb03.setText(parseInt(board[0][2]));
		lb04.setText(parseInt(board[0][3]));
		lb05.setText(parseInt(board[1][0]));
		lb06.setText(parseInt(board[1][1]));
		lb07.setText(parseInt(board[1][2]));
		lb08.setText(parseInt(board[1][3]));
		lb09.setText(parseInt(board[2][0]));
		lb10.setText(parseInt(board[2][1]));
		lb11.setText(parseInt(board[2][2]));
		lb12.setText(parseInt(board[2][3]));
		lb13.setText(parseInt(board[3][0]));
		lb14.setText(parseInt(board[3][1]));
		lb15.setText(parseInt(board[3][2]));
		lb16.setText(parseInt(board[3][3]));
	}
}
