package application;
	
import java.io.IOException;

import com.oct.dp.model.PlayerInfo;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.scene.Parent;
import javafx.scene.Scene;
//import javafx.scene.image.Image;

/**
 * Main class for launching.
 * @author tree
 */
public class Main extends Application {
	
	public static Stage primaryStage;
	public static Parent primaryRoot;
	
	/**
	 * static playerInfo that can be access in all controllers
	 */
	public static PlayerInfo playerInfo;
	
	/**
	 * start the scene
	 */
	@Override
	public void start(Stage primaryStage) throws IOException {
		try {
			//Image icon = new Image("icon2048.png");
			Main.primaryStage = primaryStage;
			Main.primaryStage.setTitle("2048");
			//Main.primaryStage.getIcons().add(icon);
			Main.primaryStage.centerOnScreen();
			Main.showLogInView();

		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * Go to the first scene
	 * @throws IOException
	 */
	public static void showLogInView() throws IOException{
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("SceneLogIn.fxml"));
		primaryRoot = loader.load();
		
		Scene scene = new Scene(primaryRoot);
		primaryStage.setScene(scene);
		primaryStage.centerOnScreen();
		primaryStage.show();
	}
	
	/**
	 * Show the Instruction scene.
	 * @throws IOException
	 */
	public static void showInstructionStage() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("SceneInstruction.fxml"));
		Parent jumpRoot = loader.load();
		
		Stage jumpStage = new Stage();
		jumpStage.setTitle("Instruction");
		jumpStage.initModality(Modality.WINDOW_MODAL);
		jumpStage.initOwner(primaryStage);
		
		Scene scene = new Scene(jumpRoot);
		jumpStage.setScene(scene);
		jumpStage.showAndWait();
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
