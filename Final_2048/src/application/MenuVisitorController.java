package application;

import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

public class MenuVisitorController {
	
	private Stage stage;
	private Scene scene;
	private Parent root;
	
	/**
	 * @param event of clicking the "4x4" button
	 * @throws IOException
	 */
	@FXML
	private void goGame4x4(ActionEvent event) throws IOException {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("SceneGame4x4.fxml"));
		root = loader.load();
		scene = new Scene(root);
		
		Game4x4Controller controller = loader.getController();
		
		controller.initialize();
		controller.draw();
		
		// anonymous class announced for key event
		scene.setOnKeyPressed(new EventHandler<KeyEvent>() {
			/**
			 * if wasd is pressed, pass wasd into controller
			 */
			@Override
			public void handle(KeyEvent event) {
				try {
					switch (event.getCode()) {
						case W:
							controller.play("w"); break;
						case S:
							controller.play("s"); break;
						case A:
							controller.play("a"); break;
						case D:
							controller.play("d"); break;
						case U:
							controller.undo(); break;
						case N:
							controller.newGame(); break;
						case C:
							controller.selectCkBox(); break;
						case UNDEFINED: {
							Alert alert = new Alert(AlertType.CONFIRMATION);
							alert.setTitle("Input Alert");
							alert.setHeaderText("Undetectable input!");
							alert.setContentText("You may have to switch to English IME.");
							if (alert.showAndWait().get() == ButtonType.OK){}}; break;
						default:
							System.out.println(event.getCode() + " (Invalid input)");
							break;
					}
				}
				catch(Exception e) {
					System.out.println("Unexpected error");
				}
			}
		});
		
		stage = (Stage)((Node)event.getSource()).getScene().getWindow();
		stage.setScene(scene);
		stage.centerOnScreen();
		stage.show();
	}
	
	
	/**
	 * @param event of clicking the "5x5" button
	 * @throws IOException
	 */
	@FXML
	private void goGame5x5(ActionEvent event) throws IOException {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("SceneGame5x5.fxml"));
		root = loader.load();
		scene = new Scene(root);
		
		Game5x5Controller controller = loader.getController();
		
		controller.initialize();
		controller.draw();
		
		// anonymous class announced for key event
		scene.setOnKeyPressed(new EventHandler<KeyEvent>() {
			/**
			 * if wasd is pressed, pass wasd into controller
			 */
			@Override
			public void handle(KeyEvent event) {
				try {
					switch (event.getCode()) {
						case W:
							controller.play("w"); break;
						case S:
							controller.play("s"); break;
						case A:
							controller.play("a"); break;
						case D:
							controller.play("d"); break;
						case U:
							controller.undo(); break;
						case N:
							controller.newGame(); break;
						case C:
							controller.selectCkBox(); break;
						case UNDEFINED: {
							Alert alert = new Alert(AlertType.CONFIRMATION);
							alert.setTitle("Input Alert");
							alert.setHeaderText("Undetectable input!");
							alert.setContentText("You may have to switch to English IME.");
							if (alert.showAndWait().get() == ButtonType.OK){}}; break;
						default:
							System.out.println(event.getCode() + " (Invalid input)");
							break;
					}
				}
				catch(Exception e) {
					System.out.println("Unexpected error");
				}
			}
		});
		
		stage = (Stage)((Node)event.getSource()).getScene().getWindow();
		stage.setScene(scene);
		stage.centerOnScreen();
		stage.show();
	}
	
	/**
	 * @param event of clicking the "5x5" button
	 * @throws IOException
	 */
	@FXML
	private void goGame6x6(ActionEvent event) throws IOException {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("SceneGame6x6.fxml"));
		root = loader.load();
		scene = new Scene(root);
		
		Game6x6Controller controller = loader.getController();
		
		controller.initialize();
		controller.draw();
		
		// anonymous class announced for key event
		scene.setOnKeyPressed(new EventHandler<KeyEvent>() {
			/**
			 * if wasd is pressed, pass wasd into controller
			 */
			@Override
			public void handle(KeyEvent event) {
				try {
					switch (event.getCode()) {
						case W:
							controller.play("w"); break;
						case S:
							controller.play("s"); break;
						case A:
							controller.play("a"); break;
						case D:
							controller.play("d"); break;
						case U:
							controller.undo(); break;
						case N:
							controller.newGame(); break;
						case C:
							controller.selectCkBox(); break;
						case UNDEFINED: {
							Alert alert = new Alert(AlertType.CONFIRMATION);
							alert.setTitle("Input Alert");
							alert.setHeaderText("Undetectable input!");
							alert.setContentText("You may have to switch to English IME.");
							if (alert.showAndWait().get() == ButtonType.OK){}}; break;
						default:
							break;
					}
				}
				catch(Exception e) {
					System.out.println("Unexpected error");
				}
			}
		});
		
		stage = (Stage)((Node)event.getSource()).getScene().getWindow();
		stage.setScene(scene);
		stage.centerOnScreen();
		stage.show();
	}


	@FXML
	private void goInstruction() throws IOException {
		Main.showInstructionStage();
	}

	@FXML
	private void logIn() throws IOException {
		Main.showLogInView();
	}
}
