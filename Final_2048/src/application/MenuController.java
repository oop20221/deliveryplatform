package application;

import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

/**
 * @author tree
 * Menu for players who logged in
 */
public class MenuController {
	
	private Stage stage;
	private Scene scene;
	private Parent root;
	
	@FXML
	private Label lbHiName;
	
	//----------------------------------------------
	public void initialize() {
		lbHiName.setText("Hi!  " + Main.playerInfo.getAccount());
	}
	
	
	/**
	 * @param event of clicking the "START" button
	 * @throws IOException
	 */
	@FXML
	private void goGame(ActionEvent event) throws IOException {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("SceneGame.fxml"));
		root = loader.load();
		scene = new Scene(root);
		
		GameController controller = loader.getController();
		controller.initialize();
		controller.draw();
		
		// anonymous class announced for key event
		scene.setOnKeyPressed(new EventHandler<KeyEvent>() {
			/**
			 * if wasd is pressed, pass wasd into controller
			 */
			@Override
			public void handle(KeyEvent event) {
				try {
					switch (event.getCode()) {
						case W:
							controller.play("w"); break;
						case S:
							controller.play("s"); break;
						case A:
							controller.play("a"); break;
						case D:
							controller.play("d"); break;
						case U:
							controller.undo(); break;
						case N:
							controller.newGame(); break;
						case C:
							controller.selectCkBox(); break;
						case UNDEFINED: {
							Alert alert = new Alert(AlertType.CONFIRMATION);
							alert.setTitle("Input Alert");
							alert.setHeaderText("Undetectable input!");
							alert.setContentText("You may have to switch to English IME.");
							if (alert.showAndWait().get() == ButtonType.OK){}}; break;
						default:
							break;
					}
				}
				catch(Exception e) {
					System.out.println("Unexpected error");
				}
			}
		});
		
		stage = (Stage)((Node)event.getSource()).getScene().getWindow();
		stage.setScene(scene);
		stage.centerOnScreen();
		stage.show();
	}
	/**
	 * Show the rank.
	 * @param event of clicking the "RANK" button
	 * @throws IOException
	 */
	@FXML
	private void goRank(ActionEvent event) throws IOException {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("SceneRank.fxml"));
		root = loader.load();
		
		RankController controller = loader.getController();
		controller.initialize();
		
		stage = (Stage)((Node)event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.centerOnScreen();
		stage.show();
	}
	/**
	 * Show the Instructions.
	 * @throws IOException
	 */
	@FXML
	private void goInstruction() throws IOException {
		Main.showInstructionStage();
	}
	
	/**
	 * Back to the first log in scene
	 * @throws IOException
	 */
	@FXML
	private void logOut() throws IOException {
		// log out 
		Main.showLogInView();
	}

}
