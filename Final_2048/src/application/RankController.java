package application;

import java.io.IOException;
import java.util.List;

import com.oct.dp.model.PlayerInfo;

import BackUps.PlayerDAOOperation;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;

/**
 * @author tree
 * Rank board that get data from the database, rank them, and show
 */
public class RankController {
	
	private Scene scene;
	private Parent root;
	
	private List<PlayerInfo> allRank;
	
	//----------------------------
	@FXML
	private Label rankMy;
	@FXML
	private Label name1; @FXML
	private Label name2; @FXML
	private Label name3; @FXML
	private Label name4; @FXML
	private Label name5; @FXML
	private Label name6; @FXML
	private Label nameMy;
	@FXML
	private Label score1; @FXML
	private Label score2; @FXML
	private Label score3; @FXML
	private Label score4; @FXML
	private Label score5; @FXML
	private Label score6; @FXML
	private Label scoreMy;
	//----------------------------

	
	/**
	 * Get the info, calculate the result, and show.
	 */
	public void initialize() {
		allRank = PlayerDAOOperation.getAllRank();
		//System.out.println("Top 6: " + allRank);
		//System.out.println("All: " + allRank);
		int playerRank = 0;
		for (int i = 0; i < allRank.size(); i++) {
			if (Main.playerInfo.equals(allRank.get(i))) {
				playerRank = i;
				break;
			}
		}
		switch (playerRank + 1) {
			case (1):
				rankMy.setText("1st"); break;
			case (2):
				rankMy.setText("2nd"); break;
			case (3):
				rankMy.setText("3rd"); break;
			default:
				rankMy.setText((playerRank + 1) + "th"); break;
			
		}
		
		name1.setText(allRank.get(0).getAccount());
		name2.setText(allRank.get(1).getAccount());
		name3.setText(allRank.get(2).getAccount());
		name4.setText(allRank.get(3).getAccount());
		name5.setText(allRank.get(4).getAccount());
		name6.setText(allRank.get(5).getAccount());
		nameMy.setText(Main.playerInfo.getAccount() + " (YOU)");
		
		score1.setText(allRank.get(0).getBestScore() + "  pts");
		score2.setText(allRank.get(1).getBestScore() + "  pts");
		score3.setText(allRank.get(2).getBestScore() + "  pts");
		score4.setText(allRank.get(3).getBestScore() + "  pts");
		score5.setText(allRank.get(4).getBestScore() + "  pts");
		score6.setText(allRank.get(5).getBestScore() + "  pts");
		scoreMy.setText(Main.playerInfo.getBestScore() + "  pts");
	}

	/**
	 * Go back to the menu.
	 * @throws IOException
	 */
	public void goMenu() throws IOException {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("SceneMenu.fxml"));
		root = loader.load();
		
		MenuController controller = loader.getController();
		controller.initialize();
		
		//stage = (Stage)((Node)event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		Main.primaryStage.setScene(scene);
		Main.primaryStage.centerOnScreen();
		Main.primaryStage.show();	
	}
}
