package application;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import java.util.stream.IntStream;

import BackUps.ColorMap;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import two048.two048;

/**
 * Parent class for all 2048 game scene.
 * In children classes, there will be some FXML components declared,
 * and override some methods that controll the scene.
 * @author tree
 */
public class GameNxNController extends two048 {
	
	public Stage stage;
	public Scene scene;
	public Parent root;
	
	/**
	 * Color code for grid background
	 */
	public Map<Integer, Color> colorMap;// = new HashMap<Integer, Color>();
	/**
	 * Color code for the text
	 */
	public Map<Integer, Color> textColorMap;// = new HashMap<Integer, Color>();
	


	/**
	 * Set up the maps and the pre-setting.
	 * @throws IOException 
	 */
	public GameNxNController() throws IOException {
		ColorMap colors = new ColorMap();
		this.colorMap = colors.colorMap;
		this.textColorMap = colors.textColorMap;
	}
	
	/**
	 * Call when building up the scene or restart a new game.
	 */
	public void initialize(){
		this.setBoard(side, winning_cond);
		this.setNPosition(2);
		this.score = 0;
	}
	
	/**
	 * @param event of clicking the "UNDO" button
	 */
	public void undo() {
		try {
			undoPlay();
			draw();
		}
		catch(Exception e) {
			Alert alert = new Alert(AlertType.CONFIRMATION);
			alert.setTitle("Undo Alert");
			alert.setHeaderText("You can't undo before second move!");
			alert.setContentText("Continue?");
			if (alert.showAndWait().get() == ButtonType.OK) {}
		}
	}
	
	/**
	 * @param event of clicking the "MENU" button
	 * @throws IOException
	 */
	public void goMenu(ActionEvent event) throws IOException {
		// jump alert
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Go Menu Alert");
		alert.setHeaderText("You will lose your work!");
		alert.setContentText("Are you sure you want to go back to menu?");
		
		if (alert.showAndWait().get() == ButtonType.OK) {
			
			FXMLLoader loader = new FXMLLoader(getClass().getResource("SceneMenuVisitor.fxml"));
			root = loader.load();
			//MenuVisitorController controller = loader.getController();
			
			stage = (Stage)((Node)event.getSource()).getScene().getWindow();
			scene = new Scene(root);
			stage.setScene(scene);
			stage.centerOnScreen();
			stage.show();
		}
	}

	/**
	 * @param event of clicking the "NEW" button
	 */
	public void newGame() {
		// jump alert
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("New Game Alert");
		alert.setHeaderText("You will lose your work!");
		alert.setContentText("Are you sure you want to create a new game?");
		
		if (alert.showAndWait().get() == ButtonType.OK) {
			initialize();
			draw();
		}
	}
	/**
	 * Secret feature that only exists in practice mode,
	 * which is to automatically play four moves in four direction
	 * at one single mouse-scroll on the 2048 image
	 */
	public void autoPlay() {
		play("w");
		play("a");
		play("s");
		play("d");
	}
	
	/**
	 * @param the keyboard command "w, a, s, d" passed in by the user
	 */
	public void play(String move) {
		int continueMessage = playMove(move);
		this.draw();
		
		if (continueMessage == 2) { // lose
			// jump alert
			Alert alert = new Alert(AlertType.CONFIRMATION);
			alert.setTitle("Gameover");
			alert.setHeaderText("Game over, you lose :(");
			alert.setContentText("Try again?");
			
			if (alert.showAndWait().get() == ButtonType.OK) {
				initialize();
				draw();
			}
			
		}
		else if (continueMessage == 1) { // win
			// jump alert
			Alert alert = new Alert(AlertType.CONFIRMATION);
			alert.setTitle("Victory");
			alert.setHeaderText("Victory! 2048 reached :)");
			alert.setContentText("Restart?");
						
			if (alert.showAndWait().get() == ButtonType.OK) {
				initialize();
				draw();
			}
			else {
				winning_cond = 9999999;
			}
		}
	}
	
	/**
	 * Select the "DARK" checkBox from the controller
	 * @throws IOException 
	 */
	public void selectCkBox() throws IOException {}
	
	/**
	 * Change color when the "DARK" checkBox is selected / canceled
	 * @throws IOException
	 * @see {@link #draw()}
	 */
	public void changeColor() throws IOException {}
	
	/**
	 * This is for showing the result on the game scene
	 * @param n a number
	 * @return String ("" + n) if it's not 0, or it returns ("")
	 */
	public String parseInt(int n) {
		return (n == 0) ? "" : "" + n;
	}
	
	/**
	 * Draw the status on this.board[][] in parent class "two048" onto the game scene, and also change the colors
	 * It is called when every move is done
	 * In children classes, this will be controlling some FXML components.
	 */
	public void draw(){}
}
