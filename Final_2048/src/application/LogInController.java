package application;

import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * @author tree
 * Show the first scene shown to the user.
 */
public class LogInController {
	
	private Stage stage;
	private Scene scene;
	private Parent root;
	
	/**
	 * Go to InputController.
	 * @param event click
	 * @throws IOException
	 */
	@FXML
	private void logIn(ActionEvent event) throws IOException {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("SceneInput.fxml"));
		root = loader.load();
		//InputController controller = loader.getController();
		
		stage = (Stage)((Node)event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.centerOnScreen();
		stage.show();
	}
	
	/**
	 * Go to MenuVisitorController.
	 * @param event click
	 * @throws IOException
	 */
	@FXML
	private void visitor(ActionEvent event) throws IOException {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("SceneMenuVisitor.fxml"));
		root = loader.load();
		//MenuVisitorController controller = loader.getController();

		
		stage = (Stage)((Node)event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.centerOnScreen();
		stage.show();
		
	}
	
	/**
	 * Leave the program.
	 * @throws IOException
	 */
	@FXML
	private void leave() throws IOException {
		System.exit(0);
	}

}
