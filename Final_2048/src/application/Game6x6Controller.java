package application;

import java.io.IOException;

import BackUps.ColorMap;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.GridPane;
/**
 * Similar to Game4x4Controller class, but this is a 5x5 version.
 * @author tree
 */
public class Game6x6Controller extends GameNxNController {
	
	//------------playing grids-----------
	@FXML
	private Label lb01; @FXML
	private Label lb02; @FXML
	private Label lb03; @FXML
	private Label lb04; @FXML
	private Label lb05; @FXML
	private Label lb06; @FXML
	private Label lb07; @FXML
	private Label lb08; @FXML
	private Label lb09; @FXML
	private Label lb10; @FXML
	private Label lb11; @FXML
	private Label lb12; @FXML
	private Label lb13; @FXML
	private Label lb14; @FXML
	private Label lb15; @FXML
	private Label lb16; @FXML
	private Label lb17; @FXML
	private Label lb18; @FXML
	private Label lb19; @FXML
	private Label lb20; @FXML
	private Label lb21; @FXML
	private Label lb22; @FXML
	private Label lb23; @FXML
	private Label lb24; @FXML
	private Label lb25; @FXML
	private Label lb26; @FXML
	private Label lb27; @FXML
	private Label lb28; @FXML
	private Label lb29; @FXML
	private Label lb30; @FXML
	private Label lb31; @FXML
	private Label lb32; @FXML
	private Label lb33; @FXML
	private Label lb34; @FXML
	private Label lb35; @FXML
	private Label lb36;
	
	//--------------Pane------------------
	@FXML
	private AnchorPane acPane0; @FXML
	private AnchorPane acPane1;
	
	@FXML
	private GridPane gdPane;

	//----------other components-----------
	@FXML
	private Label lbPractice; @FXML
	private Label lbMode;
	
	@FXML
	private Label lbScore; @FXML
	private Label lbScoreValue;
	
	@FXML
	private Label lbPlayer; @FXML
	private Label lbPlayerValue;
	
	@FXML
	private Button btMenu;
	@FXML
	private Button btNewGame;
	@FXML
	private Button btUndo;
	@FXML
	private CheckBox ckBoxDark;
	

	/**
	 * Set up the maps and the pre-setting.
	 * @throws IOException 
	 */
	public Game6x6Controller() throws IOException {
		super();
		this.side = 6;
	}
	
	/**
	 * Select the "DARK" checkBox from the controller
	 */
	@Override
	public void selectCkBox() throws IOException {
		if (ckBoxDark.isSelected()) {
			ckBoxDark.setSelected(false);
		}
		else {
			ckBoxDark.setSelected(true);
		}
		changeColor();
	}

	/**
	 * Change color when the "DARK" checkBox is selected / canceled
	 */
	@Override
	public void changeColor() throws IOException {
		ColorMap colors = new ColorMap();
		
		if (ckBoxDark.isSelected()) {
			this.colorMap = colors.darkColorMap;
			this.textColorMap = colors.darkTextColorMap;

			
			acPane0.setStyle("-fx-background-color: #202020");
			acPane1.setStyle("-fx-background-color: #404040");
			gdPane.setStyle("-fx-background-color: #505050");
			
			lbPractice.setStyle("-fx-background-color: #1e50bf");
			lbMode.setStyle("-fx-background-color: #1e50bf");
			
			lbScore.setStyle("-fx-background-color: #1e50bf");
			lbScoreValue.setStyle("-fx-background-color: #1e50bf");
			
			lbPlayer.setStyle("-fx-background-color: #1e50bf");
			lbPlayerValue.setStyle("-fx-background-color: #1e50bf");
			
			btNewGame.setStyle("-fx-background-color: #012c8c");
			btUndo.setStyle("-fx-background-color: #012c8c");
			btMenu.setStyle("-fx-background-color: #012c8c");
			ckBoxDark.setStyle("-fx-background-color: #012c8c");
			
			draw();
		}
		else {
			this.colorMap = colors.colorMap;
			this.textColorMap = colors.textColorMap;
			
			acPane0.setStyle("-fx-background-color: #faf8ef");
			acPane1.setStyle("-fx-background-color: #bbada0");
			gdPane.setStyle("-fx-background-color: #bbada0");
			
			lbPractice.setStyle("-fx-background-color: #bbada0");
			lbMode.setStyle("-fx-background-color: #bbada0");
			
			lbScore.setStyle("-fx-background-color: #bbada0");
			lbScoreValue.setStyle("-fx-background-color: #bbada0");
			
			lbPlayer.setStyle("-fx-background-color: #bbada0");
			lbPlayerValue.setStyle("-fx-background-color: #bbada0");
			
			btNewGame.setStyle("-fx-background-color: #8f7a66");
			btUndo.setStyle("-fx-background-color: #8f7a66");
			btMenu.setStyle("-fx-background-color: #8f7a66");
			ckBoxDark.setStyle("-fx-background-color: #8f7a66");
			draw();
		}
	}
	
	/**
	 * Draw the status on this.board[][] in parent class "two048" onto the game scene, and also change the colors
	 * It is called when every move is done
	 */
	@Override
	public void draw(){
		
		lbScoreValue.setText("" + score);
		
		lb01.setBackground(new Background(new BackgroundFill(colorMap.get(board[0][0]), null, null)));
		lb02.setBackground(new Background(new BackgroundFill(colorMap.get(board[0][1]), null, null)));
		lb03.setBackground(new Background(new BackgroundFill(colorMap.get(board[0][2]), null, null)));
		lb04.setBackground(new Background(new BackgroundFill(colorMap.get(board[0][3]), null, null)));
		lb05.setBackground(new Background(new BackgroundFill(colorMap.get(board[0][4]), null, null)));
		lb06.setBackground(new Background(new BackgroundFill(colorMap.get(board[0][5]), null, null)));
		lb07.setBackground(new Background(new BackgroundFill(colorMap.get(board[1][0]), null, null)));
		lb08.setBackground(new Background(new BackgroundFill(colorMap.get(board[1][1]), null, null)));
		lb09.setBackground(new Background(new BackgroundFill(colorMap.get(board[1][2]), null, null)));
		lb10.setBackground(new Background(new BackgroundFill(colorMap.get(board[1][3]), null, null)));
		lb11.setBackground(new Background(new BackgroundFill(colorMap.get(board[1][4]), null, null)));
		lb12.setBackground(new Background(new BackgroundFill(colorMap.get(board[1][5]), null, null)));
		lb13.setBackground(new Background(new BackgroundFill(colorMap.get(board[2][0]), null, null)));
		lb14.setBackground(new Background(new BackgroundFill(colorMap.get(board[2][1]), null, null)));
		lb15.setBackground(new Background(new BackgroundFill(colorMap.get(board[2][2]), null, null)));
		lb16.setBackground(new Background(new BackgroundFill(colorMap.get(board[2][3]), null, null)));
		lb17.setBackground(new Background(new BackgroundFill(colorMap.get(board[2][4]), null, null)));
		lb18.setBackground(new Background(new BackgroundFill(colorMap.get(board[2][5]), null, null)));
		lb19.setBackground(new Background(new BackgroundFill(colorMap.get(board[3][0]), null, null)));
		lb20.setBackground(new Background(new BackgroundFill(colorMap.get(board[3][1]), null, null)));
		lb21.setBackground(new Background(new BackgroundFill(colorMap.get(board[3][2]), null, null)));
		lb22.setBackground(new Background(new BackgroundFill(colorMap.get(board[3][3]), null, null)));
		lb23.setBackground(new Background(new BackgroundFill(colorMap.get(board[3][4]), null, null)));
		lb24.setBackground(new Background(new BackgroundFill(colorMap.get(board[3][5]), null, null)));
		lb25.setBackground(new Background(new BackgroundFill(colorMap.get(board[4][0]), null, null)));
		lb26.setBackground(new Background(new BackgroundFill(colorMap.get(board[4][1]), null, null)));
		lb27.setBackground(new Background(new BackgroundFill(colorMap.get(board[4][2]), null, null)));
		lb28.setBackground(new Background(new BackgroundFill(colorMap.get(board[4][3]), null, null)));
		lb29.setBackground(new Background(new BackgroundFill(colorMap.get(board[4][4]), null, null)));
		lb30.setBackground(new Background(new BackgroundFill(colorMap.get(board[4][5]), null, null)));
		lb31.setBackground(new Background(new BackgroundFill(colorMap.get(board[5][0]), null, null)));
		lb32.setBackground(new Background(new BackgroundFill(colorMap.get(board[5][1]), null, null)));
		lb33.setBackground(new Background(new BackgroundFill(colorMap.get(board[5][2]), null, null)));
		lb34.setBackground(new Background(new BackgroundFill(colorMap.get(board[5][3]), null, null)));
		lb35.setBackground(new Background(new BackgroundFill(colorMap.get(board[5][4]), null, null)));
		lb36.setBackground(new Background(new BackgroundFill(colorMap.get(board[5][5]), null, null)));
		
		
		lb01.setTextFill(textColorMap.get(board[0][0]));
		lb02.setTextFill(textColorMap.get(board[0][1]));
		lb03.setTextFill(textColorMap.get(board[0][2]));
		lb04.setTextFill(textColorMap.get(board[0][3]));
		lb05.setTextFill(textColorMap.get(board[0][4]));
		lb06.setTextFill(textColorMap.get(board[0][5]));
		lb07.setTextFill(textColorMap.get(board[1][0]));
		lb08.setTextFill(textColorMap.get(board[1][1]));
		lb09.setTextFill(textColorMap.get(board[1][2]));
		lb10.setTextFill(textColorMap.get(board[1][3]));
		lb11.setTextFill(textColorMap.get(board[1][4]));
		lb12.setTextFill(textColorMap.get(board[1][5]));
		lb13.setTextFill(textColorMap.get(board[2][0]));
		lb14.setTextFill(textColorMap.get(board[2][1]));
		lb15.setTextFill(textColorMap.get(board[2][2]));
		lb16.setTextFill(textColorMap.get(board[2][3]));
		lb17.setTextFill(textColorMap.get(board[2][4]));
		lb18.setTextFill(textColorMap.get(board[2][5]));
		lb19.setTextFill(textColorMap.get(board[3][0]));
		lb20.setTextFill(textColorMap.get(board[3][1]));
		lb21.setTextFill(textColorMap.get(board[3][2]));
		lb22.setTextFill(textColorMap.get(board[3][3]));
		lb23.setTextFill(textColorMap.get(board[3][4]));
		lb24.setTextFill(textColorMap.get(board[3][5]));
		lb25.setTextFill(textColorMap.get(board[4][0]));
		lb26.setTextFill(textColorMap.get(board[4][1]));
		lb27.setTextFill(textColorMap.get(board[4][2]));
		lb28.setTextFill(textColorMap.get(board[4][3]));
		lb29.setTextFill(textColorMap.get(board[4][4]));
		lb30.setTextFill(textColorMap.get(board[4][5]));
		lb31.setTextFill(textColorMap.get(board[5][0]));
		lb32.setTextFill(textColorMap.get(board[5][1]));
		lb33.setTextFill(textColorMap.get(board[5][2]));
		lb34.setTextFill(textColorMap.get(board[5][3]));
		lb35.setTextFill(textColorMap.get(board[5][4]));
		lb36.setTextFill(textColorMap.get(board[5][5]));
		
		
		lb01.setText(parseInt(board[0][0]));
		lb02.setText(parseInt(board[0][1]));
		lb03.setText(parseInt(board[0][2]));
		lb04.setText(parseInt(board[0][3]));
		lb05.setText(parseInt(board[0][4]));
		lb06.setText(parseInt(board[0][5]));
		lb07.setText(parseInt(board[1][0]));
		lb08.setText(parseInt(board[1][1]));
		lb09.setText(parseInt(board[1][2]));
		lb10.setText(parseInt(board[1][3]));
		lb11.setText(parseInt(board[1][4]));
		lb12.setText(parseInt(board[1][5]));
		lb13.setText(parseInt(board[2][0]));
		lb14.setText(parseInt(board[2][1]));
		lb15.setText(parseInt(board[2][2]));
		lb16.setText(parseInt(board[2][3]));
		lb17.setText(parseInt(board[2][4]));
		lb18.setText(parseInt(board[2][5]));
		lb19.setText(parseInt(board[3][0]));
		lb20.setText(parseInt(board[3][1]));
		lb21.setText(parseInt(board[3][2]));
		lb22.setText(parseInt(board[3][3]));
		lb23.setText(parseInt(board[3][4]));
		lb24.setText(parseInt(board[3][5]));
		lb25.setText(parseInt(board[4][0]));
		lb26.setText(parseInt(board[4][1]));
		lb27.setText(parseInt(board[4][2]));
		lb28.setText(parseInt(board[4][3]));
		lb29.setText(parseInt(board[4][4]));
		lb30.setText(parseInt(board[4][5]));
		lb31.setText(parseInt(board[5][0]));
		lb32.setText(parseInt(board[5][1]));
		lb33.setText(parseInt(board[5][2]));
		lb34.setText(parseInt(board[5][3]));
		lb35.setText(parseInt(board[5][4]));
		lb36.setText(parseInt(board[5][5]));
		
	}
}
