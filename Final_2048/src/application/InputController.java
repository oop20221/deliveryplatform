package application;

import java.io.IOException;

import BackUps.PlayerDAOOperation;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;

/**
 * @author tree
 * The scene where players input their account and password.
 */
public class InputController {
	
	private Stage stage;
	private Scene scene;
	private Parent root;
			 
	@FXML
	private TextField txBoxAccount;
	@FXML
	private PasswordField pwBoxPassword;
	
	/**
	 * First check if the boxes are filled, and check if the account exists in the database,
	 * if all pass, log in and set up Main.playerInfo.
	 * @param event of clicking the "LOG IN" button
	 * @throws IOException
	 */
	@FXML
	public void logIn(ActionEvent event) throws IOException {
		if (checkTextField()) {
			Main.playerInfo = PlayerDAOOperation.login(txBoxAccount.getText(),pwBoxPassword.getText());
			if (Main.playerInfo == null) {
				
				Alert alert = new Alert(AlertType.CONFIRMATION);
				alert.setTitle("Log in failed Alert");
				alert.setHeaderText("Account doesn't exist or Password mistake!");
				alert.setContentText("Please check your account, password or try to register.");
				if (alert.showAndWait().get() == ButtonType.OK) {}
			}
			else {
				
				FXMLLoader loader = new FXMLLoader(getClass().getResource("SceneMenu.fxml"));
				root = loader.load();
				MenuController controller = loader.getController();
				controller.initialize();
				
				stage = (Stage)((Node)event.getSource()).getScene().getWindow();
				scene = new Scene(root);
				stage.setScene(scene);
				stage.centerOnScreen();
				stage.show();
			}
		}
	}
	
	/**
	 * First check if the boxes are filled, and check if the account exists in the database,
	 * if not, create an new playerInfo object in the database,
	 * if exists, jump alert.
	 * @param event of clicking the "REGISTER" Button
	 * @throws IOException
	 */
	@FXML
	public void register(ActionEvent event) throws IOException {
		if (checkTextField()) {
			try {
				Main.playerInfo = PlayerDAOOperation.register(txBoxAccount.getText(), pwBoxPassword.getText());	
				Main.playerInfo.setBestScore(0);
				
				Alert alert = new Alert(AlertType.CONFIRMATION);
				alert.setTitle("Register success Alert");
				alert.setHeaderText("Register successfuly!");
				alert.setContentText("Welcome, " + Main.playerInfo.getAccount());
				if (alert.showAndWait().get() == ButtonType.OK) {}
			}
			catch (Exception e) {
				Alert alert = new Alert(AlertType.CONFIRMATION);
				alert.setTitle("Account Existance Alert");
				alert.setHeaderText("Account exists!");
				alert.setContentText("Please select a different account, or try yo log in.");
				if (alert.showAndWait().get() == ButtonType.OK) {}
			}
		}
	}
	
	/**
	 * @return false if either of the two boxes are empty, otherwise true
	 */
	private boolean checkTextField() {
		if (txBoxAccount.getText().equals("")) {
			Alert alert = new Alert(AlertType.CONFIRMATION);
			alert.setTitle("Empty Alert");
			alert.setHeaderText("Account empty!");
			alert.setContentText("Please properly input your account.");
			if (alert.showAndWait().get() == ButtonType.OK) {}
			return false;
		}
		else if (pwBoxPassword.getText().equals("")) {
			Alert alert = new Alert(AlertType.CONFIRMATION);
			alert.setTitle("Empty Alert");
			alert.setHeaderText("Password empty!");
			alert.setContentText("Please properly input your password.");
			if (alert.showAndWait().get() == ButtonType.OK) {}
			return false;
		}
		else {
			return true;
		}
	}
	
	/**
	 * Go to the first Log in choice scene.
	 * @throws IOException
	 */
	public void backToLogin() throws IOException {
		Main.showLogInView();
	}
}
