package application;

import java.io.IOException;

import BackUps.ColorMap;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.GridPane;

/**
 * @author tree
 * Similar to GameController class, but this is the 4x4 version of Visitor mode.
 */
public class Game4x4Controller extends GameNxNController {
	
	//------------playing grids-----------
	@FXML
	private Label lb01; @FXML
	private Label lb02; @FXML
	private Label lb03; @FXML
	private Label lb04; @FXML
	private Label lb05; @FXML
	private Label lb06; @FXML
	private Label lb07; @FXML
	private Label lb08; @FXML
	private Label lb09; @FXML
	private Label lb10; @FXML
	private Label lb11; @FXML
	private Label lb12; @FXML
	private Label lb13; @FXML
	private Label lb14; @FXML
	private Label lb15; @FXML
	private Label lb16;
	
	//--------------Pane------------------
	@FXML
	private AnchorPane acPane0; @FXML
	private AnchorPane acPane1;
	
	@FXML
	private GridPane gdPane;
	
	//----------other components------------
	@FXML
	private Label lbPractice; @FXML
	private Label lbMode;
	
	@FXML
	private Label lbScore; @FXML
	private Label lbScoreValue;
	
	@FXML
	private Label lbPlayer; @FXML
	private Label lbPlayerValue;
	
	@FXML
	private Button btMenu;
	@FXML
	private Button btNewGame;
	@FXML
	private Button btUndo;
	@FXML
	private CheckBox ckBoxDark;


	/**
	 * Set up the maps and the pre-setting.
	 * @throws IOException 
	 */
	public Game4x4Controller() throws IOException {
		super();
		this.side = 4;
	}
	
	/**
	 * Select the "DARK" checkBox from the controller
	 */
	@Override
	public void selectCkBox() throws IOException {
		if (ckBoxDark.isSelected()) {
			ckBoxDark.setSelected(false);
		}
		else {
			ckBoxDark.setSelected(true);
		}
		changeColor();
	}
	
	/**
	 * Change color when the "DARK" checkBox is selected / canceled
	 */
	@Override
	public void changeColor() throws IOException {
		ColorMap colors = new ColorMap();
		
		if (ckBoxDark.isSelected()) {
			this.colorMap = colors.darkColorMap;
			this.textColorMap = colors.darkTextColorMap;

			
			acPane0.setStyle("-fx-background-color: #202020");
			acPane1.setStyle("-fx-background-color: #404040");
			gdPane.setStyle("-fx-background-color: #505050");
			
			lbPractice.setStyle("-fx-background-color: #1e50bf");
			lbMode.setStyle("-fx-background-color: #1e50bf");
			
			lbScore.setStyle("-fx-background-color: #1e50bf");
			lbScoreValue.setStyle("-fx-background-color: #1e50bf");
			
			lbPlayer.setStyle("-fx-background-color: #1e50bf");
			lbPlayerValue.setStyle("-fx-background-color: #1e50bf");
			
			btNewGame.setStyle("-fx-background-color: #012c8c");
			btUndo.setStyle("-fx-background-color: #012c8c");
			btMenu.setStyle("-fx-background-color: #012c8c");
			ckBoxDark.setStyle("-fx-background-color: #012c8c");
			
			draw();
		}
		else {
			this.colorMap = colors.colorMap;
			this.textColorMap = colors.textColorMap;
			
			acPane0.setStyle("-fx-background-color: #faf8ef");
			acPane1.setStyle("-fx-background-color: #bbada0");
			gdPane.setStyle("-fx-background-color: #bbada0");
			
			lbPractice.setStyle("-fx-background-color: #bbada0");
			lbMode.setStyle("-fx-background-color: #bbada0");
			
			lbScore.setStyle("-fx-background-color: #bbada0");
			lbScoreValue.setStyle("-fx-background-color: #bbada0");
			
			lbPlayer.setStyle("-fx-background-color: #bbada0");
			lbPlayerValue.setStyle("-fx-background-color: #bbada0");
			
			btNewGame.setStyle("-fx-background-color: #8f7a66");
			btUndo.setStyle("-fx-background-color: #8f7a66");
			btMenu.setStyle("-fx-background-color: #8f7a66");
			ckBoxDark.setStyle("-fx-background-color: #8f7a66");
			draw();
		}
	}
	
	/**
	 * Draw the status on this.board[][] in parent class "two048" onto the game scene, and also change the colors
	 * It is called when every move is done
	 */
	@Override
	public void draw(){
		
		lbScoreValue.setText("" + score);
		
		lb01.setBackground(new Background(new BackgroundFill(colorMap.get(board[0][0]), null, null)));
		lb02.setBackground(new Background(new BackgroundFill(colorMap.get(board[0][1]), null, null)));
		lb03.setBackground(new Background(new BackgroundFill(colorMap.get(board[0][2]), null, null)));
		lb04.setBackground(new Background(new BackgroundFill(colorMap.get(board[0][3]), null, null)));
		lb05.setBackground(new Background(new BackgroundFill(colorMap.get(board[1][0]), null, null)));
		lb06.setBackground(new Background(new BackgroundFill(colorMap.get(board[1][1]), null, null)));
		lb07.setBackground(new Background(new BackgroundFill(colorMap.get(board[1][2]), null, null)));
		lb08.setBackground(new Background(new BackgroundFill(colorMap.get(board[1][3]), null, null)));
		lb09.setBackground(new Background(new BackgroundFill(colorMap.get(board[2][0]), null, null)));
		lb10.setBackground(new Background(new BackgroundFill(colorMap.get(board[2][1]), null, null)));
		lb11.setBackground(new Background(new BackgroundFill(colorMap.get(board[2][2]), null, null)));
		lb12.setBackground(new Background(new BackgroundFill(colorMap.get(board[2][3]), null, null)));
		lb13.setBackground(new Background(new BackgroundFill(colorMap.get(board[3][0]), null, null)));
		lb14.setBackground(new Background(new BackgroundFill(colorMap.get(board[3][1]), null, null)));
		lb15.setBackground(new Background(new BackgroundFill(colorMap.get(board[3][2]), null, null)));
		lb16.setBackground(new Background(new BackgroundFill(colorMap.get(board[3][3]), null, null)));
		
		lb01.setTextFill(textColorMap.get(board[0][0]));
		lb02.setTextFill(textColorMap.get(board[0][1]));
		lb03.setTextFill(textColorMap.get(board[0][2]));
		lb04.setTextFill(textColorMap.get(board[0][3]));
		lb05.setTextFill(textColorMap.get(board[1][0]));
		lb06.setTextFill(textColorMap.get(board[1][1]));
		lb07.setTextFill(textColorMap.get(board[1][2]));
		lb08.setTextFill(textColorMap.get(board[1][3]));
		lb09.setTextFill(textColorMap.get(board[2][0]));
		lb10.setTextFill(textColorMap.get(board[2][1]));
		lb11.setTextFill(textColorMap.get(board[2][2]));
		lb12.setTextFill(textColorMap.get(board[2][3]));
		lb13.setTextFill(textColorMap.get(board[3][0]));
		lb14.setTextFill(textColorMap.get(board[3][1]));
		lb15.setTextFill(textColorMap.get(board[3][2]));
		lb16.setTextFill(textColorMap.get(board[3][3]));
		
		lb01.setText(parseInt(board[0][0]));
		lb02.setText(parseInt(board[0][1]));
		lb03.setText(parseInt(board[0][2]));
		lb04.setText(parseInt(board[0][3]));
		lb05.setText(parseInt(board[1][0]));
		lb06.setText(parseInt(board[1][1]));
		lb07.setText(parseInt(board[1][2]));
		lb08.setText(parseInt(board[1][3]));
		lb09.setText(parseInt(board[2][0]));
		lb10.setText(parseInt(board[2][1]));
		lb11.setText(parseInt(board[2][2]));
		lb12.setText(parseInt(board[2][3]));
		lb13.setText(parseInt(board[3][0]));
		lb14.setText(parseInt(board[3][1]));
		lb15.setText(parseInt(board[3][2]));
		lb16.setText(parseInt(board[3][3]));
	}
}
