package two048;

/**
 * @author tree
 * Playing 2048 in a board.
 */
public class two048 extends two048Support{
	
	public int side = 4;
	public int winning_cond = 2048;
	
	public int[][] board = new int[side][side];
	
	public int score;
	public int bestScore;
	
	// store all the boards for the use of undo in a 3D array
	public int[][][] history = new int[0][side][side];
	public int[] score_history = new int[0];
	
	/**
	 * Set up the board, clean the histories
	 * @param Side numbers of the columns and rows
	 * @param Winning_Cond when to win
	 */
	public void setBoard(int Side, int Winning_Cond) {
		side = Side;
		winning_cond = Winning_Cond;
		board = new int[side][side];
		history = new int[0][side][side];
		score_history = new int[0];
	}
	
	
	/**
	 * put a 2 or 4 on a random empty grid, the probability is about 8:1 
	 */
	public void putOnRdPosition() {
		int[][] avail_spot = new int[0][2];
		int rdNum = 0;
		for (int i = 0; i < side; i++) {
			for (int j = 0; j < side; j++) {
				if (board[i][j] == 0) {
					avail_spot = add(avail_spot, i, j);
					// array storing x,y coordinates of an empty grid
				}
			}
		}
		rdNum = getRdNum(avail_spot.length);
		int ans_x = avail_spot[rdNum][0];
		int ans_y = avail_spot[rdNum][1];
		
		board[ans_x][ans_y] = getTwoOrfour();
	}
	
	/**
	 * @param N repeat the putOnRdPosition() N times
	 */
	public void setNPosition(int N) {
		for (int i = 0; i < N; i++) {
			putOnRdPosition();
		}
	}
	
	/**
	 * @return last board
	 * for undo, pop the last board and return it
	 */
	public int[][] getHistoryAndPop() {
		int[][] last_board = history[history.length-2];
		history = pop(history);
		return last_board;
	}
	
	/**
	 * @return last score
	 * for undo, pop the last score and return it
	 */
	public int getScoreHistoryAndPop() {
		int last_score = score_history[score_history.length-2];
		score_history = pop(score_history);
		return last_score;
	}
	
	/**
	 * as the game proceed, add the board and score into history
	 */
	public void updateHistory() {
		history = add(history, board);
		score_history = add(score_history, score);
	}
	
	/**
	 * @return true if win, false if not yet
	 */
	public boolean checkWin() {
		int biggest_num = 0;
		for (int i = 0; i < side; i++) {
			for(int j = 0; j < side; j++) {
				if (biggest_num < board[i][j]) {
					biggest_num = board[i][j];
				}
			}
		}
		if (biggest_num >= winning_cond) {
			return true;
		}
		return false;
		
	}
	
	/**
	 * @return true if lose, false if not yet
	 */
	public boolean checkLose() {
		if (in(board, 0) == false) {
			for (int i = 0; i < side; i++) {
				for (int j = 0; j < side-1; j++) {
					if ((board[i][j] == board[i][j+1]) || (board[j][i] == board[j+1][i])) {
						return false;
					}
				}
			}
		}
		else {
			return false;
		}
		return true;
	}
	
	/**
	 * undo
	 */
	public void undoPlay() {
		board = getHistoryAndPop();
		score = getScoreHistoryAndPop();
	}
	
	/**
	 * @param Move the board by wasd => up left down right
	 * @return true if the move actually change the board
	 * main algorithm for 2048
	 */
	public boolean move(String Move) {
		boolean moved = false;
		for (int i = 0; i < side; i++) {
			int[][] plus = new int[0][2];
			for (int k = 0; k < side - 1; k++) {
				if (Move.equals("w")) {
					for (int j = 1; j < side - k; j++) {
						if (board[j][i] != 0) {
							if (board[j-1][i] == 0) {
								board[j-1][i] = board[j][i];
								board[j][i] = 0;
								moved = true;
							}
							else if ((board[j-1][i] == board[j][i]) && (in(plus, j, i) == false)) {
								board[j-1][i] *= 2;
								board[j][i] = 0;
								score += board[j-1][i];
								plus = add(plus, j-1, i);
								plus = add(plus, j, i);
								moved = true;
							}
						}
					}
				}
				else if (Move.equals("s")) {
					for (int j = side - 2; j >= 0 + k; j--) {
						if (board[j][i] != 0) {
							if (board[j+1][i] == 0) {
								board[j+1][i] = board[j][i];
								board[j][i] = 0;
								moved = true;
							}
							else if ((board[j+1][i] == board[j][i]) && (in(plus, j, i) == false)) {
								board[j+1][i] *= 2;
								board[j][i] = 0;
								score += board[j+1][i];
								plus = add(plus, j+1, i);
								plus = add(plus, j, i);
								moved = true;
							}
						}
					}
				}
				else if (Move.equals("d")) {
					for (int j = side - 2; j >= 0 + k; j--) {
						if (board[i][j] != 0) {
							if (board[i][j+1] == 0) {
								board[i][j+1] = board[i][j];
								board[i][j] = 0;
								moved = true;
							}
							else if ((board[i][j+1] == board[i][j]) && (in(plus, i, j) == false)) {
								board[i][j+1] *= 2;
								board[i][j] = 0;
								score += board[i][j+1];
								plus = add(plus, i, j+1);
								plus = add(plus, j, i);
								moved = true;
							}
						}
					}
				}
				else if (Move.equals("a")) {
					for (int j = 1; j < side - k; j++) {
						if (board[i][j] != 0) {
							if (board[i][j-1] == 0) {
								board[i][j-1] = board[i][j];
								board[i][j] = 0;
								moved = true;
							}
							else if ((board[i][j-1] == board[i][j]) && (in(plus, i, j) == false)) {
								board[i][j-1] *= 2;
								board[i][j] = 0;
								score += board[i][j-1];
								plus = add(plus, i, j-1);
								plus = add(plus, j, i);
								moved = true;
							}
						}
					}
				}
				else {
					return moved;
				}
			}
		}
		return moved;
	}
	
	/**
	 * @param Move for wasd => up left down right
	 * @return true if not lose yet, false if lose
	 */
	public int playMove(String Move) {
		if (move(Move) == false) {
			return 0;
		}
		putOnRdPosition();
		if (checkWin()) {
			return 1;
		}
		else if (checkLose()) {
			return 2;
		}
		updateHistory();
		
		return 0;
	}
}
