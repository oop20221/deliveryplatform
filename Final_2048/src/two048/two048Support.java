package two048;

/**
 * @author tree
 * include all the static method used in "two048",
 *   which are some methods for calculating or simple operations about array
 */
public class two048Support {
	
	/**
	 * @param range the range to produce random number
	 * @return a random integer number between 0 to range
	 */
	public static int getRdNum(int range) {
		return (int)(Math.random()*range);
	}
	
	
	/**
	 * @return 2 or 4 in a probability of 8:1
	 */
	public static int getTwoOrfour() {
		int rdNum = getRdNum(8);
		return (rdNum > 0) ? 2 : 4;
	}
	
	/**
	 * @param a 2D-array
	 * @param x an integer
	 * @return true if x is in a, otherwise false
	 * it is similar to how ArrayList.contains() works, which judges if x is in a 
	 */
	public static boolean in(int[][] a, int x) {
		for (int i = 0; i < a.length; i++) {
			for (int j = 0; j < a[0].length; j++) {
				if (a[i][j] == x) {
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * @param a 2D-array that only have two columns
	 * @param x an integer
	 * @param y an integer
	 * @return if "x y" is in a
	 * overloaded version of in(), while this one is only for array
	 *   having 2 columns, it will judge if any row of data is "x y"
	 */
	public static boolean in(int[][] a, int x, int y) {
		for (int i = 0; i < a.length; i++) {
			if ((a[i][0] == x) && (a[i][1] == y)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * @param a array
	 * @param x integer obj to add
	 * @return added array
	 * it imitates how ArrayList.add() works, which adds x at the back of array a,
	 *   and also lengthen it
	 */
	public static int[] add(int[] a, int x) {
		int[] copy = new int[a.length + 1];
		for (int i = 0; i < a.length; i++) {
			copy[i] = a[i];
		}
		copy[a.length] = x;
		return copy;
		
	}
	
	/**
	 * @param a 2D-array that only has 2 columns
	 * @param x first integer obj to add
	 * @param y second integer obj to add
	 * @return added array
	 * overloaded version of 1D-add(), here is a 2D-add() that only for array
	 *   having 2 columns, it will add x and y at the 2 columns 
	 */
	public static int[][] add(int[][] a, int x, int y) {
		int[][] copy = new int[a.length + 1][2];
		for (int i = 0; i < a.length; i++) {
			for (int j = 0; j < 2; j++) {
				copy[i][j] = a[i][j];
			}
		}
		copy[a.length][0] = x;
		copy[a.length][1] = y;
		return copy;
	}
	
	/**
	 * @param a 3D-array (can be seen as an 1D array that store a bunch of 2D-array)
	 * @param x 2D-array to add to a
	 * @return added array
	 * another overloaded version of 1D-add(), here is a 3D-add() that can add a
	 *   2D-array to a 3D-array
	 */
	public static int[][][] add(int a[][][], int x[][]){
		int[][][] copy = new int[a.length + 1][x.length][x[0].length];
		for (int i = 0; i < a.length; i++) {
			for (int j = 0; j < x.length; j++) {
				for (int k = 0; k < x[0].length; k++) {
					copy[i][j][k] = a[i][j][k];
				}
			}
		}
		for (int j = 0; j < x.length; j++) {
			for (int k = 0; k < x[0].length; k++) {
				copy[a.length][j][k] = x[j][k];
			}
		}
		return copy;
	}
	
	/**
	 * @param a array
	 * @return popped array
	 * similar to add, pop() imitates ArrayList that it delete the last element
	 *   in an array and return it
	 */
	public static int[] pop(int a[]){
		int[] copy = new int[a.length - 1];
		for (int i = 0; i < a.length-1; i++) {
			copy[i] = a[i];
		}
		return copy;
	}
	
	/**
	 * @param a 3D-array
	 * @return popped array
	 * overloaded version of 1D-pop(), here is a 3D-pop() that delete the last
	 *   2D-array in the 3D-array and return it
	 */
	public static int[][][] pop(int a[][][]){
		int[][][] copy = new int[a.length - 1][a[0].length][a[0][0].length];
		for (int i = 0; i < a.length-1; i++) {
			for (int j = 0; j < a[0].length; j++) {
				for (int k = 0; k < a[0][0].length; k++) {
					copy[i][j][k] = a[i][j][k];
				}
			}
		}

		return copy;
	}
}
