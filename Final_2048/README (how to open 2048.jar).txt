﻿自選題:
1. 下載 2048.jar
2. 到 eclipse 中，找一個有正確設定 JavaFX 和 SceneBuilder 的JavaFX專案下
   1. 右鍵
   2. Run As >
   3. Run Configurations
   4. Arguments
   5. 複製 VM arguments
3. 在存放 2048.jar 的資料夾底下開啟 cmd，打上
   1. java
   2. 空格
   3. 貼上 2.e.
   4. 空格
   5. -jar
   6. 空格
   7. 2048.jar
並執行，即可開啟

java <VM argument> -jar 2048.jar